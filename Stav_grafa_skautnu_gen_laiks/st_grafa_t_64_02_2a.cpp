#include <iostream>
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;


void uzgenere_ietekm_grafu(int ietekmejosie_geni[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen){

    mt19937 my_rand;
    uniform_int_distribution <int> my_dist(0, genu_skaits-1);

    int cur_ietkm;
    int ietkm_skaits;

    bool ir_orig;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        ietkm_skaits = 0;

        while(ietkm_skaits<ietekmejoso_genu_skaits){

            do{
                cur_ietkm = my_dist(my_rand);
                ir_orig = true;
                //parbauda vai neatkartojas
                for(int k=0; k<ietkm_skaits; k++){
                    if(ietekmejosie_geni[cur_g][k]==cur_ietkm){
                        ir_orig=false;
                        break;
                    }
                }
                //beidzas parbaude
            }
            while(!ir_orig);

            ietekmejosie_geni[cur_g][ietkm_skaits] = cur_ietkm;

            ietkm_skaits++;
        }

    }//beidzas generesana


}//end of uzgenere_ietekm_grafu


void uzgenere_bula_funkcijas(bool genu_bula_funkc[][128], const int genu_skaits, const int bula_f_arg_konf_sk, const ll seeds_gen){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        for(int k=0; k<bula_f_arg_konf_sk; k++){
            genu_bula_funkc[cur_g][k] = my_dist(my_rand);
        }
    }



}//uzgenere_bula_funkcijas


void uzgenere_bula_funkcijas_2var(bool genu_bula_funkc[][128], const int genu_skaits, const int bula_f_arg_konf_sk, const ll seeds_gen){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);


    bool pirma_puse;
    bool cur_vertiba;

    int k_ind;
    int l_ind;
    int cur_ind;

    int no, lidz;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        k_ind = 0;
        l_ind = bula_f_arg_konf_sk-1;
        cur_ind = l_ind/2;

        while(k_ind<=l_ind){
            pirma_puse = my_dist(my_rand);
            cur_vertiba = my_dist(my_rand);

            if(pirma_puse){
                //izvelas pirmo pusi
                no=k_ind;
                lidz=cur_ind;

                k_ind = cur_ind+1;
            }
            else{
                //izvelas otro pusi
                no = cur_ind;
                lidz = l_ind;

                l_ind = cur_ind-1;
            }

            for(int i=no; i<=lidz; i++) genu_bula_funkc[cur_g][i] = cur_vertiba;

            cur_ind = (l_ind-k_ind)/2 + k_ind;

        }//beidzas bin meklesanas while


    }//beidzas fciju generesana, katram genam



}//uzgenere_bula_funkcijas_2_var



ll atrod_jaunu_stavokli(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis){
    //funkcija atrod stavokli uz kuru japariet

    int cur_g_cur_ietekm_st;
    int cur_g_arg_konf;

    ll rez=0;



    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        cur_g_arg_konf=0;

        for(int cur_arg=0; cur_arg<ietekmejoso_genu_skaits; cur_arg++){

            cur_g_cur_ietekm_st = cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][cur_arg]) & 1;

            cur_g_arg_konf = cur_g_arg_konf<<1;

            cur_g_arg_konf += cur_g_cur_ietekm_st; // cur_g_cur_ietekm_st bus 0 vai 1

        }//for, kas atrod cur g konf beigas

        rez = rez<<(ll)1;
        if(genu_bula_funkc[cur_g][cur_g_arg_konf]) rez++; //ja ir true jeb 1

    }//liela for beigas

    return rez;

}//end of gena_paslegsana




void uztaisa_genu_parejas(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen=5489){
    //uztaisa genu ietekmejoso grafu un prieksh katra gena bula funkcijas
    /*** pielikts parametrs ar taisis seedu abiem mersena generatoriem(abaas funckijaas vienadi), ja seeds_gen nonodod, ta vertiba bus 5489.
    *    5489 ir defaulta vertiba merseena generatoram - seedojot ar so skaitli, rezultats bus tads, ka speciali nesidojot vispar.
    ***/

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;


    uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, seeds_gen);

    uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, bula_f_arg_konf_sk, seeds_gen);
    //uzgenere_bula_funkcijas_2var(genu_bula_funkc, genu_skaits, bula_f_arg_konf_sk, seeds_gen);


}//end of uztaisa_genu_parejas







int main()
{
    int genu_skaits;
    int ietekmejoso_genu_skaits;

    int ietekmejosie_geni[100][7];

    bool genu_bula_funkc[100][128];

    mt19937_64 my_rand;

    ll stavoklu_skaits;
    ll cur_rez;
    ll lidz;//sim isti laikam nevajag ll


    time_t sakuma_t;
    time_t beigu_t;





    cout<<"ievadi genu skaitu un ietekmejoso genu skaitu:  ";
    cin>>genu_skaits>>ietekmejoso_genu_skaits;

    while(genu_skaits<ietekmejoso_genu_skaits || ietekmejoso_genu_skaits>7){
        cout<<endl<<"genu skaitam jabut >= ietekmejoso genu skaitu!"<<endl;
        cout<<"ietekmejoso genu skaitam jabut <=7"<<endl;
        cin>>genu_skaits>>ietekmejoso_genu_skaits;
    }
    cout<<endl;


    stavoklu_skaits = (ll)1<<genu_skaits;

    cout<<"\tgenu skaits:= "<<genu_skaits<<endl;

    uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, my_rand() );



    // si dala ir lai varetu sagaidit, ja palaiz ar lielu genu skaitu, piemeram, 40 geniem;
    lidz = (ll)1<<25;
    if(stavoklu_skaits<lidz) lidz = stavoklu_skaits;


    cout<<"sakas laika uznemsana"<<endl;

    time(&sakuma_t);

    for(ll i=0; i<lidz; i++){
        cur_rez = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i);
    }

    time(&beigu_t);
    cout<<"beidzas laika uznemsana"<<endl<<endl;

    cout<<"\tlaiks:= "<<beigu_t-sakuma_t<<"s"<<endl;

    system("pause");



    return 0;
}//main
