#include <bits/stdc++.h>
#include "my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;

//0  prieksh a varianta
//1 prieksh b varianta
//2 priesksh slieksna varianta
#define rand_var 0
#define noteic_var 1
#define sliek_var 2

#define bula_funkciju_tips 1






void stav_grafa_analize_01(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){

    int stavoklu_sk;

    char *apmekleti;

    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    double vid_cikla_garums;
    double vid_baseins;
    ///
    int cur_v;
    int v_skaits_cela;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    time_t sakuma_t;
    time_t beigu_t;


    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;


    stavoklu_sk = 1<<genu_skaits;

    apmekleti = new char[stavoklu_sk];

    memset(apmekleti, '-', stavoklu_sk*sizeof(char));

    //'-' nav vispar apmeklets
    //'c' saja cela
    //'v' vispar

    time(&sakuma_t);

    for(int i=0; i<stavoklu_sk; i++){
        if(apmekleti[i]=='-'){
            //nav vispar apmeklets
            cur_v=i;
            v_skaits_cela=0;
            do{
               apmekleti[cur_v] = 'c';
               v_skaits_cela++; //sis lai velak varetu salikt 'v'
               cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
            }
            while(apmekleti[cur_v]=='-');

            if(apmekleti[cur_v]=='c'){
                //atrasts cikls!!!
                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;
                do{
                    cur_cikla_garums++;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                }
                while(cur_v != cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                ciklu_skaits++;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;
            }

            ///sakas nonullesana
            cur_v = i;
            for(int k=0; k<v_skaits_cela; k++){
                apmekleti[cur_v] = 'v';
                cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
            }
            ///beidzas nonullesana
        }
    }//beidzas info pa cikliem meklesana;

    time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)(stavoklu_sk - kop_ciklu_garums)/(double)ciklu_skaits;


    printf("ciklu skaits := %d\n", ciklu_skaits);
    printf("garakais cikls := %d\n", garakais_cikls);
    printf("vid cikla garums := %.2f\n", vid_cikla_garums);
    printf("vid baseins := %.2f\n", vid_baseins);
    printf("analize01 laiks: %ds\n", (int)(beigu_t-sakuma_t));
    printf("\n##################\n");

    delete[] apmekleti;


}//end of atrod_ciklus_un_bas


void izdruka_stav_gr(const int *stavoklu_parejas, const int stavoklu_skaits, const char faila_v[]="stav_gr.txt"){

    fstream fout;

    if(stavoklu_parejas){
        //ja ir izveidots stavoklu grafs
        cout<<"izdrukaa stav grafu"<<endl;
        cout<<"stavoklu skaits:= "<<stavoklu_skaits<<endl;
        fout.open(faila_v,ios::out);

        fout<<stavoklu_skaits<<endl;

        for(int i=0; i<stavoklu_skaits; i++)fout<<i<<" "<<stavoklu_parejas[i]<<endl;
    }


}//izdruka_stav_gr




struct baseins{
    int baseina_lielums;

    int cikla_garums;
    //int cikla_starta_v;
    int lielakais_att_lidz_ciklam;

    baseins(){
        baseina_lielums=0;
        cikla_garums=0;
        lielakais_att_lidz_ciklam=0;
    }

};



/*
void izdruka_info_stav_grafa_analize_02(FILE* out_file, const int g_sk, const int ietekmejoso_g_sk, const int ciklu_sk, const int gar_cikls, const double vid_cikla_gar, const double vid_bas,const int liel_bas, const time_t laiks, const baseins *bas){



    fprintf(out_file, "\t\tgenu sk: %d ietekm sk: %d\n", g_sk, ietekmejoso_g_sk);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "vid cikla garums := %.2f\n", vid_cikla_gar);
    fprintf(out_file, "vid baseins := %.2f\n", vid_bas);
    fprintf(out_file, "lielakais baseins ir: %d\n", liel_bas);
    fprintf(out_file, "analize01 laiks: %lds\n\n", laiks);
    fprintf(out_file, "\tinfo par visiem baseiniem/cikliem:\n\n");

    fprintf(out_file, "cikls  baseina_lielums  cikla_garums\n\n");

    for(int8_t i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d  %15d %14d\n", (int)i+1, bas[i].baseina_lielums, bas[i].cikla_garums);
    }
    fprintf(out_file, "\n###############################################\n");



}//izdruka_info_stav_grafa_analize_02



void stav_grafa_analize_02(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){

    int stavoklu_sk = 1<<genu_skaits;

    //fstream fout("stav_gr_analize02var.txt", ios::out);
    FILE *out_file = fopen("stav_gr_analize02var.txt", "a");

    int8_t *pieder_baseinam;
    baseins *baseini;
    int8_t maks_bas_sk = 20;

    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    time_t sakuma_t;
    time_t beigu_t;




    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];



    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    printf("sakas stav_grafa_analize_02 veiktaa analize\n\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(int i=0; i<stavoklu_sk; i++){
        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                v_skaits_cela++;
                cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
            }
            while(pieder_baseinam[cur_v]==-1);

            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls

                if(maks_bas_sk==ciklu_skaits){
                    printf("Kluda!! Ciklu skaits lielaks, kaa: %d\n", (int)maks_bas_sk);
                    fprintf(out_file,"\t!!!!!Ciklu skaits lielaks, kaa: %d\n", (int)maks_bas_sk);

                    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
                    vid_baseins = (double)(stavoklu_sk - kop_ciklu_garums)/(double)ciklu_skaits;

                    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;

                    izdruka_info_stav_grafa_analize_02(out_file, genu_skaits, ietekmejoso_genu_skaits, ciklu_skaits, garakais_cikls, vid_cikla_garums, vid_baseins, lielakais_baseins, beigu_t-sakuma_t, baseini);
                    //fout<<"Kluda!! Ciklu skaits lielaks, kaa: "<<(int)maks_bas_sk<<endl;
                    return;
                }

                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                do{
                    cur_cikla_garums++;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;
            }//pieder baseinam ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    pieder_baseinam[cur_v] = cikla_nr;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                }
            }//nav jauns cikls else beigas
        }
    }

    time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)(stavoklu_sk - kop_ciklu_garums)/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;




    izdruka_info_stav_grafa_analize_02(out_file, genu_skaits, ietekmejoso_genu_skaits, ciklu_skaits, garakais_cikls, vid_cikla_garums, vid_baseins, lielakais_baseins, beigu_t-sakuma_t, baseini);

    printf("beidzas stav_grafa_analize_02 veiktaa analize\n\n");

    delete[] baseini;
    delete[] pieder_baseinam;
    fclose(out_file);


}//stav_grafa_analize_02

*/



void stav_grafa_analize_02(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){

    int stavoklu_sk = 1<<genu_skaits;

    //fstream fout("stav_gr_analize02var.txt", ios::out);
    FILE *out_file = fopen("stav_gr_analize02var.txt", "a");

    int8_t *pieder_baseinam;
    baseins *baseini;
    int8_t maks_bas_sk = 20;

    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    time_t sakuma_t;
    time_t beigu_t;




    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];



    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    printf("sakas stav_grafa_analize_02 veiktaa analize\n\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(int i=0; i<stavoklu_sk; i++){
        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                v_skaits_cela++;
                cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
            }
            while(pieder_baseinam[cur_v]==-1);

            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls

                if(maks_bas_sk==ciklu_skaits){
                    printf("Kluda!! Ciklu skaits lielaks, kaa: %d\n", (int)maks_bas_sk);
                    fprintf(out_file,"\t!!!!!Ciklu skaits lielaks, kaa: %d\n", (int)maks_bas_sk);
                    fprintf(out_file, "Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");
                    break;
                }

                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                do{
                    cur_cikla_garums++;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;
            }//pieder baseinam ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    pieder_baseinam[cur_v] = cikla_nr;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                }
            }//nav jauns cikls else beigas
        }
    }

    time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    //vid_baseins = (double)(stavoklu_sk - kop_ciklu_garums)/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;



    fprintf(out_file, "\t\tgenu sk: %d ietekm sk: %d\n", genu_skaits, ietekmejoso_genu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_skaits);
    fprintf(out_file, "garakais cikls := %d\n", garakais_cikls);
    fprintf(out_file, "vid cikla garums := %.2f\n", vid_cikla_garums);
    fprintf(out_file, "vid baseins := %.2f\n", vid_baseins);
    fprintf(out_file, "lielakais baseins ir: %d\n", lielakais_baseins);
    fprintf(out_file, "analize02 laiks: %ds\n\n", (int)(beigu_t-sakuma_t));
    fprintf(out_file, "\tinfo par visiem baseiniem/cikliem:\n\n");

    fprintf(out_file, "cikls  baseina_lielums  cikla_garums\n\n");

    for(int8_t i=0; i<ciklu_skaits; i++){
        fprintf(out_file,"%5d  %15d %14d\n", (int)i+1, baseini[i].baseina_lielums, baseini[i].cikla_garums);
    }
    fprintf(out_file, "\n###############################################\n");



    printf("beidzas stav_grafa_analize_02 veiktaa analize\n\n");

    delete[] baseini;
    delete[] pieder_baseinam;
    fclose(out_file);


}//stav_grafa_analize_02






void stav_grafa_analize_03(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){


    int stavoklu_sk = 1<<genu_skaits;

    FILE *out_file = fopen("stav_gr_analize03var.txt", "a");

    int8_t *pieder_baseinam;
    baseins *baseini;
    int8_t maks_bas_sk = 20;

    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    time_t sakuma_t;
    time_t beigu_t;



    int *att_lidz_cikliem;
    ll lielakais_att_lidz_ciklam;
    ll kop_att_lidz_ciklam;
    int cur_att_lidz_ciklam;

    double vid_att_lidz_ciklam;
    int lapu_sk;



    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];


    att_lidz_cikliem = new int[stavoklu_sk];
    memset(att_lidz_cikliem, 0, stavoklu_sk*sizeof(int));



    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    lielakais_att_lidz_ciklam=0;
    kop_att_lidz_ciklam=0;
    lapu_sk=0;


    printf("sakas stav_grafa_analize_03 veiktaa analize\n\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(int i=0; i<stavoklu_sk; i++){

        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                v_skaits_cela++;
                cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
            }
            while(pieder_baseinam[cur_v]==-1);

            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls

                if(maks_bas_sk==ciklu_skaits){
                    /*
                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");
                    */
                    fprintf(out_file, "\n!!!!!!!Kluda!!!!!!!!\n");
                    fprintf(out_file, "Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    fprintf(out_file, "Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");
                    break;
                }

                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                do{
                    cur_cikla_garums++;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;

                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, sakums
                cur_att_lidz_ciklam = v_skaits_cela - cur_cikla_garums;
                cur_v = i;
                while(cur_v != cur_cikla_sakums){
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    cur_att_lidz_ciklam--;
                }
                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, beigas


            }//pieder baseinam ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                if(att_lidz_cikliem[cur_v]<0) att_lidz_cikliem[cur_v] *= -1;//atzime, ka noteikti nav lapa

                cur_att_lidz_ciklam = v_skaits_cela + att_lidz_cikliem[cur_v];

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    pieder_baseinam[cur_v] = cikla_nr;
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    cur_att_lidz_ciklam--;
                }
            }//nav jauns cikls else beigas

            att_lidz_cikliem[i] *= -1; //virsotni, no kuras saaka staigat atzimee kaa potencialo lapu
        }
    }

    time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;


    //for(int i=0; i<stavoklu_sk; i++) printf("%d: %d\n", i, att_lidz_cikliem[i]); vnk prieksh parbaudes

    for(int i=0; i<stavoklu_sk; i++){
        if(att_lidz_cikliem[i]<0){
            //ir lapa
            att_lidz_cikliem[i]*=-1;
            lapu_sk++;
            kop_att_lidz_ciklam += att_lidz_cikliem[i];

            if(att_lidz_cikliem[i] > lielakais_att_lidz_ciklam) lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

            if( att_lidz_cikliem[i] > baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam ) baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

        }
    }

    if(lapu_sk!=0) vid_att_lidz_ciklam = (double)kop_att_lidz_ciklam/(double)lapu_sk;
    else vid_att_lidz_ciklam = 0;



    /*
    printf("\t\tgenu sk: %d ietekm sk: %d\n\n", genu_skaits, ietekmejoso_genu_skaits);
    printf("ciklu skaits := %d\n", ciklu_skaits);
    printf("garakais cikls := %d\n", garakais_cikls);
    printf("vid cikla garums := %.2f\n", vid_cikla_garums);
    printf("vid baseins := %.2f\n", vid_baseins);
    printf("lielakais baseins := %d\n", lielakais_baseins);

    printf("\nlapu sk: %d\n", lapu_sk);
    printf("lielakais attalums lidz ciklam: %d\n", lielakais_att_lidz_ciklam);
    printf("videjais attalums lidz ciklam: %.2f\n", vid_att_lidz_ciklam);

    printf("\nanalize03 laiks: %llds\n\n", beigu_t-sakuma_t);

    printf("\n#####################################################\n");
    */


    fprintf(out_file, "\t\tgenu sk: %d ietekm sk: %d\n\n", genu_skaits, ietekmejoso_genu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_skaits);
    fprintf(out_file, "garakais cikls := %d\n", garakais_cikls);
    fprintf(out_file, "vid cikla garums := %.2f\n", vid_cikla_garums);
    fprintf(out_file, "vid baseins := %.2f\n", vid_baseins);
    fprintf(out_file, "lielakais baseins := %d\n", lielakais_baseins);

    fprintf(out_file, "\nlapu sk: %d\n", lapu_sk);
    fprintf(out_file, "lielakais attalums lidz ciklam: %lld\n", lielakais_att_lidz_ciklam);
    fprintf(out_file, "videjais attalums lidz ciklam: %.2f\n", vid_att_lidz_ciklam);

    fprintf(out_file, "\nanalize03 laiks: %llds\n\n", beigu_t-sakuma_t);

    fprintf(out_file, "\ncikls  baseina_lielums  cikla_garums   liel_att_lidz_c\n\n");

    for(int8_t i=0; i<ciklu_skaits; i++){
        fprintf(out_file, "%5d  %15d %14d %16d\n", (int)i+1, baseini[i].baseina_lielums, baseini[i].cikla_garums, baseini[i].lielakais_att_lidz_ciklam);
    }

    fprintf(out_file, "\n#####################################################\n");


    printf("beidzas stav_grafa_analize_03 veiktaa analize\n\n");

    delete[] baseini;
    delete[] pieder_baseinam;
    delete[] att_lidz_cikliem;
    fclose(out_file);


}//stav_grafa_analize_03







struct ietekme{
    bool gena_stav;
    bool vertiba;
};





void uzgenere_bula_funkcijas_b2(ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen=5489){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        for(int i=0; i<ietekmejoso_genu_skaits; i++){
            genu_bula_funkc_b2[cur_g][i].gena_stav = my_dist(my_rand);
            genu_bula_funkc_b2[cur_g][i].vertiba = my_dist(my_rand);
        }//beidzas cur gena bula f generesana
    }//beidzas generesana katram genam


}//end of uzgenere_bula_funkcijas_b2


ll atrod_jaunu_stavokli_b2(const int ietekmejosie_geni[][7], const ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis){
    //funkcija atrod stavokli uz kuru japariet

    int cur_g_cur_ietekm_st;
    bool cur_g_vertiba;
    bool skaidrs;


    ll rez=0;


    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        skaidrs = false;

        for(int cur_arg=0; cur_arg<ietekmejoso_genu_skaits; cur_arg++){

            cur_g_cur_ietekm_st = cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][cur_arg]) & 1;

            if( (int)genu_bula_funkc_b2[cur_g][cur_arg].gena_stav == cur_g_cur_ietekm_st){
                //ir skaidrs kada stavokli bus cur gens
                skaidrs = true;
                cur_g_vertiba = genu_bula_funkc_b2[cur_g][cur_arg].vertiba;
                break;
            }

        }//for, kas mekle cur gena vertibu

        //ja tika iziets visam cauri - nedereja ari pedeja, tapec viennozimigi zin, ka bus !pedeja vertiba
        if(!skaidrs) cur_g_vertiba = !genu_bula_funkc_b2[cur_g][ietekmejoso_genu_skaits-1].vertiba;

        rez = rez<<(ll)1;
        if(cur_g_vertiba) rez++; //ja ir true jeb 1

    }//liela for beigas

    return rez;

}//end of atrod_jaunu_stavokli_b2




ll atrod_jaunu_stavokli_b2_2var(const int ietekmejosie_geni[][7], const ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis){
    //funkcija atrod stavokli uz kuru japariet

    ///speciali ja 7 ietekmjosie

    bool cur_g_vertiba;


    ll rez=0;


    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        if((int)genu_bula_funkc_b2[cur_g][0].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][0]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][0].vertiba;
        }
        else if((int)genu_bula_funkc_b2[cur_g][1].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][1]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][1].vertiba;
        }
        else if((int)genu_bula_funkc_b2[cur_g][2].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][2]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][2].vertiba;
        }
        else if((int)genu_bula_funkc_b2[cur_g][3].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][3]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][3].vertiba;
        }
        else if((int)genu_bula_funkc_b2[cur_g][4].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][4]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][4].vertiba;
        }
        else if((int)genu_bula_funkc_b2[cur_g][5].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][5]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][5].vertiba;
        }
        else if((int)genu_bula_funkc_b2[cur_g][6].gena_stav == (cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][6]) & 1) ){
            cur_g_vertiba = genu_bula_funkc_b2[cur_g][6].vertiba;
        }
        else{
            cur_g_vertiba = !genu_bula_funkc_b2[cur_g][6].vertiba;
        }

        rez = rez<<(ll)1;
        if(cur_g_vertiba) rez++; //ja ir true jeb 1

    }//liela for beigas

    return rez;

}//end of atrod_jaunu_stavokli b2 2var



void uzgenere_bula_funkcijas_sliek(bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen){

    mt19937_64 my_rand;

    uniform_int_distribution <int> my_int_dist(0, ietekmejoso_genu_skaits);
    bernoulli_distribution my_bool_dist(0.5);

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;

    int *vien_sk = new int[bula_f_arg_konf_sk];
    int cur_1_sk;
    int cur_bit_vert;

    my_rand.seed(seeds_gen);

    ///saskaita un aizpilda vienieku skaitus sakums

    for(int i=0; i<bula_f_arg_konf_sk; i++){
        cur_1_sk=0;
        for(int j=0; j<ietekmejoso_genu_skaits; j++){
            cur_bit_vert = (i>>j) &1;
            cur_1_sk += cur_bit_vert;
        }
        vien_sk[i] = cur_1_sk;
    }
    ///saskaita un aizpilda vienieku skaitus beigas


    int slieksnis;
    bool sk_1;//vai skaitis 1
    bool vertiba;


    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        sk_1 = my_bool_dist(my_rand);
        slieksnis = my_int_dist(my_rand);
        vertiba = my_bool_dist(my_rand);

        for(int j=0; j<bula_f_arg_konf_sk; j++){
            if(sk_1){
                //skaita 1
                if(vien_sk[j]>=slieksnis) genu_bula_funkc[cur_g][j] = vertiba;
                else genu_bula_funkc[cur_g][j] = !vertiba;
            }
            else{
                //skaita 0
                if(vien_sk[j]<=slieksnis) genu_bula_funkc[cur_g][j] = vertiba;
                else genu_bula_funkc[cur_g][j] = !vertiba;
            }
        }
    }


}//end of uzgenere_bula_funkcijas_sliek




struct faili_stav_grafa_analizei_03{

    FILE *ciklu_skaits;
    FILE *garakais_cikls;
    FILE *vid_cikla_garums;
    FILE *vid_baseins;
    FILE *lielakais_baseins;

    FILE *lapu_sk;
    FILE *lielakais_att_lidz_ciklam;
    FILE *vid_att_lidz_ciklam;

    FILE *visu_ciklu_info;//sis jauns, noradis failu, kur info par visiem cikliem (info par katru ta garums, uc)

};


void stav_grafa_analize_03_2var(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7],  const int genu_skaits, const int ietekmejoso_genu_skaits, faili_stav_grafa_analizei_03 &izvada_f){

    /**
    viss tapat, ka 03, tacu druka vairakos failos un prieksh a vai b varianta izmanto dazadas parejas fcijas, a bula funkcijas glaba genu_bula_funkc
    b bula funkcijas glaba genu_bula_funkcijas_b2
    **/

    int stavoklu_sk = 1<<genu_skaits;

    int8_t *pieder_baseinam;
    baseins *baseini;
    int8_t maks_bas_sk = 60;

    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    //time_t sakuma_t;
    //time_t beigu_t;



    int *att_lidz_cikliem;
    int lielakais_att_lidz_ciklam;
    ll kop_att_lidz_ciklam;
    int cur_att_lidz_ciklam;

    double vid_att_lidz_ciklam;
    int lapu_sk;



    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];


    att_lidz_cikliem = new int[stavoklu_sk];
    memset(att_lidz_cikliem, 0, stavoklu_sk*sizeof(int));



    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    lielakais_att_lidz_ciklam=0;
    kop_att_lidz_ciklam=0;
    lapu_sk=0;


    /*
    printf("sakas stav_grafa_analize_03 veiktaa analize\n\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));
    */


    for(int i=0; i<stavoklu_sk; i++){

        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                v_skaits_cela++;
                cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);//avar
                //cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
            }
            while(pieder_baseinam[cur_v]==-1);

            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls

                if(maks_bas_sk==ciklu_skaits){

                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");

                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka sakums
                    {
                        fprintf(izvada_f.ciklu_skaits, "!!");
                        fprintf(izvada_f.garakais_cikls, "!!");
                        fprintf(izvada_f.vid_cikla_garums, "!!");

                        fprintf(izvada_f.lielakais_baseins, "!!");
                        fprintf(izvada_f.vid_baseins, "!!");

                        fprintf(izvada_f.lapu_sk, "!!");
                        fprintf(izvada_f.lielakais_att_lidz_ciklam, "!!");
                        fprintf(izvada_f.vid_att_lidz_ciklam, "!!");
                    }
                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka beigas


                    break;
                }

                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                do{
                    cur_cikla_garums++;
                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    //cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;

                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, sakums
                cur_att_lidz_ciklam = v_skaits_cela - cur_cikla_garums;
                cur_v = i;
                while(cur_v != cur_cikla_sakums){
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    //cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    cur_att_lidz_ciklam--;
                }
                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, beigas


            }//pieder baseinam ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                if(att_lidz_cikliem[cur_v]<0) att_lidz_cikliem[cur_v] *= -1;//atzime, ka noteikti nav lapa

                cur_att_lidz_ciklam = v_skaits_cela + att_lidz_cikliem[cur_v];

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    pieder_baseinam[cur_v] = cikla_nr;
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    //cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    cur_att_lidz_ciklam--;
                }
            }//nav jauns cikls else beigas

            att_lidz_cikliem[i] *= -1; //virsotni, no kuras saaka staigat atzimee kaa potencialo lapu
        }
    }

    //time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;


    //for(int i=0; i<stavoklu_sk; i++) printf("%d: %d\n", i, att_lidz_cikliem[i]); vnk prieksh parbaudes

    for(int i=0; i<stavoklu_sk; i++){
        if(att_lidz_cikliem[i]<0){
            //ir lapa
            att_lidz_cikliem[i]*=-1;
            lapu_sk++;
            kop_att_lidz_ciklam += (ll)att_lidz_cikliem[i];

            if(att_lidz_cikliem[i] > lielakais_att_lidz_ciklam) lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

            if( att_lidz_cikliem[i] > baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam ) baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

        }
    }

    if(lapu_sk!=0) vid_att_lidz_ciklam = (double)kop_att_lidz_ciklam/(double)lapu_sk;
    else vid_att_lidz_ciklam = 0;



    //izdruka failos rezultatus bloka sakums
    {
        fprintf(izvada_f.ciklu_skaits, "%d ", ciklu_skaits);
        fprintf(izvada_f.garakais_cikls, "%d ", garakais_cikls);
        fprintf(izvada_f.vid_cikla_garums, "%.2f ", vid_cikla_garums);

        fprintf(izvada_f.vid_baseins, "%.2f ", vid_baseins);
        fprintf(izvada_f.lielakais_baseins, "%d ", lielakais_baseins);

        fprintf(izvada_f.lapu_sk, "%d ", lapu_sk);
        fprintf(izvada_f.lielakais_att_lidz_ciklam, "%lld ", lielakais_att_lidz_ciklam);
        fprintf(izvada_f.vid_att_lidz_ciklam, "%.2f ", vid_att_lidz_ciklam);

    }
    //izdruka failos rezultatus bloka beigas

    //fprintf(out_file, "\nanalize03 laiks: %llds\n\n", beigu_t-sakuma_t);


    delete[] baseini;
    delete[] pieder_baseinam;
    delete[] att_lidz_cikliem;


}//stav_grafa_analize_03_2var




void izdruka_stav_gr2(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const char mode, const char faila_v[]="stav_gr.txt"){

    fstream fout;
    int stavoklu_skaits = 1<<genu_skaits;

    cout<<endl<<"\t\t######"<<endl;
    cout<<"izdrukaa stav grafu otraa drukas versija"<<endl;
    cout<<"stavoklu skaits:= "<<stavoklu_skaits<<endl;
    cout<<"izdrukas fails: "<<faila_v<<endl;
    //cout<<"pagaidam viss aizkomentets"<<endl;
    cout<<"\t\t######"<<endl<<endl;

    fout.open(faila_v,ios::out);

    fout<<stavoklu_skaits<<endl;
    cout<<"stavoklu skaits:"<<stavoklu_skaits<<endl;


    if(mode=='a'){
        for(int i=0; i<stavoklu_skaits; i++) fout<<i<<" "<<atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i)<<endl;
    }
    else{
       for(int i=0; i<stavoklu_skaits; i++) fout<<i<<" "<<atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, i)<<endl;
    }


    cout<<"izdruka_stav_gr2 beidz darbu"<<endl;


}//end of izdruka_stav_gr2




void statistika_pilnam_stav_grafam(int ietekmejosie_geni[100][7], bool genu_bula_funkc[][128], ietekme genu_bula_funkc_b2[][7] ){

    int genu_sk_no;
    int genu_sk_lidz;
    int genu_sk_cur;

    int iet_genu_sk_no;
    int iet_genu_sk_lidz;
    int iet_genu_sk_cur;

    int reizu_sk;

    mt19937_64 my_rand;
    int cur_seeds;

    time_t sakuma_t;
    time_t beigu_t;

    ll cur_uzgen;

    faili_stav_grafa_analizei_03 izvada_f;


    genu_sk_no=4;
    genu_sk_lidz=18;

    iet_genu_sk_no=2;
    iet_genu_sk_lidz=7;

    reizu_sk=10;


    //atver failus bloka sakums
    {
        izvada_f.ciklu_skaits = fopen("ciklu_skaits.txt", "w");
        izvada_f.garakais_cikls = fopen("garakais_cikls.txt", "w");
        izvada_f.vid_cikla_garums = fopen("vid_cikla_garums.txt", "w");

        izvada_f.lielakais_baseins = fopen("lielakais_baseins.txt", "w");
        izvada_f.vid_baseins = fopen("vid_baseins.txt", "w");

        izvada_f.lapu_sk = fopen("lapu_sk.txt", "w");
        izvada_f.lielakais_att_lidz_ciklam = fopen("lielakais_att_lidz_ciklam.txt", "w");
        izvada_f.vid_att_lidz_ciklam = fopen("vid_att_lidz_ciklam.txt", "w");

    }
    //atver failus bloka beigas



    printf("\n\tstatistika_pilnam_stav_grafam sak darbu\n");
    printf("intervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
    printf("bula funkcijas rekinas pec b2 varianta\n\n");

    //izdruka failos inetervalus uc info bloka sakums
    {
        fprintf(izvada_f.ciklu_skaits, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.garakais_cikls, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.vid_cikla_garums, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);

        fprintf(izvada_f.lielakais_baseins, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.vid_baseins, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);

        fprintf(izvada_f.lapu_sk, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.lielakais_att_lidz_ciklam, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.vid_att_lidz_ciklam, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);

    }
    //izdruka failos inetervalus uc info bloka beigas


    time(&sakuma_t);
    printf("sakuma laiks: %s \n", ctime(&sakuma_t));

    genu_sk_cur = genu_sk_no;

    while(genu_sk_cur<=genu_sk_lidz){

        iet_genu_sk_cur = iet_genu_sk_no;

        printf("gens:%d sakas\n", genu_sk_cur);


        //izdruka failos kurs gens tiek apskatits bloka sakums
        {
            /*fprintf(izvada_f.ciklu_skaits, "#");
            fprintf(izvada_f.garakais_cikls, "#");
            fprintf(izvada_f.vid_cikla_garums, "#");

            fprintf(izvada_f.lielakais_baseins, "#");
            fprintf(izvada_f.vid_baseins, "#");

            fprintf(izvada_f.lapu_sk, "#");
            fprintf(izvada_f.lielakais_att_lidz_ciklam, "#");
            fprintf(izvada_f.vid_att_lidz_ciklam, "#");*/
            fprintf(izvada_f.ciklu_skaits, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.garakais_cikls, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.vid_cikla_garums, "\n#gens:%d #\n", genu_sk_cur);

            fprintf(izvada_f.lielakais_baseins, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.vid_baseins, "\n#gens:%d #\n", genu_sk_cur);

            fprintf(izvada_f.lapu_sk, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.lielakais_att_lidz_ciklam, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.vid_att_lidz_ciklam, "\n#gens:%d #\n", genu_sk_cur);
        }
        //izdruka failos kurs gens tiek apskatits bloka beigas



        while(iet_genu_sk_cur<=iet_genu_sk_lidz && iet_genu_sk_cur<=genu_sk_cur){

            printf("\tiet gens:%d sakas\n", iet_genu_sk_cur);

            cur_seeds = genu_sk_cur*10 + iet_genu_sk_cur;// uztaisa seedu: genu_skaitsietekmejoso_genu skaits, piem 12-7 bus 127
            my_rand.seed(cur_seeds);

            for(int i=0; i<reizu_sk; i++){
                ///uzgenere genu parejas sakums
                cur_uzgen = my_rand();

                uzgenere_ietekm_grafu(ietekmejosie_geni, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);

                uzgenere_bula_funkcijas(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//avar
                //uzgenere_bula_funkcijas_b(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//bvar
                //uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//b2var


                ///uzgenere genu parejas beigas

                stav_grafa_analize_03_2var(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_sk_cur, iet_genu_sk_cur, izvada_f);

            }//reizu for beigas

            //izdruka failos linefeed apskatits bloka sakums
            {
               fprintf(izvada_f.ciklu_skaits, "\n");
                fprintf(izvada_f.garakais_cikls, "\n");
                fprintf(izvada_f.vid_cikla_garums, "\n");

                fprintf(izvada_f.lielakais_baseins, "\n");
                fprintf(izvada_f.vid_baseins, "\n");

                fprintf(izvada_f.lapu_sk, "\n");
                fprintf(izvada_f.lielakais_att_lidz_ciklam, "\n");
                fprintf(izvada_f.vid_att_lidz_ciklam, "\n");
            }
            //izdruka failos linefeed apskatits bloka beigas

            printf("\tiet gens:%d beidzas\n", iet_genu_sk_cur);

            iet_genu_sk_cur++;

        }//iet genu while beigas

        printf("gens:%d beidzas\n", genu_sk_cur);
        genu_sk_cur++;
    }//genu while beigas

    time(&beigu_t);


    printf("\n\tstatistika_pilnam_stav_grafam beidz darbu.\n");
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));


}//end of statistika_pilnam_stav_grafam




void izdruka_genu_parejas(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const char genu_pareju_mode, const char faila_v[]="genu_parejas.txt"){

    fstream fout(faila_v, ios::out);
    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;


    printf("izdruka faila: \"%s\" ietekmejoso grafu un bula funkcijas\n", faila_v);

    fout<<"#genu_skaits: "<<genu_skaits<<endl;
    fout<<"#ietkm_genu_skaits: "<<ietekmejoso_genu_skaits<<endl;
    fout<<"#mode: "<<(int)genu_pareju_mode<<" jeb "<<genu_pareju_mode<<endl<<endl;

    fout<<"\tietekmejoso genu grafs"<<endl<<endl;

    for(int i=0; i<genu_skaits; i++){
        fout<<i<<": ";
        for(int j=0; j<ietekmejoso_genu_skaits; j++)fout<<" "<<ietekmejosie_geni[i][j];
        fout<<endl;
    }
    fout<<endl;

    fout<<"\tbula funkcijas"<<endl;


    if(genu_pareju_mode=='a'){
        //a variants
        for(int i=0; i<genu_skaits; i++){
            fout<<i<<": "<<endl;
            for(int j=0; j<bula_f_arg_konf_sk; j++) fout<<"\t"<<bitset<7>(j)<<" --> "<<genu_bula_funkc[i][j]<<endl;
        }
    }
    else{
        //b2 variants
        for(int i=0; i<genu_skaits; i++){
            fout<<i<<": "<<endl;
            for(int j=0; j<ietekmejoso_genu_skaits; j++) {
                fout<<"\t"<<j<<": ";
                fout<<genu_bula_funkc_b2[i][j].gena_stav<<" --> "<<genu_bula_funkc_b2[i][j].vertiba<<endl;
            }
        }
    }


    printf("izdruka beidzas\n");


}//izdruka_genu_parejas




bool ielasa_genu_parejas(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], ietekme genu_bula_funkc_b2[][7], int &genu_skaits, int &ietekmejoso_genu_skaits, char &genu_pareju_mode, const char faila_v[]="genu_parejas.txt"){

    FILE *in_file=NULL;
    int bula_f_arg_konf_sk;


    bool *ir_ietekmejoss;

    in_file = fopen(faila_v, "r");

    printf("ielasa genu parejas sak darbu. fails:\"%s\"\n", faila_v);

    if(!in_file){
        //ja neizdevas atvert failu
        printf("\t KLUDA !!\n");
        printf("failu neizdevas atvert!!\n");
        printf("prog beidz darbu!!\n\n");
        return false;
    }


    fscanf(in_file, "%*[^#]");
    if(fscanf(in_file, "#genu_skaits: %d%*[^#] #ietkm_genu_skaits: %d%*[^#] #mode: %d", &genu_skaits,&ietekmejoso_genu_skaits, &genu_pareju_mode) != 3){
        printf("\t KLUDA !!\n");
        printf("faila galva kada sintakses kluda!!\n\n");
        return false;
    }

    if(genu_skaits<ietekmejoso_genu_skaits || ietekmejoso_genu_skaits>7 || ietekmejoso_genu_skaits<2 || (genu_pareju_mode!='a' && genu_pareju_mode!='b') ){
       printf("\t KLUDA !!\n");
       printf("kluda genu sk vai iet genu skaita vai modee\n");
       printf("genu skaitam jabut >= ietekmejoso genu skaitu!\n");
       printf("ietekmejoso genu skaitam jabut intervala: [2;7]\n");
       printf("\n");
       return false;
    }

    bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;

    ir_ietekmejoss = new bool[genu_skaits];


    /// ielasa ietekmejoso grafu sakums

    fscanf(in_file, "%*[^0-9]");
    for(int i=0, cur_g; i<genu_skaits; i++){
        if(fscanf(in_file, "%d:", &cur_g)==-1 ||  cur_g!=i){
            printf("\t KLUDA !!\n");
            printf("sintakses kluda vai nepareiza rindinu seciba ietkm graffaa, rindina= %d\n\n", i);
            return false;
        }

        memset(ir_ietekmejoss, false, genu_skaits);

        for(int j=0, cur_iet; j<ietekmejoso_genu_skaits; j++){
            if(fscanf(in_file, "%d", &cur_iet)==-1 ){
                printf("\t KLUDA !!\n");
                printf("iet grafaa rinda= %d\n\n", i);
                return false;
            }
            else if(cur_iet>genu_skaits-1 || cur_iet<0){
                printf("\t KLUDA !!\n");
                printf("nepareizs %d ietekmejosais gens genam %d\n", j, i);
                cout<<cur_iet;
                return false;
            }
            else if(ir_ietekmejoss[cur_iet]){
                printf("\t KLUDA !!\n");
                printf("%d genam 2 vienadi ietekmejosie geni\n", i);
                return false;
            }
            ietekmejosie_geni[i][j] = cur_iet;
            ir_ietekmejoss[cur_iet] = true;
        }//iet for beigas

    }//genu for beigas

        //sis gabals parbaudei
    printf("\tietekmejosais grafs OK\n");
    for(int i=0; i<genu_skaits; i++){
        printf("%d:", i);
        for(int j=0; j<ietekmejoso_genu_skaits; j++){
            printf(" %d", ietekmejosie_geni[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    /// ielasa ietekmejoso grafu beigas



    /*
    if(fscanf(in_file, "%*[^:]%*c%d%*[^:]%*c%d%*[^:]%*c%d", &genu_skaits, &ietekmejoso_genu_skaits, &genu_pareju_mode) != 3){
        printf("\t KLUDA !!\n");
        printf("faila galva kada sintakses kluda!!\n");
        return false;
    }
    */


    /// sakas bula fcju ielasisana

    printf("sakas bula funkcijas\n");

    fscanf(in_file, "%*[^0-9]");

    if(genu_pareju_mode=='a'){
        printf("ielasis bula fcijas pec a varianta\n\n");
        for(int i=0, cur_g; i<genu_skaits; i++){
            if(fscanf(in_file, " %d:", &cur_g)!=1 || cur_g!=i){
                printf("\t KLUDA !!\n");
                printf("kaut kas nepareizs par %d genu\n\n", i);
                return false;
            }

            for(int j=0, cur_vertiba; j<bula_f_arg_konf_sk; j++){
                if(fscanf(in_file, "%*[^:-] --> %d", &cur_vertiba)!=1){
                    printf("\t KLUDA !!\n");
                    printf("kaut kas nepareizs par %d genu %d bula fcijas rindina\n\n", i, j);
                    return false;
                }

                genu_bula_funkc[i][j] = (bool)cur_vertiba;
            }

        }//genu for beigas
    }
    else{
        printf("ielasis bula fcijas pec b varianta\n\n");

        for(int i=0, cur_g, cur_g_stav, cur_vertiba, cur_iet; i<genu_skaits; i++){
            if( fscanf(in_file, "%d:", &cur_g) != 1 || cur_g!=i){
                printf("\t KLUDA !!\n");
                printf("kaut kas nepareizs par %d genu\n\n", i);
                return false;
            }
            for(int j=0; j<ietekmejoso_genu_skaits; j++){
                if(fscanf(in_file ,"%d: %d --> %d", &cur_iet, &cur_g_stav, &cur_vertiba)!=3 || cur_iet!=j){
                    printf("\t KLUDA !!\n");
                    printf("kaut kas nepareizs par %d genu %d bula fcijas rindina\n\n", i, j);
                    return false;
                }

                genu_bula_funkc_b2[i][j].gena_stav = (bool)cur_g_stav;
                genu_bula_funkc_b2[i][j].vertiba = (bool)cur_vertiba;
            }
        }//genu for beigas
    }






    printf("\tbula funkciju lasisana:OK\n");
    printf("prog beidz darbu: OK\n\n");


    return true;

}//ielasa_genu_parejas




struct cikls{
    //prieksh stav_grafa_analize_saujot

    ll starta_virs;
    int cikla_garums;
    int reizes_trapits;
    int gar_cels_lidz;

    cikls(){
        starta_virs=-1;
        cikla_garums=0;
        reizes_trapits=0;
        gar_cels_lidz=-1;
    }
};

void stav_grafa_analize_saujot(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7],  const int genu_skaits, const int ietekmejoso_genu_skaits){

    FILE *out_file=fopen("stav_grafa_analize_saujot.txt", "a");

    ll virsotnu_skaits = (ll)1<<genu_skaits;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, virsotnu_skaits-1);

    ll cur_virsotne;
    ll prev_virsotne;

    int it_skaits = 1000;
    int cur_it;

    map<ll, int> cels;
    map<ll, int>::iterator atr_virs;

    int cur_cela_garums;
    int cur_cikla_garums;
    int cur_cels_lidz;

    cikls *cikli;
    int maks_ciklu_sk=1000;
    int ciklu_sk=0;
    bool jauns_cikls;
    int atr_cikla_ind;

    int gar_cikls=0;

    time_t sakuma_t;
    time_t beigu_t;
    time_t cur_it_t;



    cikli = new cikls[maks_ciklu_sk];

    printf("sakas stav_grafa_analize ar sausanu.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){

        cels.erase(cels.begin(), cels.end());

        cur_virsotne = my_dist(my_rand);
        cur_cela_garums = 0;

        do{
            cur_cela_garums++;
            prev_virsotne = cur_virsotne;
            cels.insert(pair<ll, int>(prev_virsotne, cur_cela_garums));

            cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, prev_virsotne);//priesh a
            //cur_virsotne = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, prev_virsotne);//prieksh b2

            atr_virs = cels.find(cur_virsotne);
        }
        while(atr_virs == cels.end());


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){
            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }

        if(jauns_cikls){
            //atrasts jauns cikls
            if(cur_cikla_garums>gar_cikls) gar_cikls = cur_cikla_garums;

            if(ciklu_sk==maks_ciklu_sk){
                cout<<"\n\t!!!ciklu vairak ka: "<<maks_ciklu_sk<<endl;
                cur_it++;
                break;
            }

            cikli[ciklu_sk].starta_virs = atr_virs->first;
            cikli[ciklu_sk].cikla_garums = cur_cikla_garums;
            cikli[ciklu_sk].gar_cels_lidz = cur_cels_lidz;
            cikli[ciklu_sk].reizes_trapits = 1;

            ciklu_sk++;
        }//endif jauns cikls
        else{
            //nav atrasts jauns cikls -trapits pa kadu velreiz
            if(cikli[atr_cikla_ind].gar_cels_lidz < cur_cels_lidz) cikli[atr_cikla_ind].gar_cels_lidz = cur_cels_lidz;

            cikli[atr_cikla_ind].reizes_trapits++;
        }

        if(cur_it_t-sakuma_t>60){
            //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
            cur_it++;
            break;
        }


    }//iteraciju for beigas

    time(&beigu_t);


    fprintf(out_file, "\t\tgenu sk: %d ietekm sk: %d\n\n", genu_skaits, ietekmejoso_genu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "analize ar sausanu laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz  reizes_trapits\n\n");

    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d %10d/%d\n", (int)i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz, cikli[i].reizes_trapits, cur_it);
    }
    fprintf(out_file, "\n###############################################\n");




}//stav_grafa_analize_saujot







/***
* sakot no sis vietas liek preproc. Augstak esosaas to neizmanto.
*
***/

void rty(){

    /*
    #define mana_mode 8

    #ifdef mana_mode
        #if mana_mode==1
        cout<<"mode ir 1"<<endl;
        #else
        cout<<"nav 1"<<endl;
        #endif // mana_mode
    #else
    cout<<"nav definets!!"<<endl;
    #endif // defined

    cout<<"beidzas"<<endl;
    */




    return;


    map<ll, int> mymap;
    map<ll, int>::iterator it;
    map<ll, int> *mymap2;

    //map<ll, int> maparr[1000];

    unordered_map <ll, int> mymap3;
    unordered_map <ll, int>::iterator it2;

    unordered_map<ll, int> maparr[1000];


    ll cur_skaitlis;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> mydist(0, (ll)1<<60);

    time_t a,b;



    //mymap3.max_load_factor(0.8);
    mymap3.reserve(10000);

    time(&a);

    for(int i=0; i<10000; i++){
        //mymap.erase(mymap.begin(), mymap.end());
        //mymap2 = new map<ll, int>;
        mymap3.erase(mymap3.begin(), mymap3.end());

        //for(int a=0; a<1000; a++) maparr[a].erase(maparr[a].begin(), maparr[a].end());

        for(int j=0; j<10000; j++){
            cur_skaitlis = mydist(my_rand);
            //mymap.insert(pair<ll,int>(my_rand(),121));
            //mymap2->insert(pair<ll,int>(my_rand(),121));
            mymap3.insert(pair<ll,int>(cur_skaitlis,121));


            //maparr[cur_skaitlis%1000].insert(pair<ll,int>(cur_skaitlis,121));

            //it = mymap.find(123);
            //it = mymap2->find(123);
            //it=maparr[cur_skaitlis%1000].find(123);
            it2=mymap3.find(123);
        }
        //delete mymap2;
    }

    time(&b);

    cout<<"beidzaas"<<endl;
    cout<<"laiks:= "<<b-a<<endl;



    return;

}



void izdruka_stav_gr3(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const char faila_v[]="stav_gr.txt"){

    /***
    tas pats, kas izdruka_stav_gr2, tacu nav modes char parametrs - izmanto preproc.
    ***/

    fstream fout;
    int stavoklu_skaits = 1<<genu_skaits;

    cout<<endl<<"\t\t######"<<endl;
    cout<<"izdrukaa stav grafu tresaa drukas versija"<<endl;
    cout<<"stavoklu skaits:= "<<stavoklu_skaits<<endl;
    cout<<"izdrukas fails: "<<faila_v<<endl;
    //cout<<"pagaidam viss aizkomentets"<<endl;
    cout<<"\t\t######"<<endl<<endl;

    fout.open(faila_v,ios::out);

    fout<<stavoklu_skaits<<endl;
    cout<<"stavoklu skaits:"<<stavoklu_skaits<<endl;



    #ifdef bula_funkciju_tips
        #if bula_funkciju_tips == rand_var
            for(int i=0; i<stavoklu_skaits; i++) fout<<i<<" "<<atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i)<<endl;
        #elif bula_funkciju_tips == noteic_var
            for(int i=0; i<stavoklu_skaits; i++) fout<<i<<" "<<atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, i)<<endl;
        #elif bula_funkciju_tips == sliek_var
            for(int i=0; i<stavoklu_skaits; i++) fout<<i<<" "<<atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i)<<endl;
        #endif // bula_funkciju_tips vetibas
    #endif // bula_funkciju_tips vai definets


    cout<<"izdruka_stav_gr3 beidz darbu"<<endl;


}//end of izdruka_stav_gr3




void stav_grafa_analize_03_3var(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7],  const int genu_skaits, const int ietekmejoso_genu_skaits, faili_stav_grafa_analizei_03 &izvada_f){

    /**
    viss tapat, ka 03, tacu druka vairakos failos un prieksh a vai b varianta izmanto dazadas parejas fcijas, a bula funkcijas glaba genu_bula_funkc
    b bula funkcijas glaba genu_bula_funkcijas_b2
    Atskiriba no 2var katru pareju rekina tikai x1 - celu glaba masiva.
    Rezultata gandriz x2 atrak!
    **/

    int stavoklu_sk = 1<<genu_skaits;

    int8_t *pieder_baseinam;

    baseins *baseini;
    int8_t maks_bas_sk = (1<<7)-5;//pietiktu ar -1, tacu vnk drosibai, iekavas obligati!!!


    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;

    //vector <int> cels(1<<15);
    vector <int> cels;
    static int maks_v_cela = 1<<2;
    int cur_ind;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    //time_t sakuma_t;
    //time_t beigu_t;



    int *att_lidz_cikliem;
    int lielakais_att_lidz_ciklam;
    ll kop_att_lidz_ciklam;
    int cur_att_lidz_ciklam;

    double vid_att_lidz_ciklam;
    int lapu_sk;


    cels.resize(maks_v_cela);

    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];


    att_lidz_cikliem = new int[stavoklu_sk];
    memset(att_lidz_cikliem, 0, stavoklu_sk*sizeof(int));




    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    lielakais_att_lidz_ciklam=0;
    kop_att_lidz_ciklam=0;
    lapu_sk=0;



    for(int i=0; i<stavoklu_sk; i++){

        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                //cels[v_skaits_cela] = cur_v;
                cels.at(v_skaits_cela) = cur_v;

                v_skaits_cela++;
                //cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);//avar
                //cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var

                #ifdef bula_funkciju_tips
                    #if bula_funkciju_tips == rand_var
                        cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);//avar
                    #elif bula_funkciju_tips == noteic_var
                        cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    #elif bula_funkciju_tips == sliek_var
                        cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);//a vai sliek var
                    #endif // bula_funkciju_tips vetibas
                #endif // bula_funkciju_tips vai definets

                //cela resizosanas ifa sakums
                if(v_skaits_cela==maks_v_cela-1){

                    maks_v_cela = maks_v_cela<<1;
                    cels.resize(maks_v_cela);
                }
                //cela resizosanas ifa beigas
            }
            while(pieder_baseinam[cur_v]==-1);


            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls

                /*if(maks_bas_sk==ciklu_skaits){

                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");

                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka sakums
                    {
                        fprintf(izvada_f.ciklu_skaits, "!!");
                        fprintf(izvada_f.garakais_cikls, "!!");
                        fprintf(izvada_f.vid_cikla_garums, "!!");

                        fprintf(izvada_f.lielakais_baseins, "!!");
                        fprintf(izvada_f.vid_baseins, "!!");

                        fprintf(izvada_f.lapu_sk, "!!");
                        fprintf(izvada_f.lielakais_att_lidz_ciklam, "!!");
                        fprintf(izvada_f.vid_att_lidz_ciklam, "!!");
                    }
                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka beigas

                    break;
                }
                */

                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                cur_ind = v_skaits_cela-1;

                do{
                    cur_cikla_garums++;
                    cur_v = cels[cur_ind];
                    cur_ind--;

                    ///cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    ///cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;

                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, sakums
                cur_att_lidz_ciklam = v_skaits_cela - cur_cikla_garums;
                cur_v = cels[0]; //jeb cur_v = i;
                cur_ind = 1;
                while(cur_v != cur_cikla_sakums){
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = cels[cur_ind];
                    cur_ind++;
                    ///cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    ///cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    cur_att_lidz_ciklam--;
                }
                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, beigas


                if(maks_bas_sk==ciklu_skaits){

                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");

                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka sakums
                    {
                        fprintf(izvada_f.ciklu_skaits, "!!");
                        fprintf(izvada_f.garakais_cikls, "!!");
                        fprintf(izvada_f.vid_cikla_garums, "!!");

                        fprintf(izvada_f.lielakais_baseins, "!!");
                        fprintf(izvada_f.vid_baseins, "!!");

                        fprintf(izvada_f.lapu_sk, "!!");
                        fprintf(izvada_f.lielakais_att_lidz_ciklam, "!!");
                        fprintf(izvada_f.vid_att_lidz_ciklam, "!!");
                    }
                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka beigas

                    break;
                }//ifa, kas parbauda vai nav parsniegts ciklu skaits beigas


            }//atrasts jauns cikls ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                if(att_lidz_cikliem[cur_v]<0) att_lidz_cikliem[cur_v] *= -1;//atzime, ka noteikti nav lapa

                cur_att_lidz_ciklam = v_skaits_cela + att_lidz_cikliem[cur_v];

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_ind=0; //cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    cur_v = cels[cur_ind];//tas pats, kas [k], tacu lai visur vienadi liku [cur_ind]
                    cur_ind++;
                    pieder_baseinam[cur_v] = cikla_nr;
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    ///cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    ///cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    cur_att_lidz_ciklam--;
                }
            }//nav jauns cikls else beigas

            att_lidz_cikliem[i] *= -1; //virsotni, no kuras saaka staigat atzimee kaa potencialo lapu
        }
    }

    //time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;


    //for(int i=0; i<stavoklu_sk; i++) printf("%d: %d\n", i, att_lidz_cikliem[i]); vnk prieksh parbaudes

    for(int i=0; i<stavoklu_sk; i++){
        if(att_lidz_cikliem[i]<0){
            //ir lapa
            att_lidz_cikliem[i]*=-1;
            lapu_sk++;
            kop_att_lidz_ciklam += (ll)att_lidz_cikliem[i];

            if(att_lidz_cikliem[i] > lielakais_att_lidz_ciklam) lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

            if( att_lidz_cikliem[i] > baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam ) baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

        }
    }

    if(lapu_sk!=0) vid_att_lidz_ciklam = (double)kop_att_lidz_ciklam/(double)lapu_sk;
    else vid_att_lidz_ciklam = 0;



    //izdruka failos rezultatus bloka sakums
    {
        fprintf(izvada_f.ciklu_skaits, "%d ", ciklu_skaits);
        fprintf(izvada_f.garakais_cikls, "%d ", garakais_cikls);
        fprintf(izvada_f.vid_cikla_garums, "%.2f ", vid_cikla_garums);

        fprintf(izvada_f.vid_baseins, "%.2f ", vid_baseins);
        fprintf(izvada_f.lielakais_baseins, "%d ", lielakais_baseins);

        fprintf(izvada_f.lapu_sk, "%d ", lapu_sk);
        fprintf(izvada_f.lielakais_att_lidz_ciklam, "%lld ", lielakais_att_lidz_ciklam);
        fprintf(izvada_f.vid_att_lidz_ciklam, "%.2f ", vid_att_lidz_ciklam);

        //$cikls  baseina_lielums  cikla_garums  liel_att_lidz_c$
        for(int8_t i=0; i<ciklu_skaits; i++){
            fprintf(izvada_f.visu_ciklu_info, "%6d  %15d  %12d %17d\n", (int)i+1, baseini[i].baseina_lielums, baseini[i].cikla_garums, baseini[i].lielakais_att_lidz_ciklam);
        }
        fprintf(izvada_f.visu_ciklu_info, "\n#########################################################\n");

    }
    //izdruka failos rezultatus bloka beigas

    //fprintf(out_file, "\nanalize03 laiks: %llds\n\n", beigu_t-sakuma_t);



    delete[] baseini;
    delete[] pieder_baseinam;
    delete[] att_lidz_cikliem;


}//stav_grafa_analize_03_3var





void statistika_pilnam_stav_grafam_2(int ietekmejosie_geni[100][7], bool genu_bula_funkc[][128], ietekme genu_bula_funkc_b2[][7] ){

    int genu_sk_no;
    int genu_sk_lidz;
    int genu_sk_cur;

    int iet_genu_sk_no;
    int iet_genu_sk_lidz;
    int iet_genu_sk_cur;

    int reizu_sk;

    mt19937_64 my_rand;
    int cur_seeds;

    time_t sakuma_t;
    time_t beigu_t;

    ll cur_uzgen;

    faili_stav_grafa_analizei_03 izvada_f;


    char failu_pr_v[100];
    char failu_pr_v_temp[100]="";

    //strcpy(failu_pr_v_temp, "Sliek_fcijas_");
    //strcpy(failu_pr_v_temp, "stav_gr_30_6_");

    genu_sk_no=4;
    genu_sk_lidz=18;

    iet_genu_sk_no=2;
    iet_genu_sk_lidz=7;

    reizu_sk=10;


    //atver failus bloka sakums
    {
        /*
        izvada_f.ciklu_skaits = fopen("ciklu_skaits_j.txt", "w");
        izvada_f.garakais_cikls = fopen("garakais_cikls_j.txt", "w");
        izvada_f.vid_cikla_garums = fopen("vid_cikla_garums_j.txt", "w");

        izvada_f.lielakais_baseins = fopen("lielakais_baseins_j.txt", "w");
        izvada_f.vid_baseins = fopen("vid_baseins_j.txt", "w");

        izvada_f.lapu_sk = fopen("lapu_sk_j.txt", "w");
        izvada_f.lielakais_att_lidz_ciklam = fopen("lielakais_att_lidz_ciklam_j.txt", "w");
        izvada_f.vid_att_lidz_ciklam = fopen("vid_att_lidz_ciklam_j.txt", "w");
        izvada_f.visu_ciklu_info = fopen("visu_ciklu_info_j.txt", "w");
        */


        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "ciklu_skaits_j.txt");
        izvada_f.ciklu_skaits = fopen(failu_pr_v, "w");


        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "garakais_cikls_j.txt");
        izvada_f.garakais_cikls = fopen(failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "vid_cikla_garums_j.txt");
        izvada_f.vid_cikla_garums = fopen(failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "lielakais_baseins_j.txt");
        izvada_f.lielakais_baseins = fopen( failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "vid_baseins_j.txt");
        izvada_f.vid_baseins = fopen(failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "lapu_sk_j.txt");
        izvada_f.lapu_sk = fopen(failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "lielakais_att_lidz_ciklam_j.txt");
        izvada_f.lielakais_att_lidz_ciklam = fopen(failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "vid_att_lidz_ciklam_j.txt");
        izvada_f.vid_att_lidz_ciklam = fopen(failu_pr_v, "w");

        strcpy(failu_pr_v, failu_pr_v_temp);
        strcat(failu_pr_v, "visu_ciklu_info_j.txt");
        izvada_f.visu_ciklu_info = fopen(failu_pr_v, "w");
    }
    //atver failus bloka beigas




    printf("\n\tstatistika_pilnam_stav_grafam 2 sak darbu\n");
    printf("intervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
    printf("bula funkcijas rekinas pec a varianta\n");

    printf("failu priesksh nosaukums: ");
    if(failu_pr_v_temp[0]!='\0') printf(" \"%s\" ", failu_pr_v_temp);
    else printf(" bez ipasa prieksh nosaukuma");
    printf("\n\n");

    ///!!!! pec testesanas beigam vajag statistika 03 2var pilnam grafam.... parlabot uz statistika 03 3var pilnam grafam...
    //izdruka failos intervalus uc info bloka sakums
    {
        fprintf(izvada_f.ciklu_skaits, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.garakais_cikls, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.vid_cikla_garums, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);

        fprintf(izvada_f.lielakais_baseins, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.vid_baseins, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);

        fprintf(izvada_f.lapu_sk, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.lielakais_att_lidz_ciklam, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.vid_att_lidz_ciklam, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);
        fprintf(izvada_f.visu_ciklu_info, "statistika 03 2var pilnam grafam\nintervals\tgeni: [%d;%d]\t ietekmejosie: [%d ; %d]\nbula funkcijas rekinas pec a varianta\n\n", genu_sk_no, genu_sk_lidz, iet_genu_sk_no, iet_genu_sk_lidz);

    }
    //izdruka failos inetervalus uc info bloka beigas


    time(&sakuma_t);
    printf("sakuma laiks: %s \n", ctime(&sakuma_t));

    genu_sk_cur = genu_sk_no;

    while(genu_sk_cur<=genu_sk_lidz){

        iet_genu_sk_cur = iet_genu_sk_no;

        printf("gens:%d sakas\n", genu_sk_cur);


        //izdruka failos kurs gens tiek apskatits bloka sakums
        {
            //aizkomente, ja grib lai ertak kopejams
            fprintf(izvada_f.ciklu_skaits, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.garakais_cikls, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.vid_cikla_garums, "\n#gens:%d #\n", genu_sk_cur);

            fprintf(izvada_f.lielakais_baseins, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.vid_baseins, "\n#gens:%d #\n", genu_sk_cur);

            fprintf(izvada_f.lapu_sk, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.lielakais_att_lidz_ciklam, "\n#gens:%d #\n", genu_sk_cur);
            fprintf(izvada_f.vid_att_lidz_ciklam, "\n#gens:%d #\n", genu_sk_cur);
        }
        //izdruka failos kurs gens tiek apskatits bloka beigas



        while(iet_genu_sk_cur<=iet_genu_sk_lidz && iet_genu_sk_cur<=genu_sk_cur){

            printf("\tiet gens:%d sakas\n", iet_genu_sk_cur);

            //papildus izdruka visu ciklu info faila
            fprintf(izvada_f.visu_ciklu_info, "\n#gens:%d   iet gens:%d #\n",genu_sk_cur, iet_genu_sk_cur);
            fprintf(izvada_f.visu_ciklu_info, "\n$cikls  baseina_lielums  cikla_garums   liel_att_lidz_c$\n\n");

            cur_seeds = genu_sk_cur*10 + iet_genu_sk_cur;// uztaisa seedu: genu_skaitsietekmejoso_genu skaits, piem 12-7 bus 127
            my_rand.seed(cur_seeds);

            for(int i=0; i<reizu_sk; i++){
                ///uzgenere genu parejas sakums
                cur_uzgen = my_rand();


                /*
                uzgenere_ietekm_grafu(ietekmejosie_geni, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);

                uzgenere_bula_funkcijas(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//avar
                //uzgenere_bula_funkcijas_b(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//bvar
                //uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//b2var
                //uzgenere_bula_funkcijas_sliek(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//kan var
                */

                uzgenere_ietekm_grafu(ietekmejosie_geni, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);
                #ifdef bula_funkciju_tips
                    #if bula_funkciju_tips == rand_var
                        uzgenere_bula_funkcijas(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//avar
                    #elif bula_funkciju_tips == noteic_var
                        //uzgenere_bula_funkcijas_b(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//bvar
                        uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//b2var
                    #elif bula_funkciju_tips == sliek_var
                        uzgenere_bula_funkcijas_sliek(genu_bula_funkc, genu_sk_cur, iet_genu_sk_cur, cur_uzgen);//kan var
                    #endif // bula_funkciju_tips vetibas
                #endif // bula_funkciju_tips vai definets


                ///uzgenere genu parejas beigas

                //stav_grafa_analize_03_2var(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_sk_cur, iet_genu_sk_cur, izvada_f);
                stav_grafa_analize_03_3var(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_sk_cur, iet_genu_sk_cur, izvada_f);

            }//reizu for beigas

            //izdruka failos linefeed apskatits bloka sakums
            {
                fprintf(izvada_f.ciklu_skaits, "\n");
                fprintf(izvada_f.garakais_cikls, "\n");
                fprintf(izvada_f.vid_cikla_garums, "\n");

                fprintf(izvada_f.lielakais_baseins, "\n");
                fprintf(izvada_f.vid_baseins, "\n");

                fprintf(izvada_f.lapu_sk, "\n");
                fprintf(izvada_f.lielakais_att_lidz_ciklam, "\n");
                fprintf(izvada_f.vid_att_lidz_ciklam, "\n");
            }
            //izdruka failos linefeed apskatits bloka beigas

            printf("\tiet gens:%d beidzas\n", iet_genu_sk_cur);

            iet_genu_sk_cur++;

        }//iet genu while beigas

        printf("gens:%d beidzas\n", genu_sk_cur);
        genu_sk_cur++;
    }//genu while beigas

    time(&beigu_t);


    printf("\n\tstatistika_pilnam_stav_grafam beidz darbu.\n");
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    //izvada katra faila kopejo darbibas laiku bloka sakums
    {
        fprintf(izvada_f.ciklu_skaits, "\n# Kopejais darbibas laiks: %ds  #\n", (int)(beigu_t-sakuma_t) );
        fprintf(izvada_f.garakais_cikls, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);
        fprintf(izvada_f.vid_cikla_garums, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);

        fprintf(izvada_f.lielakais_baseins, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);
        fprintf(izvada_f.vid_baseins, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);

        fprintf(izvada_f.lapu_sk, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);
        fprintf(izvada_f.lielakais_att_lidz_ciklam, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);
        fprintf(izvada_f.vid_att_lidz_ciklam, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);
        fprintf(izvada_f.visu_ciklu_info, "\n# Kopejais darbibas laiks: %ds  #\n", (int)beigu_t-sakuma_t);
    }
    //izvada katra faila kopejo darbibas laiku bloka beigas


}//end of statistika_pilnam_stav_grafam_2





void stav_grafa_analize_saujot2(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const ietekme genu_bula_funkc_b2[][7],  const int genu_skaits, const int ietekmejoso_genu_skaits){

    FILE *out_file=fopen("stav_grafa_analize_saujot2.txt", "a");

    ll virsotnu_skaits = (ll)1<<genu_skaits;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, virsotnu_skaits-1);

    ll cur_virsotne;
    //ll prev_virsotne;

    int it_skaits = 10000000;
    int cur_it;

    int maks_laiks = 240;

    unordered_map<ll, int> cels;
    unordered_map<ll, int>::iterator atr_virs;
    int izmers = 1<<10;//1<<10
    int maska = izmers-1;
    int ind;


    //unordered_map<ll, int> *cels = new unordered_map<ll, int>[izmers];
    //unordered_map<ll, int>::iterator atr_virs;
    //map<ll, int> *cels = new map<ll, int>[izmers];
    //map<ll, int>::iterator atr_virs;

    int cur_cela_garums;
    int cur_cikla_garums;
    int cur_cels_lidz;

    cikls *cikli;
    int maks_ciklu_sk=1000;
    int ciklu_sk=0;
    bool jauns_cikls;
    int atr_cikla_ind;

    int gar_cikls=0;

    time_t sakuma_t;
    time_t beigu_t;
    time_t cur_it_t;



///
/*
int asdqw=0;
//ll (*fpointeri[2])(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis);
fpointeri[0] = atrod_jaunu_stavokli;
fpointeri[1] = atrod_jaunu_stavokli;

//ll (*fpointeri[2]) (const int ietekmejosie_geni[][7], const ietekme genu_bula_funkc_b2[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis);
fpointeri[0] = atrod_jaunu_stavokli_b2;
fpointeri[1] = atrod_jaunu_stavokli_b2;
*/
///

    pair <unordered_map<ll, int>::iterator, bool> atr_rez;

    cels.rehash(40);

    cikli = new cikls[maks_ciklu_sk];

    printf("sakas stav_grafa_analize ar sausanu 2.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){


        cels.clear();

        cur_virsotne = my_dist(my_rand);
        cur_cela_garums = 0;

        cels.emplace(cur_virsotne, 1);



        do{
            cur_cela_garums++;


            //cur_virsotne = (*fpointeri[asdqw])(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//priesh a
            //cur_virsotne = (*fpointeri[asdqw])(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//prieksh b2


            #ifdef bula_funkciju_tips
                #if bula_funkciju_tips == rand_var
                    cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//priesh a
                #elif bula_funkciju_tips == noteic_var
                    cur_virsotne = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//prieksh b2
                #elif bula_funkciju_tips == sliek_var
                    cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//priesh slieksna
                #endif // bula_funkciju_tips vetibas
            #endif // bula_funkciju_tips vai definets


            atr_rez = cels.emplace(cur_virsotne, cur_cela_garums+1);
        }
        while(atr_rez.second);

        atr_virs = atr_rez.first;


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){

            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }





    /*
    cels.rehash(300);

    cikli = new cikls[maks_ciklu_sk];

    printf("sakas stav_grafa_analize ar sausanu 2.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){


        cels.clear();

        cur_virsotne = my_dist(my_rand);
        cur_cela_garums = 0;

        do{
            cur_cela_garums++;

            cels.insert(pair<ll, int>(cur_virsotne, cur_cela_garums));

            //cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//priesh a
            cur_virsotne = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//prieksh b2

            atr_virs = cels.find(cur_virsotne);
        }
        while(atr_virs == cels.end());


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){

            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }
        */



    /*
    for(int i=0; i<izmers; i++) cels[i].rehash(1000);


    cikli = new cikls[maks_ciklu_sk];

    printf("sakas stav_grafa_analize ar sausanu 2.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){

        ind = cur_it & maska;

        cels[ind].clear();

        cur_virsotne = my_dist(my_rand);
        cur_cela_garums = 0;

        do{
            cur_cela_garums++;

            cels[ind].insert(pair<ll, int>(cur_virsotne, cur_cela_garums));

            //cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//priesh a
            cur_virsotne = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//prieksh b2

            atr_virs = cels[ind].find(cur_virsotne);
        }
        while(atr_virs == cels[ind].end());


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){

            if(cels[ind].find(cikli[i].starta_virs) != cels[ind].end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }
        */



    /*
    //cels.rehash(1<<20);
    for(int i=0; i<izmers; i++) cels[i].rehash(100);

    cikli = new cikls[maks_ciklu_sk];

    printf("sakas stav_grafa_analize ar sausanu 2.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){

        ///cels.erase(cels.begin(), cels.end());
        //for(int i=0; i<izmers; i++) cels[i].erase(cels[i].begin(), cels[i].end());
        for(int i=0; i<izmers; i++) cels[i].clear();

        cur_virsotne = my_dist(my_rand);
        cur_cela_garums = 0;
        ind = cur_virsotne & maska;

        do{
            cur_cela_garums++;
            //prev_virsotne = cur_virsotne;
            ///cels.insert(pair<ll, int>(prev_virsotne, cur_cela_garums));
            cels[ind].insert(pair<ll, int>(cur_virsotne, cur_cela_garums));

            //cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//priesh a
            cur_virsotne = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_virsotne);//prieksh b2

            ind = cur_virsotne & maska;//tas pats kas % izmers
            ///atr_virs = cels.find(cur_virsotne);
            atr_virs = cels[ind].find(cur_virsotne);
        }
        while(atr_virs == cels[ind].end());


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){
            ind = (cikli[i].starta_virs) & maska;
            ///if(cels.find(cikli[i].starta_virs) != cels.end()){
            if(cels[ind].find(cikli[i].starta_virs) != cels[ind].end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }
        */



        if(jauns_cikls){
            //atrasts jauns cikls
            if(cur_cikla_garums>gar_cikls) gar_cikls = cur_cikla_garums;

            if(ciklu_sk==maks_ciklu_sk){
                cout<<"\n\t!!!ciklu vairak ka: "<<maks_ciklu_sk<<endl;
                cur_it++;
                break;
            }

            cikli[ciklu_sk].starta_virs = atr_virs->first;
            cikli[ciklu_sk].cikla_garums = cur_cikla_garums;
            cikli[ciklu_sk].gar_cels_lidz = cur_cels_lidz;
            cikli[ciklu_sk].reizes_trapits = 1;

            ciklu_sk++;
        }//endif jauns cikls
        else{
            //nav atrasts jauns cikls -trapits pa kadu velreiz
            if(cikli[atr_cikla_ind].gar_cels_lidz < cur_cels_lidz) cikli[atr_cikla_ind].gar_cels_lidz = cur_cels_lidz;

            cikli[atr_cikla_ind].reizes_trapits++;
        }

        if(cur_it_t-sakuma_t>maks_laiks){
            //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
            cur_it++;
            break;
        }


    }//iteraciju for beigas

    time(&beigu_t);


    fprintf(out_file, "\t\tgenu sk: %d ietekm sk: %d\n\n", genu_skaits, ietekmejoso_genu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "analize ar sausanu laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz  reizes_trapits\n\n");

    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d %10d/%d\n", (int)i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz, cikli[i].reizes_trapits, cur_it);
    }
    fprintf(out_file, "\n###############################################\n");


    printf("\n\tstav_grafa_analize ar sausanu 2.beidz darbu.\n");
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));



}//stav_grafa_analize_saujot2





















/***
* Sakas Musas genu dala
***/

#define musa_ne_cikla 0
#define musa_cikla 1
#define musas_gali 1


struct suuna{
    bool SLP;
    bool wg;
    bool WG;
    bool en;
    bool EN;
    bool hh;
    bool HH;
    bool ptc;
    bool PTC;
    bool PH;
    bool SMO;
    bool ci;
    bool CI;
    bool CIA;
    bool CIR;

    void izdruka_suunu(){

        cout<<SLP<<wg<<WG;
        cout<<en<<EN<<hh;
        cout<<HH<<ptc<<PTC;
        cout<<PH<<SMO<<ci;
        cout<<CI<<CIA<<CIR;

    }
};


bool derigs_musas_stav(ll cur_stavoklis){
     //parbauda vai cur stavoklis atbilst iespejamam konstanto genu vertibam
     // tas ir, vai visi SLP ir pareizi

     /**
     sis tiri prieksh si briza 15x2 geniem:
     1. sunas pirmajam (SLP) jabut false un
     2. sunas pirmajam (SLP) jabut true.
     **/


    static ll maska1 = 1<<14;

    if( (cur_stavoklis&maska1) == 0) return false;




    /**
    sis tiri prieksh si briza 15x4 geniem
    1. = false
    2. = false
    3. = true
    4. = true
    **/

    /*
    static ll jabut = ((ll)1<<29) + ((ll)1<<14);

    static ll maska = ((ll)1<<59) + ((ll)1<<44) + jabut;

    if( (ll)(cur_stavoklis&maska) != jabut) return false;
    */


     return true;

}//end of derigs_musas_stav



ll atrod_jaunu_stavokli_musai(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, suuna &cur_suuna, const ll cur_stavoklis){


    ll temp_sk = cur_stavoklis;
    ll rez=0;





    for(int cur_suna=musas_sunu_skaits-1; cur_suna>=0; cur_suna--){
        //par cik SLP nemainaas - i==0 ir jau aizpildits
        //bija i>=0
        //parlabots uz: i>0
        for(int i=musas_genu_skaits-1; i>0; i--){
            musas_genu_vertibas[cur_suna][i] = (bool)(temp_sk & 1);
            temp_sk = temp_sk>>1;
        }
    }



    /// izrekina rez sakums

    #if musas_gali==musa_ne_cikla

        //pirmaas suunas sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //pirmaas suunas beigas


        for(int i=1; i<musas_sunu_skaits-1; ++i){

            //par cur suunu sakums
            {
                //SLP paliek ka konstante

                cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.wg;


                cur_suuna.WG = visas_sunas[i]->wg;
                rez = rez<<1;
                rez += (ll)cur_suuna.WG;


                cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
                rez = rez<<1;
                rez += (ll)cur_suuna.en;


                cur_suuna.EN = visas_sunas[i]->en;
                rez = rez<<1;
                rez += (ll)cur_suuna.EN;


                cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.hh;


                cur_suuna.HH = visas_sunas[i]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.HH;


                cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.ptc;


                cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
                rez = rez<<1;
                rez += (ll)cur_suuna.PTC;


                cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.PH;


                cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.SMO;


                cur_suuna.ci = !visas_sunas[i]->EN;
                rez = rez<<1;
                rez += (ll)cur_suuna.ci;


                cur_suuna.CI = visas_sunas[i]->ci;
                rez = rez<<1;
                rez += (ll)cur_suuna.CI;

                cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.CIA;

                cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.CIR;
            }

            //par cur suunu beigas


        }//end of for




        //par pedejo suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && visas_sunas[musas_sunu_skaits-2]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //par pedejo suunu beigas


    #elif musas_gali == musa_cikla

        //pirmaas suunas sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[musas_sunu_skaits-1]->WG || visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[musas_sunu_skaits-1]->HH && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;


            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[musas_sunu_skaits-1]->hh && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;

        }
        //pirmaas suunas beigas


        for(int i=1; i<musas_sunu_skaits-1; ++i){

            //par cur suunu sakums
            {
                //SLP paliek ka konstante

                cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.wg;


                cur_suuna.WG = visas_sunas[i]->wg;
                rez = rez<<1;
                rez += (ll)cur_suuna.WG;


                cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
                rez = rez<<1;
                rez += (ll)cur_suuna.en;


                cur_suuna.EN = visas_sunas[i]->en;
                rez = rez<<1;
                rez += (ll)cur_suuna.EN;


                cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.hh;


                cur_suuna.HH = visas_sunas[i]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.HH;


                cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.ptc;


                cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
                rez = rez<<1;
                rez += (ll)cur_suuna.PTC;


                cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.PH;


                cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.SMO;


                cur_suuna.ci = !visas_sunas[i]->EN;
                rez = rez<<1;
                rez += (ll)cur_suuna.ci;


                cur_suuna.CI = visas_sunas[i]->ci;
                rez = rez<<1;
                rez += (ll)cur_suuna.CI;

                cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.CIA;

                cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.CIR;
            }

            //par cur suunu beigas


        }//end of for


        //par pedejo suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG || visas_sunas[0]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH && !visas_sunas[0]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;


            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh && !visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //par pedejo suunu beigas


    #endif // musas_gali



    /// izrekina rez beigas


    return rez;

}//end of atrod_jaunu_stavokli_musai



struct stavoklis{
    ll skaitli[4];
    int skaitlu_sk;


    stavoklis(int sk=4){

        while(sk>4 || sk<=0){
            printf("noraditajam skaitlu skaitam (%d) jabut <= par %d\n", sk, 4);
            cout<<"ievadiet sk skaitu: ";
            cin>>sk;
        }
        skaitlu_sk = sk;
    }

    //kopijas konstruktora sakums
    stavoklis(const stavoklis &s){

        skaitlu_sk = s.skaitlu_sk;
        for(int i=0; i<skaitlu_sk; ++i) skaitli[i] = s.skaitli[i];
    }//kopijas konstruktora beigas

    bool operator< (const stavoklis &la)const{
        for(int i=0; i<skaitlu_sk; i++){
            if(skaitli[i] < la.skaitli[i]) return true;
            else if(skaitli[i]>la.skaitli[i]) return false;
        }
        return false;
    }

};







void atrod_jaunu_stavokli_musai_2(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, suuna &cur_suuna, stavoklis &cur_stavoklis){


    ll temp_sk;
    ll rez;

    int cur_suunas_nr = musas_sunu_skaits-1;






    for(int sk_ind=cur_stavoklis.skaitlu_sk-1; sk_ind>=0; --sk_ind){

        temp_sk = cur_stavoklis.skaitli[sk_ind];

        for(int k=0; k<4; ++k){//2 vai 4 apzime cik viena skaitli glaba suunas

            for(int i=musas_genu_skaits-1; i>0; i--){
                musas_genu_vertibas[cur_suunas_nr][i] = (bool)(temp_sk & 1);
                temp_sk = temp_sk>>1;
            }

            --cur_suunas_nr;
        }//viena skaitla for beigas


    }//skaitlu for beigas


    for(int i=0; i<cur_stavoklis.skaitlu_sk; ++i) cur_stavoklis.skaitli[i]=0; //nonulle, jo so pasu masivu izmantos, lai saglabatu jauno rezultatu






    /// izrekina rez sakums

    #if musas_gali == musa_cikla

        ///gali noslegti cikla


        //pirmaas suunas sakums
        rez = 0;
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[musas_sunu_skaits-1]->WG || visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[musas_sunu_skaits-1]->HH && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[musas_sunu_skaits-1]->hh && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;

        }
        //pirmaas suunas beigas



        for(int i=1; i<musas_sunu_skaits-1; ++i){

            //par cur suunu sakums
            {
                //SLP paliek ka konstante

                cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.wg;

                cur_suuna.WG = visas_sunas[i]->wg;
                rez = rez<<1;
                rez += (ll)cur_suuna.WG;

                cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
                rez = rez<<1;
                rez += (ll)cur_suuna.en;


                cur_suuna.EN = visas_sunas[i]->en;
                rez = rez<<1;
                rez += (ll)cur_suuna.EN;

                cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.hh;

                cur_suuna.HH = visas_sunas[i]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.HH;

                cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.ptc;

                cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
                rez = rez<<1;
                rez += (ll)cur_suuna.PTC;


                cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.PH;



                cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.SMO;


                cur_suuna.ci = !visas_sunas[i]->EN;
                rez = rez<<1;
                rez += (ll)cur_suuna.ci;


                cur_suuna.CI = visas_sunas[i]->ci;
                rez = rez<<1;
                rez += (ll)cur_suuna.CI;

                cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.CIA;

                cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.CIR;
            }

            //par cur suunu beigas

            if(i%4==3){ //ja viena skatli 4 sunas
            //if(i%2==1){ //ja viena skaitli 2 sunas
                cur_stavoklis.skaitli[i/4] = rez; //ja viena skatli 4 sunas
                //cur_stavoklis.skaitli[i/2] = rez; //ja viena skatli 2 sunas
                rez=0;
            }


        }//end of for

        //par pedejo suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG || visas_sunas[0]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH && !visas_sunas[0]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh && !visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //par pedejo suunu beigas


        ///gali noslegti cikla

    #elif musas_gali == musa_ne_cikla


    ///gali noslegti ne-cikla sakums

    //pirmaas suunas sakums
    rez = 0;
    {
        //SLP paliek ka konstante

        cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.wg;


        cur_suuna.WG = visas_sunas[0]->wg;
        rez = rez<<1;
        rez += (ll)cur_suuna.WG;


        cur_suuna.en = (visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
        rez = rez<<1;
        rez += (ll)cur_suuna.en;


        cur_suuna.EN = visas_sunas[0]->en;
        rez = rez<<1;
        rez += (ll)cur_suuna.EN;


        cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.hh;


        cur_suuna.HH = visas_sunas[0]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.HH;


        cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.ptc;


        cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[1]->HH);
        rez = rez<<1;
        rez += (ll)cur_suuna.PTC;


        cur_suuna.PH = cur_suuna.PTC && visas_sunas[1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.PH;



        cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.SMO;


        cur_suuna.ci = !visas_sunas[0]->EN;
        rez = rez<<1;
        rez += (ll)cur_suuna.ci;


        cur_suuna.CI = visas_sunas[0]->ci;
        rez = rez<<1;
        rez += (ll)cur_suuna.CI;

        cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[1]->hh);
        rez = rez<<1;
        rez += (ll)cur_suuna.CIA;

        cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.CIR;

    }
    //pirmaas suunas beigas



    for(int i=1; i<musas_sunu_skaits-1; ++i){

        //par cur suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[i]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[i]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[i]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[i]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[i]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }

        //par cur suunu beigas

        if(i%4==3){ //ja viena skatli 4 sunas
        //if(i%2==1){ //ja viena skaitli 2 sunas
            cur_stavoklis.skaitli[i/4] = rez; //ja viena skatli 4 sunas
            //cur_stavoklis.skaitli[i/2] = rez; //ja viena skatli 2 sunas
            rez=0;
        }


    }//end of for

    //par pedejo suunu sakums
    {
        //SLP paliek ka konstante

        cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.wg;


        cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
        rez = rez<<1;
        rez += (ll)cur_suuna.WG;


        cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
        rez = rez<<1;
        rez += (ll)cur_suuna.en;


        cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
        rez = rez<<1;
        rez += (ll)cur_suuna.EN;


        cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.hh;


        cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.HH;


        cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.ptc;


        cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH);
        rez = rez<<1;
        rez += (ll)cur_suuna.PTC;


        cur_suuna.PH = cur_suuna.PTC && visas_sunas[musas_sunu_skaits-2]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.PH;



        cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.SMO;


        cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
        rez = rez<<1;
        rez += (ll)cur_suuna.ci;


        cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
        rez = rez<<1;
        rez += (ll)cur_suuna.CI;

        cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh);
        rez = rez<<1;
        rez += (ll)cur_suuna.CIA;

        cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.CIR;
    }
    //par pedejo suunu beigas


    ///gali noslegti ne-cikla beigas


    #endif // musas_gali






    cur_stavoklis.skaitli[ cur_stavoklis.skaitlu_sk-1 ] = rez; //saglaba info pedeja skaitli


    /// izrekina rez beigas




}//end of atrod_jaunu_stavokli_musai_2





void atrod_jaunu_stavokli_musai_test(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, suuna &cur_suuna){


    #define gena_tips en
    #define num 4


    fstream fin("musas_test_1_in.txt", ios::in);

    ll rez=0;

    int test_piem_sk=2;

    fin>>test_piem_sk;

    for(int i=0; i<musas_sunu_skaits; i++) memset(musas_genu_vertibas[i], false, musas_genu_skaits);



    cout<<"test piemeru skaits: "<<test_piem_sk<<endl<<endl;


    for(int a=0; a<test_piem_sk; a++){
        cout<<"testpiem: "<<a+1<<"/"<<test_piem_sk<<endl;

        fin>>visas_sunas[0]->gena_tips>>visas_sunas[1]->gena_tips;

        #if num == 2
            fin>>visas_sunas[0]->CIA>>visas_sunas[0]->SLP>>visas_sunas[0]->CIR;
            fin>>visas_sunas[1]->CIA>>visas_sunas[1]->SLP>>visas_sunas[1]->CIR;
        #elif num==3
            fin>>visas_sunas[0]->wg;
            fin>>visas_sunas[1]->wg;
        #elif num == 4
            fin>>visas_sunas[0]->WG>>visas_sunas[0]->SLP;
            fin>>visas_sunas[1]->WG>>visas_sunas[1]->SLP;
        #endif // num


        if(!fin){
            cout<<"ielases kluda!!"<<endl;
            return;
        }

      /// tikai prieksh 2 suunaam !!!! - ja vairak vajadzetu ielikt ciklaa!

      /// iistaas bula funkcijas
        //pirma suuna
        {
            //SLP paliek ka konstante
            rez = rez<<1;
            rez += (ll)visas_sunas[0]->SLP;

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[1]->WG && visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[1]->HH && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[1]->hh || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[1]->hh && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }


        cout<<endl<<"suna1"<<endl;
        cout<<"\tveca vertiba: "<<visas_sunas[0]->gena_tips<<endl;
        cout<<"\tjaunaa vertiba: "<<cur_suuna.gena_tips<<endl;

        //otraa suuna
        {
            //SLP paliek ka konstante
            rez = rez<<1;
            rez += (ll)visas_sunas[1]->SLP;

            cur_suuna.wg = ( (visas_sunas[1]->CIA && visas_sunas[1]->SLP) || (visas_sunas[1]->wg && (visas_sunas[1]->CIA || visas_sunas[1]->SLP) ) ) && !visas_sunas[1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[0]->WG && visas_sunas[0]->WG) && !visas_sunas[1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[1]->EN && !visas_sunas[1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[1]->CIA && !visas_sunas[1]->EN && !visas_sunas[1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[1]->ptc || (visas_sunas[1]->PTC && !visas_sunas[0]->HH && !visas_sunas[0]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[0]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[0]->hh || visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[1]->CI && (visas_sunas[1]->SMO || visas_sunas[0]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[1]->CI && !visas_sunas[1]->SMO && !visas_sunas[0]->hh && !visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;

        }


        cout<<endl<<"suna2"<<endl;
        cout<<"\tveca vertiba: "<<visas_sunas[1]->gena_tips<<endl;
        cout<<"\tjaunaa vertiba: "<<cur_suuna.gena_tips<<endl;

        cout<<"#######"<<endl<<endl;

    }//end of for






    #undef gena_tips
    #undef num

}//end of atrod_jaunu_stavokli_musai



void izdruka_musas_genu_vertibas(FILE *out_file2,FILE *out_file3, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll &cur_stavoklis){


    ll temp_sk = cur_stavoklis;
    ll rez=0;



    for(int cur_suna=musas_sunu_skaits-1; cur_suna>=0; cur_suna--){
        //par cik SLP nemainaas - i==0 ir jau aizpildits
        //bija i>=0
        //parlabots uz: i>0
        for(int i=musas_genu_skaits-1; i>0; i--){
            musas_genu_vertibas[cur_suna][i] = (bool)(temp_sk & 1);
            temp_sk = temp_sk>>1;
        }
    }

    ///




    //izdruka faila par katru genu kada ta vertiba katra suuna
    // sis der tikai tad, ja genu skaits ir 15
    static char genu_nos[15][5]={"SLP", "wg", "WG", "en", "EN", "hh",
                 "HH", "ptc", "PTC", "PH", "SMO", "ci", "CI", "CIA","CIR"};



    for(int cur_g=1; cur_g<musas_genu_skaits; ++cur_g){//sak no 0 ja grib izdrukat SLP no 1 ja nee.

        fprintf(out_file2 ,"%s: ", genu_nos[cur_g]);//lai butu klat gena nosaukums
        for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
            //printf("%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
            fprintf(out_file2 ,"%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        //printf("\n");
        fprintf(out_file2, "\n");
    }



    for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
        for(int cur_g=0; cur_g<musas_genu_skaits; ++cur_g){
            fprintf(out_file3, "%d", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        fprintf(out_file3, "  ");
    }

    fprintf(out_file2, "\n\n\n\n");//4 enter lai labak izskatitos
    fprintf(out_file3, "\n");



}//end of izdruka_musas_genu_vertibas









void stav_grafa_analize_saujot2_musai(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll musas_stav_skaits, suuna &cur_suuna){

    /***
    * butiba stav_grafa analize saujot2 pielagota musai
    ***/

    FILE *out_file=fopen("stav_grafa_analize_saujot2_musai.txt", "a");
    FILE *out_file2=fopen("genu_v_pa_geniem_saujot_musai.txt", "a");
    FILE *out_file3=fopen("genu_v_pa_suunam_saujot_musai.txt", "a");

    ll virsotnu_skaits = musas_stav_skaits;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, virsotnu_skaits-1);

    ll cur_virsotne;

    //int it_skaits = 10000000;
    int it_skaits = 1000000000;
    int cur_it;

    int maks_laiks = 60;

    unordered_map<ll, int> cels;
    unordered_map<ll, int>::iterator atr_virs;



    int cur_cela_garums;
    int cur_cikla_garums;
    int cur_cels_lidz;

    cikls *cikli;
    int maks_ciklu_sk=1000;
    int ciklu_sk=0;
    bool jauns_cikls;
    int atr_cikla_ind;

    int gar_cikls=0;

    time_t sakuma_t;
    time_t beigu_t;
    time_t cur_it_t;





    pair <unordered_map<ll, int>::iterator, bool> atr_rez;

    cels.rehash(40);

    cikli = new cikls[maks_ciklu_sk];

    printf("sakas stav_grafa_analize ar sausanu 2 Musai.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){


        cels.clear();


        //genere kamer atrod derigu
        /*
        do{
           cur_virsotne = my_dist(my_rand);
        }
        while(!derigs_musas_stav(cur_virsotne) );
        */

        cur_virsotne = my_dist(my_rand);

        cur_cela_garums = 0;

        cels.emplace(cur_virsotne, 1);



        do{
            cur_cela_garums++;

            cur_virsotne = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_virsotne);

            atr_rez = cels.emplace(cur_virsotne, cur_cela_garums+1);
        }
        while(atr_rez.second);

        atr_virs = atr_rez.first;


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){

            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }




        if(jauns_cikls){
            //atrasts jauns cikls
            if(cur_cikla_garums>gar_cikls) gar_cikls = cur_cikla_garums;

            if(ciklu_sk==maks_ciklu_sk){
                cout<<"\n\t!!!ciklu vairak ka: "<<maks_ciklu_sk<<endl;
                cur_it++;
                break;
            }

            cikli[ciklu_sk].starta_virs = atr_virs->first;
            cikli[ciklu_sk].cikla_garums = cur_cikla_garums;
            cikli[ciklu_sk].gar_cels_lidz = cur_cels_lidz;
            cikli[ciklu_sk].reizes_trapits = 1;

            ciklu_sk++;
        }//endif jauns cikls
        else{
            //nav atrasts jauns cikls -trapits pa kadu velreiz
            if(cikli[atr_cikla_ind].gar_cels_lidz < cur_cels_lidz) cikli[atr_cikla_ind].gar_cels_lidz = cur_cels_lidz;

            cikli[atr_cikla_ind].reizes_trapits++;
        }

        if(cur_it_t-sakuma_t>maks_laiks){
            //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
            cur_it++;
            break;
        }


    }//iteraciju for beigas

    time(&beigu_t);


    /*
    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "analize ar sausanu2 musai laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz  reizes_trapits\n\n");
    //fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz     reizes_trapits  sakuma_virsotne(binari)\n\n");

    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d %10d/%d\n", (int)i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz, cikli[i].reizes_trapits, cur_it);
        //cout<<bitset<60>(cikli[i].starta_virs)<<endl;
        //fprintf(out_file,"%5d %13d %14d %10d/%d  %60s\n", (int)i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz, cikli[i].reizes_trapits, cur_it, (bitset<60>(cikli[i].starta_virs).to_string()).c_str());
    }
    fprintf(out_file, "\n####################################################################################################################\n");
    */


    ///
    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file2, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file3, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);

    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file2, "ciklu skaits := %d\n\n", ciklu_sk);
    fprintf(out_file3, "ciklu skaits := %d\n\n", ciklu_sk);

    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "iteraciju skaits := %d\n", cur_it);
    fprintf(out_file, "analize ar sausanu2 musai laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz  relativie_trapijumi    starta_stavoklis\n\n");


    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d", i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz);
        fprintf(out_file,"%21.4f",(double)cikli[i].reizes_trapits/(double)cur_it);

        fprintf(out_file, "%20lld\n", cikli[i].starta_virs);

        fprintf(out_file3, "cikls: %d\t", i+1);
        izdruka_musas_genu_vertibas(out_file2,out_file3, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cikli[i].starta_virs);
    }
    fprintf(out_file, "\n####################################################################################################################\n");
    fprintf(out_file2, "\n####################################################################################################################\n");
    fprintf(out_file3, "\n####################################################################################################################\n");
    ///


    printf("\n\tstav_grafa_analize ar sausanu 2 musai.beidz darbu.\n");
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));



}//stav_grafa_analize_saujot2_musai




void stav_grafa_analize_musai(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll musas_stav_skaits, suuna &cur_suuna){

    /**
    stav_grafa_analize_03_3var pielagota musai
    **/

    FILE *out_file = fopen("stav_gr_analize_Musai.txt", "a");

    int stavoklu_sk = musas_stav_skaits;

    int8_t *pieder_baseinam;

    baseins *baseini;
    int8_t maks_bas_sk = (1<<7)-5;//pietiktu ar -1, tacu vnk drosibai, iekavas obligati!!!


    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;


    vector <int> cels;
    static int maks_v_cela = 1<<2;
    int cur_ind;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    time_t sakuma_t;
    time_t beigu_t;



    int *att_lidz_cikliem;
    ll lielakais_att_lidz_ciklam;
    ll kop_att_lidz_ciklam;
    int cur_att_lidz_ciklam;

    double vid_att_lidz_ciklam;
    int lapu_sk;


    cels.resize(maks_v_cela);

    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];


    att_lidz_cikliem = new int[stavoklu_sk];
    memset(att_lidz_cikliem, 0, stavoklu_sk*sizeof(int));




    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    lielakais_att_lidz_ciklam=0;
    kop_att_lidz_ciklam=0;
    lapu_sk=0;


    printf("sakas stav_grafa_analize_musai veiktaa analize\n\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));

    for(int i=0; i<stavoklu_sk; i++){

        //if(pieder_baseinam[i]==-1 && derigs_musas_stav((ll)i) ){ //aizkomentets, jo tagadeja varianta, stavoklii vispaar konstantais SLP nav ieklauts.
        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                cels.at(v_skaits_cela) = cur_v;

                v_skaits_cela++;


                cur_v = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_v);



                //cela resizosanas ifa sakums
                if(v_skaits_cela==maks_v_cela-1){

                    maks_v_cela = maks_v_cela<<1;
                    cels.resize(maks_v_cela);
                }
                //cela resizosanas ifa beigas
            }
            while(pieder_baseinam[cur_v]==-1);


            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls


                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                cur_ind = v_skaits_cela-1;

                do{
                    cur_cikla_garums++;
                    cur_v = cels[cur_ind];
                    cur_ind--;

                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;

                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, sakums
                cur_att_lidz_ciklam = v_skaits_cela - cur_cikla_garums;
                cur_v = cels[0]; //jeb cur_v = i;
                cur_ind = 1;
                while(cur_v != cur_cikla_sakums){
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = cels[cur_ind];
                    cur_ind++;

                    cur_att_lidz_ciklam--;
                }
                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, beigas


                if(maks_bas_sk==ciklu_skaits){

                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");

                    fprintf(out_file, "\n!!!!!!!Kluda!!!!!!!!\n");
                    fprintf(out_file, "Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    fprintf(out_file, "Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");


                    break;
                }//ifa, kas parbauda vai nav parsniegts ciklu skaits beigas


            }//atrasts jauns cikls ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                if(att_lidz_cikliem[cur_v]<0) att_lidz_cikliem[cur_v] *= -1;//atzime, ka noteikti nav lapa

                cur_att_lidz_ciklam = v_skaits_cela + att_lidz_cikliem[cur_v];

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_ind=0; //cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    cur_v = cels[cur_ind];//tas pats, kas [k], tacu lai visur vienadi liku [cur_ind]
                    cur_ind++;
                    pieder_baseinam[cur_v] = cikla_nr;
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_att_lidz_ciklam--;
                }
            }//nav jauns cikls else beigas

            att_lidz_cikliem[i] *= -1; //virsotni, no kuras saaka staigat atzimee kaa potencialo lapu
        }
    }

    time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;



    for(int i=0; i<stavoklu_sk; i++){
        if(att_lidz_cikliem[i]<0){
            //ir lapa
            att_lidz_cikliem[i]*=-1;
            lapu_sk++;
            kop_att_lidz_ciklam += (ll)att_lidz_cikliem[i];

            if(att_lidz_cikliem[i] > lielakais_att_lidz_ciklam) lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

            if( att_lidz_cikliem[i] > baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam ) baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

        }
    }

    if(lapu_sk!=0) vid_att_lidz_ciklam = (double)kop_att_lidz_ciklam/(double)lapu_sk;
    else vid_att_lidz_ciklam = 0;




    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_skaits);
    fprintf(out_file, "garakais cikls := %d\n", garakais_cikls);
    fprintf(out_file, "vid cikla garums := %.2f\n", vid_cikla_garums);
    fprintf(out_file, "vid baseins := %.2f\n", vid_baseins);
    fprintf(out_file, "lielakais baseins := %d\n", lielakais_baseins);

    fprintf(out_file, "\nlapu sk: %d\n", lapu_sk);
    fprintf(out_file, "lielakais attalums lidz ciklam: %lld\n", lielakais_att_lidz_ciklam);
    fprintf(out_file, "videjais attalums lidz ciklam: %.2f\n", vid_att_lidz_ciklam);

    fprintf(out_file, "\nanalize03 laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\ncikls  baseina_lielums  cikla_garums   liel_att_lidz_c\n\n");

    for(int8_t i=0; i<ciklu_skaits; i++){
        fprintf(out_file, "%5d  %15d %14d %16d\n", (int)i+1, baseini[i].baseina_lielums, baseini[i].cikla_garums, baseini[i].lielakais_att_lidz_ciklam);
    }

    fprintf(out_file, "\n#####################################################\n");



    delete[] baseini;
    delete[] pieder_baseinam;
    delete[] att_lidz_cikliem;


}//stav_grafa_analize_musai



void aizpilda_musas_SLP(suuna **visas_sunas, const int &musas_sunu_skaits){

    if(musas_sunu_skaits==2){
        //musas_genu_vertibas[0][0] = false; //1.sunas SLP ir false
        //musas_genu_vertibas[1][0] = true; //2.suunas SLP ir true
        visas_sunas[0]->SLP = false; //1.sunas SLP ir false
        visas_sunas[1]->SLP = true; //2.suunas SLP ir true
    }
    else if(musas_sunu_skaits%4==0){
        //%4==0 nozime, ka ik pa 4 suunam
        /**
        15x4 gadijumaa:
        1. = false
        2. = false
        3. = true
        4. = true
        **/
        for(int i=0; i<musas_sunu_skaits; i++){
            //if( (i+1)%4 == 1 || (i+1)%4 == 2) musas_genu_vertibas[i][0] = false;
            //else musas_genu_vertibas[i][0] = true;
            if( (i+1)%4 == 1 || (i+1)%4 == 2) visas_sunas[i]->SLP = false;
            else visas_sunas[i]->SLP = true;
        }
    }


}//end of aizpilda_musas_SLP





void izdruka_musas_genu_vertibas_2(FILE *out_file2,FILE *out_file3, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const stavoklis &cur_stavoklis){


    ll temp_sk;
    ll rez;

    int cur_suunas_nr = musas_sunu_skaits-1;



    for(int sk_ind=cur_stavoklis.skaitlu_sk-1; sk_ind>=0; --sk_ind){

        temp_sk = cur_stavoklis.skaitli[sk_ind];

        for(int k=0; k<4; ++k){//2 vai 4 apzime cik viena skaitli glaba suunas

            for(int i=musas_genu_skaits-1; i>0; i--){
                musas_genu_vertibas[cur_suunas_nr][i] = (bool)(temp_sk & 1);
                temp_sk = temp_sk>>1;
            }

            --cur_suunas_nr;
        }//viena skaitla for beigas

    }//skaitlu for beigas

    ///




    //izdruka faila par katru genu kada ta vertiba katra suuna
    // sis der tikai tad, ja genu skaits ir 15
    static char genu_nos[15][5]={"SLP", "wg", "WG", "en", "EN", "hh",
                 "HH", "ptc", "PTC", "PH", "SMO", "ci", "CI", "CIA","CIR"};



    for(int cur_g=1; cur_g<musas_genu_skaits; ++cur_g){//sak no 0 ja grib izdrukat SLP no 1 ja nee.
        //printf("cur gens= %d : ", cur_g);
        fprintf(out_file2 ,"%s: ", genu_nos[cur_g]);//lai butu klat gena nosaukums
        for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
            //printf("%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
            fprintf(out_file2 ,"%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        //printf("\n");
        fprintf(out_file2, "\n");
    }



    for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
        for(int cur_g=0; cur_g<musas_genu_skaits; ++cur_g){
            fprintf(out_file3, "%d", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        fprintf(out_file3, "  ");
    }

    fprintf(out_file2, "\n\n\n\n");//4 enter lai labak izskatitos
    fprintf(out_file3, "\n");



}//end of izdruka_musas_genu_vertibas_2







struct cikls_2{
    //prieksh stav_grafa_analize_saujot

    stavoklis starta_virs;
    int cikla_garums;
    int reizes_trapits;
    int gar_cels_lidz;

    cikls_2(){
        cikla_garums=0;
        reizes_trapits=0;
        gar_cels_lidz=-1;
    }
};



void stav_grafa_analize_saujot2_musai_2(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll musas_skaitla_robeza, suuna &cur_suuna){

    /***
    * butiba stav_grafa analize saujot2 pielagota musai
    * Atskiribaa ar 1 variantu, pielagota musas stavokla glabasanai varakos skaitlos
    ***/

    FILE *out_file=fopen("stav_grafa_analize_saujot2_musai_2.txt", "a");
    FILE *out_file2=fopen("genu_v_pa_geniem_saujot_musai_2.txt", "a");
    FILE *out_file3=fopen("genu_v_pa_suunam_saujot_musai_2.txt", "a");


    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, musas_skaitla_robeza-1);

    //stavoklis cur_virsotne(musas_sunu_skaits/2); //jo 2 1 skaitli
    stavoklis cur_virsotne(musas_sunu_skaits/4); //jo 4 1 skaitli

    int it_skaits = 1000000000;
    int cur_it;

    int maks_laiks = 60;




    map<stavoklis, int> cels;
    map<stavoklis, int>::iterator atr_virs;





    int cur_cela_garums;
    int cur_cikla_garums;
    int cur_cels_lidz;

    cikls_2 *cikli;
    int maks_ciklu_sk=1000;
    int ciklu_sk=0;
    bool jauns_cikls;
    int atr_cikla_ind;

    int gar_cikls=0;

    time_t sakuma_t;
    time_t beigu_t;
    time_t cur_it_t;




    pair < map<stavoklis, int>::iterator, bool> atr_rez;

    //cels.rehash(40);

    cikli = new cikls_2[maks_ciklu_sk];


    printf("sakas stav_grafa_analize ar sausanu 2 Musai 2 variants.\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    for(cur_it=0; cur_it<it_skaits; cur_it++){


        cels.clear();


        //uzgenere virsotni
        for(int i=0; i<cur_virsotne.skaitlu_sk; ++i){
            cur_virsotne.skaitli[i] = my_dist(my_rand);
        }


        cur_cela_garums = 0;

        cels.emplace(cur_virsotne, 1);



        do{
            cur_cela_garums++;

            atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_virsotne);

            atr_rez = cels.emplace(cur_virsotne, cur_cela_garums+1);
        }
        while(atr_rez.second);

        atr_virs = atr_rez.first;


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;

        ///

        /*
        cout<<"$$$$"<<endl;
        map<stavoklis, int>::iterator it1=cels.begin();
        cout<<"\tvardnica"<<endl;
        cout<<"varndicas izmers: "<<cels.size()<<endl;
        while(it1!=cels.end()){
            //cout<<it1->first.skaitli[0]<<" "<<it1->first.skaitli[1]<<" i= "<<it1->second<<endl;
            for(int k=0; k<it1->first.skaitlu_sk; k++) cout<<it1->first.skaitli[k]<<" ";
            cout<<"i= "<<it1->second<<endl;
            it1++;
        }
        cout<<"$$$$"<<endl<<endl;


        cout<<"atrastais: "<<atr_virs->first.skaitli[0]<<" "<<atr_virs->first.skaitli[1]<<endl;
        cout<<"ciklu skaits: "<<ciklu_sk<<endl;
        system("pause");
        system("cls");
        */

        ///


        for(int i=0; i<ciklu_sk; i++){

            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }

        //if(jauns_cikls) cout<<"jauns cikls"<<endl;
        //else cout<<"nav j cikls"<<endl;
        //cout<<endl<<"#####################################################"<<endl<<endl;;



        if(jauns_cikls){
            //atrasts jauns cikls
            if(cur_cikla_garums>gar_cikls) gar_cikls = cur_cikla_garums;

            if(ciklu_sk==maks_ciklu_sk){
                cout<<"\n\t!!!ciklu vairak ka: "<<maks_ciklu_sk<<endl;
                cur_it++;
                break;
            }

            //cikli[ciklu_sk].starta_virs = atr_virs->first;
            cikli[ciklu_sk].starta_virs.skaitlu_sk = atr_virs->first.skaitlu_sk;

            for(int i=0; i<cur_virsotne.skaitlu_sk; ++i){
                cikli[ciklu_sk].starta_virs.skaitli[i] = atr_virs->first.skaitli[i];
            }

            cikli[ciklu_sk].cikla_garums = cur_cikla_garums;
            cikli[ciklu_sk].gar_cels_lidz = cur_cels_lidz;
            cikli[ciklu_sk].reizes_trapits = 1;

            ciklu_sk++;
        }//endif jauns cikls
        else{
            //nav atrasts jauns cikls -trapits pa kadu velreiz
            if(cikli[atr_cikla_ind].gar_cels_lidz < cur_cels_lidz) cikli[atr_cikla_ind].gar_cels_lidz = cur_cels_lidz;

            cikli[atr_cikla_ind].reizes_trapits++;
        }

        if(cur_it_t-sakuma_t>maks_laiks){
            //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
            cur_it++;
            break;
        }


    }//iteraciju for beigas

    time(&beigu_t);


    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file2, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file3, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);

    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file2, "ciklu skaits := %d\n\n", ciklu_sk);
    fprintf(out_file3, "ciklu skaits := %d\n\n", ciklu_sk);

    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "iteraciju skaits := %d\n", cur_it);
    fprintf(out_file, "analize ar sausanu2 musai laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz  relativie_trapijumi    starta_stavoklis\n\n");

    //fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz     reizes_trapits  sakuma_virsotne(binari)\n\n");
    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d", i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz);
        //fprintf(out_file,"%18d",(int) round((double)cikli[i].reizes_trapits/(double)cur_it)*100);
        fprintf(out_file,"%21.4f",(double)cikli[i].reizes_trapits/(double)cur_it);

        fprintf(out_file, "%20lld", cikli[i].starta_virs.skaitli[0]);
        for(int a=1; a<cikli[i].starta_virs.skaitlu_sk; ++a) fprintf(out_file, "%  lld", cikli[i].starta_virs.skaitli[a]);
        fprintf(out_file, "\n");

        fprintf(out_file3, "cikls: %d\t", i+1);
        izdruka_musas_genu_vertibas_2(out_file2,out_file3, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cikli[i].starta_virs);
    }
    fprintf(out_file, "\n####################################################################################################################\n");
    fprintf(out_file2, "\n####################################################################################################################\n");
    fprintf(out_file3, "\n####################################################################################################################\n");


    printf("\n\tstav_grafa_analize ar sausanu 2 musai 2.beidz darbu.\n");
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));





    /***
    parbaudes sadala
    ##############################################
    ##############################################
    ***/
    /*
    //for(int i=0; i<ciklu_sk; i++) fprintf(out_file, "%d %d\n", cikli[i].starta_virs.skaitli[0], cikli[i].starta_virs.skaitli[1]);

    bool ok;
    stavoklis abcd(2);
    ll a,b;

    for(int i=0; i<ciklu_sk; i++){
        a = cikli[i].starta_virs.skaitli[0] << 28;
        a += cikli[i].starta_virs.skaitli[1];
        b = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, a);
        if(a!=b){
            cout<<"\n\tne ok!!"<<endl;
            cout<<i<<endl;
            cout<<cikli[i].starta_virs.skaitli[0]<<" "<<cikli[i].starta_virs.skaitli[1]<<endl;
            break;
        }
    }
    */

    /*cur_virsotne.skaitli[0]=121;
    cur_virsotne.skaitli[1]=369;
    cels.emplace(cur_virsotne, 1);


    cikli[0].starta_virs.skaitlu_sk = cur_virsotne.skaitlu_sk;
    for(int i=0; i<cur_virsotne.skaitlu_sk; ++i){
        cikli[ciklu_sk].starta_virs.skaitli[i] = cur_virsotne.skaitli[i];
    }

    cout<<"ir jabut"<<endl;
    atr_virs = cels.find(cikli[0].starta_virs);
    if(atr_virs!=cels.end()) cout<<"ir"<<endl;
    else cout<<"nav"<<endl;

    cels.clear();

    cur_virsotne.skaitli[0]=122;
    cur_virsotne.skaitli[1]=370;
    cels.emplace(cur_virsotne, 1);

    cout<<"nav jabut"<<endl;
    atr_virs = cels.find(cikli[0].starta_virs);
    if(atr_virs!=cels.end()) cout<<"ir"<<endl;
    else cout<<"nav"<<endl;



    cels.clear();

    cur_virsotne.skaitli[0]=121;
    cur_virsotne.skaitli[1]=369;
    cels.emplace(cur_virsotne, 1);

    cout<<"ir jabut"<<endl;
    atr_virs = cels.find(cikli[0].starta_virs);
    if(atr_virs!=cels.end()) cout<<"ir"<<endl;
    else cout<<"nav"<<endl;

*/



    ///


}//stav_grafa_analize_saujot2_musai_2





void musas_dala(){

    //ll cur_stavoklis;
    int musas_genu_skaits=15;

    int musas_sunu_skaits=4;

    int kop_musas_genu_skaits;

    ll musas_stav_skaits;

    suuna **visas_sunas = new suuna*[musas_sunu_skaits];

    bool **musas_genu_vertibas = new bool*[musas_sunu_skaits];

    suuna cur_suuna;


    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);


    ///kop_musas_genu_skaits = musas_genu_skaits*musas_sunu_skaits;


    //si briza 2x15 un 4x15
    ///musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits-1);//-1 jo pirmas sunas pirmais genam jabut 0

    kop_musas_genu_skaits = (musas_genu_skaits-1)*musas_sunu_skaits;//jo SLP jau aizpildits
    musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits);

    //aizpilda_musas_SLP(visas_sunas, musas_genu_skaits, musas_sunu_skaits);
    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);



    /*
    time_t a,b;

    time(&a);
    cout<<"sakuma laiks:"<<ctime(&a)<<endl;

    for(int i=0; i<musas_stav_skaits; i++){
        cur_stavoklis = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, i);
    }
    time(&b);

    cout<<"bez parb laiks: "<<b-a<<"s"<<endl;


    time(&a);
    cout<<"sakuma laiks jaunai:"<<ctime(&a)<<endl;

    for(int i=0; i<musas_stav_skaits; i++){

        if(derigs_musas_stav(i)) cur_stavoklis = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, i);
    }
    time(&b);

    cout<<"ar parb laiks: "<<b-a<<"s"<<endl;

    */




    /*
    ll jabut = ((ll)1<<29) + ((ll)1<<14);
    ll maska = ((ll)1<<59) + ((ll)1<<44) + jabut;

    ll a = maska+7;

    for(int i=0; i<60; i++) printf("%x", i%15);
    cout<<endl;


    a = (a&maska);

    if(a!=jabut)cout<<"nav";
    */




    //stav_grafa_analize_saujot2_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, musas_stav_skaits, cur_suuna);

    //stav_grafa_analize_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, musas_stav_skaits, cur_suuna);

    /// sakas visadas lietas ar metodi, kur izmanto stavoklim vairakus skaitlus

    //int skaitlu_sk = musas_sunu_skaits/2; // sunu skaitu 1 skaitli
    int skaitlu_sk = musas_sunu_skaits/4; // sunu skaitu 1 skaitli

    //stavoklis cur_stavoklis(skaitlu_sk);

    //ll musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    ll musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli



    /*
    ll a = (ll)17173340298113278;
    ll b = (ll)  17173340298100813;
    ll c = (ll)  338670252146765;
    cur_stavoklis.skaitli[0]=a;
    cur_stavoklis.skaitli[1]=b;
    cur_stavoklis.skaitli[2]=c;

    atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
    if(a!=cur_stavoklis.skaitli[0] || b!=cur_stavoklis.skaitli[1] || c!=cur_stavoklis.skaitli[2])cout<<"kluda!!"<<endl;
    else cout<<"ok"<<endl;
    */


    /*
    cur_stavoklis.skaitli[0]=1261645;
    cur_stavoklis.skaitli[1]=77;

    //cout<<bitset<28>(cur_stavoklis.skaitli[0])<<" "<<bitset<28>(cur_stavoklis.skaitli[1])<<endl;

    //atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
    ll a = (ll)1261645<<28;
    a+=77;
    cout<<"a: "<<a<<endl;

    ll op = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, a);

    atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);


    //cout<<bitset<28>( (op>>28) )<<" "<<bitset<28>(op)<<endl;
    //cout<<bitset<28>(cur_stavoklis.skaitli[0])<<" "<<bitset<28>(cur_stavoklis.skaitli[1])<<endl;

    ll b = cur_stavoklis.skaitli[0]<<28;
    b+=cur_stavoklis.skaitli[1];
   // cout<<b<<endl;
   */



    ///
/*
    fstream fin("raksta_f_in.txt", ios::in);
    fstream fout("raksta_f_rez.txt", ios::out|ios::app);
    int reizes;
    ll temp;

    fin>>reizes;

    stavoklis cur_stavoklis(1);


    //prieksh 1 skaitla
    //prieksh daudz skaitlu var
    while(reizes--){
        fin>>cur_stavoklis.skaitli[0];
        temp=cur_stavoklis.skaitli[0];
        cur_stavoklis.skaitli[0] = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis.skaitli[0]);

        cout<<temp<<" --> "<<cur_stavoklis.skaitli[0]<<"\t: ";
        fout<<temp<<" --> "<<cur_stavoklis.skaitli[0]<<"\t: ";
        if(temp==cur_stavoklis.skaitli[0]) {
            cout<<"OK"<<endl;
            fout<<"OK"<<endl;
        }
        else {
            cout<<"NOT"<<endl;
            fout<<"NOT"<<endl;

        }
        cout<<endl;
        fout<<endl;
    }//end of while

    //prieksh daudz skaitlu var
/*
    while(reizes--){
        fin>>cur_stavoklis.skaitli[0];
        temp=cur_stavoklis.skaitli[0];
        atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

        cout<<temp<<" --> "<<cur_stavoklis.skaitli[0]<<"\t: ";
        fout<<temp<<" --> "<<cur_stavoklis.skaitli[0]<<"\t: ";
        if(temp==cur_stavoklis.skaitli[0]) {
            cout<<"OK"<<endl;
            fout<<"OK"<<endl;
        }
        else {
            cout<<"NOT"<<endl;
            fout<<"NOT"<<endl;

            ///
            ll temp_sk;

            int cur_suunas_nr = musas_sunu_skaits-1;
            cur_stavoklis.skaitli[0]=temp;



            for(int sk_ind=cur_stavoklis.skaitlu_sk-1; sk_ind>=0; --sk_ind){

                temp_sk = cur_stavoklis.skaitli[sk_ind];

                for(int k=0; k<4; ++k){//2 vai 4 apzime cik viena skaitli glaba suunas

                    for(int i=musas_genu_skaits-1; i>0; i--){
                        musas_genu_vertibas[cur_suunas_nr][i] = (bool)(temp_sk & 1);
                        temp_sk = temp_sk>>1;
                    }

                    --cur_suunas_nr;
                }//viena skaitla for beigas

            }//skaitlu for beigas



            for(int cur_g=1; cur_g<musas_genu_skaits; ++cur_g){//sak no 0 ja grib izdrukat SLP no 1 ja nee.
                for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){

                    fout<<(int)musas_genu_vertibas[suunas_nr][cur_g]<<" ";
                }
                fout<<endl;
            }
            ///
        }
        cout<<endl;
        fout<<endl;
    }//end of while
    */

    ///



    stav_grafa_analize_saujot2_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, musas_stav_skaits, cur_suuna);
    //stav_grafa_analize_saujot2_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, musas_skaitla_robeza, cur_suuna);


    /// beidzas visadas lietas ar metodi, kur izmanto stavoklim vairakus skaitlus






}//end of musas dala










int main()
{
    int genu_skaits;
    int ietekmejoso_genu_skaits;

    int ietekmejosie_geni[100][7];

    bool genu_bula_funkc[100][128];

    ietekme genu_bula_funkc_b2[100][7];

    mt19937_64 my_rand;

    //int *stavoklu_parejas=NULL; //pointeris uz masivu, kur glaba stavoklu parejas
    ll stavoklu_skaits;
    int bula_f_arg_konf_sk;
    ll cur_seeds;


    char genu_pareju_mode;


    //bloks, kur parbauda vai preproc ir definets un vai definets ar pareizam vertiba sakums
    {
        //par cik gara logiska izteiksme editors nepadara neredzamu arii ja viss ok.


        #ifndef rand_var
            cout<<"\n\tPREPROC ZINOJUMS: netika definets preproc mainigais: rand_var!"<<endl;
            return 0;
        #endif // bula_funkciju_tips
        #ifndef noteic_var
            cout<<"\n\tPREPROC ZINOJUMS: netika definets preproc mainigais: noteic_var!"<<endl;
            return 0;
        #endif // bula_funkciju_tips
        #ifndef sliek_var
            cout<<"\n\tPREPROC ZINOJUMS: netika definets preproc mainigais: sliek_var!"<<endl;
            return 0;
        #endif // bula_funkciju_tips

        #ifdef bula_funkciju_tips
            #if bula_funkciju_tips != rand_var && bula_funkciju_tips!=noteic_var && bula_funkciju_tips!=sliek_var
                cout<<"\n\tPREPROC ZINOJUMS: netika noradits pareizs bula funkciju tips!"<<endl;
                return 0;
            #endif // bula_funkciju_tips
        #else
            cout<<"\n\tPREPROC ZINOJUMS: netika definets preproc mainigais: bula_funkciju_tips!"<<endl;
            return 0;
        #endif // bula_funkciju_tips

    }
    //bloks, kur parbauda vai preproc ir definets un vai definets ar pareizam vertiba beigas


    ///musas dala sakums

    musas_dala();

    ///musas dala beigas



/*
    genu_skaits=50;
    ietekmejoso_genu_skaits=7;

    my_rand.seed(genu_skaits*10+ietekmejoso_genu_skaits);

    for(int i=0; i<5; i++){
        cur_seeds = my_rand();

        printf("\tit: %d/%d\n", (i+1),5);
        uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);

        #ifdef bula_funkciju_tips
            #if bula_funkciju_tips == rand_var
                cout<<"qw: a"<<endl;
                uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
            #elif bula_funkciju_tips == noteic_var
                //cout<<"qw: b"<<endl;
                uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
            #elif bula_funkciju_tips == sliek_var
                cout<<"qw: slieksna"<<endl;
                uzgenere_bula_funkcijas_sliek(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
            #endif // bula_funkciju_tips vetibas
        #endif // bula_funkciju_tips vai definets

        stav_grafa_analize_saujot2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits);
    }//for cikla beigas


*/




    /*
    /// prieksh normalas darbinasanas


    cin>>genu_skaits>>ietekmejoso_genu_skaits;

    while(genu_skaits<ietekmejoso_genu_skaits || ietekmejoso_genu_skaits>7 || ietekmejoso_genu_skaits<2){
        cout<<"genu skaitam jabut >= ietekmejoso genu skaitu!"<<endl;
        cout<<"ietekmejoso genu skaitam jabut intervala: [2;7]"<<endl;
        cin>>genu_skaits>>ietekmejoso_genu_skaits;
    }
    */


/*
    genu_skaits=50;
    ietekmejoso_genu_skaits=7;


    stavoklu_skaits = (ll)1<<genu_skaits;
    bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;

    my_rand.seed(genu_skaits*10+ietekmejoso_genu_skaits);
    ll cur_seeds = my_rand();
    cur_seeds=my_rand();


    uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);

    #ifdef bula_funkciju_tips
        #if bula_funkciju_tips == rand_var
            cout<<"qw: a"<<endl;
            uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
        #elif bula_funkciju_tips == noteic_var
            //cout<<"qw: b"<<endl;
            uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
        #elif bula_funkciju_tips == sliek_var
            cout<<"qw: slieksna"<<endl;
            uzgenere_bula_funkcijas_sliek(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
        #endif // bula_funkciju_tips vetibas
    #endif // bula_funkciju_tips vai definets


/*
    #ifdef bula_funkciju_tips
        #if bula_funkciju_tips == rand_var

        #elif bula_funkciju_tips == noteic_var

        #elif bula_funkciju_tips == sliek_var

        #endif // bula_funkciju_tips vetibas
    #endif // bula_funkciju_tips vai definets
 */




    //stav_grafa_analize_saujot(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits);

    //stav_grafa_analize_saujot2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits);

    //izdruka_stav_gr2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, 'a');

    //stav_grafa_analize_saujot(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits);



    //statistika_pilnam_stav_grafam(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2);
    //system("pause");
    //statistika_pilnam_stav_grafam_2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2);




    /// gabals, kur izdruka grafu failus kaut kada intervala
    ///sakums
    /*
    ll cur_seeds, cur_uzgen;
    char cur_faila_v[100]="";
    char post_vards[100]="stav_grafs_bvar.txt";

    genu_skaits=14;

    for(int i=2; i<8; i++){
        ietekmejoso_genu_skaits=i;
        cur_seeds = genu_skaits*10 + i;// uztaisa seedu: genu_skaitsietekmejoso_genu skaits, piem 12-7 bus 127
        my_rand.seed(cur_seeds);

        for(int cur_reize=0; cur_reize<5; cur_reize++){
            ///uzgenere genu parejas sakums
            cur_uzgen = my_rand();

            uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, cur_uzgen);
            #ifdef bula_funkciju_tips
                #if bula_funkciju_tips == rand_var
                    uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_uzgen);//avar
                #elif bula_funkciju_tips == noteic_var
                    //uzgenere_bula_funkcijas_b(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_uzgen);//bvar
                    uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_uzgen);//b2var
                #elif bula_funkciju_tips == sliek_var
                    uzgenere_bula_funkcijas_sliek(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_uzgen);//kan var
                #endif // bula_funkciju_tips vetibas
            #endif // bula_funkciju_tips vai definets
            ///uzgenere genu parejas beigas
            sprintf(cur_faila_v,"geni_%d-%d_r%d_%s", genu_skaits, i, cur_reize+1, post_vards);
            izdruka_stav_gr3(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_faila_v);
        }
    }
    */
    ///beigas









/*

    my_rand.seed(genu_skaits*10+ietekmejoso_genu_skaits);
    ll cur_seeds = my_rand();






    uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);

    //uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);//avar
    uzgenere_bula_funkcijas_b(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);//bvar
    uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);//b2var

    izdruka_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, 'a', "g_parejas_test_ab.txt");//a/b
    izdruka_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, 'b', "g_parejas_test_b2.txt");//b




    ielasa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, genu_pareju_mode, "g_parejas_test_ab.txt");
    izdruka_stav_gr2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, genu_pareju_mode,  "st_grafs_test_ab.txt");

    ielasa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, genu_pareju_mode, "g_parejas_test_b2.txt");
    izdruka_stav_gr2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, genu_pareju_mode,  "st_grafs_test_b2.txt");

*/




    /*
    //Testa gabals, lai apskatitos, cik laiku visiem stavokliem atrast uz kadu no stavokliem japariet, ja
    //bula funkcijas taisa ar b(kipa bin meklesana, prieks katra gena 128 vertibas) un b2( prieks katra gena 2*ietkmejoso g sk vertibas)
    // un izmantojot pareju rekinasanai parasto variantu prieks(128) un jauno atrod_jaunu_stavokli_b2 (lidzigs parastajam),
    // atrod_jaunu_stavokli_b2_2var daudie ifi

    time_t a,b;
    int nak_stav;
    int lidz;

    lidz = 1<<25;
    if(lidz>stavoklu_skaits) lidz = stavoklu_skaits;

    cout<<"sakas laiks"<<endl;
    time(&a);
    cout<<ctime(&a)<<endl;


    ll cur_seeds = my_rand();

    //uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_seeds , 'b');

    uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);
    uzgenere_bula_funkcijas_b2(genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_seeds);

    for(int i=0; i<lidz; i++){
        //nak_stav = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i);
        nak_stav = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, i);
        //nak_stav = atrod_jaunu_stavokli_b2_2var(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, i);
    }

    time(&b);
    cout<<"laiks:= "<<b-a<<"s"<<endl;

    //izdruka_stav_gr2(ietekmejosie_geni, genu_bula_funkc, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, "st_grafs_4gen_b.txt");

    return 0;
    */







    //genu_pareju_mode = 'a';
    //cout<<"\t\t#genu parejam izmantos bula funkcijas "<< genu_pareju_mode<<" variantu#"<<endl<<endl;
    //uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, my_rand(), genu_pareju_mode);


    //stav_grafa_analize_01(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);
    //stav_grafa_analize_02(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);
    //stav_grafa_analize_03(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);







    return 0;
}//main
