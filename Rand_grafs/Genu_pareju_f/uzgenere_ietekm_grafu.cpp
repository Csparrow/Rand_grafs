#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

void uzgenere_ietekm_grafu(int ietekmejosie_geni[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen){

    mt19937 my_rand;
    uniform_int_distribution <int> my_dist(0, genu_skaits-1);

    int cur_ietkm;
    int ietkm_skaits;

    bool ir_orig;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        ietkm_skaits = 0;

        while(ietkm_skaits<ietekmejoso_genu_skaits){

            do{
                cur_ietkm = my_dist(my_rand);
                ir_orig = true;
                //parbauda vai neatkartojas
                for(int k=0; k<ietkm_skaits; k++){
                    if(ietekmejosie_geni[cur_g][k]==cur_ietkm){
                        ir_orig=false;
                        break;
                    }
                }
                //beidzas parbaude
            }
            while(!ir_orig);

            ietekmejosie_geni[cur_g][ietkm_skaits] = cur_ietkm;

            ietkm_skaits++;
        }

    }//beidzas generesana


}//end of uzgenere_ietekm_grafu


/// beigas
