#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

 //seeds_gen ir defautla vertiba 5489 - tas redzams hederi
 //mode ir defautla vertiba a - tas redzams hederi
void uztaisa_genu_parejas(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen, const char mode){
    //uztaisa genu ietekmejoso grafu un prieksh katra gena bula funkcijas
    /*** pielikts parametrs ar taisis seedu abiem mersena generatoriem(abaas funckijaas vienadi), ja seeds_gen nonodod, ta vertiba bus 5489.
    *    5489 ir defaulta vertiba merseena generatoram - seedojot ar so skaitli, rezultats bus tads, ka speciali nesidojot vispar.
    ***/

    //int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;


    uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, seeds_gen);

    if(mode == 'a'){
        uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, seeds_gen);
    }
    else{
        uzgenere_bula_funkcijas_b(genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, seeds_gen);
    }

    //druka_g_pareju_info(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);

}//end of uztaisa_genu_parejas


/// beigas
