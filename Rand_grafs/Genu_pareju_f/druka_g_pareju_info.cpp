#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

void druka_g_pareju_info(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;


     cout<<"\tsakas genu info druka"<<endl;

     cout<<"\t\tietekmejosais grafs"<<endl;

     for(int i=0; i<genu_skaits; i++){
        printf("genu %d ietekme: ", i);
        for(int j=0; j<ietekmejoso_genu_skaits; j++) printf("%d ", ietekmejosie_geni[i][j]);
        cout<<endl;
    }
    cout<<endl;

    cout<<"\t\tgenu bula funkcijas"<<endl;
    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        printf("gens: %d\n", cur_g);
        for(int k=0; k<bula_f_arg_konf_sk; k++){
            printf("\t%d\n", genu_bula_funkc[cur_g][k]);
        }
        cout<<"####"<<endl;
    }
    cout<<endl<<"\t beidzas genu info druka"<<endl;

}//druka_g_pareju_info


/// beigas
