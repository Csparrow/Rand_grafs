#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums


void uzgenere_bula_funkcijas_b(bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;

    bool otra_puse;
    bool cur_vertiba;

    int k_ind;
    int l_ind;
    int cur_ind;

    int no, lidz;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        k_ind = 0;
        l_ind = bula_f_arg_konf_sk-1;
        cur_ind = l_ind/2;


        /**
        nepieciesams likt < un pedejam not
        **/

        while(k_ind<l_ind){
            otra_puse = my_dist(my_rand);
            cur_vertiba = my_dist(my_rand);

            if(!otra_puse){
                //izvelas pirmo pusi
                no=k_ind;
                lidz=cur_ind;

                k_ind = cur_ind+1;
            }
            else{
                //izvelas otro pusi
                no = cur_ind+1;
                //no = cur_ind;
                lidz = l_ind;

                //l_ind = cur_ind-1;
                l_ind = cur_ind;//nevar sanakt muzigais cikls, jo apstajas, ja palicis tikai 1 el jeb k_ind==l_ind
            }

            for(int i=no; i<=lidz; i++) genu_bula_funkc[cur_g][i] = cur_vertiba;

            cur_ind = (l_ind-k_ind)/2 + k_ind;

        }//beidzas bin meklesanas while
        genu_bula_funkc[cur_g][cur_ind] = !cur_vertiba;

    }//beidzas fciju generesana, katram genam


}//uzgenere_bula_funkcijas_2_var


/// beigas
