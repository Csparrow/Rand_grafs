#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums


void uzgenere_bula_funkcijas(bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        for(int k=0; k<bula_f_arg_konf_sk; k++){
            genu_bula_funkc[cur_g][k] = my_dist(my_rand);
        }
    }

}//uzgenere_bula_funkcijas



/// beigas
