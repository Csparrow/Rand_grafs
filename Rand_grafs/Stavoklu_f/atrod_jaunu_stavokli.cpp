#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

ll atrod_jaunu_stavokli(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis){
    //funkcija atrod stavokli uz kuru japariet

    int cur_g_cur_ietekm_st;
    int cur_g_arg_konf;

    ll rez=0;



    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        cur_g_arg_konf=0;

        for(int cur_arg=0; cur_arg<ietekmejoso_genu_skaits; cur_arg++){

            cur_g_cur_ietekm_st = cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][cur_arg]) & 1;

            cur_g_arg_konf = cur_g_arg_konf<<1;

            cur_g_arg_konf += cur_g_cur_ietekm_st; // cur_g_cur_ietekm_st bus 0 vai 1

        }//for, kas atrod cur g konf beigas

        rez = rez<<(ll)1;
        if(genu_bula_funkc[cur_g][cur_g_arg_konf]) rez++; //ja ir true jeb 1

    }//liela for beigas

    return rez;

}//end of atrod_jaunu_stavokli


/// beigas
