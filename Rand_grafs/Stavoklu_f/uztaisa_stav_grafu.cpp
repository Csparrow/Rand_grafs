#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums


int* uztaisa_stav_grafu(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const int stavoklu_skaits){

    int *stavoklu_parejas;

    stavoklu_parejas = new int[stavoklu_skaits];


    ///sakas pareju aizpildisana
    for(int i=0; i<stavoklu_skaits; i++){
        stavoklu_parejas[i] = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i);
    }
    ///beidzas pareju aizpildisana

    return stavoklu_parejas;

}//end of uztaisa_stav_grafu_un_stat


/// beigas
