#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

void stat_mekle_1ciklu(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits){

    bool ok;

    while(genu_skaits<=15){
        ietekmejoso_genu_skaits=2;
        ok=true;
        while(ietekmejoso_genu_skaits<=7 &&ok){
            uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);
            ok = mekle_1ciklu(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);
            ietekmejoso_genu_skaits++;
        }
        genu_skaits+=5;
    }//genu while beigas


}//stat_mekle_1ciklu


/// beigas
