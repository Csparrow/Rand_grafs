#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

void stat_mekle_1ciklu_2var(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits){


    bool ok;
    faili mani_f;

    mani_f.v_cig = fopen("vid_cikla_garums_01.txt", "w");
    mani_f.v_ceg = fopen("vid_cela_garums_01.txt", "w");
    mani_f.gar_ceg = fopen("garakais_cels_01.txt", "w");
    mani_f.laiks = fopen("laiks_01.txt", "w");
    mani_f.it = fopen("iteracijas_01.txt", "w");



    while(genu_skaits<=20){
        ietekmejoso_genu_skaits=2;
        ok=true;
        cout<<"genu sk:= "<<genu_skaits<<endl; //lai butu skaidrs cik talu tikts
        while(ietekmejoso_genu_skaits<=7 && ok){
            ok = mekle_1ciklu_2var(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, mani_f);
            cout<<"\tpab iet genu sk:= "<<ietekmejoso_genu_skaits<<endl; //lai butu skaidrs cik talu tikts
            ietekmejoso_genu_skaits++;
        }

        fprintf(mani_f.v_cig, "\n");
        fprintf(mani_f.v_ceg, "\n");
        fprintf(mani_f.gar_ceg, "\n");
        fprintf(mani_f.it, "\n");
        fprintf(mani_f.laiks, "\n");

        genu_skaits+=5;
    }//genu while beigas


}//stat_mekle_1ciklu_2var


/// beigas
