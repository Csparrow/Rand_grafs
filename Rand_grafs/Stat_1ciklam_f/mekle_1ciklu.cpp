#include <bits/stdc++.h>

#include "../my_rand_gr_fcijas.h"

using namespace std;

typedef long long ll;



/// sakums

bool mekle_1ciklu(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){
    //staiga 1 celu lidz atrod ciklu
    //tad savac dazadu statistiku
    //visu stavoklu grafu neglaba

    ll *cels;
    int maks_iesp_cels = 1000000;

    int it_skaits=10000;

    ll kop_virsotnu_skaits = (ll)1<<genu_skaits;

    ll cur_virsotne;
    ll prev_virsotne;

    int cur_cela_garums;
    int cur_cikla_garums;

    int maks_cela_garums;
    ll kop_celu_garums;
    ll kop_ciklu_garums;

    double vid_cela_garums;
    double vid_cikla_garums;

    bool ir_cikls;
    //bool beidzies_laiks;

    int cur_it;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, kop_virsotnu_skaits-1);

    time_t sakums_t;
    time_t beigas_t;
    time_t cur_it_t;

    fstream fout;
    fout.open("mekle1_cikla_stat.txt", ios::out|ios::app);



    cels = new ll[maks_iesp_cels];

    kop_celu_garums=0;
    kop_ciklu_garums=0;
    maks_cela_garums=0;

    //beidzies_laiks=false;


    /*cout<<"\tgenu skaits = "<<genu_skaits<<endl;
    cout<<"ietekmejoso_genu_skaits = "<<ietekmejoso_genu_skaits<<endl;
    cout<<"iteraciju skaits= "<<it_skaits<<endl<<endl;*/

    fout<<"\tgenu skaits = "<<genu_skaits<<endl;
    fout<<"ietekmejoso_genu_skaits = "<<ietekmejoso_genu_skaits<<endl;
    fout<<"iteraciju skaits= "<<it_skaits<<endl<<endl;


    time(&sakums_t);

    //for(int k=0; k<it_skaits; k++){
    for(cur_it=0; cur_it<it_skaits; cur_it++){

        prev_virsotne = my_dist(my_rand);

        cels[0] = prev_virsotne;
        cur_cela_garums=1;
        ir_cikls = false;

        while(cur_cela_garums<maks_iesp_cels && !ir_cikls){

            cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, prev_virsotne);

            ///parbauda vai nav cikls
            for(int i=0; i<cur_cela_garums; i++){
                if(cels[i] == cur_virsotne){
                    ir_cikls=true;
                    cur_cikla_garums = cur_cela_garums - i;
                    break;
                }
            }
            ///beidzas parbaude vai ir cikls

            cels[cur_cela_garums] = cur_virsotne;
            cur_cela_garums++;

            prev_virsotne = cur_virsotne;

        }//end of while

        time(&cur_it_t);

        if(ir_cikls){
            cur_cela_garums--;//ja sis nav sanak cikla sakuma virsotni pieskaitit divas reizes!!

            kop_ciklu_garums += (ll)cur_cikla_garums;
            kop_celu_garums += (ll)cur_cela_garums;


            if(cur_cela_garums>maks_cela_garums) maks_cela_garums = cur_cela_garums;

            if(cur_it_t-sakums_t>60){
                //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
                fout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
                cur_it++;
                //beidzies_laiks=true;
                break;
            }

        }
        else{
            /*cout<<"\t\t!!neatrada ciklu!!"<<endl;
            cout<<"cels pa garu "<<cur_it<<" iteracijaa"<<endl;
            cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;*/

            fout<<"\t\t!!neatrada ciklu!!"<<endl;
            fout<<"cels pa garu "<<cur_it<<" iteracijaa"<<endl;
            fout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
            return false;
        }


    }//beigas iteraciju for ciklam

    time(&beigas_t);


    vid_cikla_garums = (double)kop_ciklu_garums/(double)cur_it;
    vid_cela_garums = (double)kop_celu_garums/(double)cur_it;



    ///ja izgaja visas cur it = it skaits
    /*cout<<setprecision(5)<<"vid. cikla garums = "<<(double)kop_ciklu_garums/(double)cur_it<<endl;
    cout<<setprecision(5)<<"vid. cela garums = "<<(double)kop_celu_garums/(double)cur_it<<endl;
    cout<<"garakais cels = "<<maks_cela_garums<<endl<<endl;
    cout<<"laiks:"<<beigas_t-sakums_t<<"sekundes"<<endl;
    cout<<"######################################"<<endl<<endl;*/

    fout<<setprecision(5)<<"vid. cikla garums = "<<vid_cikla_garums<<endl;
    fout<<setprecision(5)<<"vid. cela garums = "<<vid_cela_garums<<endl;
    fout<<"garakais cels = "<<maks_cela_garums<<endl<<endl;
    fout<<"laiks:"<<beigas_t-sakums_t<<"sekundes"<<endl;
    fout<<"######################################"<<endl<<endl;

    //return !beidzies_laiks; //atgriez true, ja laiks nav beidzies
    return true;

}//end of mekle 1 ciklu



/// beigas
