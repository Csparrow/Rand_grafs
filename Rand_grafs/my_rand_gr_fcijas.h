#ifndef MY_RAND_GR_FCIJAS_H_INCLUDED
#define MY_RAND_GR_FCIJAS_H_INCLUDED


/************************************

* seit tiek glabati visu funkciju hederi

**************************************/

typedef long long ll;

///rekordi
struct faili{
    //seit visi failu mainigie prieksh mekle 1ciklu 2var

    FILE *v_cig;
    FILE *v_ceg;
    FILE *gar_ceg;
    FILE *it;
    FILE *laiks;

};//end of faili

///fcijas

//sai fcijai ir defaltais parametrs prieksh seeds_gen  - skatit failu uztaisa_genu_parejas.
void uztaisa_genu_parejas(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen=5489, const char mode='a');

void uzgenere_ietekm_grafu(int ietekmejosie_geni[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen);

void uzgenere_bula_funkcijas(bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen);

void uzgenere_bula_funkcijas_b(bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen);

void druka_g_pareju_info(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits);



ll atrod_jaunu_stavokli(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis);

int* uztaisa_stav_grafu(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const int stavoklu_skaits);



void stat_mekle_1ciklu(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits);

void stat_mekle_1ciklu_2var(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits);

void stat_mekle_1ciklu_map(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits);


bool mekle_1ciklu(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits);

bool mekle_1ciklu_2var(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, faili &mani_f);

void mekle_1ciklu_map(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, faili &mani_f);









#endif // MY_RAND_GR_FCIJAS_H_INCLUDED
