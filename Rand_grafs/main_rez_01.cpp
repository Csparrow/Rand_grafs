#include <iostream>
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;


void uzgenere_ietekm_grafu(int ietekmejosie_geni[][7], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen){

    mt19937 my_rand;
    uniform_int_distribution <int> my_dist(0, genu_skaits-1);

    int cur_ietkm;
    int ietkm_skaits;

    bool ir_orig;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        ietkm_skaits = 0;

        while(ietkm_skaits<ietekmejoso_genu_skaits){

            do{
                cur_ietkm = my_dist(my_rand);
                ir_orig = true;
                //parbauda vai neatkartojas
                for(int k=0; k<ietkm_skaits; k++){
                    if(ietekmejosie_geni[cur_g][k]==cur_ietkm){
                        ir_orig=false;
                        break;
                    }
                }
                //beidzas parbaude
            }
            while(!ir_orig);

            ietekmejosie_geni[cur_g][ietkm_skaits] = cur_ietkm;

            ietkm_skaits++;
        }

    }//beidzas generesana


}//end of uzgenere_ietekm_grafu


void uzgenere_bula_funkcijas(bool genu_bula_funkc[][128], const int genu_skaits, const int bula_f_arg_konf_sk, const ll seeds_gen){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        for(int k=0; k<bula_f_arg_konf_sk; k++){
            genu_bula_funkc[cur_g][k] = my_dist(my_rand);
        }
    }



}//uzgenere_bula_funkcijas


void uzgenere_bula_funkcijas_2var(bool genu_bula_funkc[][128], const int genu_skaits, const int bula_f_arg_konf_sk, const ll seeds_gen){

    mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);


    bool pirma_puse;
    bool cur_vertiba;

    int k_ind;
    int l_ind;
    int cur_ind;

    int no, lidz;

    my_rand.seed(seeds_gen); //noseedo generatoru

    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        k_ind = 0;
        l_ind = bula_f_arg_konf_sk-1;
        cur_ind = l_ind/2;

        while(k_ind<=l_ind){
            pirma_puse = my_dist(my_rand);
            cur_vertiba = my_dist(my_rand);

            if(pirma_puse){
                //izvelas pirmo pusi
                no=k_ind;
                lidz=cur_ind;

                k_ind = cur_ind+1;
            }
            else{
                //izvelas otro pusi
                no = cur_ind;
                lidz = l_ind;

                l_ind = cur_ind-1;
            }

            for(int i=no; i<=lidz; i++) genu_bula_funkc[cur_g][i] = cur_vertiba;

            cur_ind = (l_ind-k_ind)/2 + k_ind;

        }//beidzas bin meklesanas while


    }//beidzas fciju generesana, katram genam



}//uzgenere_bula_funkcijas_2_var



ll atrod_jaunu_stavokli(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll cur_stavoklis){
    //funkcija atrod stavokli uz kuru japariet

    int cur_g_cur_ietekm_st;
    int cur_g_arg_konf;

    ll rez=0;



    for(int cur_g=0; cur_g<genu_skaits; cur_g++){

        cur_g_arg_konf=0;

        for(int cur_arg=0; cur_arg<ietekmejoso_genu_skaits; cur_arg++){

            cur_g_cur_ietekm_st = cur_stavoklis>>(genu_skaits-1-ietekmejosie_geni[cur_g][cur_arg]) & 1;

            cur_g_arg_konf = cur_g_arg_konf<<1;

            cur_g_arg_konf += cur_g_cur_ietekm_st; // cur_g_cur_ietekm_st bus 0 vai 1

        }//for, kas atrod cur g konf beigas

        rez = rez<<(ll)1;
        if(genu_bula_funkc[cur_g][cur_g_arg_konf]) rez++; //ja ir true jeb 1

    }//liela for beigas

    return rez;

}//end of gena_paslegsana


void druka_g_pareju_info(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;


     cout<<"\tsakas genu info druka"<<endl;

     cout<<"\t\tietekmejosais grafs"<<endl;

     for(int i=0; i<genu_skaits; i++){
        printf("genu %d ietekme: ", i);
        for(int j=0; j<ietekmejoso_genu_skaits; j++) printf("%d ", ietekmejosie_geni[i][j]);
        cout<<endl;
    }
    cout<<endl;

    cout<<"\t\tgenu bula funkcijas"<<endl;
    for(int cur_g=0; cur_g<genu_skaits; cur_g++){
        printf("gens: %d\n", cur_g);
        for(int k=0; k<bula_f_arg_konf_sk; k++){
            printf("\t%d\n", genu_bula_funkc[cur_g][k]);
        }
        cout<<"####"<<endl;
    }
    cout<<endl<<"\t beidzas genu info druka"<<endl;

}//druka_g_pareju_info



void uztaisa_genu_parejas(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const ll seeds_gen=5489){
    //uztaisa genu ietekmejoso grafu un prieksh katra gena bula funkcijas
    /*** pielikts parametrs ar taisis seedu abiem mersena generatoriem(abaas funckijaas vienadi), ja seeds_gen nonodod, ta vertiba bus 5489.
    *    5489 ir defaulta vertiba merseena generatoram - seedojot ar so skaitli, rezultats bus tads, ka speciali nesidojot vispar.
    ***/

    int bula_f_arg_konf_sk = 1<<ietekmejoso_genu_skaits;


    uzgenere_ietekm_grafu(ietekmejosie_geni, genu_skaits, ietekmejoso_genu_skaits, seeds_gen);

    //uzgenere_bula_funkcijas(genu_bula_funkc, genu_skaits, bula_f_arg_konf_sk, seeds_gen);
    uzgenere_bula_funkcijas_2var(genu_bula_funkc, genu_skaits, bula_f_arg_konf_sk, seeds_gen);

    //druka_g_pareju_info(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);

}//end of uztaisa_genu_parejas



int* uztaisa_stav_grafu(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, const int stavoklu_skaits){

    int *stavoklu_parejas;

    stavoklu_parejas = new int[stavoklu_skaits];


    ///sakas pareju aizpildisana
    for(int i=0; i<stavoklu_skaits; i++){
        stavoklu_parejas[i] = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, i);
    }
    ///beidzas pareju aizpildisana

    return stavoklu_parejas;

}//end of uztaisa_stav_grafu_un_stat



bool mekle_1ciklu(const int ietekmejosie_geni[][7], const bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits){
    //staiga 1 celu lidz atrod ciklu
    //tad savac dazadu statistiku
    //visu stavoklu grafu neglaba

    ll *cels;
    int maks_iesp_cels = 1000000;

    int it_skaits=10000;

    ll kop_virsotnu_skaits = (ll)1<<genu_skaits;

    ll cur_virsotne;
    ll prev_virsotne;

    int cur_cela_garums;
    int cur_cikla_garums;

    int maks_cela_garums;
    ll kop_celu_garums;
    ll kop_ciklu_garums;

    double vid_cela_garums;
    double vid_cikla_garums;

    bool ir_cikls;
    bool beidzies_laiks;

    int cur_it;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, kop_virsotnu_skaits-1);

    time_t sakums_t;
    time_t beigas_t;
    time_t cur_it_t;

    fstream fout;
    fout.open("mekle1_cikla_stat.txt", ios::out|ios::app);



    cels = new ll[maks_iesp_cels];

    kop_celu_garums=0;
    kop_ciklu_garums=0;
    maks_cela_garums=0;

    //beidzies_laiks=false;


    /*cout<<"\tgenu skaits = "<<genu_skaits<<endl;
    cout<<"ietekmejoso_genu_skaits = "<<ietekmejoso_genu_skaits<<endl;
    cout<<"iteraciju skaits= "<<it_skaits<<endl<<endl;*/

    fout<<"\tgenu skaits = "<<genu_skaits<<endl;
    fout<<"ietekmejoso_genu_skaits = "<<ietekmejoso_genu_skaits<<endl;
    fout<<"iteraciju skaits= "<<it_skaits<<endl<<endl;


    time(&sakums_t);

    //for(int k=0; k<it_skaits; k++){
    for(cur_it=0; cur_it<it_skaits; cur_it++){

        prev_virsotne = my_dist(my_rand);

        cels[0] = prev_virsotne;
        cur_cela_garums=1;
        ir_cikls = false;

        while(cur_cela_garums<maks_iesp_cels && !ir_cikls){

            cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, prev_virsotne);

            ///parbauda vai nav cikls
            for(int i=0; i<cur_cela_garums; i++){
                if(cels[i] == cur_virsotne){
                    ir_cikls=true;
                    cur_cikla_garums = cur_cela_garums - i;
                    break;
                }
            }
            ///beidzas parbaude vai ir cikls

            cels[cur_cela_garums] = cur_virsotne;
            cur_cela_garums++;

            prev_virsotne = cur_virsotne;

        }//end of while

        time(&cur_it_t);

        if(ir_cikls){
            cur_cela_garums--;//ja sis nav sanak cikla sakuma virsotni pieskaitit divas reizes!!

            kop_ciklu_garums += (ll)cur_cikla_garums;
            kop_celu_garums += (ll)cur_cela_garums;


            if(cur_cela_garums>maks_cela_garums) maks_cela_garums = cur_cela_garums;

            if(cur_it_t-sakums_t>60){
                //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
                fout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
                cur_it++;
                //beidzies_laiks=true;
                break;
            }

        }
        else{
            /*cout<<"\t\t!!neatrada ciklu!!"<<endl;
            cout<<"cels pa garu "<<cur_it<<" iteracijaa"<<endl;
            cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;*/

            fout<<"\t\t!!neatrada ciklu!!"<<endl;
            fout<<"cels pa garu "<<cur_it<<" iteracijaa"<<endl;
            fout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
            return false;
        }


    }//beigas iteraciju for ciklam

    time(&beigas_t);





    ///ja izgaja visas cur it = it skaits
    /*cout<<setprecision(5)<<"vid. cikla garums = "<<(double)kop_ciklu_garums/(double)cur_it<<endl;
    cout<<setprecision(5)<<"vid. cela garums = "<<(double)kop_celu_garums/(double)cur_it<<endl;
    cout<<"garakais cels = "<<maks_cela_garums<<endl<<endl;
    cout<<"laiks:"<<beigas_t-sakums_t<<"sekundes"<<endl;
    cout<<"######################################"<<endl<<endl;*/

    fout<<setprecision(5)<<"vid. cikla garums = "<<(double)kop_ciklu_garums/(double)cur_it<<endl;
    fout<<setprecision(5)<<"vid. cela garums = "<<(double)kop_celu_garums/(double)cur_it<<endl;
    fout<<"garakais cels = "<<maks_cela_garums<<endl<<endl;
    fout<<"laiks:"<<beigas_t-sakums_t<<"sekundes"<<endl;
    fout<<"######################################"<<endl<<endl;

    //return !beidzies_laiks; //atgriez true, ja laiks nav beidzies
    return true;

}//end of mekle 1 ciklu



struct faili{
    //seit visi failu mainigie prieksh mekle 1ciklu 2var

    FILE *v_cig;
    FILE *v_ceg;
    FILE *gar_ceg;
    FILE *it;
    FILE *laiks;

};//end of faili

bool mekle_1ciklu_2var(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, faili &mani_f){

    //staiga 1 celu lidz atrod ciklu
    //tad savac dazadu statistiku
    //visu stavoklu grafu neglaba

    // atskiriba no mekle_1ciklu - genus genere uz katru iteraciju, nevis uzgenere un tad visas iteracijas uz vienu generejumu


    ll *cels;
    int maks_iesp_cels = 1000000;

    int it_skaits=10000;

    ll kop_virsotnu_skaits = (ll)1<<genu_skaits;

    ll cur_virsotne;
    ll prev_virsotne;

    int cur_cela_garums;
    int cur_cikla_garums;

    int maks_cela_garums;
    ll kop_celu_garums;
    ll kop_ciklu_garums;

    double vid_cela_garums;
    double vid_cikla_garums;

    bool ir_cikls;
    bool beidzies_laiks;

    int cur_it;

    mt19937_64 my_rand; // genere cela sakuma virsotni
    uniform_int_distribution<ll> my_dist(0, kop_virsotnu_skaits-1);

    mt19937_64 my_rand_seed; //genere seeda skaitlus prieksh genu pareju taisisanu. defaulta sadalijums 0 - 2^64 jeb long long izmeri

    time_t sakums_t;
    time_t beigas_t;
    time_t cur_it_t;




    cels = new ll[maks_iesp_cels];

    kop_celu_garums=0;
    kop_ciklu_garums=0;
    maks_cela_garums=0;


    /*
    cout<<"\tgenu skaits = "<<genu_skaits<<endl;
    cout<<"ietekmejoso_genu_skaits = "<<ietekmejoso_genu_skaits<<endl;
    cout<<"iteraciju skaits= "<<it_skaits<<endl<<endl;
    */



    time(&sakums_t);

    for(cur_it=0; cur_it<it_skaits; cur_it++){

        uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, my_rand_seed() ); // uz katru iteraciju uztiasa visu pa jaunam
        // prognoze - sadi vajadzetu sanakt labakiem/sakarigakiem rezultatiem

        prev_virsotne = my_dist(my_rand);

        cels[0] = prev_virsotne;
        cur_cela_garums=1;
        ir_cikls = false;

        while(cur_cela_garums<maks_iesp_cels && !ir_cikls){

            cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, prev_virsotne);

            ///parbauda vai nav cikls
            for(int i=0; i<cur_cela_garums; i++){
                if(cels[i] == cur_virsotne){
                    ir_cikls=true;
                    cur_cikla_garums = cur_cela_garums - i;
                    break;
                }
            }
            ///beidzas parbaude vai ir cikls

            cels[cur_cela_garums] = cur_virsotne;
            cur_cela_garums++;

            prev_virsotne = cur_virsotne;

        }//end of while

        time(&cur_it_t);

        if(ir_cikls){
            cur_cela_garums--;//ja sis nav sanak cikla sakuma virsotni pieskaitit divas reizes!!

            kop_ciklu_garums += (ll)cur_cikla_garums;
            kop_celu_garums += (ll)cur_cela_garums;


            if(cur_cela_garums>maks_cela_garums) maks_cela_garums = cur_cela_garums;

            if(cur_it_t-sakums_t>60){
                //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
                cur_it++;
                break;
            }

        }
        else{
            cout<<"\t\t!!neatrada ciklu!!"<<endl;
            cout<<"cels pa garu "<<cur_it<<" iteracijaa"<<endl;
            cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
            return false;
        }


    }//beigas iteraciju for ciklam

    time(&beigas_t);



    ///ja izgaja visas cur it = it skaits
    /*
    cout<<setprecision(5)<<"vid. cikla garums = "<<(double)kop_ciklu_garums/(double)cur_it<<endl;
    cout<<setprecision(5)<<"vid. cela garums = "<<(double)kop_celu_garums/(double)cur_it<<endl;
    cout<<"garakais cels = "<<maks_cela_garums<<endl<<endl;
    cout<<"laiks:"<<beigas_t-sakums_t<<"sekundes"<<endl;
    cout<<"######################################"<<endl<<endl;
    */

    ///sakas druka failos
    fprintf(mani_f.v_cig, "%.5f ", vid_cikla_garums);
    fprintf(mani_f.v_ceg, "%.5f ", vid_cela_garums);
    fprintf(mani_f.gar_ceg, "%d ", maks_cela_garums);
    fprintf(mani_f.it, "%d ", cur_it);
    fprintf(mani_f.laiks, "%ld ", beigas_t-sakums_t);

    ///beidzas druka failos



    return true;

}//end of mekle 1 ciklu_2var






void mekle_1ciklu_map(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], const int genu_skaits, const int ietekmejoso_genu_skaits, faili &mani_f){

    //staiga 1 celu lidz atrod ciklu
    //tad savac dazadu statistiku
    //visu stavoklu grafu neglaba

    // atskiriba no mekle_1ciklu - genus genere uz katru iteraciju, nevis uzgenere un tad visas iteracijas uz vienu generejumu


    //unordered_map<ll,int> cela_map;
    //unordered_map<ll,int>::iterator atrastaa_virs;

    map<ll,int> cela_map;
    map<ll,int>::iterator atrastaa_virs;



    int it_skaits=10000;

    ll kop_virsotnu_skaits = (ll)1<<genu_skaits;

    ll cur_virsotne;
    ll prev_virsotne;

    int cur_cela_garums;
    int cur_cikla_garums;

    int maks_cela_garums;
    ll kop_celu_garums;
    ll kop_ciklu_garums;

    double vid_cela_garums;
    double vid_cikla_garums;

    bool ir_cikls;
    bool beidzies_laiks;

    int cur_it;

    mt19937_64 my_rand; // genere cela sakuma virsotni
    uniform_int_distribution<ll> my_dist(0, kop_virsotnu_skaits-1);

    mt19937_64 my_rand_seed; //genere seeda skaitlus prieksh genu pareju taisisanu. defaulta sadalijums 0 - 2^64 jeb long long izmeri

    time_t sakums_t;
    time_t beigas_t;
    time_t cur_it_t;




    kop_celu_garums=0;
    kop_ciklu_garums=0;
    maks_cela_garums=0;



    time(&sakums_t);

    for(cur_it=0; cur_it<it_skaits; cur_it++){

        cela_map.erase(cela_map.begin(), cela_map.end());

        uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, my_rand_seed() ); // uz katru iteraciju uztiasa visu pa jaunam

        prev_virsotne = my_dist(my_rand);

        cela_map.insert({{prev_virsotne, 0}});
        cur_cela_garums=1;
        ir_cikls = false;

        while(true){

            cur_virsotne = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, prev_virsotne);

            ///parbauda vai nav cikls
            atrastaa_virs = cela_map.find(cur_virsotne);

            if(atrastaa_virs!=cela_map.end()){
                ///ir atrasta sada virsotne
                ir_cikls=true;
                cur_cikla_garums = cur_cela_garums - atrastaa_virs->second;
                break;
            }
            else{
                ///nav atrasta tada virsotne
                cela_map.insert({{cur_virsotne, cur_cela_garums}});
                cur_cela_garums++;
                prev_virsotne = cur_virsotne;
            }


        }//end of while

        time(&cur_it_t);

        kop_ciklu_garums += (ll)cur_cikla_garums;
        kop_celu_garums += (ll)cur_cela_garums;


        if(cur_cela_garums>maks_cela_garums) maks_cela_garums = cur_cela_garums;

        if(cur_it_t-sakums_t>60){
            //cout<<"\t!!!!!!!!!!!beidzas laiks iteracija = "<<cur_it<<endl;
            cur_it++;
            break;
        }


    }//beigas iteraciju for ciklam

    time(&beigas_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)cur_it;
    vid_cela_garums = (double)kop_celu_garums/(double)cur_it;



    ///ja izgaja visas cur it = it skaits

    /*
    cout<<setprecision(5)<<"vid. cikla garums = "<<(double)kop_ciklu_garums/(double)cur_it<<endl;
    cout<<setprecision(5)<<"vid. cela garums = "<<(double)kop_celu_garums/(double)cur_it<<endl;
    cout<<"garakais cels = "<<maks_cela_garums<<endl<<endl;
    cout<<"laiks:"<<beigas_t-sakums_t<<"sekundes"<<endl;
    cout<<"######################################"<<endl<<endl;
    */

    ///sakas druka failos

    fprintf(mani_f.v_cig, "%.5f ", vid_cikla_garums);
    fprintf(mani_f.v_ceg, "%.5f ", vid_cela_garums);
    fprintf(mani_f.gar_ceg, "%d ", maks_cela_garums);
    fprintf(mani_f.it, "%d ", cur_it);
    fprintf(mani_f.laiks, "%ld ", beigas_t-sakums_t);


    ///beidzas druka failos


}//end of mekle 1 ciklu_map






void stat_mekle_1ciklu(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits){

    bool ok;

    while(genu_skaits<=15){
        ietekmejoso_genu_skaits=2;
        ok=true;
        while(ietekmejoso_genu_skaits<=7 &&ok){
            uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);
            ok = mekle_1ciklu(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);
            ietekmejoso_genu_skaits++;
        }
        genu_skaits+=5;
    }//genu while beigas


}//stat_mekle_1ciklu



void stat_mekle_1ciklu_2var(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits){


    bool ok;
    faili mani_f;

    mani_f.v_cig = fopen("vid_cikla_garums_01.txt", "w");
    mani_f.v_ceg = fopen("vid_cela_garums_01.txt", "w");
    mani_f.gar_ceg = fopen("garakais_cels_01.txt", "w");
    mani_f.laiks = fopen("laiks_01.txt", "w");
    mani_f.it = fopen("iteracijas_01.txt", "w");



    while(genu_skaits<=20){
        ietekmejoso_genu_skaits=2;
        ok=true;
        cout<<"genu sk:= "<<genu_skaits<<endl; //lai butu skaidrs cik talu tikts
        while(ietekmejoso_genu_skaits<=7 && ok){
            ok = mekle_1ciklu_2var(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, mani_f);
            cout<<"\tpab iet genu sk:= "<<ietekmejoso_genu_skaits<<endl; //lai butu skaidrs cik talu tikts
            ietekmejoso_genu_skaits++;
        }

        fprintf(mani_f.v_cig, "\n");
        fprintf(mani_f.v_ceg, "\n");
        fprintf(mani_f.gar_ceg, "\n");
        fprintf(mani_f.it, "\n");
        fprintf(mani_f.laiks, "\n");

        genu_skaits+=5;
    }//genu while beigas


}//stat_mekle_1ciklu_2var



void stat_mekle_1ciklu_map(int ietekmejosie_geni[][7], bool genu_bula_funkc[][128], int genu_skaits, int ietekmejoso_genu_skaits){


    faili mani_f;

    mani_f.v_cig = fopen("vid_cikla_garums_01.txt", "w");
    mani_f.v_ceg = fopen("vid_cela_garums_01.txt", "w");
    mani_f.gar_ceg = fopen("garakais_cels_01.txt", "w");
    mani_f.laiks = fopen("laiks_01.txt", "w");
    mani_f.it = fopen("iteracijas_01.txt", "w");


    while(genu_skaits<=30){
        ietekmejoso_genu_skaits=2;
        cout<<"genu sk:= "<<genu_skaits<<endl; //lai butu skaidrs cik talu tikts
        while(ietekmejoso_genu_skaits<=7){
            mekle_1ciklu_map(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, mani_f);
            cout<<"\tpab iet genu sk:= "<<ietekmejoso_genu_skaits<<endl; //lai butu skaidrs cik talu tikts
            ietekmejoso_genu_skaits++;
        }

        fprintf(mani_f.v_cig, "\n");
        fprintf(mani_f.v_ceg, "\n");
        fprintf(mani_f.gar_ceg, "\n");
        fprintf(mani_f.it, "\n");
        fprintf(mani_f.laiks, "\n");

        genu_skaits+=5;
    }//genu while beigas




}//stat_mekle_1ciklu_map






int main()
{
    int genu_skaits;
    int ietekmejoso_genu_skaits;

    int ietekmejosie_geni[100][7];

    bool genu_bula_funkc[100][128];

    mt19937_64 my_rand;
    int *stavoklu_parejas;
    int stavoklu_skaits;




    /// prieksh normalas darbinasanas
    cin>>genu_skaits>>ietekmejoso_genu_skaits;

    while(genu_skaits<ietekmejoso_genu_skaits || ietekmejoso_genu_skaits>7){
        cout<<"genu skaitam jabut >= ietekmejoso genu skaitu!"<<endl;
        cout<<"ietekmejoso genu skaitam jabut <=7"<<endl;
        cin>>genu_skaits>>ietekmejoso_genu_skaits;
    }


    stavoklu_skaits = 1<<genu_skaits;



    uztaisa_genu_parejas(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, my_rand());



    stavoklu_parejas = uztaisa_stav_grafu(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, stavoklu_skaits);






    /// sadala prieksh 1 cikla meklesanas stat - sakums
    ///######################################################

    /*

    // prieksh mekle 1ciklu
    //stat_mekle_1ciklu(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);


    //prieksh mekle 1ciklu 2var
    //stat_mekle_1ciklu_2var(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);


    //prieksh mekle 1ciklu map

    //stat_mekle_1ciklu_map(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits);

    */


    ///######################################################
    /// sadala prieksh 1 cikla meklesanas stat - beigas.








    return 0;
}//main
