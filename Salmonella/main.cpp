#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

#define GENU_SKAITS 34

struct geni{
    //konstantes
    bool Glucose;
    bool Iron;
    bool Magnesium;
    bool Calcium;
    bool Osmolarity;
    bool Stacionary_phase;

    //mainigie
    bool Mle;
    bool HilE;
    bool HilD;
    bool HilC;
    bool RtsA;
    bool HilA;

    bool IHF;
    bool SirA_BarA;
    bool CsrBC;
    bool CsrA;
    bool Fur;
    bool H_NS;

    bool PHOP;
    bool SlyA;
    bool ssrAB;
    bool Fis;
    bool EnvZ;
    bool OmpR;

    bool YfhA;
    bool MviA;
    bool RcsB;
    bool PmrA;
    bool SciS;
    bool VrgS;

    bool SciG;
    bool SPI_1;
    bool SPI_2;
    bool TSSS;



    void izdruka(){

        cout<<Glucose;
        cout<<Iron;
        cout<<Magnesium;
        cout<<Calcium;
        cout<<Osmolarity;
        cout<<Stacionary_phase;

        cout<<Mle;
        cout<<HilE;
        cout<<HilD;
        cout<<HilC;
        cout<<RtsA;
        cout<<HilA;

        cout<<IHF;
        cout<<SirA_BarA;
        cout<<CsrBC;
        cout<<CsrA;
        cout<<Fur;
        cout<<H_NS;

        cout<<PHOP;
        cout<<SlyA;
        cout<<ssrAB;
        cout<<Fis;
        cout<<EnvZ;
        cout<<OmpR;

        cout<<YfhA;
        cout<<MviA;
        cout<<RcsB;
        cout<<PmrA;
        cout<<SciS;
        cout<<VrgS;

        cout<<SciG;
        cout<<SPI_1;
        cout<<SPI_2;
        cout<<TSSS;

        cout<<endl<<endl;


    }//izdruka_raksta_seciba


    void izdruka2(fstream &fout){

        fout<<Glucose<<" ";
        fout<<Iron<<" ";
        fout<<Magnesium<<" ";
        fout<<Calcium<<" ";
        fout<<Osmolarity<<" ";
        fout<<Stacionary_phase<<" ";

        fout<<Mle<<" ";
        fout<<HilE<<" ";
        fout<<HilD<<" ";
        fout<<HilC<<" ";
        fout<<RtsA<<" ";
        fout<<HilA<<" ";

        fout<<IHF<<" ";
        fout<<SirA_BarA<<" ";
        fout<<CsrBC<<" ";
        fout<<CsrA<<" ";
        fout<<Fur<<" ";
        fout<<H_NS<<" ";

        fout<<PHOP<<" ";
        fout<<SlyA<<" ";
        fout<<ssrAB<<" ";
        fout<<Fis<<" ";
        fout<<EnvZ<<" ";
        fout<<OmpR<<" ";

        fout<<YfhA<<" ";
        fout<<MviA<<" ";
        fout<<RcsB<<" ";
        fout<<PmrA<<" ";
        fout<<SciS<<" ";
        fout<<VrgS<<" ";

        fout<<SciG<<" ";
        fout<<SPI_1<<" ";
        fout<<SPI_2<<" ";
        fout<<TSSS;

        fout<<endl;


    }//izdruka_raksta_seciba

};//geni


void ielasa_g_v(bool genu_v[], const int no, const int lidz, const int skaitlis){

    int cur=lidz;
    int temp=skaitlis;

    while(cur>=no){

        genu_v[cur] = (bool)(temp & 1);
        temp = temp>>1;
        --cur;
    }


    /*
    cout<<bitset<12>(skaitlis)<<endl;
    for(int a=22; a<34; ++a)cout<<genu_v[a];
    cout<<endl<<endl;
    */

}//ielasa_g_v



int atrod_jaunu_stavokli(bool *&genu_v_prev, bool *&genu_v_next, geni *&visi_geni_prev, geni *&visi_geni_next, const int prev_stavoklis){


    int rez=0;



    //ielasa_g_v(genu_v_prev, 6, GENU_SKAITS-1, prev_stavoklis);


    //bula fciju bloka sakums
    {

        visi_geni_next->Mle = !visi_geni_prev->Glucose;

        visi_geni_next->HilE = !visi_geni_prev->Mle;

        visi_geni_next->IHF = visi_geni_prev->Osmolarity && visi_geni_prev->Stacionary_phase;

        visi_geni_next->SirA_BarA = visi_geni_prev->Osmolarity && !visi_geni_prev->Glucose;

        visi_geni_next->CsrBC = visi_geni_prev->SirA_BarA;

        visi_geni_next->CsrA = !visi_geni_prev->CsrBC;

        visi_geni_next->Fur = visi_geni_prev->Iron;

        visi_geni_next->PHOP = !visi_geni_prev->Magnesium;

        visi_geni_next->SlyA = !visi_geni_prev->Osmolarity && !visi_geni_prev->Calcium;

        visi_geni_next->Fis = visi_geni_prev->Stacionary_phase;

        visi_geni_next->EnvZ = visi_geni_prev->Osmolarity;

        visi_geni_next->OmpR = !visi_geni_prev->EnvZ;

        visi_geni_next->YfhA = !visi_geni_prev->EnvZ;

        visi_geni_next->PmrA = visi_geni_prev->PHOP;

        visi_geni_next->VrgS = visi_geni_prev->PmrA;





        //visi_geni_next->HilD = visi_geni_prev->SirA_BarA || visi_geni_prev->HilD || visi_geni_prev->HilC || visi_geni_prev->RtsA
           //|| (visi_geni_prev->Fur && !visi_geni_prev->CsrA && !visi_geni_prev->HilE);
        visi_geni_next->HilD = (visi_geni_prev->SirA_BarA || visi_geni_prev->HilD || visi_geni_prev->HilC || visi_geni_prev->RtsA
           || visi_geni_prev->Fur) && !visi_geni_prev->CsrA && !visi_geni_prev->HilE;






        visi_geni_next->HilC = visi_geni_prev->HilD || visi_geni_prev->HilC || visi_geni_prev->RtsA;



        visi_geni_next->RtsA = visi_geni_next->HilC;//tas pats, kas HilC



        visi_geni_next->HilA = visi_geni_prev->HilD && (visi_geni_prev->HilC || visi_geni_prev->RtsA) && !visi_geni_prev->PHOP
            && (!visi_geni_prev->H_NS || (visi_geni_prev->H_NS && visi_geni_prev->IHF) || visi_geni_prev->SirA_BarA);




        visi_geni_next->H_NS = !visi_geni_prev->PHOP || !visi_geni_prev->Fur || !visi_geni_prev->SlyA || !visi_geni_prev->HilD || !visi_geni_prev->IHF;


        //visi_geni_next->ssrAB = visi_geni_prev->OmpR && ((!visi_geni_prev->H_NS && (visi_geni_prev->HilD || visi_geni_prev->SlyA || visi_geni_prev->PHOP))
            //|| (visi_geni_prev->H_NS && (visi_geni_prev->HilD || (visi_geni_prev->PHOP && visi_geni_prev->SlyA))) );
        visi_geni_next->ssrAB = visi_geni_prev->OmpR && ((!visi_geni_prev->H_NS && (visi_geni_prev->HilD || visi_geni_prev->SlyA || visi_geni_prev->PHOP))
            || (visi_geni_prev->H_NS && (visi_geni_prev->HilD && (visi_geni_prev->PHOP && visi_geni_prev->SlyA))) );
        //der gan and or gan and and



        visi_geni_next->MviA = visi_geni_prev->H_NS && !visi_geni_prev->PHOP;


        visi_geni_next->RcsB = visi_geni_prev->YfhA && !visi_geni_prev->MviA;



        //visi_geni_next->SciS = visi_geni_prev->RcsB && (visi_geni_prev->PmrA || (visi_geni_prev->PmrA && visi_geni_prev->YfhA && !visi_geni_prev->ssrAB));
        visi_geni_next->SciS = ((visi_geni_prev->RcsB && visi_geni_prev->PmrA) || (visi_geni_prev->RcsB && visi_geni_prev->PmrA && visi_geni_prev->YfhA)) && !visi_geni_prev->ssrAB;


        visi_geni_next->SciG = visi_geni_prev->RcsB && visi_geni_prev->PmrA;



        visi_geni_next->SPI_1 = visi_geni_prev->HilA;



        visi_geni_next->SPI_2 = visi_geni_prev->ssrAB && (visi_geni_prev->SlyA || visi_geni_prev->Fis);



        visi_geni_next->TSSS = visi_geni_prev->SciS && visi_geni_prev->SciG && visi_geni_prev->VrgS;


    }
    //bula fciju bloka beigas





    for(int i=6; i<GENU_SKAITS; ++i){
        rez = rez<<1;
        rez += (int)genu_v_next[i];
    }




    swap(genu_v_prev, genu_v_next);
    swap(visi_geni_prev, visi_geni_next);





    return rez;


}//atrod_jaunu_stavokli



///////////////////////////////////////////////////////////////////////////////////////////





struct faili_stav_grafa_analizei_03{

    FILE *ciklu_skaits;
    FILE *garakais_cikls;
    FILE *vid_cikla_garums;
    FILE *vid_baseins;
    FILE *lielakais_baseins;

    FILE *lapu_sk;
    FILE *lielakais_att_lidz_ciklam;
    FILE *vid_att_lidz_ciklam;

    FILE *visu_ciklu_info;//sis jauns, noradis failu, kur info par visiem cikliem (info par katru ta garums, uc)
    FILE *ciklu_starta_virs;

};

struct baseins{
    int baseina_lielums;

    int cikla_garums;
    int cikla_starta_v;
    int lielakais_att_lidz_ciklam;

    baseins(){
        baseina_lielums=0;
        cikla_garums=0;
        lielakais_att_lidz_ciklam=0;
    }

};







void stav_grafa_analize_03_3var_salm(bool genu_v_prev[], bool genu_v_next[], geni *visi_geni_prev, geni *visi_geni_next, faili_stav_grafa_analizei_03 &izvada_f){

    /**
        tapat kaa stav_grafa_analize_03_3var, tacu modificeta salmonellai.
    **/

    int stavoklu_sk = 1<<28;//ir 28 mainigie - tapec stav skaits ir 2^28

    int8_t *pieder_baseinam;

    baseins *baseini;
    int8_t maks_bas_sk = (1<<7)-5;//pietiktu ar -1, tacu vnk drosibai, iekavas obligati!!!


    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;


    vector <int> cels;
    static int maks_v_cela = 1<<4;
    int cur_ind;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;




    int *att_lidz_cikliem;
    int lielakais_att_lidz_ciklam;
    ll kop_att_lidz_ciklam;
    int cur_att_lidz_ciklam;

    double vid_att_lidz_ciklam;
    int lapu_sk;


    cels.resize(maks_v_cela);

    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];


    att_lidz_cikliem = new int[stavoklu_sk];
    memset(att_lidz_cikliem, 0, stavoklu_sk*sizeof(int));



    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    lielakais_att_lidz_ciklam=0;
    kop_att_lidz_ciklam=0;
    lapu_sk=0;



    for(int i=0; i<stavoklu_sk; i++){

        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            ielasa_g_v(genu_v_prev, 6, GENU_SKAITS-1, i);//ielasa genu vertibas no i genu v prev masiva

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                //cels[v_skaits_cela] = cur_v;
                cels.at(v_skaits_cela) = cur_v;

                v_skaits_cela++;
                //cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);//avar
                //cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var

                cur_v = atrod_jaunu_stavokli(genu_v_prev, genu_v_next, visi_geni_prev, visi_geni_next, cur_v);


                //cela resizosanas ifa sakums
                if(v_skaits_cela==maks_v_cela-1){

                    maks_v_cela = maks_v_cela<<1;
                    cels.resize(maks_v_cela);
                }
                //cela resizosanas ifa beigas
            }
            while(pieder_baseinam[cur_v]==-1);


            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls


                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                cur_ind = v_skaits_cela-1;

                do{
                    cur_cikla_garums++;
                    cur_v = cels[cur_ind];
                    cur_ind--;

                    ///cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    ///cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;

                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, sakums
                cur_att_lidz_ciklam = v_skaits_cela - cur_cikla_garums;
                cur_v = cels[0]; //jeb cur_v = i;
                cur_ind = 1;
                while(cur_v != cur_cikla_sakums){
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = cels[cur_ind];
                    cur_ind++;
                    ///cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    ///cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    cur_att_lidz_ciklam--;
                }
                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, beigas


                if(maks_bas_sk==ciklu_skaits){

                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");

                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka sakums
                    {
                        fprintf(izvada_f.ciklu_skaits, "!!");
                        fprintf(izvada_f.garakais_cikls, "!!");
                        fprintf(izvada_f.vid_cikla_garums, "!!");

                        fprintf(izvada_f.lielakais_baseins, "!!");
                        fprintf(izvada_f.vid_baseins, "!!");

                        fprintf(izvada_f.lapu_sk, "!!");
                        fprintf(izvada_f.lielakais_att_lidz_ciklam, "!!");
                        fprintf(izvada_f.vid_att_lidz_ciklam, "!!");
                    }
                    //izdruka failos "!!", kas pavesta, ka neprecizs rez bloka beigas

                    break;
                }//ifa, kas parbauda vai nav parsniegts ciklu skaits beigas


            }//atrasts jauns cikls ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                if(att_lidz_cikliem[cur_v]<0) att_lidz_cikliem[cur_v] *= -1;//atzime, ka noteikti nav lapa

                cur_att_lidz_ciklam = v_skaits_cela + att_lidz_cikliem[cur_v];

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_ind=0; //cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    cur_v = cels[cur_ind];//tas pats, kas [k], tacu lai visur vienadi liku [cur_ind]
                    cur_ind++;
                    pieder_baseinam[cur_v] = cikla_nr;
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    ///cur_v = atrod_jaunu_stavokli(ietekmejosie_geni, genu_bula_funkc, genu_skaits, ietekmejoso_genu_skaits, cur_v);
                    ///cur_v = atrod_jaunu_stavokli_b2(ietekmejosie_geni, genu_bula_funkc_b2, genu_skaits, ietekmejoso_genu_skaits, cur_v);//b2var
                    cur_att_lidz_ciklam--;
                }
            }//nav jauns cikls else beigas

            att_lidz_cikliem[i] *= -1; //virsotni, no kuras saaka staigat atzimee kaa potencialo lapu
        }
    }



    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;



    for(int i=0; i<stavoklu_sk; i++){
        if(att_lidz_cikliem[i]<0){
            //ir lapa
            att_lidz_cikliem[i]*=-1;
            lapu_sk++;
            kop_att_lidz_ciklam += (ll)att_lidz_cikliem[i];

            if(att_lidz_cikliem[i] > lielakais_att_lidz_ciklam) lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

            if( att_lidz_cikliem[i] > baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam ) baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

        }
    }

    if(lapu_sk!=0) vid_att_lidz_ciklam = (double)kop_att_lidz_ciklam/(double)lapu_sk;
    else vid_att_lidz_ciklam = 0;



    //izdruka failos rezultatus bloka sakums
    {
        fprintf(izvada_f.ciklu_skaits, "%d ", ciklu_skaits);
        fprintf(izvada_f.garakais_cikls, "%d ", garakais_cikls);
        fprintf(izvada_f.vid_cikla_garums, "%.2f ", vid_cikla_garums);

        fprintf(izvada_f.vid_baseins, "%.2f ", vid_baseins);
        fprintf(izvada_f.lielakais_baseins, "%d ", lielakais_baseins);

        fprintf(izvada_f.lapu_sk, "%d ", lapu_sk);
        fprintf(izvada_f.lielakais_att_lidz_ciklam, "%d ", lielakais_att_lidz_ciklam);
        fprintf(izvada_f.vid_att_lidz_ciklam, "%.2f ", vid_att_lidz_ciklam);

        //cikls  baseina_lielums  cikla_garums   liel_att_lidz_c  cikla_starta_v
        for(int8_t i=0; i<ciklu_skaits; i++){
            fprintf(izvada_f.visu_ciklu_info, "%6d  %15d  %12d %16d %15d\n", (int)i+1, baseini[i].baseina_lielums, baseini[i].cikla_garums, baseini[i].lielakais_att_lidz_ciklam, baseini[i].cikla_starta_v);
            fprintf(izvada_f.ciklu_starta_virs, "%d\n",  baseini[i].cikla_starta_v);
        }
        fprintf(izvada_f.visu_ciklu_info, "\n###################################################################\n");
        fprintf(izvada_f.ciklu_starta_virs, "#############\n");

    }
    //izdruka failos rezultatus bloka beigas




    delete[] baseini;
    delete[] pieder_baseinam;
    delete[] att_lidz_cikliem;


}//stav_grafa_analize_03_3var











int main()
{
    bool *genu_v_prev = new bool[GENU_SKAITS];// genu skaits 34
    bool *genu_v_next = new bool[GENU_SKAITS];// genu skaits 34
    geni *visi_geni_prev = (geni*)(genu_v_prev);
    geni *visi_geni_next = (geni*)(genu_v_next);

    const int konst_stav_sk = 1<<6;
    const int mainigo_stav_sk = 1<<28;

    faili_stav_grafa_analizei_03 izvada_f;

    time_t a,b;



    memset(genu_v_prev, false, GENU_SKAITS);
    memset(genu_v_next, false, GENU_SKAITS);



    izvada_f.ciklu_skaits = fopen("out_faili/ciklu_skaits.txt", "w");
    izvada_f.garakais_cikls = fopen("out_faili/garakais_cikls.txt", "w");
    izvada_f.vid_cikla_garums = fopen("out_faili/vid_cikla_garums.txt", "w");
    izvada_f.lapu_sk = fopen("out_faili/lapu_sk.txt", "w");
    izvada_f.lielakais_baseins = fopen("out_faili/lielakais_baseins.txt", "w");
    izvada_f.vid_baseins = fopen("out_faili/vid_baseins.txt", "w");
    izvada_f.lielakais_att_lidz_ciklam = fopen("out_faili/lielakais_att_lidz_ciklam.txt", "w");
    izvada_f.vid_att_lidz_ciklam = fopen("out_faili/vid_att_lidz_ciklam.txt", "w");
    izvada_f.visu_ciklu_info = fopen("out_faili/visu_ciklu_info.txt", "w");
    izvada_f.ciklu_starta_virs = fopen("out_faili/ciklu_starta_virs.txt", "w");



    if(izvada_f.ciklu_skaits == NULL){ //pietiek parbaudit tikai 1 failu
        printf("neizdevas atvert failus!\n");
        printf("beidz darbu\n");
        return 0;
    }




    fprintf(izvada_f.ciklu_skaits, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.garakais_cikls, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.vid_cikla_garums, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.lapu_sk, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.lielakais_baseins, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.vid_baseins, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.lielakais_att_lidz_ciklam, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.vid_att_lidz_ciklam, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.visu_ciklu_info, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");
    fprintf(izvada_f.ciklu_starta_virs, "\n\tSalmonellas pilna stav grafa analize.\n Kopejais genu skaits: 34.\n Konstanto skaits:6.\n\n");




    printf("\tSakas statistikas vaksana salmonellai.\n");
    time(&a);
    printf("sakuma laiks: %s\n", ctime(&a));


    for(int i=0; i<konst_stav_sk; ++i){
        printf("analize %d/%d : ", i+1, konst_stav_sk);
        fflush(stdout);
        ielasa_g_v(genu_v_prev, 0, 5, i);
        for(int a=0; a<6; ++a)genu_v_next[a]=genu_v_prev[a];

        fprintf(izvada_f.visu_ciklu_info, "\nKonst.genu vertibu skaitlis: %d\n",i);
        fprintf(izvada_f.visu_ciklu_info, "\ncikls  baseina_lielums  cikla_garums   liel_att_lidz_c  cikla_starta_v\n\n");

        stav_grafa_analize_03_3var_salm(genu_v_prev, genu_v_next, visi_geni_prev, visi_geni_next, izvada_f);

        printf("pab\n");
    }//liela for beigas

    time(&b);

    printf("\nbeigu laiks: %s", ctime(&b));
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(b-a));


    return 0;






































//    fstream fout("out1.txt", ios::out);
//    int stav=0;
//
//    memset(genu_v_prev,false, 34);
//    memset(genu_v_next,false, 34);
//
//
//
////    //test1 -- ok
////    genu_v_prev[0]=0;
////    genu_v_prev[1]=1;
////    genu_v_prev[2]=1;
////    genu_v_prev[3]=1;
////    genu_v_prev[4]=1;
////    genu_v_prev[5]=1;
////
////    stav=0;
////
////
////    ielasa_g_v(genu_v_prev, 6, GENU_SKAITS-1, stav);
////    for(int a=0; a<34; ++a)genu_v_next[a]=genu_v_prev[a];
////
////
////
////
////
////
////    for(int i=0; i<8; ++i){
////
////        visi_geni_prev->izdruka2(fout);
////        //cout<<visi_geni_prev->HilD<<endl;
////        //cout<<visi_geni_prev->ssrAB<<endl;
////        stav = atrod_jaunu_stavokli(genu_v_prev, genu_v_next, visi_geni_prev, visi_geni_next, stav);
////    }
//
//
//
////    test2 -- ok
////    genu_v_prev[0]=1;
////    genu_v_prev[1]=1;
////    genu_v_prev[2]=0;
////    genu_v_prev[3]=0;
////    genu_v_prev[4]=0;
////    genu_v_prev[5]=1;
////
////    visi_geni_prev->HilD=1;
////    visi_geni_prev->HilC=1;
////    visi_geni_prev->RtsA=1;
////    visi_geni_prev->HilA=1;
////    visi_geni_prev->SPI_1=1;
////
////    for(int a=0; a<34; ++a)genu_v_next[a]=genu_v_prev[a];
////
////    stav = 62914564;
////
////
////    for(int i=0; i<6; ++i){
////
////        visi_geni_prev->izdruka2(fout);
////        //cout<<visi_geni_prev->HilD<<endl;
////        //cout<<visi_geni_prev->ssrAB<<endl;
////        stav = atrod_jaunu_stavokli(genu_v_prev, genu_v_next, visi_geni_prev, visi_geni_next, stav);
////    }















    return 0;
}//main
