
		genu sk: 40 ietekm sk: 2

ciklu skaits := 3
garakais cikls := 21
analize ar sausanu laiks: 0s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            21             45        911/1000
    2            12             13         83/1000
    3             4              9          6/1000

###############################################
		genu sk: 40 ietekm sk: 7

ciklu skaits := 5
garakais cikls := 273954
analize ar sausanu laiks: 61s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1        273954         747569         29/36
    2         14657          52108          1/36
    3         53233         160999          3/36
    4         60378           5745          1/36
    5          7091          57257          2/36

###############################################

		genu sk: 40 ietekm sk: 7

ciklu skaits := 5
garakais cikls := 273954
analize ar sausanu laiks: 63s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1        273954         747569         26/33
    2         14657          52108          1/33
    3         53233         160999          3/33
    4         60378           5745          1/33
    5          7091          57257          2/33

###############################################

		genu sk: 40 ietekm sk: 7

ciklu skaits := 5
garakais cikls := 273954
analize ar sausanu laiks: 242s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1        273954         822489         90/111
    2         14657          98029          4/111
    3         53233         189611         13/111
    4         60378           5745          2/111
    5          7091          57257          2/111

###############################################
		genu sk: 40 ietekm sk: 7

ciklu skaits := 5
garakais cikls := 273954
analize ar sausanu laiks: 601s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1        273954         844422        230/271
    2         14657         165370         10/271
    3         53233         189611         24/271
    4         60378           7383          4/271
    5          7091          57257          3/271

###############################################
		genu sk: 45 ietekm sk: 7

ciklu skaits := 3
garakais cikls := 2226523
analize ar sausanu laiks: 602s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1       2195466        1617807         33/54
    2       2226523        1505003         19/54
    3        293177         571859          2/54

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 1
garakais cikls := 721066
analize ar sausanu laiks: 700s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1        721066       26531577         13/13

###############################################


// ar b2 veida funkcijam - ar noteicosajiem

		genu sk: 50 ietekm sk: 7

ciklu skaits := 2
garakais cikls := 18
analize ar sausanu laiks: 0s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            18             42        991/1000
    2             3             13          9/1000

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 2
garakais cikls := 18
analize ar sausanu laiks: 2s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            18             45       9844/10000
    2             3             23        156/10000

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 2
garakais cikls := 18
analize ar sausanu laiks: 18s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            18             45      98541/100000
    2             3             23       1459/100000

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 3
garakais cikls := 18
analize ar sausanu laiks: 601s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            18             46    3127219/3172781
    2             3             24      45560/3172781
    3             4              6          2/3172781

###############################################
		genu sk: 55 ietekm sk: 7

ciklu skaits := 9
garakais cikls := 4
analize ar sausanu laiks: 601s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             4             12     588702/4521875
    2             2             11     271974/4521875
    3             1             15     914688/4521875
    4             4             14    1120939/4521875
    5             4             12     333147/4521875
    6             4             18     972868/4521875
    7             1             16     184088/4521875
    8             1             10      51329/4521875
    9             2             13      84140/4521875


###############################################
		genu sk: 60 ietekm sk: 7

ciklu skaits := 38
garakais cikls := 4
analize ar sausanu laiks: 61s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             4             32      52314/459239
    2             2             36     258767/459239
    3             2             23      28436/459239
    4             2             30      47504/459239
    5             4             26      15964/459239
    6             4             10       1667/459239
    7             2             22      26045/459239
    8             4             17       1286/459239
    9             4             10       1361/459239
   10             4             13       5746/459239
   11             2             12        470/459239
   12             4             20       2557/459239
   13             4             13        554/459239
   14             4             11       1228/459239
   15             2             19       4580/459239
   16             2             20       2826/459239
   17             2             10        111/459239
   18             4             11        719/459239
   19             4             16       2008/459239
   20             4             10         90/459239
   21             2             13       1106/459239
   22             4              9       1412/459239
   23             2              8        242/459239
   24             2             13        656/459239
   25             4             14        162/459239
   26             4             10         40/459239
   27             4             14        106/459239
   28             4             13        462/459239
   29             2              8        282/459239
   30             2             13         50/459239
   31             4             14        125/459239
   32             4             10        119/459239
   33             4             10        151/459239
   34             4              7          5/459239
   35             4              7         21/459239
   36             2              8         26/459239
   37             4             10         19/459239
   38             2              8         22/459239

###############################################
