#include "catch.hpp"
#include <bits/stdc++.h>
///#include "catch.hpp"

#include "E:\LU_DF\LUMII\Projekti\Lielais_projekts\Auglu_musina\visas_musas_fcijas_klases.hpp"
///#include "visas_musas_fcijas_klases.hpp"


using namespace std;

typedef long long ll;





/******
sakas genereta dala

*****/

TEST_CASE("piemers ar 15x4 cikli", "[musha] [musha4]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;

    int musas_sunu_skaits=4;
    int laika_limits;


    int kop_musas_genu_skaits;

    ll musas_stav_skaits;
    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;

    ll cur_stavoklis;









    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);




    kop_musas_genu_skaits = (musas_genu_skaits-1)*musas_sunu_skaits;//jo SLP jau aizpildits
    musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits);

    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16959902639436030) == 16959902639436030);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 338670252146765) == 338670252146765);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16958935530696958) == 16958935530696958);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17170041763229950) == 17170041763229950);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1118151991328845) == 1118151991328845);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17173340298113278) == 17173340298113278);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1118139106426957) == 1118139106426957);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55161347519774797) == 55161347519774797);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55161334634872909) == 55161334634872909);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16962234065580286) == 16962234065580286);

}

/// tik talu OK!!






TEST_CASE("piemers ar 15x4 100 stav", "[musha] [musha4]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;

    int musas_sunu_skaits=4;
    int laika_limits;


    int kop_musas_genu_skaits;

    ll musas_stav_skaits;
    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;

    ll cur_stavoklis;









    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);




    kop_musas_genu_skaits = (musas_genu_skaits-1)*musas_sunu_skaits;//jo SLP jau aizpildits
    musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits);

    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56918763867757321) == 28705642161898780);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18119791137912325) == 9125827343978769);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51410079535364304) == 32035481990870136);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68482112341758565) == 32018361194152314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1394071680657802) == 9140589354595532);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29290691138509517) == 14744277590902142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18180374386202519) == 9123696217948482);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1643021735918430) == 9123341166097822);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37663415644127515) == 18121964574965788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24933509513921229) == 1205988168040830);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19835357356071835) == 10124928130843980);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40585159094469417) == 27111582857101596);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10130478489774655) == 14065261607163166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39342641922349673) == 28676325319836956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37755472673499160) == 18561066242257948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62001118827800343) == 28218801126117650);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36153763582803625) == 27153915059148148);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30337300023207160) == 14955314512491124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53841381454755781) == 71424898878509426);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18024865559454950) == 9676533608706674);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17309312105385434) == 5728282196878964);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23153549582250278) == 11542144472019097);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65865789001830532) == 32968587925192826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11924046478614714) == 5955398120124668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17761881620188914) == 8384330890454896);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14346480382556861) == 16281601092556158);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51787803208934969) == 23037894337597716);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70012608152276277) == 23736840285815826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55660818511777028) == 27155276490901009);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5838383598678813) == 9104868088156446);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33271044377504359) == 16285834229355801);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18610365228267123) == 9545442951893360);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56211607451760761) == 27576390586930204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42238172066381207) == 27515490622313586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68744873340584350) == 69899834769023230);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31692227519899947) == 13591974923539484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23314426978480423) == 9298174920511513);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38514271122334314) == 19254955999605244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18545046230050092) == 125101868790294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3294361350775605) == 1680705008931098);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36530078342559685) == 27131498622390593);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50366020457669984) == 32037371455124032);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6598794181007489) == 11567694021461064);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65621196769392336) == 33198180117787504);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2236375197615994) == 9562402372494100);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10998345562962105) == 14051159056487500);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71003348809148342) == 34973951463306098);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44880690860498243) == 30711425779844112);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21612821718238840) == 1795328550634260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26138288868245841) == 1205965697679384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34793366425623413) == 15138837928281370);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21546292048064036) == 1232103799011090);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20630500381334672) == 10805593554421360);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66856327002699863) == 32757437616198682);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45494915068845977) == 31648416333661556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54619103659973640) == 63932095823676948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51643097501229971) == 70324831364354424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52284422085104768) == 35396265380250224);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5052022752258365) == 2350512267940894);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35217692507246443) == 5710481173775636);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6428210095288899) == 9509899759554840);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54951190104123122) == 64168328886629184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30659013980299784) == 5753271509365020);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43185827209894881) == 28235490295448952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6251365065313347) == 9310946405516304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19746048452836383) == 9556916646548502);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45126354499489747) == 22635781245732208);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16069291921808745) == 14717887770461204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16308924695074079) == 16986291334086678);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31405791206362790) == 15174983825172338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71421960783075276) == 32748752185688950);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15263023421389788) == 16281919299597646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61721699182541280) == 28430930638189688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20552880472034410) == 1232413650626164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60502824031947147) == 20787178410247244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26528543137937564) == 10438496587265150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67648729139710117) == 22601146570671226);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62205667924870360) == 28246281010578036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35512579309281214) == 17400603817191286);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1597169647959829) == 9130651861488914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26195620656478005) == 12464892764033298);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53274552849045241) == 33144740123705716);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37571361548623569) == 63746871600252272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30596445642790736) == 14772656127378192);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2100246302860481) == 555355536822384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65774702303806229) == 32775168856621338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8115090519463500) == 10213165100794646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70905406036220587) == 71442424422761852);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49757647059201881) == 22590022268294420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35253589606937934) == 14735454595461654);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53235736975880287) == 34991751022546974);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42584444958258871) == 65795880912851314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38228298482343994) == 18551596394492060);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32961404172294673) == 15843515365721368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59919618166994394) == 29362190285894260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62964311725981506) == 30919166984468240);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35554466277975878) == 8383534039216914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70136003811516194) == 32739751540631312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64295654138043280) == 31649187797763400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10002088967807079) == 5384202909388826);

}



TEST_CASE("piemers ar 15x4 1000 stav", "[musha] [musha4]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;

    int musas_sunu_skaits=4;
    int laika_limits;


    int kop_musas_genu_skaits;

    ll musas_stav_skaits;
    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;

    ll cur_stavoklis;









    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);




    kop_musas_genu_skaits = (musas_genu_skaits-1)*musas_sunu_skaits;//jo SLP jau aizpildits
    musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits);

    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56918763867757321) == 28705642161898780);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18119791137912325) == 9125827343978769);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51410079535364304) == 32035481990870136);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68482112341758565) == 32018361194152314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1394071680657802) == 9140589354595532);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29290691138509517) == 14744277590902142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18180374386202519) == 9123696217948482);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1643021735918430) == 9123341166097822);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37663415644127515) == 18121964574965788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24933509513921229) == 1205988168040830);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19835357356071835) == 10124928130843980);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40585159094469417) == 27111582857101596);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10130478489774655) == 14065261607163166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39342641922349673) == 28676325319836956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37755472673499160) == 18561066242257948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62001118827800343) == 28218801126117650);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36153763582803625) == 27153915059148148);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30337300023207160) == 14955314512491124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53841381454755781) == 71424898878509426);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18024865559454950) == 9676533608706674);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17309312105385434) == 5728282196878964);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23153549582250278) == 11542144472019097);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65865789001830532) == 32968587925192826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11924046478614714) == 5955398120124668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17761881620188914) == 8384330890454896);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14346480382556861) == 16281601092556158);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51787803208934969) == 23037894337597716);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70012608152276277) == 23736840285815826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55660818511777028) == 27155276490901009);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5838383598678813) == 9104868088156446);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33271044377504359) == 16285834229355801);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18610365228267123) == 9545442951893360);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56211607451760761) == 27576390586930204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42238172066381207) == 27515490622313586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68744873340584350) == 69899834769023230);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31692227519899947) == 13591974923539484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23314426978480423) == 9298174920511513);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38514271122334314) == 19254955999605244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18545046230050092) == 125101868790294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3294361350775605) == 1680705008931098);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36530078342559685) == 27131498622390593);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50366020457669984) == 32037371455124032);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6598794181007489) == 11567694021461064);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65621196769392336) == 33198180117787504);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2236375197615994) == 9562402372494100);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10998345562962105) == 14051159056487500);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71003348809148342) == 34973951463306098);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44880690860498243) == 30711425779844112);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21612821718238840) == 1795328550634260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26138288868245841) == 1205965697679384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34793366425623413) == 15138837928281370);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21546292048064036) == 1232103799011090);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20630500381334672) == 10805593554421360);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66856327002699863) == 32757437616198682);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45494915068845977) == 31648416333661556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54619103659973640) == 63932095823676948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51643097501229971) == 70324831364354424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52284422085104768) == 35396265380250224);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5052022752258365) == 2350512267940894);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35217692507246443) == 5710481173775636);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6428210095288899) == 9509899759554840);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54951190104123122) == 64168328886629184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30659013980299784) == 5753271509365020);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43185827209894881) == 28235490295448952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6251365065313347) == 9310946405516304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19746048452836383) == 9556916646548502);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45126354499489747) == 22635781245732208);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16069291921808745) == 14717887770461204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16308924695074079) == 16986291334086678);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31405791206362790) == 15174983825172338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71421960783075276) == 32748752185688950);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15263023421389788) == 16281919299597646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61721699182541280) == 28430930638189688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20552880472034410) == 1232413650626164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60502824031947147) == 20787178410247244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26528543137937564) == 10438496587265150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67648729139710117) == 22601146570671226);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62205667924870360) == 28246281010578036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35512579309281214) == 17400603817191286);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1597169647959829) == 9130651861488914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26195620656478005) == 12464892764033298);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53274552849045241) == 33144740123705716);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37571361548623569) == 63746871600252272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30596445642790736) == 14772656127378192);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2100246302860481) == 555355536822384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65774702303806229) == 32775168856621338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8115090519463500) == 10213165100794646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70905406036220587) == 71442424422761852);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49757647059201881) == 22590022268294420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35253589606937934) == 14735454595461654);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53235736975880287) == 34991751022546974);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42584444958258871) == 65795880912851314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38228298482343994) == 18551596394492060);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32961404172294673) == 15843515365721368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59919618166994394) == 29362190285894260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62964311725981506) == 30919166984468240);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35554466277975878) == 8383534039216914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70136003811516194) == 32739751540631312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64295654138043280) == 31649187797763400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10002088967807079) == 5384202909388826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31580150152739659) == 15860417880724764);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24878569809209859) == 3470020669445400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8603234343718886) == 3879934965524338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39660129006199771) == 19265469544173948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54908052940235163) == 18337387254354044);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2343932905444498) == 10251189892555376);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7350292448565856) == 10634509231719704);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42458484598788672) == 27525041283334424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50279159000278775) == 25280323027505522);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57280653816272106) == 28675886787863156);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61488185969607428) == 30922252388598042);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63317985362625938) == 31658943278293568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43801296858016879) == 28433437288305686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68980987244086676) == 33852731646149234);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71679332250269112) == 33163293764334196);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62724052394182758) == 28650600153752730);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46595123417347926) == 68223604529378066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43740810717684411) == 19652153358782844);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66024098334981053) == 33188490816886142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33782736940512737) == 14707753140260976);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5725128577920077) == 81175647163413);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65419626324398353) == 69367944994586648);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59850387308545715) == 29362202496270656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10656524294554070) == 4618003270672586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58390845198592965) == 19696202494711162);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48386197933479677) == 33197929401778550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10029793101939470) == 5052859599397277);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48636680710314955) == 69352689204921724);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60217828638976844) == 27111639138284829);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69219739569953226) == 69895916601451772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45556114557633827) == 68231326536630288);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25733248867345446) == 1425660317905426);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23382103445529839) == 11761274024890486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4580440394095974) == 9078054962275954);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20294326734384051) == 10811263783048568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18590296790897434) == 10099982149263772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6330887268841926) == 9527230898550010);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5325598798392281) == 11770580345165180);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7282323052433420) == 10214015831572510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9454171286794184) == 4619343964714820);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45421398644573168) == 22633728800751944);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22568395669620840) == 9096002015609932);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68816542634233991) == 31622647600316489);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70375066842236879) == 33144507639693686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14592800178390066) == 7046226798338576);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52527652914317618) == 32937827239437464);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48922605099667340) == 23753193169061750);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41029830151502182) == 27092191474463897);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51966139257622490) == 23727423504131956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15201328714912904) == 16044939262755396);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54007066129646272) == 33153650952372600);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54178684096613313) == 63749271355983224);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64686708818832337) == 22632998105421176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8455262241491706) == 3874170041509372);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47329726181671690) == 23768769052418460);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1920806191126429) == 9545346935816566);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20671306527511933) == 10265664625737846);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44260396644881399) == 28247895775707514);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26454904233274826) == 1223548682122868);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20339706471935439) == 1233408860886134);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51450521721909771) == 70299298874462492);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62843805474833417) == 19422390866952212);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8860123830428297) == 3898419957731708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64166051382021790) == 23074164273686342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43070188756065581) == 28218449995039766);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57517210873837439) == 28263803460751638);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41029348353699683) == 29345790331458832);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71514330486683292) == 35405173346349942);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69645488558078359) == 32039330283066490);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36625539813542128) == 18552422637470792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57393955192069068) == 28694742021506430);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52051734755384817) == 32739502424736880);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29094327899069863) == 14047985501307969);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8391689120884729) == 3456974759170420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60142218235575779) == 65394045162198128);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24674322374312454) == 11769810603909529);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58734623820853228) == 56385022829508470);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17116738312348017) == 14708448386551832);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67577417902265836) == 31600545082081910);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33139411465402093) == 13608126064267589);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61696011903352992) == 21911727067431032);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54239942084296560) == 27157873859966832);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26741661334058202) == 12878103160730228);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46615469798211328) == 31640379304851216);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42666232105011337) == 18516126594434172);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61001700200905150) == 66525777178244350);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 412782165960188) == 9140876102578758);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13497684873174193) == 14979788361139312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21493913214127400) == 1241141409285652);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24500823153428598) == 11544490448217242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52516118710776137) == 35405450234957948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71946909949504946) == 71219221472949872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65240459414030577) == 31846134595404872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55486979229085693) == 27145325056071030);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9318321367671997) == 5182497877168198);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20205774078079030) == 9887412554739866);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9286459085008118) == 13643663921886834);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64298679998447674) == 31650159673909404);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58291325687404112) == 28688293790778128);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28993719053284141) == 14039464802226462);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38149584084613145) == 63930858814329884);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21497701235984399) == 1249628465923102);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66384175350441965) == 60772789214122358);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58797006995079855) == 29363290794885502);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37460011261061452) == 27136311041176605);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43415958200684424) == 19661225021343604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60318909565032160) == 65796802028503920);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34573220434884541) == 15138761361297782);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23811729890403781) == 2331899347669106);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42843445535307356) == 28244740027626358);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59763251587106972) == 20347029877163086);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33784969232110404) == 14711057562304794);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19283256053433382) == 109443778912785);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58414675356216197) == 28677192246826362);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9253936295257745) == 4626845339062640);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26601260662807779) == 12878415415673968);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1414531224092384) == 9536895585171264);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8971500544054585) == 10425326673166356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11702560982402930) == 14752935320134416);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31456519414326701) == 14954250631483510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50036308184640230) == 34271508626781641);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6217897934015676) == 9512534442608246);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71409820805447645) == 35001747080910198);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44771598510280518) == 28641485551150866);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26850939523671141) == 1425960485408786);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17121029854733977) == 16971748513616244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11497556086088306) == 6307284582379928);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2667417250267779) == 2244110427455864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7193539202148830) == 1198017311356158);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49476621791059281) == 33180339296207888);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68876916846780639) == 33877713657532542);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56939275935430555) == 65268071649841524);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17056969250905014) == 16967798741575162);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26332529351757192) == 10204186929399412);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26259378737699214) == 12457333671142654);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67575978247099376) == 33858323642450296);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14539389673187925) == 14022721008440698);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39989320550634876) == 28470210861400598);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50448762433342811) == 61319563366564932);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50067648985385233) == 24857764791010328);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6229604725132827) == 11769616598533396);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25922727175192628) == 3453298129715226);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20713316734384690) == 10671687745877400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6823128125617997) == 10230163813929334);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68260663555397752) == 34271577948619804);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34158347188289708) == 7962394253230206);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37599711392313496) == 27136309908549708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11001175393615166) == 13832688654956054);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28027063768719826) == 5042034646069448);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7157107541850682) == 10221641665557916);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11957070102940733) == 6748626814960662);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42242124579173106) == 29776922448146888);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34249579377760829) == 15147728164488478);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46978491900851807) == 32072005788668182);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39385277220677070) == 19686006777655926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22863843040403368) == 11357713211033460);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67412208134640943) == 32995828101627926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71610101202434993) == 71442425339843960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56218230470265315) == 55162038066086984);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16289477886343930) == 14717589219358204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57217650003842231) == 28676988379857010);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3180358989970456) == 10450589608654356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27803081246327763) == 5033431692247368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48462108089204368) == 32774250668275568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21780868363877175) == 10257076687017242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23258924328424496) == 11558908331164176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8226804614483736) == 3476206965950228);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49982817287860233) == 33853433664770076);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23885978700112812) == 79817223713150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57782893469013979) == 64845449143779700);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56626087426441759) == 28693515271602462);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 519236659280846) == 9685149111203326);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54391942374533077) == 27154191623131506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28913544359626698) == 5621637601174388);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30417401444585483) == 14751808274956316);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14880716451760939) == 13590463361975580);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50657330313081184) == 70113940568682616);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56157192458456779) == 18544176821863748);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10222065692955248) == 13625646326448408);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12304163700878946) == 6185034987812720);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54152166435406234) == 27146413273211084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55811545205003808) == 64141678948614928);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66088554188022679) == 69772335559086458);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40484994085654713) == 28684407466131580);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9071902003016618) == 14207220785328588);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54727579087648791) == 27568678509712410);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22537855254645903) == 2328235804410949);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52459353512532843) == 33153855516902684);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26785219212888852) == 12895630181467418);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 321005093266111) == 9131793593078142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6881274070250431) == 12482855037276542);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53923449858023070) == 35405381513815926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43608314260012252) == 28446908036036214);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59005126990074326) == 29362201549824202);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14497957859042423) == 16044904145883202);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60207444659790762) == 18104038793261564);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4018741029062034) == 10451414305301104);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9367560954188553) == 13618364417376540);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26948461678163614) == 10652855774262134);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69496545906259977) == 32037396959040540);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41510599468697879) == 65822090747249690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42738232155087708) == 18516950701125918);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37895804630009265) == 18350319722381376);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6812166352694078) == 12483105571288478);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20067558079221920) == 9888462677672056);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66213207436825580) == 33206109084778878);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41169216076127134) == 29767179261781502);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7533364138119790) == 12879436686631326);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29974447350622569) == 14980605096891516);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44385996287397423) == 21489684188038422);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5372917257973193) == 502828672390260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26574656005254207) == 12684797911503894);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13531342637603859) == 15843775886492688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36866723708852691) == 63957524110574712);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30966744876681866) == 6157855811872252);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56091936989538486) == 27340049809224906);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1102894391502580) == 10117163962544962);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37758291903979104) == 27567441601595152);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4061364300805187) == 10467655640371216);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3686719609880188) == 11233193503995670);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50427367503474807) == 32039442911072330);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8247239243548166) == 1197235020538642);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4589539639329032) == 72119645700124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25761489145094530) == 10626578087948536);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38413197827687268) == 55864294966559098);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14365791681145346) == 16256024957105560);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46064005099427810) == 32072553935610736);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21209262707740982) == 11233537107435674);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38179033147729407) == 27568956522860662);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16957629940131769) == 5730548844990836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35439751939192169) == 16977151559815188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42020903233947489) == 65372857164562704);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13624877212451458) == 13603026142994888);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22721587419976712) == 11356408549147156);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52139534407631612) == 24163218337137022);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28599802914285591) == 14185504967037978);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42690308337482122) == 65603219891762428);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41917055880697000) == 65391259547861620);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67831413118277848) == 31622535338886268);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53561329995377311) == 71424652761044350);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17522706800399649) == 17170278606405648);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37582418981890042) == 27127708632593220);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8238503030106393) == 1197124981593116);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15040813673014794) == 14022916020515604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45835184493262697) == 32063302039081236);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10134375723462229) == 13616440112583954);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8566160214616559) == 1637521853191294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69656283563418704) == 61107014978466320);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42749468263913471) == 29784206647132486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70324182166079663) == 32732011472651390);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58041309870990414) == 19479640771932694);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63662117292652387) == 32071136279662864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6682703518010955) == 9526654285907220);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11338195534644735) == 6298585319604342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70892492910932109) == 32942087382044798);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9266852744074982) == 13634020099072882);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23154343258437670) == 11544740026823834);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65748129048865458) == 32766277724878328);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34344261683621516) == 17383241469966198);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14536798481158224) == 14021657339101304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58662382082081219) == 65390814877778040);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37741704238813914) == 27558645392712140);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40384351010892090) == 65082322874184860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22225148389156471) == 10688153981423890);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26779221786996360) == 12897029413713780);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41109011677121353) == 18507753903785292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52709391140711624) == 26390302550144628);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47365721270976038) == 32758605251819282);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22512790451367289) == 11040751056344084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54566176482807177) == 63729707833033796);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65917491682532455) == 32987305192064114);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64623429880068692) == 32063772619158810);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56398295028578677) == 28262745208296570);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50166559931060873) == 70325951539937612);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25962441913661043) == 10230756068921616);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47158039017321118) == 68645084704092670);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23711113823604) == 9118016998867570);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65760987797801641) == 33197009188160892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40440013226093531) == 65286763543041404);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2823640380532904) == 2005515656049788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25259825319476698) == 10230816663975540);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38721591436198742) == 28262428938510842);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62365372599671145) == 30479387554415644);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29638308442225981) == 15309536237622294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69123941489636751) == 33853857328074870);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35657269477194823) == 14937447436014610);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60858161462806195) == 19241040797761904);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1179601067850909) == 9135709530231926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53968787677344680) == 33170072174309756);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12223937571917259) == 14971257956287604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18441760931521338) == 106465805578652);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56502895800657577) == 28279996548059508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70287060118344682) == 32748780446982652);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29296229607378199) == 5740281463063570);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14370757343038634) == 5000139812808308);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69122356779936154) == 24850712456180988);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60218233740246606) == 29793005097269013);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18624435741828056) == 1110236906008956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44003368708297785) == 28227080265629716);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18588737110397233) == 9890812540633112);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65163564334148827) == 32054012020757572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38542350097744598) == 19267333033763322);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35238282372268061) == 17192521637904414);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57172025149790570) == 28685020029658364);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43172722604213694) == 30487427754205814);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63516709626374984) == 59233507297604988);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42059815576517307) == 27532458042234236);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18043181081323793) == 679049921696792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5654661509898704) == 80612866633848);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7437196492833472) == 1636655496954232);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26021553444231680) == 12483515933525272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29023815304403725) == 14047917897483549);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63040177630962961) == 19663500612112400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43174666638062002) == 30491216831754488);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53595925684050625) == 71442494143137144);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3647234900501331) == 10265665682149656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30977626788690507) == 15165526911128860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30707458865335641) == 6303092891288596);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21512630294242317) == 10257102472187926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66868739773371713) == 32761878669590544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28549897677962011) == 13634992225490204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41731594256093361) == 29345243417903224);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49564927614924644) == 31606045550129938);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48318896472414483) == 69578089554936856);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39640092189867465) == 28262607303840884);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63701851702766969) == 23055332922594324);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33506125818478752) == 16256081061511240);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44214205233650338) == 28227495268394488);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42525558384253095) == 27307887400387657);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57938151885749327) == 28263569315828758);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2197010957106921) == 9536165889512820);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22919587061977311) == 9095797989507150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48118512662829545) == 32996101838816380);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33568692126018122) == 7249937014011292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47836933073569499) == 33208787753959796);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39740546456841433) == 28272159119021172);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36454517143607005) == 27128326768722294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42546832598169107) == 65812422031839504);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70672284876152441) == 33153031902397812);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39825082448821756) == 28280339940488822);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41271422121134545) == 20776998269846648);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65344300741458899) == 32775923078337912);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3788091445565256) == 10265692861268756);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34852881517645361) == 8384608865813776);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61625273149440396) == 28448656499998846);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6581790010288394) == 2554782539855004);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43341638009815546) == 57519126197149948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57058490047908725) == 19696201961997690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8086691952574842) == 3471464226398364);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35923030343239752) == 14924664020304924);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1603769146994861) == 9136137478541389);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62972481588941319) == 66947723896389914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13996583634798648) == 15855237948867612);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36590159341098564) == 27577158977487634);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4613051306952692) == 11348904547156802);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44011365430213714) == 28226696016967184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36478790541271260) == 63934557086691406);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41847284558345267) == 65391602954080272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16071489608813642) == 8383738054223476);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33737702516673161) == 5005532406973772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19980526637510084) == 554840524568178);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70479202120655512) == 24154695905718652);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66872241644382841) == 32767443672110364);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6204229863138957) == 2746381672649029);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47107239018779262) == 68655036457951646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63377926581423787) == 68663200454673732);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9967730127290732) == 14408844332498973);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17863641759718243) == 15157665581597976);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33075529226135074) == 7249637719552784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44773783356589477) == 30686003263045746);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19058604743113608) == 9562253059437948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15529664261698513) == 14021048727245176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3261956438206921) == 1666171047154812);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60130786713723951) == 20777273143529493);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63510728594617979) == 32072568962093332);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45482141089900322) == 31640513195324176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22053228705584451) == 11022222580809744);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45167977733882407) == 59242256125034777);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16210186823846191) == 14712571618657310);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23025198764157347) == 9078136613377096);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6624637317027632) == 11751587811033872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49554780364078537) == 31596009126007924);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58191429593451555) == 19479940140450832);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2046161353147378) == 528139384496632);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11295819797263720) == 14760494327869972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32827967542187026) == 15851402782188048);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58360046998460069) == 28685784531829106);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5196025721671985) == 9315479140237336);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59200696809874964) == 20769601260696338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17338174145226129) == 7961387693183088);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56491831202077296) == 28283354554369400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14588977968328112) == 4794199271552576);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1604798877209657) == 9347243048255508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37671730254578427) == 18129948193390972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36171905953058890) == 27131121579663564);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 746900208206768) == 9553824578896248);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47909345845147075) == 32990328315613296);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9589833120618428) == 14048421417976694);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18168052921410114) == 9677727944552952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51705133955710133) == 34081307534496834);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22695048020281919) == 11347551649006878);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40372098836202906) == 19686832482296436);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16435580079348243) == 15146879837209880);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16831135063788416) == 17382621728081272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64219080840966902) == 31652690880475594);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33819584695280342) == 5721720203719162);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60036367717050032) == 56791841480946496);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36852296106027058) == 18560131707384984);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26459005230497673) == 12878034399237492);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54246453861918616) == 54739080766272372);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27527221989483057) == 14181244619657496);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39238864124988931) == 28694813478323472);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16254988988806683) == 14735522836943124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17070757733179740) == 14725077563306014);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35191745712714028) == 14709068536983582);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1721376420195115) == 9544626449290524);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8569696498876875) == 3686670571470972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33202375697884795) == 14030529655312660);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47494114505123891) == 69376785063449624);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46357041234604973) == 31657788490649982);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42659870127785706) == 18509127346856772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46120563596537208) == 59647630724544068);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16411673686704699) == 6123873020281116);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71883566338080835) == 35413996077482008);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4341986050985833) == 10678498337096980);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 311313494547271) == 9685673781396761);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35934769505917460) == 17400558580925722);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13026763468018631) == 15165844338540922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14485189440913545) == 5023745485602892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23840762975879972) == 11357424686924057);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44710294268497101) == 28439698276910198);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14338115112377871) == 16281642425812245);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6805262924964576) == 10221641071931768);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15853958208788108) == 16968770157577078);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49850883063842990) == 69881692879791358);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37256068777588927) == 27136022615528526);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59993231409307490) == 29365523636823824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38456115160729858) == 28263296520168600);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41728892278033376) == 20336643638887752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68481322118058250) == 70300261685797020);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17313226650734455) == 7953348500063506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58246067659799893) == 28696940558291986);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59916495724792511) == 27101688724981118);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25229905220783949) == 3467382951445782);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13027088894061511) == 6720771877246330);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44669386607165540) == 21696944481304602);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45214232415835263) == 31650025797845014);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40505332643683326) == 28696365590390270);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64425369807983273) == 31650299737047420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70300564400268415) == 34974327858729078);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2765984562057977) == 10671661913538940);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31785441428284475) == 4592461905399828);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60524816744183013) == 27304728535912513);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14567805958491426) == 16044216505607696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21245956623381129) == 11251860306235772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30772727461212587) == 14753048916398204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31470943242794749) == 15166465497862518);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8761320286254209) == 12684547587051632);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62905887993825490) == 19449027646170360);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39414983981843498) == 28255075881460244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57769446038253658) == 28486704202608796);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36550887524075886) == 54724224023291085);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58796123659981799) == 29363287491477882);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16390717180339590) == 17193093473284722);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35088710111483766) == 16977384565973778);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57706606929558438) == 28257342224938490);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10264342168032661) == 13646756613423218);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18894543179193054) == 9553331345139150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14128682697410992) == 16282053851284552);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57566152075988331) == 19246433572307988);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28841361549226163) == 13855503835795576);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1777195595925374) == 536580131300118);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61886293900400724) == 28438461305521178);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47059911521154935) == 32080264681001234);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58786616981196950) == 65383042747932922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54563090682938814) == 54721475034636870);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55890824182869283) == 18551609944806416);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6010158413074659) == 11339888402399304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40326388672818289) == 19668963816182800);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67941725782488954) == 33865859229201268);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62077228199765465) == 57493288619085940);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29888947421661175) == 15183986678923634);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41891006606734572) == 29354012035756613);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7244681489417321) == 1223032427118708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12024557136351122) == 6746771531147120);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57386912066494723) == 28482340906012696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60095256981392851) == 20337121722467392);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57361847731753194) == 65059921612316924);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56215227692331002) == 27579811589568372);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48350291309959598) == 32968861007897854);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56571254536080887) == 64872840901788786);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19478715868952567) == 9141083722617154);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44457978369851527) == 21701092204281978);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36443598773941890) == 64168191244677952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71367041486712230) == 71004060037035634);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12692375414480333) == 14755282531746934);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14351691577904415) == 4812305788470294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20336557211502727) == 10267064564941946);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7141026692862576) == 10214290711904536);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20013349375192909) == 1101180223034645);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63436699411276468) == 31641351646785866);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61566585587514782) == 66948813350057590);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27172852206304232) == 14180168545678668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30292829582983475) == 15175010683879448);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59713190997732175) == 18103189455082782);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40830586862913795) == 27093142798697496);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10758954902882620) == 14628562252071966);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31050569585410946) == 6166615345275384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36058195186433497) == 63729157951198276);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32041879977899591) == 14013225450737985);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11098875439989669) == 5050074355106121);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28838810210803871) == 5058667460563022);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69154401731769607) == 31615116362387474);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33392618278843771) == 16274659282423828);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22819362208617705) == 11335423130533956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33477815944885629) == 14025306663030806);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11451727711958244) == 14763310147875442);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16494821026093218) == 17390362936747256);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60668061054400831) == 65584224698586142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33050294150258191) == 15852215894148374);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22692316522695622) == 83342835688905);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60969229199200236) == 66511218770085750);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48351844766769644) == 33182571605340286);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21794195947847399) == 10257179783009650);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69721194293219363) == 34082132168032280);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68683000913240692) == 24842233341882650);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69757360743054035) == 34280371688641912);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67255631598663469) == 33206152787824918);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 378364471727042) == 9685082019013112);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21144169019693285) == 10456087766533242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52203615067444694) == 32750015428895346);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 833987683215218) == 557769707303792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66751228898751552) == 33197353946984976);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44456815162114912) == 30496191431344408);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35848890077850393) == 6131637205013788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61421758880892123) == 19448029623959668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30825748883132899) == 14744207726379128);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54506476727863750) == 27145447667999945);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52475887678171051) == 24156029007040884);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58386624805325799) == 19689646831011194);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34872405770554494) == 5929259345585686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59229982028742176) == 27532352613660432);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21415985593830648) == 10248375014557300);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7224478897770601) == 12896522077175836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17274020288520342) == 6132326943535354);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54790254857573668) == 27568815862448658);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67944385321279608) == 34077747622260860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59188997060977162) == 56788133087228316);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7988451465522622) == 12473070985688694);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63756638337591174) == 23049753744880449);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41532158802211452) == 27514883886985550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37295529009432369) == 18120507841216784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56294616158718735) == 27577752028579102);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69520377784372228) == 23037741207815697);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22950626998205343) == 11330154475591038);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59986077433846167) == 27096920758568058);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59541493308425808) == 56788489385509696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65381569754881272) == 69349023982945908);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54780636045374643) == 27560160088461376);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36423181272343040) == 18139544260292888);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32162115249683933) == 16045754840978550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27044986243622691) == 13625853760409872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69948625153517525) == 34974301568799090);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71420364320240548) == 32748615068070266);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53291896588031605) == 32731163009515794);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43250045887811686) == 28237458057240826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26565716740259048) == 12895834404599412);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47738253774417986) == 69357544142778520);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50384627623921716) == 25052130340570138);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33445988944952643) == 16268309775160336);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5566726197637075) == 494054924424560);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57870449464392629) == 64854450388637050);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23129299361825528) == 11773520248209740);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30961378966274531) == 14770366451089528);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48904471652909339) == 23773121870168092);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6282236979191377) == 11761302502704400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59199532388865753) == 29775700535673204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36075036706531746) == 54730959380231368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11811683006837041) == 14770117120692248);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47820622928863758) == 23767385421263262);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14337084030030954) == 13811248092308084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9160809783683199) == 14181314211419214);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58246034636153826) == 19687379741061624);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3843916568054694) == 1812002683266554);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62014567548320567) == 30479524324218138);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3104312265682913) == 10662681526998384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28818233841178176) == 14060047438709520);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59851648578871489) == 20356089376100472);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60013811931095597) == 18112906281652510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33041623064999888) == 15852310433739632);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61966746973182682) == 28237207736985084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55193490182901055) == 18130486845375510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44146554628278458) == 28227795529409788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52560180814378129) == 71451839455203440);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25984997137661364) == 12464690036520562);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57575943651699064) == 28253607127397404);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17458156111479781) == 6123213610058106);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22449006567954207) == 10661557323661590);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58714520511266713) == 65382204689061188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45025049805489376) == 19449294097547888);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67653624305804135) == 60881951162601850);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7209600499792013) == 10208458326889598);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4734809541854983) == 2332233281503506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35582363031541042) == 17399596160885264);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54268505659775891) == 27128061634250056);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19687851656210136) == 9140188689707844);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5058105481186736) == 2349112499521088);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63036288387269706) == 30920018313355796);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11171058280504919) == 14064960264963346);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23426041130981205) == 11778810572144962);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12044443947482300) == 15519775363200630);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50542336449224385) == 32019240185627968);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20145157525577331) == 9544673293403408);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26885667179050965) == 10626260338512242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52978378421311967) == 32744864508937342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12188788935604806) == 15164815643227642);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45284988394605693) == 22642867556025366);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32267805509075684) == 16276845398065481);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2180591382655419) == 9563103939760252);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5064959684730118) == 9105364511860242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48016397597687183) == 33189135923418230);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1093579704566884) == 336209717314066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62036211401376342) == 28236315468571034);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66934669303571613) == 23750411108192374);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70926793268837597) == 71243748953035894);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1237887890581321) == 9130649589682452);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11591077019824276) == 14974841231913082);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54055574580272928) == 63720551624869648);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5059185106981876) == 11355773969727818);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65070432770762160) == 32071181376893552);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66112615019780596) == 32985906114757754);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3520785277940946) == 10239828048032368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53100859784741494) == 23733856743858586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4249590883806343) == 2225786891572338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5437665358139929) == 11760704937656604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60088266970673795) == 65399851111420280);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67454888716966768) == 33197447407142168);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1479840189993096) == 9536025699188860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3544126287966494) == 10244729657831582);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23204057949597581) == 9519370969355589);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56577675917401554) == 28253633854645872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28677243936085349) == 13634936536891410);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63551244004751976) == 32054893650752796);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9403373598590555) == 5190387463618884);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30476943707552148) == 14746442413749370);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18152658248741282) == 9140066439835896);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42408304727550481) == 56808578426049816);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6985600447513087) == 12464095698714750);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31560363582146619) == 13592112814064668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56177713656551661) == 27343209298855038);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1204920691214820) == 9114707122455674);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70935231917161787) == 34974991361479700);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24454521609089739) == 11769863959515516);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17945154485995182) == 17381637710914422);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46675841183221655) == 31643618372780354);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44965008238558872) == 28668354489005940);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5177567218899582) == 9518434268161438);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28037095931493374) == 14612267031442942);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67386656052691389) == 32987441560780926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41154044487069119) == 27540778869724278);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60202318335656418) == 29362259394601160);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26616078640054427) == 10627431853424764);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18285297773459792) == 9143200486732824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35905767892879943) == 8383258827818266);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63277105201183954) == 31634708326332016);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71780608710501890) == 24137036010992400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36927962578583528) == 27576194631546740);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57120447201017832) == 28694881124202356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6145159565994891) == 80075907405132);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24919971134808988) == 3448935738028414);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70001475810970970) == 35000716151858708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41803770286300440) == 27092534602991124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68704801782276676) == 22599162490925338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45796574410308369) == 32083611328812312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6895701464189125) == 10231813817894010);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62708448960720257) == 66710126959137904);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43980258211742122) == 28241262734057724);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47104022026504772) == 59647287648748825);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40969102856004015) == 27092605539946573);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9041378880056228) == 13626254047458633);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62919145482471647) == 57914410614031486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47937622914692330) == 32968863743226484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64635374962122035) == 31649175521657872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42993207264580288) == 28245936398860664);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68035672288607489) == 31619212837803024);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49884787123392743) == 69902899345326153);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63829085114458772) == 23046416088084858);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38233304889311009) == 27559084673078544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19131520100634830) == 9913813328470270);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14104542040201440) == 13796679642186824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4903398494644905) == 11351137602868556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26708202389515957) == 3889871886620018);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14111346509719930) == 7269532396139724);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48054705230732401) == 69797098543121520);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49045231550633026) == 33206975192244752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21881483371809623) == 10265598596777242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38338100664478330) == 19272958171952540);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9640787758397439) == 14038667154461046);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32738834797531955) == 13583110784385304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27926623643167066) == 13851118686123212);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47957896814695397) == 33196529921819002);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66812166156500486) == 33188377461208850);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17571641170789096) == 15147564764987772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8060131894862668) == 3467111431123230);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7828941962373984) == 3879151184328056);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63803247924640316) == 32071468443907358);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8186382494939286) == 10212913617286394);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62057899568486497) == 66525406668538904);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10069578954457642) == 5033116553388444);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2793040985581929) == 1249737793885212);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35034328601060210) == 14735412725330328);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53274817935511418) == 32722151111304980);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3401285208293427) == 10248209188716568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29739705266395310) == 15323377154794750);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13245307516475166) == 15165871577932566);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66358722846484633) == 24172724499092604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63297627889994966) == 22641998687446642);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47872858505817531) == 23965537956893812);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6455585166722197) == 9308057625624690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6302066588502426) == 11770622082786508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52170342805477652) == 71011785294971930);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49364394044235556) == 33196840568850194);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54851402796962096) == 27350955948277784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19578512315109269) == 107510635988338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28484107870429047) == 13646698173957394);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20391600121526392) == 10268955970155644);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35535025504794160) == 15157693019206416);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56471578827205447) == 19264300797203738);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22528615484154723) == 11330280578945352);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40363102979497570) == 28694511376672528);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6750244934222424) == 11779636843086108);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65236150718831607) == 32054974376969546);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45711899591966146) == 32072017597281856);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14534314711854079) == 7267022062490958);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51408428912154034) == 34289171011248752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61892640956785099) == 30702254498648188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64902251158139630) == 32062451966915021);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33538188533492569) == 5015864677831964);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37187786631286416) == 18130416785295168);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37927366491223763) == 27580010243985776);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42718214057232675) == 29768126824907800);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62820873883974670) == 28448356910151190);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32534875626443314) == 14013993163628952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70348294567198883) == 33161920445907064);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24567958375602698) == 11752367562991004);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7259766296783244) == 3450093768586366);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32361286055914035) == 7275154998367512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1092659040203528) == 9552974856433948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67781776059244892) == 31597304727646238);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7519982151353365) == 10653379225092114);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16447781385115344) == 15148623614870384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23737787304456035) == 9087124771345680);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69919526418015008) == 32740781064532248);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67208333813299178) == 33179721023861620);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49810869071039559) == 22616781317145618);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62163276659247126) == 66507911120856218);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60506609312536322) == 27542428808356624);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4284485313827664) == 11250553763692824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44468515871836952) == 66529185218102556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23247095444208220) == 9510245169050958);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61958583559160385) == 28230553377177880);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30429014752479276) == 15311255632970782);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26007330268813857) == 10221891859973400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5753476230078831) == 2348837105766430);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33859948995737752) == 16960590769960060);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6374026721281985) == 9517998730051952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43991412493760225) == 66498950347592048);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33825849201537623) == 14725308475052314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67829010565739623) == 69904275815141441);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61199009389770056) == 28236110446735892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6517826850634912) == 9518834704692800);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67164706851428712) == 33188695880565788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32771217206715404) == 13599398894187029);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7523472960288223) == 12690320609579126);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9296359478044786) == 4611163530605720);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61911313985832863) == 19659824117318006);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21429324229792591) == 10257099785175326);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44804901406940966) == 28658087886268178);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25847134179881816) == 12890542807724316);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37175469720004520) == 18130333719606084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59501097907657857) == 29787932181529712);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30402820192428848) == 14743976340664600);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67778319189094557) == 31596183760834686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48452028257281614) == 23771335836447510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2490764805478402) == 10810953139578008);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21161714183432905) == 1673144311911804);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13932672172502213) == 13609555242292338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55561424717703015) == 18138510737641746);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49518486221472750) == 24190247948886526);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61801220143336519) == 28642077719659538);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6380139546526287) == 2763974211043613);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36511752296377174) == 27154741427713434);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24492236380297190) == 11755757202416449);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27547698749208810) == 13629220073223372);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13441086309895438) == 5947017498109462);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11505763902318902) == 5752881680888986);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8436181996110669) == 3475417971130654);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45253876971052912) == 31632501458110320);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53824823125719576) == 71451286547379484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29407709474181610) == 15335549632422140);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40356282559393036) == 28474881638282774);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25901368522083803) == 10204119345923188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62004831271431608) == 66499021210334836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8909364633719737) == 10644378596868476);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14095351051731473) == 16267660075335960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67253015738966767) == 24190910985965950);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34036489502273453) == 5718018292159862);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53250286001162730) == 23733813930910972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27810854951764892) == 14039531385127246);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14843363423164320) == 15861354691167040);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16612475634684283) == 15130341534957692);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33365892224435013) == 7251895586193690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28628855627204777) == 13850442216768636);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48568268047971343) == 32757481558446102);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48991154886168368) == 33178972403402512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28685850283584169) == 14620007946229068);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32646819598016852) == 4812032110080074);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20428227061191905) == 10244867775365232);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10425226324831679) == 14180236128523382);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20858340594655429) == 2011865438851194);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65002767949524836) == 32072567686969105);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39470000244961769) == 64867755665461372);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16518794002253545) == 15148251411874164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58746730996786406) == 27114785674343026);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65716111091019944) == 69375434890195068);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9090879274375940) == 13616934507775258);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43492092117561600) == 30682840162119704);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17468329409870113) == 14921640341492760);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63188224316962339) == 31657707066822928);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24629210157979540) == 11752207030813554);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37777828294601859) == 18570027856630896);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15855986443563418) == 14716925586452732);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22029360820008820) == 10662795884835602);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23046210859822734) == 2762621839062902);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48165507654903607) == 24184475531284754);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45253128569045247) == 31631523669377102);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27553708407302245) == 13638279094342682);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12938095085341529) == 14769814863906076);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48526670454874329) == 23770753749356668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26505284215683754) == 12899157576398332);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61931520603362533) == 28219727962505330);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68655087658127441) == 70316590479769624);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47811270593552098) == 32766064477382512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53987986893376084) == 33144534487500058);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24265017012646522) == 9527204925190556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2255826813514791) == 10240128762777618);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15456370168381128) == 16273409031649092);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49092747888180582) == 69577952673805842);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18897055386798249) == 336206966572156);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11601341873179835) == 14768259543073908);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14689396225824262) == 13600770931204881);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38423267813585631) == 28254017224020350);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60954310136664914) == 28218723959349752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1908046110764542) == 9324989479302774);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41541256864585676) == 27523283253429110);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24978825555526817) == 1219940431560816);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35324512375073402) == 14735179727582620);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69747558254084789) == 70302292055523706);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57898169836336108) == 28283328178073982);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70721571989160169) == 26389314155806844);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64929789016861233) == 32071136059331864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9557649573111320) == 5058104624198420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39931065201764682) == 28262404360451228);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 296569081115313) == 9113678697697656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24028516836501456) == 9087356317873008);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43979689761379687) == 28240986975338514);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48693343540389599) == 23777631656677750);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6071082066919388) == 11338459738683206);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41922290188859180) == 29370738406208278);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42790015928552076) == 28217898109306238);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67025750974272075) == 33189590850076956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51576437066008604) == 23037191184090142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46934985485219907) == 31867200538219544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28554767284692124) == 5059503223386742);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66207275231943080) == 33207002446267516);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64714703291752253) == 68241002357163286);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13546896323289732) == 6844203419840841);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23187650310193470) == 11558649362194590);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20902535948091732) == 1657925576130682);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33144770552733214) == 15834028856747798);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24373603447715516) == 9510590521062214);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39150645795002095) == 28685876344979830);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6014997545471921) == 80560165127488);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44209025845639703) == 28219272490423578);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3483025360087662) == 10248990333089182);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49412052373879779) == 69772634401084792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40923750918822683) == 29363506666931484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19344348740261299) == 9698558219731008);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59476834797259817) == 27303915849647124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25085583032480361) == 10214014740926844);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55721821454231556) == 18148340370125849);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25792605210382260) == 3889625153963890);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4696431479528832) == 9098669393839680);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68971288325281223) == 69876719703949377);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59317763589802270) == 27541192201479326);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 226180119864250) == 9113163829981692);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39973255191644862) == 28676986677175806);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11632807495417003) == 14753208589522036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48380797629160216) == 33197587332793628);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8417741325438483) == 10647056570221840);

}


TEST_CASE("piemers ar 15x4 20 stav ar seed", "[musha] [musha4]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;

    int musas_sunu_skaits=4;
    int laika_limits;


    int kop_musas_genu_skaits;

    ll musas_stav_skaits;
    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;

    ll cur_stavoklis;









    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);




    kop_musas_genu_skaits = (musas_genu_skaits-1)*musas_sunu_skaits;//jo SLP jau aizpildits
    musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits);

    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51770818893537304) == 31824844380205084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65735642321993584) == 69352382795724656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57671420259688261) == 28274935273522450);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15246375697687719) == 7267321588813938);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59183424574467376) == 29556529623938576);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71924162612665678) == 33145677974709782);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27663653932545748) == 14040562374357874);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13825864732394442) == 4584709347025356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17478589588539830) == 8383053160555770);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9312567637633141) == 13626073729995802);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60568293460129806) == 27322609879463069);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38298513285253233) == 19250830167589912);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36447827643845303) == 27153605819991418);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29564337952818466) == 6298861208412688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39790223175145902) == 28262908280284414);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45341202953972636) == 31640378377844094);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26643647685993920) == 3677459476394608);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14736414343488036) == 6836564946465049);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52614710353600722) == 23943409204146424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50405532884048200) == 34066485737817116);

}


TEST_CASE("piemers ar 15x4 1000 stav ar seed", "[musha] [musha4]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;

    int musas_sunu_skaits=4;
    int laika_limits;


    int kop_musas_genu_skaits;

    ll musas_stav_skaits;
    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;

    ll cur_stavoklis;









    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);




    kop_musas_genu_skaits = (musas_genu_skaits-1)*musas_sunu_skaits;//jo SLP jau aizpildits
    musas_stav_skaits = (ll)1<<(kop_musas_genu_skaits);

    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10323412928470279) == 13633986466284561);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71039887297942114) == 34994918087041432);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59631949728085466) == 65805569130705524);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31766685724082863) == 15845997987430733);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27405300861326659) == 13626073141379088);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7674171819088518) == 12879437147940346);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49853165990868371) == 31604438536032368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17901154681045051) == 17174928433381404);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65801016523450861) == 32757576002999422);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6817376245890092) == 12477288227649046);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50840060027963270) == 22601429640782657);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65942107286008895) == 33179924345294870);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21203237089807099) == 1663112963392892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38429031899101499) == 28254156826350620);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4772994852820122) == 9104513171694196);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36611803805777369) == 27559348676760644);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39136150004130517) == 56261494148927866);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66883804488238567) == 23759969767489650);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5727862265610964) == 11338238763270522);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67006597633712602) == 24173388738343164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1549469954499313) == 9113390192464200);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16055933805955678) == 8378171926062590);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49971267397737875) == 69876457527280704);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49121934658921937) == 24171774304784496);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19722123884033578) == 9544400143069972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10517944094000096) == 4619056008954184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61275056507649255) == 30492237965235314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69102516987014211) == 24840872296584208);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18248917035476260) == 9122115467981329);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66840938516028161) == 32784788028884248);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64830182737399983) == 32065991827947646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18336758646294934) == 9130982898443466);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42734669241323999) == 29776361029402750);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49311180165739421) == 33205257066353022);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67947026021407621) == 31613945983010170);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33549742088700609) == 16282259762184568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26146269625058429) == 12474996326400022);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25938135403633720) == 10223018217768468);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59347787162913957) == 56797009038181441);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2898820849397236) == 1653739062507122);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6981400235383732) == 10203811176252794);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32686699357518469) == 15851320510420297);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50268241321242378) == 23020826561197844);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20417223864120691) == 10802182677860464);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51827676733386379) == 25984582752736628);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15506181421608752) == 5006368526087952);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39712619952082955) == 28685809219272732);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11182951672471523) == 14629832752956784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61215219804071333) == 57519812657811570);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1249294548412590) == 132592297161982);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27520518402341928) == 13828424616490516);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34076403242186103) == 16963984281456658);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14526307138557181) == 16049878863052918);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20533966130065217) == 10266998198343024);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57323858325089470) == 28694882130835062);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53092513133403049) == 32731529298052476);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15540472273284431) == 14030266798216213);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38199079264913851) == 27361089176372300);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64665642551729393) == 31864450484110400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26090880527183208) == 12478090115642900);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32658356717685421) == 4575911869777229);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3433127575136689) == 1259727543899256);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62947523957318328) == 21904132426150268);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4129092111406159) == 11245465834586134);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53465393925894182) == 35001402329608346);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29352593096661615) == 15305792421332254);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36351201561014371) == 27144866616021064);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3387024420422763) == 1794501779390484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57695072722708060) == 64871904999027478);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63854625899110472) == 68657565862309916);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46206795993545554) == 68241056102987160);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30789694097467531) == 14761500565574780);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9552615521649165) == 5058336753844510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68008504243219908) == 31600532728952385);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50668889187572096) == 33849021660398664);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22241206105570700) == 10450313098311286);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15945305951482678) == 14725861983203738);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54477200516333) == 9135588193308741);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12440910049646450) == 14770087791600400);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19944520355347215) == 10108597469910302);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71348407927897260) == 71030475229247094);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65237166222321182) == 23046756550226710);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26535162948366897) == 10626511459848472);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14442634563052203) == 14007466237235580);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3624044157457392) == 10256277823259000);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 621761417817216) == 9561988975736384);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70314809326562200) == 23716264485041532);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71585859321298258) == 35406272669201936);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54247447380962137) == 63746127970997580);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70575071196069743) == 71452689375925622);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53714877531737941) == 33152686146651410);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33322927709232975) == 16274428880880918);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53699603422890241) == 32933953195623448);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23435487095161455) == 2746242045903133);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38522265453095150) == 64855549741053182);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11307210143925724) == 15324885830566518);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69221831073022499) == 23029069805617432);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37055354113897620) == 27567307204997746);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64021678687435294) == 32083411275298206);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44398948341519538) == 28240165013430896);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26830378448899137) == 3669627109573648);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29693154189554489) == 14769495048261916);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26331298804185443) == 10204118825899032);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64828528339296419) == 32062385820700792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27663864756329977) == 14602174321198196);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49964640397972011) == 33850980718477596);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1895009437923677) == 9910729783251998);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13764786644953625) == 13602099304502556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55783221508652920) == 27568899761354516);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3903305565440454) == 1244164126713082);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39755888747835476) == 28486563952816154);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44618421212171409) == 19632955949778032);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31139319225697343) == 14971533438156822);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54489265635456983) == 27136848195653954);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39176327746381459) == 19687132668793200);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48608595719861275) == 32778783596630036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39903013414677150) == 28254982115833342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12092488894762720) == 6184786796513136);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21953570692146433) == 1259842444333072);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69559517686519464) == 34287727354544964);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58285070178965112) == 28685094992249620);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42195355221320305) == 56383580135067928);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49012842518877847) == 33196967471645042);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71425754567788876) == 34973983724487710);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71485223688098936) == 33174152380613140);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24167562614551933) == 9087055788445718);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8637053131102100) == 10646739698877298);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48436675962468197) == 32765794941636882);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32626022674564469) == 16273398852059154);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35207300401730448) == 14716856516280696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26304889084992569) == 10652719878767636);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60708744856201473) == 18317481488700440);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43744627957287473) == 66938585208489240);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49323311963590380) == 69798128317761398);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51840837043441233) == 23732757163574544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33067334303403240) == 6854088214382196);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46122669685195487) == 23055115562680702);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69391103957490980) == 31810937812977170);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48083072031105197) == 32978233350918270);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35503162820579507) == 17399707622375544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1981208595033920) == 10098262703224592);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60402361712207221) == 27524975717879826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48879365633210792) == 32777856883913852);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49871158113761165) == 31614016801214838);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54799579607350347) == 27571302577411092);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25856157965894144) == 1627895170466072);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10703505658727106) == 5033319492199928);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36996647952093577) == 27364650450681932);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17781229657460959) == 17403031400054910);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28225875218509574) == 13618032551507730);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27148316189599823) == 13646725694592126);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17486011104625034) == 15139274590462204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8503809373129423) == 1648303831647614);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43083011104667868) == 28430926863381630);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19319389263682825) == 9132343344694292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 395833887932052) == 10115513840023362);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39869625611824396) == 28271269130384918);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63617376468430220) == 32054989129823862);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12359271682080079) == 15745940749489174);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36668291725946966) == 27339115590987930);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2592312092682504) == 10265391672407068);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1068497182941371) == 9890537947795532);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48994540052740579) == 32972297052951672);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9415508109291153) == 13643858313188672);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29323965133093391) == 14760906090911006);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20962982133083901) == 11251129842963830);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52592381169298806) == 62426522331133458);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50988722205552121) == 31613669444293748);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67520317205326264) == 32986067922760828);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59154018036470035) == 65601828762828824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48955159364989548) == 32774453612359966);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20662946381432823) == 1812850087821682);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15445066470900548) == 14013902229045530);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37549214200211899) == 18139499243607164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68527584367509497) == 32044816515011956);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59111165479680692) == 29766559687652730);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66376249388544968) == 33188515502600572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14462356833035813) == 16272872559120658);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3409037993217360) == 1236133621401104);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52117961247516075) == 71020211817894004);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43618410065087896) == 19661293590152820);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36843153236420709) == 18560668957774921);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2221663859394817) == 335117575848984);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3504294982649642) == 10266215781802772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50005135720231630) == 32039457142354430);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66893410301450950) == 32775968662136314);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9871586690734013) == 14050783125766470);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30179214404706850) == 15195145018192280);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35068215593741845) == 5710343712380178);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36130602741231669) == 27136778658926618);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23620474699931149) == 11771172330734870);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24142907836918760) == 9078548595553612);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18891533111606061) == 9544675222263069);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21782313975902610) == 10260578176053496);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54680453543248514) == 27551319707689464);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63671303389131930) == 32080252328915572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17108491156757231) == 16958873942820222);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59365815252816689) == 65813267466748184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24096050620588612) == 11339915788628250);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5809389154375317) == 11348368353526130);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22577532109073654) == 98481998149186);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16114077383903058) == 14734931150676752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49629926890049558) == 33857364254204058);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5254052345782526) == 2554507662815486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2713587018874904) == 10258267410759708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45300569827992465) == 22650840944611704);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38791462113926554) == 28262746697311860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13917192397900063) == 15860201090483230);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29361120041447086) == 14744206870820726);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12110692145957837) == 6159221699904894);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10450626377845841) == 4838156246647824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3890012613987301) == 10248483458483578);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13902524996696673) == 14021528104211736);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13603396678293254) == 13590065951253266);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69839379272488839) == 32734452954628466);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53806816112480149) == 71441909408499058);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59085028157352013) == 65602127217693726);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24263628428341464) == 11779967933034572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15657907598928943) == 7055309492327445);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52198586311017935) == 25993559381770358);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34214484596331065) == 15130204161081628);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34676310224224546) == 17408530021916824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60686839712904584) == 65589748584952436);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38794248645525378) == 28265282330179440);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28790847982096224) == 14602131906922256);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 126547377046953) == 9140259573989444);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28020023383820559) == 4820815175615517);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7662732439736458) == 12904555188855036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31504840347321113) == 15182843219378460);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51130435259303472) == 22610249897181968);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71243158072887249) == 71012513281737072);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22919097148882040) == 9307245807564356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43027197747570373) == 28227253210714482);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64412411227206292) == 31643792260411722);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8528136211325406) == 1407405486256382);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11131544135640751) == 5031922634232133);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52252675331433182) == 35415027030471166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15842479223216442) == 16959252838950044);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20397909506593286) == 10269331783328154);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21352312851530149) == 11023434294427770);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68410916313653427) == 70091286141538424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56578780975132665) == 19247534694471028);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65036334140160222) == 32054688549254398);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64759589719758872) == 31848186717226012);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13232422102636580) == 14954737582670874);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39884303467662065) == 19274170466993528);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47750359233071888) == 23767944984790296);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27825400904559156) == 14611991995823386);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58149268923401605) == 19678583465050226);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6897190868016224) == 12455779380299376);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65793999854758261) == 32780320521606266);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41237337858106894) == 29766090008015638);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22481425988159610) == 1666721805411484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31154344674152621) == 15184055952048254);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59629690149230501) == 65804607004901697);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39243006759540670) == 65286420268655478);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57484045024428868) == 64871834610139418);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43185448279765477) == 28235833406328954);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8915375188931881) == 10643509939539988);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24982241273623816) == 12455893315039764);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2093739132012699) == 554277342643276);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64314315097499919) == 31658004760056861);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50201066799476781) == 70096373605765150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14950636732784161) == 14013008030041360);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65466238779004773) == 32766072526209394);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45897035784667053) == 32056315482084677);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60451311167288462) == 27514804966961733);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26653150193160716) == 12904766323294494);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51675552095249971) == 23021374968598800);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34567149769519447) == 15131280469266458);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41428095878036799) == 27533220799550486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47081222840976199) == 23045818169000217);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70433858630666657) == 71454648296637560);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4425909039261258) == 11250345246700308);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17034033709828686) == 5700035849659806);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4487891312171622) == 10688153977202074);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42133646065226070) == 29366449743569050);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66710100064327579) == 32757437534929268);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31283522341067360) == 15182716909585688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46665853361953408) == 68226514556519800);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24149662893951857) == 11331516455879960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23591144445903961) == 2542653594353780);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39942612029259849) == 28272049576312860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20061045058226511) == 9535750886786078);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41382862994955126) == 27514416275072506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58653602194733013) == 29361985066144114);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39359025032922320) == 19466170482901616);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30967648477523528) == 6158327071651604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8163238080105026) == 3870381397878168);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59904608165182773) == 29354572946740242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 186052817601106) == 9693933722870216);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12484550644588556) == 6308799633600542);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40925184888103546) == 27109694007259644);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47069624551992046) == 68671380972680653);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43388718666029780) == 28658114213217658);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50346894761084453) == 23022810947748114);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11585227138429151) == 15323729812721790);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67817891041540574) == 33874310499349758);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12599998068409703) == 14745935980822546);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26492169426103723) == 1629026961331316);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54433414807002416) == 27146877809821200);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54746913734699745) == 18542780634371448);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40175016079812746) == 56287648347993340);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47786271310177332) == 32757368299631122);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62430459749183082) == 19633564813866772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65236765605141015) == 32053736205321498);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64433090777325574) == 68240948354625690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28205885313282653) == 13643357232173342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31073614715069589) == 14971531376660594);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5574079184952074) == 9500088095483292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6780657129485083) == 10213876243468572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39648800959266524) == 28261878629087094);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26081370059581296) == 3466653003952912);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14874709124611729) == 15841862388880752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34995280713635733) == 14716420986503538);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65254428511090341) == 32062604261986625);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49919922432256730) == 70307658162583036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27272426426293213) == 13634570243310926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 72045621566119112) == 32959543067870836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26991369249912719) == 10644139605365110);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66763323795786335) == 24199088087991582);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71751153192236581) == 71442584871866642);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32185182670178554) == 7046363411064012);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37295442264251733) == 27127158955639826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17415267922344638) == 14716995435935230);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 493520657439941) == 9113045587477569);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3263525795415743) == 2226063921119606);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19276576926978894) == 9704089375973789);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24135080161437379) == 98782190276976);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38750889407039103) == 64864416101105942);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57941879418076097) == 64854422781859192);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65198035551422755) == 31860004193829912);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2430562841488025) == 1250427136284028);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32022824103643731) == 4574800749888792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6480151980317279) == 9500707782526238);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27581655303902432) == 13643458699985520);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20140104852054160) == 9324413754355784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27942208387361097) == 13828025645880396);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54808641362364938) == 55160937371739548);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61154070407684134) == 30682452893642906);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68194011766144557) == 32021681679406357);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16633690821664081) == 17174377673196656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39287553474608825) == 28685988532262260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50042501512248301) == 69876787366565197);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36789563712493272) == 27577913675547508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44973481621306103) == 28644037971842426);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2641489985772571) == 10679656972256276);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18737206389159689) == 9544385469223188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40719688102868509) == 65391285109657878);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41949841249671458) == 65372652680193176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43464426218105100) == 57933094401094686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 380770761321161) == 1100641419924860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57114501391298887) == 65074050454736922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11875922973428996) == 14975382412527122);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60271022476057244) == 29784223277614462);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44591915005516953) == 28447147351671924);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65006867623208801) == 32081075150655760);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16705002441720135) == 17178802493259898);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42929028989960412) == 30471142105776254);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27670492029222043) == 14040302115263556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29270529483358423) == 14065575581909066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39280587300132307) == 28676049840078960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45403395935636521) == 23049723160003604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15142050405530216) == 15834924498400540);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70233606167147299) == 32722023538987288);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71291885633654149) == 32723397792728178);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6934057241631943) == 12474195663291506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20738134431372007) == 10256895151899002);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43566809961609995) == 28644370119202076);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12440164628797878) == 14765484672066810);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26602034872134559) == 3874367075751294);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19298097653457441) == 9114683455607064);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70985733144143390) == 35000280665731862);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6640663689968117) == 9510026108867658);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30227601433034720) == 15186323074037616);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69756212705093497) == 25275510504067348);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44530993675832681) == 19659654145868820);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65105126496860617) == 31842824187090044);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35902744194875108) == 15129997343668082);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67792473299185202) == 60878657456748304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14899051525652871) == 4592968775270522);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65308734669221583) == 32757549226661238);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20847135353778715) == 10670242410008860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30232526758235792) == 6738499902834544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51336046403709584) == 34287463816377672);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6096436865020027) == 11769548824645660);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22194252625325889) == 11233467866322192);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46614508974177603) == 68234976512316432);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43495431087528713) == 28642033115956508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1113649001425829) == 554996816745801);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8643146566906641) == 10652554658976024);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28611889292616543) == 4628425551483166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40476235249401239) == 28676325885416562);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31444948931203296) == 14977005171048568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39995724404906289) == 28685644731253784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67591766135676082) == 33868961532028480);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4333801815201275) == 10468183455563892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47071689244300784) == 68672821582037568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29489842686105333) == 14743244256870770);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10827895338486652) == 14066964414858526);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59343075144563918) == 27523634885375606);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19304081683803102) == 115385925711310);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31989896398130052) == 4595806218552649);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54530474912457590) == 63747037426332154);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51524919182815747) == 32018128713945368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 100210134621199) == 9686224669049877);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38785172509766579) == 64849158441111928);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36058298245166164) == 63724826470609474);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33176286202718754) == 14013088883684760);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55296330629745559) == 27155564914443642);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29089556363988772) == 5604103398721298);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25958309955416731) == 12482691249475964);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28894598823906750) == 14058042365027958);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5121290210764396) == 11778411563627286);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34550442232603068) == 15157391633395838);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45675665505429777) == 31842137196991512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71254629259187019) == 32739958171369756);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70563464865928516) == 35206945901117970);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15287183886079185) == 13794218741891136);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54702200225903805) == 27348733283962998);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69705679207571838) == 23029206784749726);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30113778897761362) == 14980439133876848);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34342364309723148) == 17382806063449118);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50911021449897232) == 31608379639214608);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19141911293654267) == 9113513676968052);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5567963336194625) == 11752162990691608);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21577872482909079) == 10820093676557690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1774065835385325) == 9324175924695166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36198403729647053) == 54719538009247862);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54024480687915068) == 24156095648235550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2442204065486544) == 10256347010761584);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54255847341230807) == 27127775492408698);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19271140443373815) == 9140796431745146);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11709343347366627) == 15314280904033656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38760076347507323) == 19272822872244604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66974610709137128) == 33205834268181364);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54055900764967131) == 63723074936111180);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28260312291634747) == 4628689494807828);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61374009157489978) == 66711360700359188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69758815468543308) == 31816161583695382);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48577176171475788) == 32765822323405086);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9117789661793103) == 4627577765135646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44837814764172706) == 66711635901067888);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68206891150559893) == 34279216673949002);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69664756069922441) == 32046193657680244);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61494743339252307) == 66947724545037592);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37693252557534224) == 63736906919522832);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29833469117161940) == 14769264567038074);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42196628896648469) == 65390999893378066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3928540757157003) == 1258903300345972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61748224947354766) == 28650901662971510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61205241077223094) == 30497022021085042);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39184812233071685) == 28703511468115058);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55010138656653106) == 27568705409660688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35854800284188630) == 17399501012547442);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41875604030100665) == 27092867937599604);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32015310484485880) == 14030598041940852);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56991530784477389) == 65294186181853310);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50110710593333839) == 32019101589148957);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19385310146269903) == 9123477115928909);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35596713538269746) == 15157325276721560);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4286286599226043) == 2244684883657084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21773602405892946) == 1805223683105176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33465122388954004) == 5014341636556106);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52430261273283856) == 32932605113533968);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59379082344999396) == 18534267273851977);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5310918608860946) == 9509681647334160);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69913141807857894) == 34991759538725114);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16750716568815479) == 8375879952924946);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16099560264682643) == 7979256366138480);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7348954065628901) == 12888231566022002);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43413415582427422) == 19444318297601182);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52939634536472466) == 32730953709561712);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55909355928840428) == 27356403908031614);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21586989828273134) == 10265595750625790);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59133119896387738) == 27308025590128892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64647123267723951) == 22641769047363917);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69699037335018042) == 32028572999034268);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31763968719739479) == 15842401941652810);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17642188723730612) == 15147658194465914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29247629666029861) == 14408831988006938);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37129935404916934) == 64158927201743049);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5048559805722544) == 9099393483683696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20676020335063892) == 10265966350709018);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26777144355293117) == 12895999691096438);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42471054286328323) == 29787530427400472);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31198247019853409) == 15177494992621840);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33742451371379217) == 7266439577044248);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63331212081359969) == 31658862495175696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49827777202120654) == 22589486257288005);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28046077740619485) == 14058085502746950);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47871545412596954) == 33189133448721012);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13719132321689217) == 13607792872953200);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4008174807139055) == 10687490945810814);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29570115703307508) == 14743106621748850);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64652674961062502) == 32080044559873434);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49858628351025794) == 25281722641592824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67907355265050793) == 34270201390208076);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71836211782914690) == 33174300225446392);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41132325116243228) == 29783922171000342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15454682279894661) == 14022970596656449);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57487549245082000) == 19268698632422520);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11765190647980522) == 5735264924512508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50809408114406414) == 33848813367865878);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59898361277531227) == 27092617217409044);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57793313173489706) == 19256647554081948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61434475305341514) == 30895895239500284);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45710573710185742) == 31860280475673109);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61793480856091116) == 66710537689642110);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23361805844629386) == 9528029090882420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67777826763601577) == 69878093579030900);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29974882630517357) == 15191776085934366);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31645660587228862) == 13599617738192758);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 739345899320120) == 9552793788264732);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12092489492114288) == 6185335083906424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38440709680038935) == 28258340250320914);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53166914782641897) == 34992515574404468);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32633903428780179) == 7063843662741624);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69301321360743732) == 32044805238162962);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15428144855923219) == 14007646618981656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55968189213865029) == 27349120164433938);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25075278885844570) == 10213119257162612);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26368371329310395) == 10644172037916020);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26985581541349047) == 10636227204350322);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20563750368373807) == 1233409469846550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1618563849611050) == 105560112673556);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 232996507942750) == 9123009911011142);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52470503942189857) == 71442793097990424);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31855577527489240) == 13599409650401660);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63295755434022848) == 31641271653073728);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45585972763317828) == 31657248845726481);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24431534072253455) == 282193919230998);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20993900654728451) == 10455123619889168);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1730023951008686) == 9553181025804749);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18093269983123415) == 9114765655769410);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67372119214919311) == 60776372284753270);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30973154711195038) == 15728476323522166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26087781664276718) == 1215861839208702);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36652665945137806) == 27577638941733374);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20023101694199818) == 549692484363420);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55399548934721744) == 27136839411433592);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42292329993387846) == 29766363939419922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67307568983243022) == 33189657296254110);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67255899394299928) == 33206149413012508);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13331258369263788) == 6166961562387582);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38686940555330853) == 28280270680588306);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49997147940870452) == 33865067678532634);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42959573618610619) == 28226428500738172);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71140309237722027) == 32748781565220220);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59259008710933777) == 27307337665483792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63219811576673761) == 68230785244942448);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61851555527829360) == 28667393465594736);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28735757597872324) == 13835747212673146);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30501321605955019) == 14751903397450868);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54981941853799221) == 27559140100702490);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22131182979574330) == 1671883688523164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26231522347066173) == 12482304766972190);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68639338367976690) == 70097734568583744);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28173175916016167) == 13629165183930650);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40041489371791302) == 28676095486605170);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53868928661105833) == 35202796030858356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8393993915634285) == 12887543441588510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42134297232005348) == 29792866441987145);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38265537943460811) == 27577419683005772);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19481890490969258) == 556166136870468);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65441104320436804) == 32784718705395066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27750807755057209) == 14051318723122204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70341777263996764) == 32740944415438710);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7473658284934676) == 10625823406916370);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30339922444500885) == 15728476485358962);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 938559812538007) == 9544605630399810);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38639584764765760) == 19249936585339760);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64715710656834666) == 59234834958333460);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24962537975272020) == 10222878546887034);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17580438354175962) == 15156361537270644);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70383322648069931) == 33144233099919636);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34540424949965845) == 5934644843515922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12582122509958817) == 14772657142535536);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55770810685000805) == 27357504167052306);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46869853415708434) == 32056747002962704);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22171600927393906) == 1654563347932312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71076010559708536) == 34975057605363220);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36683837960479953) == 27348433053569136);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31145396954043321) == 15183988901839228);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22281081289980362) == 11031132493853948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30849023511001542) == 15310156103890170);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71925365155894784) == 33145703586759440);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14156478570063131) == 16256917221281812);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59084570736550063) == 65390760061734989);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27720637794933343) == 14629858522891646);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4027262820713948) == 1443938087207542);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8850715159416640) == 12904795103069968);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70042926412774297) == 32734521833297276);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19079213033754987) == 10098538257842204);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39848538954362373) == 19677993494156562);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27639116428410039) == 14065853081261170);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45229127508696707) == 31650369605862768);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19993321153640239) == 9536894897295637);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56971940969652044) == 28697115204814710);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16482721436396670) == 6126415441213046);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36601327997272818) == 55134288487948792);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60357247617805862) == 18526530105554706);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45743292266573160) == 31845859735090708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30802874830580101) == 14763759021528186);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67672982562718403) == 22606609776050552);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38643390360968810) == 64845146207594260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25437004937392465) == 10425298756587544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1806847678647417) == 9905643215782932);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70319774929281029) == 34983784768344090);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59763767587685631) == 20347292209119350);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39972509526000228) == 19670018436104474);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65380776914062507) == 32758399230084212);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35346265544602046) == 16969415264055550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25163202660256453) == 10644265858008442);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62953863235393628) == 66938893439403038);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26672731090098122) == 12881713021854580);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21762867961361461) == 10248074448244754);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11664640501205629) == 6184166110823806);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17837938170492563) == 15141691381944688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22652062592126101) == 11356666895238210);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41077128853021271) == 27532970065432954);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36565024353851092) == 27148975920644978);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37106606873783237) == 18551102131934586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68787443334232836) == 33865788280452378);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25666055345617362) == 10647256371606776);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9662789127872666) == 4829197554184444);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58115108583712860) == 28705710302671902);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19528320524917060) == 9131380413629466);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53512702417497616) == 33152757554624272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12029621060257663) == 6184139347067254);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32622785519981113) == 14024962459697436);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1489906079050599) == 9676835881719066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29337452400573156) == 15335581846320506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49971374332003775) == 60869863599341686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67713710978159858) == 33852456895324736);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53038411235790366) == 23735817534744342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42722533719773703) == 27516326610632985);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11539008441908951) == 14770117134816626);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42314299057373660) == 18516676347560014);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13470175995048333) == 15526786965278846);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40854598047793537) == 27110128249864264);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18962582797535319) == 327135400117370);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16854560876431017) == 17398747845427572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40394652230024905) == 28678973573502324);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68582800049714716) == 32037370855371542);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 62699578446435734) == 30892802116592242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11343014898140611) == 5735701876146288);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68713048950051978) == 31608320798171716);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71999656080580141) == 71426092654593302);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63842429773894244) == 32062463441010969);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52036199111692319) == 32731738761691166);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17905051053733563) == 15138934243529076);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41283808603326322) == 65813541680522000);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6578856029951365) == 9509913038323826);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18205750554888257) == 124828081341560);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28159359512349715) == 5174196163511312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71642990556411393) == 33145702599561488);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4410796898053077) == 11245597781005690);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29832606993652115) == 14769497187943544);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30563927215721117) == 14751975247644030);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4290097155349590) == 10472577677108754);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12360828669142003) == 6738940067058040);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63366223114990337) == 31641297895100696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35225158325834303) == 14725242435764510);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16770745773985172) == 6131363444885106);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24898344155494810) == 10230780364010748);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28496485913133499) == 13643500690047052);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55250656264273740) == 63720636981098261);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17300454782784044) == 15157693358899486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54921299575954692) == 27560159840929298);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42284168309864362) == 65821814862754676);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60345282502901567) == 29785571783873814);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56827443884754981) == 19475104693158938);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54536380328138272) == 27550221337068304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16356790609727237) == 6131525382604050);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12099441265210070) == 15190926760587770);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17235738799211278) == 16988970980647326);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70225718798523958) == 33171128408909586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22953139857843279) == 11542403044348022);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18376019817233280) == 9676764259661176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58210428526459287) == 28465813892043898);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45731108004983505) == 68671723767402872);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32225371069176871) == 13819643018904593);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69648911191181946) == 34296522376455580);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23199294779190259) == 11771172912305480);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55314934421947060) == 18120853241659770);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6941683615840075) == 12473097423032692);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1408976253440802) == 9112826795041176);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23173286740698262) == 11762524393222722);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66571738854002384) == 23751452297955184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9985596495059887) == 14628209339172222);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 25586327782967187) == 1640306080874864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13385343885019459) == 15164812491031576);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10719488834281053) == 14047915347412246);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48694114211160282) == 32784721785303292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16516634420586534) == 14936621954970778);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10700755075081670) == 4820264353542721);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56373902968862537) == 28255005207531892);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3664082371177251) == 1231664619361560);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35326697134390900) == 16959628634240786);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2137380982478767) == 9545360353427781);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8862996896735712) == 1434755842191480);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55744251999176499) == 64142791820640528);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68054599166591381) == 31599581935566970);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48238949221421736) == 33196462655340924);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57668737968873065) == 28271429632230676);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12659793469682342) == 15333247667810810);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36794532453365612) == 64168285855789942);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50341285760126449) == 70307798071903296);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46832758923933594) == 32074513537938292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17432471605402583) == 14726685874455930);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49956984125126250) == 69904509220401940);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6908121804215103) == 12456741585158430);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67713756000914858) == 33852481933387380);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20829886218767936) == 1654592668829456);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20326898078060300) == 10266766622328598);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56537420409477775) == 28262497232815486);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31182808531775077) == 6721106960258322);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6378455047291185) == 11769262205403160);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4904384963793440) == 9517654060400920);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58374142718597168) == 28694878439834648);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66488949208981905) == 32780321615614072);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68744196069769052) == 69906623640221462);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10246110315696667) == 13635898937508124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8276920651818681) == 10222577851339124);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6029020113419320) == 11350414244066836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34132382992303191) == 16960618649354354);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27553532623073819) == 14058099250237724);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51603949417081549) == 32026857373668685);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55050073815019753) == 27344583689179260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 67370874595130967) == 69780028918830354);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 39116584053989254) == 65293403335993202);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71144021261502747) == 34973848379163676);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31106764221459581) == 15168550725751830);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9607681708221803) == 5050682914934804);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1330182910228442) == 9702946002744180);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43778202360555108) == 57913508878070642);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60956154044850897) == 28219546963775864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9615608218430205) == 14619261628454222);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36009533907424737) == 15148621846806640);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23073213678325071) == 2349467512964165);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1778213225521243) == 9333347282063388);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12057943578816840) == 15174711790809212);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2945737600160927) == 10688429390364790);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61263345813471369) == 19230258727157884);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52574625113559416) == 71213888062324860);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58895055006259955) == 27119281432823160);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23179200363714018) == 11549987419021936);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12714264831010053) == 15324750960562194);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 660233892672904) == 10107498159551092);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17922771485976794) == 6140083724203260);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12921438888035785) == 14764205320044660);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2929854785203467) == 2234585263739924);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42724268447837470) == 65795550444238358);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1413485268353207) == 9324738083029066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2113487699535843) == 528941349900656);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32527087165526563) == 14007351862755608);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47513338025367112) == 32756725595243380);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15519654197275648) == 16062784723858960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6067367955821901) == 9081551671070742);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42857823564335585) == 21466801680221304);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63176961603103826) == 31661041855739408);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44729810404100528) == 66727993883903096);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50159925621044734) == 32044819182727798);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55094133610416533) == 63953923991733322);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30478002592813110) == 14744184120877210);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66215395886274650) == 32991703458849948);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65208010297752480) == 68664093286282568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63388948215314894) == 31659150115452109);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50026782808148267) == 69899285147567132);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68874988021904010) == 33874541908210172);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30928727178688317) == 14760675708997910);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9189399694511196) == 13634296040293910);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51117458333113973) == 70308304538305810);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28972875531819147) == 14058169397347404);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43707922487906203) == 57913509958485364);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61598238594161892) == 28439586670790258);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1651598659516963) == 9123218419651864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57334826958875025) == 28702612616254584);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24079111849031824) == 11331296456750152);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55548870327918759) == 64150529271173242);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26124594236898148) == 12464714527028506);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22643698736935850) == 11357411428577788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16846691809774754) == 17390249640404592);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7456540960650127) == 10652444545323390);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37806037580000040) == 18551033198753052);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3755706874703447) == 10673952737105170);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30701530560365274) == 6168058596696572);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58650046912690366) == 27101810641024206);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10460931457854806) == 5193679165994514);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26142846662061482) == 3466491331786356);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64198770815246437) == 22633107699830809);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63684924570495597) == 32053546640151934);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27400021173170851) == 14610962277372280);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69985482777851649) == 34992743534956816);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2958629734501955) == 1657407562712336);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15919280671746851) == 5701799474631960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70034610971387641) == 25975445946044788);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41953578058458160) == 18296718530540048);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36704534171460786) == 18350031554982512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36360066778450361) == 27144899296429132);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44984426068341234) == 30689636738347256);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26201109303201182) == 10208832325330550);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20201007109818064) == 528665987187568);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9636207232718556) == 14065522237047622);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38913698785809205) == 28703194565902618);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21390665741778612) == 10689253076121458);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51585405396285771) == 32018712758227996);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36925968149053911) == 27571316756807802);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70170458776980007) == 71426300419245338);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17306375646769362) == 14945145091207416);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30074261272354467) == 15736834328266096);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23147660632009866) == 519883454232132);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9857996979447644) == 14038995774092102);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51020955434487398) == 34273595824219922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10066459905124219) == 14040287303635220);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42193758266727919) == 27113906822251589);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3737511148302252) == 10242086240991094);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31851509387320783) == 15851552388908150);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9862163375039191) == 14601823327326586);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53033284245749801) == 71020304817618972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68604295003468193) == 70328747367960696);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12559344221876969) == 14752523342451068);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61234310721155972) == 28218448071274874);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5902316236497904) == 2349181777621368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70034227749350114) == 32723395038492152);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11925851092894582) == 6167810351211002);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27375862299134477) == 4613111793648926);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8612713961775441) == 12886902338162712);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40265292739161265) == 28464713372472432);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5138879695366156) == 11540917723231262);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71392929527562032) == 34992607257769744);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27027534399112328) == 13616987880391236);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71467510070362964) == 32740712418017562);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11476912021033820) == 14743428191388030);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55752561730682838) == 27560172973274946);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19418902794249035) == 9139902092349724);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16637144309639285) == 17391761401812090);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14222278824265847) == 13793942865806410);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9787676191831019) == 14038871038202236);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46352692112807003) == 31648658395234324);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66825755559871650) == 69789163110343280);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21207596844549092) == 2226071554392442);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37771765435307770) == 27576389838676804);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55295837972869557) == 27155331466465402);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60473830033167451) == 65804473181437972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18750470118159134) == 9544521296097694);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51566122772527337) == 34082132167365708);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22570654931797540) == 9097183186191122);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15229486895947902) == 16265700499239070);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 42333011422449906) == 27532671429091528);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32455986152165089) == 5000357694244160);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37594547820774512) == 18336026331333136);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49293276196277337) == 33189132358190100);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52129902661822711) == 34996799677043834);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7434728607362311) == 12684959497065490);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21886683599456136) == 10239619719529332);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 14874355325780831) == 6834640582869278);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2703974540050147) == 11236051064198512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30204412205037106) == 15166393953465104);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27530695583100375) == 13618034574038130);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10542515893519501) == 4635710158504054);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6133823815274855) == 11333626277232666);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9688797725008633) == 14620717238686076);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4636497196575129) == 2348987408684108);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31401205224028469) == 15166216339362842);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52060496543858114) == 32748266328536688);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 60541261040163251) == 27524011921933432);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2417184587432497) == 1244334709932312);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8210393811196277) == 12474677234926618);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31210877286128000) == 15186015589500024);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15381042050796958) == 5014285966976630);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27379781576779037) == 13617318455300118);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 59742961208099480) == 27120377926746492);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 33368549953950906) == 13793106567766644);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10053556255200310) == 14066404116210194);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38914573139375884) == 28676120170148630);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 50768670266683014) == 31605328669783922);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32049681291106230) == 13590555959665522);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57164472839992816) == 28470646716576368);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47071417352421094) == 68675056322393921);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6627704506109431) == 11542429333668978);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49111693085539019) == 69798926644876668);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46301773759590997) == 31658315282682138);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26443967690419660) == 3476456193635958);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61981616895721550) == 30498190865997982);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35719751868171472) == 6141697475773040);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55656374692358853) == 64169237211845961);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11537289866636323) == 14768707831288848);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20955475608109809) == 1682078539972984);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40010690983362241) == 28693722569217392);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49887230097942691) == 23037654289224824);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24728780967113521) == 11762377355989272);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 32824543470376301) == 13590877475536917);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46207735024426528) == 68240440916608784);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52530774781581171) == 35413169381736816);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16013624311894025) == 14725240082038812);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37451872994532382) == 27339362010621462);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 22658435949232703) == 9105981570908446);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66096310462146149) == 24182211008237938);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 6836998511920483) == 3449270549938288);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9123279889019834) == 14197209960428028);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 48056061251732630) == 60794058479444594);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34711924716246545) == 17391694441808144);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9409183680238193) == 13634252035817752);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31002852389855040) == 15182980139714320);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65900483496401525) == 33197081663112474);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5542333058388191) == 11778333035200590);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53069456791627338) == 61996175265248020);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29104039274299852) == 14057067201331277);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27561465584494847) == 14409657704617086);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 71790126502028842) == 24137405563286292);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34433711567519956) == 14927523993845882);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 52168902350519977) == 32731915907634548);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58344664234451701) == 56260462817284474);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 54813226555377552) == 18569108178436976);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 704359869292051) == 9535822986773776);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 30296637551198556) == 6176857768329342);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 10242689971473993) == 13635406688257300);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 56568363305904953) == 55864887253374236);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38734382545101144) == 19255023504371836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 38982923956969708) == 19485412481773694);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47779042552636572) == 23983952163582582);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 127808543216636) == 9139792499868030);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 7690056586586543) == 3669078372815998);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5521988945311921) == 9510326827783288);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 20588281015365770) == 10679381489758460);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27196722569634820) == 13624740695536146);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 37309759447077202) == 27127707837112472);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 2254245469447396) == 1235437211267698);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66660863579809018) == 32767447442797172);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45194482039904555) == 31640172076827676);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19542979340263156) == 9132082141628226);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 65299720240248894) == 32080312792462494);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 66865714283646394) == 23751107637782132);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 35875572413173067) == 6149370106119188);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69925343928129030) == 32739985419215634);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26302916850962299) == 3476273600693524);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 1848649249920507) == 9328260957945972);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 4621369002796694) == 11348092406510450);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3098009166027985) == 10450615922918512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27483776122350136) == 4627462797899036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64678800645874276) == 68645267827536154);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23216317108560373) == 11568531140481098);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26260674693182289) == 12457404153139480);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 36912536276290607) == 27357778817729558);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17182344880846412) == 17384520701817622);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34819240559916901) == 17409491026710802);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41633636081409485) == 65601543558497349);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 72006141748861072) == 35404899873653880);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 70393248550126919) == 23931451157446674);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 45315627797184600) == 22651182603305084);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 21889226230648220) == 1232113380427382);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15267754500346588) == 14030116351181182);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 18310945986106074) == 9113665605476812);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26940877559321402) == 10654000920011164);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 28688430261358567) == 14201254693438834);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 61255778295688760) == 28650967373494036);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 34568802135034314) == 8383327895856380);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 49680111139911880) == 31623496850388092);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58101776588096001) == 65286393498108184);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 19204635810483588) == 692424985085554);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 13161183583864586) == 6158372882064148);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51540402786123587) == 70311430152033552);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 29085487432388464) == 14047944156579600);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 69929945451900329) == 25993378445726836);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 12286019234402212) == 6176514760476530);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 41647088216325848) == 27540859807086404);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 23844955313602551) == 9106035396384066);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58744967394307502) == 18111807308478070);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 16961889191870404) == 16960893498046322);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 31234361408559697) == 15192601443927320);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55656699353114164) == 63749357778121490);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 27351652358563222) == 4627397249282674);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 11397787711055661) == 5763191361310998);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 26392820618018488) == 10626340399429492);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 58343364524712978) == 19670087138914832);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 44882024555986780) == 30922052135204630);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 47228553312502504) == 59639179715084620);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53054961773578767) == 32748515227010334);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5351030664818576) == 2748091410986864);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 57282825156598159) == 28677148954592382);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 15588016061447872) == 16272848864032576);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 40563433099193416) == 18094214107608692);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 3019540589247203) == 11251857613526392);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 68247703158001266) == 23041315232818960);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 46350741654164498) == 68241605807677968);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 43000753961407363) == 28218214517211512);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 53816839853959397) == 26416392330712186);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55787780748324043) == 18572786555917380);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9046931161850089) == 4627371411804236);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 8448945007809976) == 10415266237317748);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 9421815191634671) == 14205723212611966);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 64811417704561273) == 32053862841816348);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 5326507268265944) == 9518021261722484);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 17138400090684296) == 16977590251959676);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 24086958271325751) == 2332963507114258);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 51197417724633998) == 33866337967026686);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 63797811262112335) == 32072509912023422);
	CHECK(atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, 55631296618855073) == 27145311143335240);

}
















TEST_CASE("piemers_ar_12x4_pirmie_336_cikli", "[musha] [musha12]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;//128

    int musas_sunu_skaits=12;


    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;


    stavoklis cur_stavoklis(musas_sunu_skaits/4);





    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);





    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065567821;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065567821 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16962234065567821;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16962234065567821 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634872909;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634872909 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634872909;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634872909 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065567821;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065567821 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161347519774797;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161347519774797 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298100813;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298100813 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161347519774797;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161347519774797 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17173340298100813;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17173340298100813 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17173340298100813;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17173340298100813 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161334634872909;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161334634872909 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298100813;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298100813 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519774797;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519774797 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16962234065567821;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16962234065567821 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519774797;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519774797 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161347519774797;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161347519774797 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634872909;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634872909 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17173340298100813;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17173340298100813 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16962234065567821;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16962234065567821 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065567821;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065567821 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161347519774797;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161347519774797 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17173340298100813;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17173340298100813 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065567821;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065567821 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16962234065567821;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16962234065567821 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065567821;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065567821 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 55161334634872909;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 55161334634872909 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17173340298100813;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17173340298100813 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634872909;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634872909 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065567821;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065567821 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16962234065567821;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16962234065567821 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161334634872909;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161334634872909 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634872909;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634872909 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17173340298100813;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17173340298100813 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161334634872909;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161334634872909 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16962234065567821;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16962234065567821 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17173340298100813;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17173340298100813 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252159230;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252159230 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 55161334634872909;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 55161334634872909 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519774797;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519774797 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161347519787262;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161347519787262 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634872909;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634872909 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17173340298100813;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17173340298100813 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161347519774797;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161347519774797 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065567821;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065567821 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17173340298100813;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17173340298100813 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 55161334634885374;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 55161334634885374 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519774797;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519774797 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 55161347519774797;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 55161347519774797 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16962234065567821;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16962234065567821 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634872909;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634872909 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298100813;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298100813 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17170041763217485;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17170041763217485 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634872909;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634872909 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 55161347519787262;
	cur_stavoklis.skaitli[2] = 16959902639423565;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 55161347519787262 == cur_stavoklis.skaitli[1] && 16959902639423565 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298100813;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252159230;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298100813 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252159230 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763217485;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763217485 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 16958935530684493;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 16958935530684493 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 55161347519774797;
	cur_stavoklis.skaitli[2] = 1118151991341310;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 55161347519774797 == cur_stavoklis.skaitli[1] && 1118151991341310 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16959902639423565;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639423565 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118139106439422;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118139106439422 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 55161334634885374;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 55161334634885374 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118139106439422;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118139106439422 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 55161347519774797;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 55161347519774797 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106439422;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106439422 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519774797;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763217485;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519774797 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763217485 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634885374;
	cur_stavoklis.skaitli[1] = 16959902639423565;
	cur_stavoklis.skaitli[2] = 55161334634872909;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634885374 == cur_stavoklis.skaitli[0] && 16959902639423565 == cur_stavoklis.skaitli[1] && 55161334634872909 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530684493;
	cur_stavoklis.skaitli[1] = 1118151991341310;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530684493 == cur_stavoklis.skaitli[0] && 1118151991341310 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519787262;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 17173340298100813;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519787262 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 17173340298100813 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065567821;
	cur_stavoklis.skaitli[1] = 338670252159230;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065567821 == cur_stavoklis.skaitli[0] && 338670252159230 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991341310;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 16958935530684493;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991341310 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 16958935530684493 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );

}


TEST_CASE("piemers_ar_12x4_10vienad_sakuma_cikli", "[musha] [musha12]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;//128

    int musas_sunu_skaits=12;


    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;


    stavoklis cur_stavoklis(musas_sunu_skaits/4);





    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);





    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);


	cur_stavoklis.skaitli[0] = 16959902639436030;
	cur_stavoklis.skaitli[1] = 16959902639436030;
	cur_stavoklis.skaitli[2] = 16959902639436030;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16959902639436030 == cur_stavoklis.skaitli[0] && 16959902639436030 == cur_stavoklis.skaitli[1] && 16959902639436030 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 338670252146765;
	cur_stavoklis.skaitli[1] = 338670252146765;
	cur_stavoklis.skaitli[2] = 338670252146765;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (338670252146765 == cur_stavoklis.skaitli[0] && 338670252146765 == cur_stavoklis.skaitli[1] && 338670252146765 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16958935530696958;
	cur_stavoklis.skaitli[1] = 16958935530696958;
	cur_stavoklis.skaitli[2] = 16958935530696958;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16958935530696958 == cur_stavoklis.skaitli[0] && 16958935530696958 == cur_stavoklis.skaitli[1] && 16958935530696958 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17170041763229950;
	cur_stavoklis.skaitli[1] = 17170041763229950;
	cur_stavoklis.skaitli[2] = 17170041763229950;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17170041763229950 == cur_stavoklis.skaitli[0] && 17170041763229950 == cur_stavoklis.skaitli[1] && 17170041763229950 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118151991328845;
	cur_stavoklis.skaitli[1] = 1118151991328845;
	cur_stavoklis.skaitli[2] = 1118151991328845;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118151991328845 == cur_stavoklis.skaitli[0] && 1118151991328845 == cur_stavoklis.skaitli[1] && 1118151991328845 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 17173340298113278;
	cur_stavoklis.skaitli[1] = 17173340298113278;
	cur_stavoklis.skaitli[2] = 17173340298113278;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (17173340298113278 == cur_stavoklis.skaitli[0] && 17173340298113278 == cur_stavoklis.skaitli[1] && 17173340298113278 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 1118139106426957;
	cur_stavoklis.skaitli[1] = 1118139106426957;
	cur_stavoklis.skaitli[2] = 1118139106426957;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (1118139106426957 == cur_stavoklis.skaitli[0] && 1118139106426957 == cur_stavoklis.skaitli[1] && 1118139106426957 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161347519774797;
	cur_stavoklis.skaitli[1] = 55161347519774797;
	cur_stavoklis.skaitli[2] = 55161347519774797;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161347519774797 == cur_stavoklis.skaitli[0] && 55161347519774797 == cur_stavoklis.skaitli[1] && 55161347519774797 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 55161334634872909;
	cur_stavoklis.skaitli[1] = 55161334634872909;
	cur_stavoklis.skaitli[2] = 55161334634872909;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (55161334634872909 == cur_stavoklis.skaitli[0] && 55161334634872909 == cur_stavoklis.skaitli[1] && 55161334634872909 == cur_stavoklis.skaitli[2]) );
	cur_stavoklis.skaitli[0] = 16962234065580286;
	cur_stavoklis.skaitli[1] = 16962234065580286;
	cur_stavoklis.skaitli[2] = 16962234065580286;
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	CHECK( (16962234065580286 == cur_stavoklis.skaitli[0] && 16962234065580286 == cur_stavoklis.skaitli[1] && 16962234065580286 == cur_stavoklis.skaitli[2]) );

}



















TEST_CASE("piemers_ar_12x4__200_generetie_cikli", "[musha] [musha12]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;//128

    int musas_sunu_skaits=12;


    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;


    stavoklis cur_stavoklis(musas_sunu_skaits/4);

    ll cur_stav_sk[32];





    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);





    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, musas_skaitla_robeza-1);







	bool ok;

	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28490137882854684;
	cur_stav_sk[1] = 9125827343978769;
	cur_stav_sk[2] = 32035481990870136;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32018361194152218;
	cur_stav_sk[1] = 9140589354595580;
	cur_stav_sk[2] = 14744277590902142;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 116496963207490;
	cur_stav_sk[1] = 9123341166097822;
	cur_stav_sk[2] = 27129163829706780;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1205988168040830;
	cur_stav_sk[1] = 1117728876102988;
	cur_stav_sk[2] = 27111582857101596;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 13854155374630174;
	cur_stav_sk[1] = 28676325319836956;
	cur_stav_sk[2] = 27568265496998940;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28218801126117650;
	cur_stav_sk[1] = 27153915059148148;
	cur_stav_sk[2] = 15166420745024116;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 62417699623768434;
	cur_stav_sk[1] = 9676533608706674;
	cur_stav_sk[2] = 5728282196878964;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 2534945217278106;
	cur_stav_sk[1] = 32968587925192826;
	cur_stav_sk[2] = 14962597374865660;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 8384330890454896;
	cur_stav_sk[1] = 7274401837815166;
	cur_stav_sk[2] = 32045093592338708;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32748437587067922;
	cur_stav_sk[1] = 27155276490901009;
	cur_stav_sk[2] = 9104868088156446;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16070329950311705;
	cur_stav_sk[1] = 538243697152272;
	cur_stav_sk[2] = 27576390586930204;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27515490622313586;
	cur_stav_sk[1] = 69899834769023230;
	cur_stav_sk[2] = 13591974923539484;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 290975665770522;
	cur_stav_sk[1] = 28262155254346188;
	cur_stav_sk[2] = 125101868790294;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1680705008931098;
	cur_stav_sk[1] = 27131498622390593;
	cur_stav_sk[2] = 32037371455124080;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11778800253994104;
	cur_stav_sk[1] = 33198180117787456;
	cur_stav_sk[2] = 9562402372494100;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14051159056487548;
	cur_stav_sk[1] = 34973951463306098;
	cur_stav_sk[2] = 21915332757636112;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10802527805375252;
	cur_stav_sk[1] = 1205965697679480;
	cur_stav_sk[2] = 6131638673540378;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10239303053752082;
	cur_stav_sk[1] = 10805593554421360;
	cur_stav_sk[2] = 32757437616198682;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31648416333661556;
	cur_stav_sk[1] = 64143202056209940;
	cur_stav_sk[2] = 70113725131821432;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 25966853660443248;
	cur_stav_sk[1] = 11568817755214878;
	cur_stav_sk[2] = 5706083127264628;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 502700504813848;
	cur_stav_sk[1] = 64168328886629232;
	cur_stav_sk[2] = 5753271509365020;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28446596527981896;
	cur_stav_sk[1] = 9526450684560400;
	cur_stav_sk[2] = 549717391807510;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31642980500473200;
	cur_stav_sk[1] = 5710688515720212;
	cur_stav_sk[2] = 16986291334086678;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15174983825172338;
	cur_stav_sk[1] = 33170964650754934;
	cur_stav_sk[2] = 16281919299597694;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28430930638189688;
	cur_stav_sk[1] = 10239612905367060;
	cur_stav_sk[2] = 20787178410247292;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10438496587265102;
	cur_stav_sk[1] = 31608345825412218;
	cur_stav_sk[2] = 28246281010578036;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 17400603817191286;
	cur_stav_sk[1] = 9130651861488914;
	cur_stav_sk[2] = 12464892764033298;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32722527658639684;
	cur_stav_sk[1] = 63746871600252272;
	cur_stav_sk[2] = 14772656127378192;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9562554791563376;
	cur_stav_sk[1] = 32775168856621338;
	cur_stav_sk[2] = 1205965846053654;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 71231318190228812;
	cur_stav_sk[1] = 31597221523035412;
	cur_stav_sk[2] = 5728255340720758;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 34991751022546974;
	cur_stav_sk[1] = 65584774680318322;
	cur_stav_sk[2] = 27558795649233052;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15843515365721368;
	cur_stav_sk[1] = 29362190285894260;
	cur_stav_sk[2] = 30919166984468240;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 17390733293958002;
	cur_stav_sk[1] = 23732552285890320;
	cur_stav_sk[2] = 31649187797763448;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5595309141921818;
	cur_stav_sk[1] = 15860417880724764;
	cur_stav_sk[2] = 12477219924186392;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 3664430686480242;
	cur_stav_sk[1] = 19265469544173900;
	cur_stav_sk[2] = 18552891533398140;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10251189892555376;
	cur_stav_sk[1] = 10419004952675608;
	cur_stav_sk[2] = 27525041283334520;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 25280323027505522;
	cur_stav_sk[1] = 19668687533122164;
	cur_stav_sk[2] = 30922252388598042;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31658943278293616;
	cur_stav_sk[1] = 28433437288305686;
	cur_stav_sk[2] = 33852731646149234;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 33163293764334196;
	cur_stav_sk[1] = 28650600153752730;
	cur_stav_sk[2] = 59427511507170066;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28448246380990844;
	cur_stav_sk[1] = 24181291562145150;
	cur_stav_sk[2] = 14707753140260976;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 81175647163510;
	cur_stav_sk[1] = 60360745739845656;
	cur_stav_sk[2] = 29362202496270656;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14047414990479610;
	cur_stav_sk[1] = 19696202494711162;
	cur_stav_sk[2] = 24190730147037558;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5052859599397278;
	cur_stav_sk[1] = 60345489950180732;
	cur_stav_sk[2] = 27111639138284877;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 60888717346710780;
	cur_stav_sk[1] = 68231326536630288;
	cur_stav_sk[2] = 1425660317905426;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11761274024890486;
	cur_stav_sk[1] = 9078054962275954;
	cur_stav_sk[2] = 10806865736537464;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10099982149263772;
	cur_stav_sk[1] = 9527230898550010;
	cur_stav_sk[2] = 11559474112632188;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10214015831572510;
	cur_stav_sk[1] = 13833251405477700;
	cur_stav_sk[2] = 22633728800751992;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9096002015609884;
	cur_stav_sk[1] = 22615448345575546;
	cur_stav_sk[2] = 32933401407160645;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 7257333030871568;
	cur_stav_sk[1] = 32937827239437464;
	cur_stav_sk[2] = 23753193169061701;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27514403939529882;
	cur_stav_sk[1] = 32734622758872948;
	cur_stav_sk[2] = 16256045495288388;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 33153650952372552;
	cur_stav_sk[1] = 63749271355983224;
	cur_stav_sk[2] = 22632998105421176;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 12881369296250364;
	cur_stav_sk[1] = 23768769052418460;
	cur_stav_sk[2] = 538147681075574;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1258465370996758;
	cur_stav_sk[1] = 28243497729196410;
	cur_stav_sk[2] = 10230747936863860;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1233408860886085;
	cur_stav_sk[1] = 70299298874462492;
	cur_stav_sk[2] = 28640696354226196;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 12905619212472700;
	cur_stav_sk[1] = 23074164273686390;
	cur_stav_sk[2] = 19211250740298774;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28263803460751734;
	cur_stav_sk[1] = 29345790331458832;
	cur_stav_sk[2] = 35405173346349942;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32039330283066490;
	cur_stav_sk[1] = 27559621892211832;
	cur_stav_sk[2] = 28694742021506381;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 23732303169995840;
	cur_stav_sk[1] = 14047985501308018;
	cur_stav_sk[2] = 12464174013911412;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 65394045162198128;
	cur_stav_sk[1] = 11769810603909529;
	cur_stav_sk[2] = 56385022829508470;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14708448386551832;
	cur_stav_sk[1] = 31600545082081910;
	cur_stav_sk[2] = 13603728017756534;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 30918926322172024;
	cur_stav_sk[1] = 18150674605225744;
	cur_stav_sk[2] = 12878103160730228;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31635981258340112;
	cur_stav_sk[1] = 27523325849175164;
	cur_stav_sk[2] = 57518577923503310;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9140876102578806;
	cur_stav_sk[1] = 5972589106398320;
	cur_stav_sk[2] = 10248340664026644;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11544490448217242;
	cur_stav_sk[1] = 35405450234957852;
	cur_stav_sk[2] = 71219221472949824;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31846134595404872;
	cur_stav_sk[1] = 27145325056071030;
	cur_stav_sk[2] = 5182497877168198;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9887412554739866;
	cur_stav_sk[1] = 13643663921886834;
	cur_stav_sk[2] = 31650159673909404;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28688293790778128;
	cur_stav_sk[1] = 14039464802226462;
	cur_stav_sk[2] = 64141965046862876;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1249628465923102;
	cur_stav_sk[1] = 60561682981589317;
	cur_stav_sk[2] = 29363290794885502;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27558523506242590;
	cur_stav_sk[1] = 19445720742299460;
	cur_stav_sk[2] = 65796802028503920;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15138761361297782;
	cur_stav_sk[1] = 11339098602410098;
	cur_stav_sk[2] = 28244740027626358;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 20347029877163134;
	cur_stav_sk[1] = 14711057562304794;
	cur_stav_sk[2] = 9116643033653777;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28466086014293321;
	cur_stav_sk[1] = 4626845339062640;
	cur_stav_sk[2] = 12878415415673968;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 107483865364336;
	cur_stav_sk[1] = 10636432905699348;
	cur_stav_sk[2] = 14752935320134416;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14954250631483510;
	cur_stav_sk[1] = 33849296161715657;
	cur_stav_sk[2] = 9512534442608246;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 25990149779658102;
	cur_stav_sk[1] = 28641485551150866;
	cur_stav_sk[2] = 1637066717941778;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16971748513616244;
	cur_stav_sk[1] = 6307284582379928;
	cur_stav_sk[2] = 2244110427455864;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1198017311356158;
	cur_stav_sk[1] = 24173140041466896;
	cur_stav_sk[2] = 33877713657532542;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 65268071649841524;
	cur_stav_sk[1] = 16967798741575162;
	cur_stav_sk[2] = 10204186929399412;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 3872346881467646;
	cur_stav_sk[1] = 33858323642450296;
	cur_stav_sk[2] = 14022721008440602;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 19463011606659654;
	cur_stav_sk[1] = 70326762621305924;
	cur_stav_sk[2] = 24857764791010328;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11769616598533396;
	cur_stav_sk[1] = 3457696176226330;
	cur_stav_sk[2] = 1664488491136408;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10225765767418181;
	cur_stav_sk[1] = 34271577948619804;
	cur_stav_sk[2] = 16969593507971198;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 18129110653808716;
	cur_stav_sk[1] = 13832688654956054;
	cur_stav_sk[2] = 14049233900810440;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10221641665557916;
	cur_stav_sk[1] = 15755826069701654;
	cur_stav_sk[2] = 20769723193405944;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14725515699422494;
	cur_stav_sk[1] = 32072005788668182;
	cur_stav_sk[2] = 28693206032396918;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11357713211033460;
	cur_stav_sk[1] = 33206934334160918;
	cur_stav_sk[2] = 71231319107310968;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 64169237320828024;
	cur_stav_sk[1] = 14717589219358204;
	cur_stav_sk[2] = 28676988379856962;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10661695841187348;
	cur_stav_sk[1] = 13829524714455416;
	cur_stav_sk[2] = 23767051413534576;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10257076687017242;
	cur_stav_sk[1] = 11770014563697168;
	cur_stav_sk[2] = 12483406220691220;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 33857831711281180;
	cur_stav_sk[1] = 9087016478454142;
	cur_stav_sk[2] = 64845449143779652;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28271302806536478;
	cur_stav_sk[1] = 677949856462334;
	cur_stav_sk[2] = 27154191623131506;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5621637601174388;
	cur_stav_sk[1] = 5744609020215324;
	cur_stav_sk[2] = 13590463361975580;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 70325046801215512;
	cur_stav_sk[1] = 27551376076604740;
	cur_stav_sk[2] = 4618447071707416;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15192234242553712;
	cur_stav_sk[1] = 27146413273211084;
	cur_stav_sk[2] = 54923373461340944;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 69772335559086458;
	cur_stav_sk[1] = 19677208211390540;
	cur_stav_sk[2] = 14202822738817532;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27568678509712410;
	cur_stav_sk[1] = 11335435059151990;
	cur_stav_sk[2] = 32942749284369692;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 12895630181467418;
	cur_stav_sk[1] = 9131793593078142;
	cur_stav_sk[2] = 3475655782535550;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 35189877234771830;
	cur_stav_sk[1] = 28658014268569206;
	cur_stav_sk[2] = 29362201549824250;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16044904145883154;
	cur_stav_sk[1] = 18104038793261564;
	cur_stav_sk[2] = 10662520537834048;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 13618364417376540;
	cur_stav_sk[1] = 10652855774262134;
	cur_stav_sk[2] = 32037396959040540;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 65822090747249690;
	cur_stav_sk[1] = 18516950701125918;
	cur_stav_sk[2] = 18561425954914368;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 12483105571288478;
	cur_stav_sk[1] = 10099568910205048;
	cur_stav_sk[2] = 32990604805734782;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 29767179261781502;
	cur_stav_sk[1] = 12879436686631326;
	cur_stav_sk[2] = 15191711329424508;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 21485286141527318;
	cur_stav_sk[1] = 502828672390260;
	cur_stav_sk[2] = 12684797911503894;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 6836576631751696;
	cur_stav_sk[1] = 63957524110574712;
	cur_stav_sk[2] = 14953948834080204;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27551156041757898;
	cur_stav_sk[1] = 9906057730011970;
	cur_stav_sk[2] = 18560242346854160;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10467655640371216;
	cur_stav_sk[1] = 11017689224951670;
	cur_stav_sk[2] = 32039442911072282;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1197235020538641;
	cur_stav_sk[1] = 72119645700124;
	cur_stav_sk[2] = 10626578087948536;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 64871494221300041;
	cur_stav_sk[1] = 16256024957105560;
	cur_stav_sk[2] = 23065354680869744;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11233537107435674;
	cur_stav_sk[1] = 27568956522860662;
	cur_stav_sk[2] = 5726150798479732;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 8392164770140180;
	cur_stav_sk[1] = 65372857164562704;
	cur_stav_sk[2] = 13603026142994936;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11356408549147156;
	cur_stav_sk[1] = 33170417591877966;
	cur_stav_sk[2] = 14189903013549082;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 65603219891762428;
	cur_stav_sk[1] = 65391259547861620;
	cur_stav_sk[2] = 31622535338886268;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 71424652761044350;
	cur_stav_sk[1] = 8374185584197648;
	cur_stav_sk[2] = 27127708632593268;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10204324236334108;
	cur_stav_sk[1] = 13600703555449620;
	cur_stav_sk[2] = 23056102784340244;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 13616440112583954;
	cur_stav_sk[1] = 1637521853191294;
	cur_stav_sk[2] = 61107014978466320;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 20777007392391542;
	cur_stav_sk[1] = 32732011472651390;
	cur_stav_sk[2] = 19479640771932693;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 22852830792388880;
	cur_stav_sk[1] = 9526654285907220;
	cur_stav_sk[2] = 6298585319604294;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32942087382044749;
	cur_stav_sk[1] = 13634020099072882;
	cur_stav_sk[2] = 11755846259356826;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32761879678367224;
	cur_stav_sk[1] = 17383241469966198;
	cur_stav_sk[2] = 14021657339101304;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 65390814877778040;
	cur_stav_sk[1] = 27558645392712188;
	cur_stav_sk[2] = 65293429106717852;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10688153981423890;
	cur_stav_sk[1] = 12897029413713780;
	cur_stav_sk[2] = 18507753903785244;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 26390302550144628;
	cur_stav_sk[1] = 23751405997078290;
	cur_stav_sk[2] = 11251857288877076;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 64151920298099828;
	cur_stav_sk[1] = 32987305192064017;
	cur_stav_sk[2] = 32063772619158858;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28262745208296522;
	cur_stav_sk[1] = 70325951539937660;
	cur_stav_sk[2] = 10230756068921616;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 68645084704092670;
	cur_stav_sk[1] = 9122415045378674;
	cur_stav_sk[2] = 32774796723094908;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 65286763543041404;
	cur_stav_sk[1] = 2216621888582780;
	cur_stav_sk[2] = 10653029129041524;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28258030891999642;
	cur_stav_sk[1] = 21472188299674748;
	cur_stav_sk[2] = 15309536237622294;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 33858255374585974;
	cur_stav_sk[1] = 14937447436014610;
	cur_stav_sk[2] = 28248240052502896;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 128510275490934;
	cur_stav_sk[1] = 32954567895265660;
	cur_stav_sk[2] = 15182364188820596;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9113665060319644;
	cur_stav_sk[1] = 28279996548059508;
	cur_stav_sk[2] = 32748780446982652;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5740281463063570;
	cur_stav_sk[1] = 5000139812808308;
	cur_stav_sk[2] = 25272924921246972;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 29793005097269061;
	cur_stav_sk[1] = 10117436160749948;
	cur_stav_sk[2] = 19219881010888724;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 1094719518425112;
	cur_stav_sk[1] = 32054012020757620;
	cur_stav_sk[2] = 19267333033763274;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16981415405371422;
	cur_stav_sk[1] = 28685020029658364;
	cur_stav_sk[2] = 30487427754205814;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 59233507297604892;
	cur_stav_sk[1] = 27110245577168252;
	cur_stav_sk[2] = 9686249176437784;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9087812121374840;
	cur_stav_sk[1] = 1425549264421240;
	cur_stav_sk[2] = 3476316678784280;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 13832413618439454;
	cur_stav_sk[1] = 28670699866853392;
	cur_stav_sk[2] = 30491216831754440;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 71442494143137144;
	cur_stav_sk[1] = 10265665682149656;
	cur_stav_sk[2] = 15165526911128860;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15310292146029588;
	cur_stav_sk[1] = 10257102472187926;
	cur_stav_sk[2] = 23965785647382544;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 13634992225490204;
	cur_stav_sk[1] = 29345243417903224;
	cur_stav_sk[2] = 31601647503618834;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 69578089554936856;
	cur_stav_sk[1] = 28262607303840884;
	cur_stav_sk[2] = 32062532177335316;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16256081061511288;
	cur_stav_sk[1] = 19215897967142392;
	cur_stav_sk[2] = 27523391679431753;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28263569315828757;
	cur_stav_sk[1] = 9536165889512820;
	cur_stav_sk[2] = 9095797989507198;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 33207208071349324;
	cur_stav_sk[1] = 16046030036219388;
	cur_stav_sk[2] = 33208787753959796;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28272159119021172;
	cur_stav_sk[1] = 27128326768722294;
	cur_stav_sk[2] = 65812422031839504;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 23930328368612628;
	cur_stav_sk[1] = 28280339940488822;
	cur_stav_sk[2] = 29784197524587640;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 23768723823596920;
	cur_stav_sk[1] = 10265692861268756;
	cur_stav_sk[2] = 17391808120554768;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28448656499998797;
	cur_stav_sk[1] = 2554782539855004;
	cur_stav_sk[2] = 57519126197149948;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 19696201961997594;
	cur_stav_sk[1] = 3475862272909468;
	cur_stav_sk[2] = 14924664020304924;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 128938223800446;
	cur_stav_sk[1] = 66947723896389914;
	cur_stav_sk[2] = 15859635995378716;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27366052744954642;
	cur_stav_sk[1] = 11348904547156850;
	cur_stav_sk[2] = 19219496762226288;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 63934557086691406;
	cur_stav_sk[1] = 65391602954080272;
	cur_stav_sk[2] = 17390937308964420;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5005532406973772;
	cur_stav_sk[1] = 554840524568178;
	cur_stav_sk[2] = 24154695905718604;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 32767443672110364;
	cur_stav_sk[1] = 2746381672649029;
	cur_stav_sk[2] = 68655036457951646;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 68240987989607748;
	cur_stav_sk[1] = 14619950565031966;
	cur_stav_sk[2] = 6150466326856984;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15834624509227792;
	cur_stav_sk[1] = 30901507542089793;
	cur_stav_sk[2] = 9562253059437948;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5013849472504184;
	cur_stav_sk[1] = 10673370301895804;
	cur_stav_sk[2] = 20355060678463509;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31650356497027348;
	cur_stav_sk[1] = 32062725660390160;
	cur_stav_sk[2] = 11233328813342784;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 59242256125034778;
	cur_stav_sk[1] = 14716969665168413;
	cur_stav_sk[2] = 9078136613377096;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 11540481578500880;
	cur_stav_sk[1] = 31596009126007924;
	cur_stav_sk[2] = 28487139395191824;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9535338639237624;
	cur_stav_sk[1] = 14760494327869972;
	cur_stav_sk[2] = 15851402782188048;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 19678585277088065;
	cur_stav_sk[1] = 9526585372770328;
	cur_stav_sk[2] = 29776800515437330;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 7961387693183088;
	cur_stav_sk[1] = 28278956507858296;
	cur_stav_sk[2] = 14012504758826608;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9347243048255508;
	cur_stav_sk[1] = 27137147448131964;
	cur_stav_sk[2] = 18123922324922524;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9553824578896248;
	cur_stav_sk[1] = 33205832594657392;
	cur_stav_sk[2] = 14048421417976694;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9677727944552856;
	cur_stav_sk[1] = 34296811813540930;
	cur_stav_sk[2] = 11347551649006878;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 19686832482296436;
	cur_stav_sk[1] = 5928574349935896;
	cur_stav_sk[2] = 17382621728081272;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 22645491625734650;
	cur_stav_sk[1] = 14728919458460106;
	cur_stav_sk[2] = 56791841480946496;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 18560131707384984;
	cur_stav_sk[1] = 3659728911963508;
	cur_stav_sk[2] = 63746280021013364;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14181244619657496;
	cur_stav_sk[1] = 28694813478323472;
	cur_stav_sk[2] = 14735522836943124;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 14725077563306014;
	cur_stav_sk[1] = 5701869282242589;
	cur_stav_sk[2] = 9329122170246428;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 3686670571470972;
	cur_stav_sk[1] = 13608317190246772;
	cur_stav_sk[2] = 69376785063449624;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31657788490649982;
	cur_stav_sk[1] = 27516326601597764;
	cur_stav_sk[2] = 59647630724544020;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 6123873020281116;
	cur_stav_sk[1] = 35413996077482008;
	cur_stav_sk[2] = 10467392104564084;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9685673781396762;
	cur_stav_sk[1] = 17400558580925722;
	cur_stav_sk[2] = 15165844338540873;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 5023745485602892;
	cur_stav_sk[1] = 11353026640412954;
	cur_stav_sk[2] = 28650804509443190;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16281642425812246;
	cur_stav_sk[1] = 10221641071931768;
	cur_stav_sk[2] = 16968770157577029;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 69886090926302462;
	cur_stav_sk[1] = 18128823360787534;
	cur_stav_sk[2] = 29576629869356864;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28263296520168600;
	cur_stav_sk[1] = 20336643638887752;
	cur_stav_sk[2] = 70300261685797020;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 8375560965129490;
	cur_stav_sk[1] = 19689741303550994;
	cur_stav_sk[2] = 27101688724981118;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 3467382951445782;
	cur_stav_sk[1] = 6720771877246330;
	cur_stav_sk[2] = 21912448760348794;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 31650025797845014;
	cur_stav_sk[1] = 28696365590390270;
	cur_stav_sk[2] = 31650299737047420;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 34974327858729078;
	cur_stav_sk[1] = 10671661913538892;
	cur_stav_sk[2] = 13599661160140820;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27515834768445505;
	cur_stav_sk[1] = 16044216505607696;
	cur_stav_sk[2] = 11040754073702732;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 15175261381464188;
	cur_stav_sk[1] = 15166465497862518;
	cur_stav_sk[2] = 12895653819584624;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28456226900911352;
	cur_stav_sk[1] = 28255075881460244;
	cur_stav_sk[2] = 28486704202608796;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 54724224023291085;
	cur_stav_sk[1] = 20356088236736890;
	cur_stav_sk[2] = 17408597752328769;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 16977384565973778;
	cur_stav_sk[1] = 28257342224938441;
	cur_stav_sk[2] = 13646756613423218;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 546132090398158;
	cur_stav_sk[1] = 16282053851284600;
	cur_stav_sk[2] = 19246433572308084;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 13855503835795576;
	cur_stav_sk[1] = 9332673153508214;
	cur_stav_sk[2] = 28649567538054170;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 23073065426260242;
	cur_stav_sk[1] = 65383042747932922;
	cur_stav_sk[2] = 63728674289377862;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 18551609944806416;
	cur_stav_sk[1] = 2332689147658360;
	cur_stav_sk[2] = 28676163070923792;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 24858659974460180;
	cur_stav_sk[1] = 66500487873826932;
	cur_stav_sk[2] = 14972880446390642;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 29354012035756662;
	cur_stav_sk[1] = 10230231681859700;
	cur_stav_sk[2] = 6531267252103024;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 28482340906012696;
	cur_stav_sk[1] = 20337121722467440;
	cur_stav_sk[2] = 56052722357575932;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 27579811589568372;
	cur_stav_sk[1] = 24172767985689854;
	cur_stav_sk[2] = 64872840901788738;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 9563296187683186;
	cur_stav_sk[1] = 30497185226489929;
	cur_stav_sk[2] = 63957085012144960;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 71004060037035634;
	cur_stav_sk[1] = 14755282531746934;
	cur_stav_sk[2] = 13819505043211286;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);
	cur_stav_sk[0] = 10267064564941946;
	cur_stav_sk[1] = 1207091457163544;
	cur_stav_sk[2] = 10108379477775638;

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);

}


















TEST_CASE("piemers_ar_128_200_piemeru_sk", "[musha] [musha128]"){


    const int musas_genu_skaits=15;
    const int max_musas_sunu_skaits=128;//128

    int musas_sunu_skaits=128;


    ll musas_skaitla_robeza;

    suuna **visas_sunas;
    bool **musas_genu_vertibas;

    suuna cur_suuna;


    stavoklis cur_stavoklis(musas_sunu_skaits/4);

    ll cur_stav_sk[32];





    visas_sunas = new suuna*[musas_sunu_skaits];
    musas_genu_vertibas = new bool*[musas_sunu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) musas_genu_vertibas[i] = new bool[musas_genu_skaits];

    for(int i=0; i<musas_sunu_skaits; i++) visas_sunas[i] = (suuna*)(&musas_genu_vertibas[i][0]);





    //musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*2); // 2 ir sunu skaits 1 skaitli
    musas_skaitla_robeza = (ll)1 << ((musas_genu_skaits-1)*4); // 4 ir sunu skaits 1 skaitli


    aizpilda_musas_SLP(visas_sunas, musas_sunu_skaits);

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, musas_skaitla_robeza-1);







	bool ok;

	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28705642161898780;
	cur_stav_sk[1] = 9125827343978769;
	cur_stav_sk[2] = 32035481990870136;
	cur_stav_sk[3] = 31807254961619226;
	cur_stav_sk[4] = 9140589354595580;
	cur_stav_sk[5] = 14744277590902142;
	cur_stav_sk[6] = 9123696217948482;
	cur_stav_sk[7] = 9123341166097822;
	cur_stav_sk[8] = 27129163829706780;
	cur_stav_sk[9] = 1205988168040830;
	cur_stav_sk[10] = 1117728876102988;
	cur_stav_sk[11] = 27111582857101596;
	cur_stav_sk[12] = 14065261607163166;
	cur_stav_sk[13] = 28676325319836956;
	cur_stav_sk[14] = 27568265496998940;
	cur_stav_sk[15] = 28218801126117650;
	cur_stav_sk[16] = 27153915059148148;
	cur_stav_sk[17] = 15166420745024116;
	cur_stav_sk[18] = 62417699623768434;
	cur_stav_sk[19] = 9676533608706674;
	cur_stav_sk[20] = 5728282196878916;
	cur_stav_sk[21] = 2534945217278106;
	cur_stav_sk[22] = 32968587925192826;
	cur_stav_sk[23] = 14962597374865660;
	cur_stav_sk[24] = 8168826611410800;
	cur_stav_sk[25] = 7274401837815166;
	cur_stav_sk[26] = 32045093592338708;
	cur_stav_sk[27] = 23741238332326930;
	cur_stav_sk[28] = 27155276490901009;
	cur_stav_sk[29] = 9104868088156446;
	cur_stav_sk[30] = 16285834229355801;
	cur_stav_sk[31] = 538243697152368;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27576390586930204;
	cur_stav_sk[1] = 27515490622313586;
	cur_stav_sk[2] = 69899834769023230;
	cur_stav_sk[3] = 13591974923539484;
	cur_stav_sk[4] = 290975665770522;
	cur_stav_sk[5] = 28262155254346188;
	cur_stav_sk[6] = 125101868790294;
	cur_stav_sk[7] = 1680705008931098;
	cur_stav_sk[8] = 27131498622390593;
	cur_stav_sk[9] = 32037371455123984;
	cur_stav_sk[10] = 11567694021461112;
	cur_stav_sk[11] = 33198180117787456;
	cur_stav_sk[12] = 9562402372494100;
	cur_stav_sk[13] = 5043959801746556;
	cur_stav_sk[14] = 34973951463306098;
	cur_stav_sk[15] = 21915332757636112;
	cur_stav_sk[16] = 10802527805375252;
	cur_stav_sk[17] = 1205965697679480;
	cur_stav_sk[18] = 6131638673540378;
	cur_stav_sk[19] = 10239303053752082;
	cur_stav_sk[20] = 10805593554421360;
	cur_stav_sk[21] = 32757437616198682;
	cur_stav_sk[22] = 32070628798727540;
	cur_stav_sk[23] = 64143202056209940;
	cur_stav_sk[24] = 70113725131821432;
	cur_stav_sk[25] = 25966853660443248;
	cur_stav_sk[26] = 11568817755214878;
	cur_stav_sk[27] = 5706083127264532;
	cur_stav_sk[28] = 502700504813848;
	cur_stav_sk[29] = 64168328886629232;
	cur_stav_sk[30] = 5753271509365020;
	cur_stav_sk[31] = 28657702760514888;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9526450684560400;
	cur_stav_sk[1] = 549717391807510;
	cur_stav_sk[2] = 31642980500473200;
	cur_stav_sk[3] = 5710688515720212;
	cur_stav_sk[4] = 16986291334086678;
	cur_stav_sk[5] = 15174983825172338;
	cur_stav_sk[6] = 33170964650754934;
	cur_stav_sk[7] = 16281919299597694;
	cur_stav_sk[8] = 28642036870722680;
	cur_stav_sk[9] = 10239612905367060;
	cur_stav_sk[10] = 20787178410247292;
	cur_stav_sk[11] = 10438496587265102;
	cur_stav_sk[12] = 31608345825412218;
	cur_stav_sk[13] = 28246281010578036;
	cur_stav_sk[14] = 8182298329917302;
	cur_stav_sk[15] = 9130651861488914;
	cur_stav_sk[16] = 12464892764033298;
	cur_stav_sk[17] = 32722527658639684;
	cur_stav_sk[18] = 63746871600252272;
	cur_stav_sk[19] = 14772656127378192;
	cur_stav_sk[20] = 9562554791563376;
	cur_stav_sk[21] = 32775168856621338;
	cur_stav_sk[22] = 1205965846053750;
	cur_stav_sk[23] = 71442424422761804;
	cur_stav_sk[24] = 31597221523035412;
	cur_stav_sk[25] = 5728255340720662;
	cur_stav_sk[26] = 34991751022546974;
	cur_stav_sk[27] = 65584774680318322;
	cur_stav_sk[28] = 27558795649233052;
	cur_stav_sk[29] = 6836316110980376;
	cur_stav_sk[30] = 29362190285894260;
	cur_stav_sk[31] = 30919166984468240;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 17390733293958002;
	cur_stav_sk[1] = 23732552285890320;
	cur_stav_sk[2] = 31649187797763400;
	cur_stav_sk[3] = 14602508396662810;
	cur_stav_sk[4] = 15860417880724764;
	cur_stav_sk[5] = 12477219924186392;
	cur_stav_sk[6] = 3879934965524338;
	cur_stav_sk[7] = 19265469544173900;
	cur_stav_sk[8] = 18552891533398140;
	cur_stav_sk[9] = 10251189892555376;
	cur_stav_sk[10] = 10419004952675608;
	cur_stav_sk[11] = 27525041283334472;
	cur_stav_sk[12] = 34287522282246514;
	cur_stav_sk[13] = 19668687533122164;
	cur_stav_sk[14] = 30922252388598042;
	cur_stav_sk[15] = 31658943278293616;
	cur_stav_sk[16] = 28433437288305686;
	cur_stav_sk[17] = 33852731646149234;
	cur_stav_sk[18] = 33163293764334196;
	cur_stav_sk[19] = 28650600153752730;
	cur_stav_sk[20] = 59427511507170162;
	cur_stav_sk[21] = 28659352613523836;
	cur_stav_sk[22] = 24181291562145150;
	cur_stav_sk[23] = 14707753140260976;
	cur_stav_sk[24] = 9088374901904502;
	cur_stav_sk[25] = 60360745739845656;
	cur_stav_sk[26] = 29362202496270656;
	cur_stav_sk[27] = 5040215735738618;
	cur_stav_sk[28] = 19696202494711162;
	cur_stav_sk[29] = 24190730147037558;
	cur_stav_sk[30] = 14060058854138270;
	cur_stav_sk[31] = 60345489950180732;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27111639138284829;
	cur_stav_sk[1] = 60888717346710780;
	cur_stav_sk[2] = 68231326536630288;
	cur_stav_sk[3] = 1425660317905426;
	cur_stav_sk[4] = 11761274024890486;
	cur_stav_sk[5] = 9078054962275954;
	cur_stav_sk[6] = 10806865736537464;
	cur_stav_sk[7] = 10099982149263772;
	cur_stav_sk[8] = 9527230898550010;
	cur_stav_sk[9] = 11559474112632188;
	cur_stav_sk[10] = 10214015831572510;
	cur_stav_sk[11] = 13833251405477700;
	cur_stav_sk[12] = 22633728800751944;
	cur_stav_sk[13] = 9096002015609884;
	cur_stav_sk[14] = 22615448345575546;
	cur_stav_sk[15] = 32933401407160645;
	cur_stav_sk[16] = 7257333030871568;
	cur_stav_sk[17] = 32937827239437464;
	cur_stav_sk[18] = 23753193169061701;
	cur_stav_sk[19] = 18084992219722906;
	cur_stav_sk[20] = 32734622758872948;
	cur_stav_sk[21] = 16256045495288436;
	cur_stav_sk[22] = 33153650952372552;
	cur_stav_sk[23] = 63749271355983224;
	cur_stav_sk[24] = 22632998105421176;
	cur_stav_sk[25] = 3874170041509372;
	cur_stav_sk[26] = 23768769052418460;
	cur_stav_sk[27] = 538147681075574;
	cur_stav_sk[28] = 10265664625737750;
	cur_stav_sk[29] = 28243497729196410;
	cur_stav_sk[30] = 10230747936863860;
	cur_stav_sk[31] = 1233408860886085;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 70299298874462492;
	cur_stav_sk[1] = 28640696354226196;
	cur_stav_sk[2] = 3898419957731708;
	cur_stav_sk[3] = 23074164273686390;
	cur_stav_sk[4] = 19211250740298774;
	cur_stav_sk[5] = 28263803460751734;
	cur_stav_sk[6] = 29345790331458832;
	cur_stav_sk[7] = 35405173346349894;
	cur_stav_sk[8] = 32039330283066490;
	cur_stav_sk[9] = 27559621892211832;
	cur_stav_sk[10] = 28694742021506430;
	cur_stav_sk[11] = 32739502424736832;
	cur_stav_sk[12] = 14047985501308018;
	cur_stav_sk[13] = 12464174013911364;
	cur_stav_sk[14] = 65394045162198128;
	cur_stav_sk[15] = 11769810603909529;
	cur_stav_sk[16] = 56385022829508470;
	cur_stav_sk[17] = 14708448386551832;
	cur_stav_sk[18] = 31600545082081910;
	cur_stav_sk[19] = 13603728017756534;
	cur_stav_sk[20] = 30918926322172024;
	cur_stav_sk[21] = 18150674605225744;
	cur_stav_sk[22] = 12878103160730228;
	cur_stav_sk[23] = 31847087490873104;
	cur_stav_sk[24] = 27523325849175164;
	cur_stav_sk[25] = 57518577923503310;
	cur_stav_sk[26] = 9140876102578806;
	cur_stav_sk[27] = 5972589106398320;
	cur_stav_sk[28] = 10248340664026644;
	cur_stav_sk[29] = 11544490448217242;
	cur_stav_sk[30] = 35405450234957852;
	cur_stav_sk[31] = 71219221472949824;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32057240827937864;
	cur_stav_sk[1] = 27145325056071030;
	cur_stav_sk[2] = 5182497877168198;
	cur_stav_sk[3] = 9887412554739866;
	cur_stav_sk[4] = 13643663921886834;
	cur_stav_sk[5] = 31650159673909404;
	cur_stav_sk[6] = 28688293790778128;
	cur_stav_sk[7] = 14039464802226462;
	cur_stav_sk[8] = 64141965046862876;
	cur_stav_sk[9] = 10256827720664094;
	cur_stav_sk[10] = 60561682981589317;
	cur_stav_sk[11] = 29363290794885502;
	cur_stav_sk[12] = 27558523506242590;
	cur_stav_sk[13] = 19445720742299460;
	cur_stav_sk[14] = 65796802028503920;
	cur_stav_sk[15] = 15138761361297782;
	cur_stav_sk[16] = 11339098602410098;
	cur_stav_sk[17] = 28244740027626262;
	cur_stav_sk[18] = 29354229131904126;
	cur_stav_sk[19] = 14711057562304794;
	cur_stav_sk[20] = 9116643033653778;
	cur_stav_sk[21] = 28466086014293321;
	cur_stav_sk[22] = 4626845339062640;
	cur_stav_sk[23] = 12878415415673920;
	cur_stav_sk[24] = 9325789352638320;
	cur_stav_sk[25] = 10636432905699348;
	cur_stav_sk[26] = 14752935320134416;
	cur_stav_sk[27] = 15165356864016502;
	cur_stav_sk[28] = 33849296161715657;
	cur_stav_sk[29] = 9512534442608246;
	cur_stav_sk[30] = 25990149779658102;
	cur_stav_sk[31] = 28641485551150865;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10644265972682770;
	cur_stav_sk[1] = 16971748513616244;
	cur_stav_sk[2] = 6307284582379928;
	cur_stav_sk[3] = 2244110427455864;
	cur_stav_sk[4] = 1198017311356158;
	cur_stav_sk[5] = 24173140041466896;
	cur_stav_sk[6] = 33877713657532542;
	cur_stav_sk[7] = 65268071649841524;
	cur_stav_sk[8] = 16967798741575162;
	cur_stav_sk[9] = 10204186929399412;
	cur_stav_sk[10] = 3450134416401662;
	cur_stav_sk[11] = 33858323642450296;
	cur_stav_sk[12] = 14022721008440602;
	cur_stav_sk[13] = 19678515885703750;
	cur_stav_sk[14] = 70326762621305924;
	cur_stav_sk[15] = 24857764791010328;
	cur_stav_sk[16] = 11558510366000404;
	cur_stav_sk[17] = 3457696176226330;
	cur_stav_sk[18] = 1664488491136408;
	cur_stav_sk[19] = 10230163813929285;
	cur_stav_sk[20] = 34271577948619804;
	cur_stav_sk[21] = 16969593507971149;
	cur_stav_sk[22] = 18129110653808716;
	cur_stav_sk[23] = 13832688654956054;
	cur_stav_sk[24] = 14049233900810488;
	cur_stav_sk[25] = 10221641665557916;
	cur_stav_sk[26] = 15755826069701654;
	cur_stav_sk[27] = 20769723193405944;
	cur_stav_sk[28] = 15147728164488478;
	cur_stav_sk[29] = 32072005788668182;
	cur_stav_sk[30] = 28693206032396869;
	cur_stav_sk[31] = 2346115909781364;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33206934334160918;
	cur_stav_sk[1] = 71231319107310968;
	cur_stav_sk[2] = 64169237320828024;
	cur_stav_sk[3] = 14717589219358204;
	cur_stav_sk[4] = 28676988379857010;
	cur_stav_sk[5] = 10450589608654356;
	cur_stav_sk[6] = 13829524714455416;
	cur_stav_sk[7] = 23767051413534576;
	cur_stav_sk[8] = 10257076687017242;
	cur_stav_sk[9] = 11770014563697168;
	cur_stav_sk[10] = 12483406220691220;
	cur_stav_sk[11] = 33857831711281180;
	cur_stav_sk[12] = 9087016478454142;
	cur_stav_sk[13] = 64845449143779700;
	cur_stav_sk[14] = 28271302806536478;
	cur_stav_sk[15] = 677949856462334;
	cur_stav_sk[16] = 27154191623131506;
	cur_stav_sk[17] = 5621637601174388;
	cur_stav_sk[18] = 5744609020215324;
	cur_stav_sk[19] = 13590463361975580;
	cur_stav_sk[20] = 61317847546474520;
	cur_stav_sk[21] = 27551376076604740;
	cur_stav_sk[22] = 4618447071707416;
	cur_stav_sk[23] = 15192234242553712;
	cur_stav_sk[24] = 27146413273211084;
	cur_stav_sk[25] = 54923373461340944;
	cur_stav_sk[26] = 69772335559086458;
	cur_stav_sk[27] = 19677208211390540;
	cur_stav_sk[28] = 14202822738817484;
	cur_stav_sk[29] = 27568678509712410;
	cur_stav_sk[30] = 11335435059151990;
	cur_stav_sk[31] = 32942749284369692;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12895630181467418;
	cur_stav_sk[1] = 9131793593078142;
	cur_stav_sk[2] = 3475655782535550;
	cur_stav_sk[3] = 35405381513815926;
	cur_stav_sk[4] = 28658014268569206;
	cur_stav_sk[5] = 29362201549824202;
	cur_stav_sk[6] = 16044904145883154;
	cur_stav_sk[7] = 18104038793261564;
	cur_stav_sk[8] = 10662520537834048;
	cur_stav_sk[9] = 13618364417376540;
	cur_stav_sk[10] = 10652855774262134;
	cur_stav_sk[11] = 32037396959040540;
	cur_stav_sk[12] = 65822090747249690;
	cur_stav_sk[13] = 18516950701125918;
	cur_stav_sk[14] = 18561425954914416;
	cur_stav_sk[15] = 12483105571288478;
	cur_stav_sk[16] = 10099568910205048;
	cur_stav_sk[17] = 32990604805734782;
	cur_stav_sk[18] = 29767179261781502;
	cur_stav_sk[19] = 12879436686631326;
	cur_stav_sk[20] = 15191711329424412;
	cur_stav_sk[21] = 21485286141527318;
	cur_stav_sk[22] = 502828672390260;
	cur_stav_sk[23] = 12684797911503894;
	cur_stav_sk[24] = 6836576631751696;
	cur_stav_sk[25] = 63957524110574712;
	cur_stav_sk[26] = 14953948834080204;
	cur_stav_sk[27] = 27551156041757898;
	cur_stav_sk[28] = 9906057730011970;
	cur_stav_sk[29] = 18560242346854160;
	cur_stav_sk[30] = 10678761872904208;
	cur_stav_sk[31] = 11017689224951574;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32039442911072282;
	cur_stav_sk[1] = 10204434275279633;
	cur_stav_sk[2] = 72119645700124;
	cur_stav_sk[3] = 10626578087948536;
	cur_stav_sk[4] = 55864294966559049;
	cur_stav_sk[5] = 16256024957105560;
	cur_stav_sk[6] = 23065354680869744;
	cur_stav_sk[7] = 11233537107435674;
	cur_stav_sk[8] = 27568956522860662;
	cur_stav_sk[9] = 5726150798479732;
	cur_stav_sk[10] = 7969952305074196;
	cur_stav_sk[11] = 65372857164562704;
	cur_stav_sk[12] = 13603026142994888;
	cur_stav_sk[13] = 11356408549147156;
	cur_stav_sk[14] = 33170417591877966;
	cur_stav_sk[15] = 14189903013549082;
	cur_stav_sk[16] = 56596020637021436;
	cur_stav_sk[17] = 65391259547861620;
	cur_stav_sk[18] = 31622535338886268;
	cur_stav_sk[19] = 71424652761044350;
	cur_stav_sk[20] = 8374185584197648;
	cur_stav_sk[21] = 27127708632593268;
	cur_stav_sk[22] = 1197124981593116;
	cur_stav_sk[23] = 13600703555449620;
	cur_stav_sk[24] = 23056102784340244;
	cur_stav_sk[25] = 13616440112583954;
	cur_stav_sk[26] = 1637521853191294;
	cur_stav_sk[27] = 61107014978466320;
	cur_stav_sk[28] = 20777007392391542;
	cur_stav_sk[29] = 32732011472651390;
	cur_stav_sk[30] = 19479640771932741;
	cur_stav_sk[31] = 22852830792388880;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9526654285907220;
	cur_stav_sk[1] = 6298585319604342;
	cur_stav_sk[2] = 23934888127303757;
	cur_stav_sk[3] = 13634020099072882;
	cur_stav_sk[4] = 11755846259356826;
	cur_stav_sk[5] = 32761879678367224;
	cur_stav_sk[6] = 17383241469966198;
	cur_stav_sk[7] = 14021657339101304;
	cur_stav_sk[8] = 65390814877778040;
	cur_stav_sk[9] = 27558645392712188;
	cur_stav_sk[10] = 65293429106717852;
	cur_stav_sk[11] = 10688153981423890;
	cur_stav_sk[12] = 12897029413713780;
	cur_stav_sk[13] = 18507753903785340;
	cur_stav_sk[14] = 26390302550144628;
	cur_stav_sk[15] = 23751405997078290;
	cur_stav_sk[16] = 11251857288877076;
	cur_stav_sk[17] = 63729707833033844;
	cur_stav_sk[18] = 32987305192064017;
	cur_stav_sk[19] = 32063772619158906;
	cur_stav_sk[20] = 28262745208296522;
	cur_stav_sk[21] = 70325951539937660;
	cur_stav_sk[22] = 10230756068921616;
	cur_stav_sk[23] = 68645084704092670;
	cur_stav_sk[24] = 9122415045378674;
	cur_stav_sk[25] = 32774796723094908;
	cur_stav_sk[26] = 65286763543041404;
	cur_stav_sk[27] = 2216621888582780;
	cur_stav_sk[28] = 10653029129041524;
	cur_stav_sk[29] = 28258030891999642;
	cur_stav_sk[30] = 21472188299674748;
	cur_stav_sk[31] = 15309536237622294;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33858255374585974;
	cur_stav_sk[1] = 14937447436014610;
	cur_stav_sk[2] = 28248240052502896;
	cur_stav_sk[3] = 132908322002038;
	cur_stav_sk[4] = 32954567895265660;
	cur_stav_sk[5] = 15182364188820596;
	cur_stav_sk[6] = 106465805578652;
	cur_stav_sk[7] = 28279996548059508;
	cur_stav_sk[8] = 32748780446982652;
	cur_stav_sk[9] = 14751878764315666;
	cur_stav_sk[10] = 5000139812808308;
	cur_stav_sk[11] = 25272924921246972;
	cur_stav_sk[12] = 20363593377462085;
	cur_stav_sk[13] = 10117436160749948;
	cur_stav_sk[14] = 19219881010888724;
	cur_stav_sk[15] = 9890812540633112;
	cur_stav_sk[16] = 32054012020757620;
	cur_stav_sk[17] = 19267333033763322;
	cur_stav_sk[18] = 17408025916948510;
	cur_stav_sk[19] = 28685020029658364;
	cur_stav_sk[20] = 30487427754205814;
	cur_stav_sk[21] = 59233507297604892;
	cur_stav_sk[22] = 27110245577168252;
	cur_stav_sk[23] = 9686249176437784;
	cur_stav_sk[24] = 80612866633848;
	cur_stav_sk[25] = 1425549264421240;
	cur_stav_sk[26] = 3476316678784280;
	cur_stav_sk[27] = 14047917897483550;
	cur_stav_sk[28] = 28670699866853392;
	cur_stav_sk[29] = 30491216831754488;
	cur_stav_sk[30] = 71442494143137144;
	cur_stav_sk[31] = 10265665682149656;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15165526911128860;
	cur_stav_sk[1] = 15736902657606676;
	cur_stav_sk[2] = 10257102472187926;
	cur_stav_sk[3] = 23965785647382544;
	cur_stav_sk[4] = 13634992225490204;
	cur_stav_sk[5] = 29345243417903224;
	cur_stav_sk[6] = 31601647503618834;
	cur_stav_sk[7] = 69789195787469848;
	cur_stav_sk[8] = 28262607303840884;
	cur_stav_sk[9] = 32062532177335316;
	cur_stav_sk[10] = 16256081061511288;
	cur_stav_sk[11] = 19215897967142392;
	cur_stav_sk[12] = 27523391679431802;
	cur_stav_sk[13] = 28263569315828757;
	cur_stav_sk[14] = 9536165889512820;
	cur_stav_sk[15] = 9095797989507198;
	cur_stav_sk[16] = 32996101838816332;
	cur_stav_sk[17] = 16046030036219388;
	cur_stav_sk[18] = 33208787753959796;
	cur_stav_sk[19] = 28272159119021172;
	cur_stav_sk[20] = 27128326768722294;
	cur_stav_sk[21] = 65812422031839504;
	cur_stav_sk[22] = 33153031902397716;
	cur_stav_sk[23] = 28280339940488822;
	cur_stav_sk[24] = 29784197524587640;
	cur_stav_sk[25] = 23768723823596920;
	cur_stav_sk[26] = 10265692861268756;
	cur_stav_sk[27] = 17391808120554768;
	cur_stav_sk[28] = 28659762732531789;
	cur_stav_sk[29] = 2554782539855004;
	cur_stav_sk[30] = 57519126197149948;
	cur_stav_sk[31] = 19696201961997594;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 3471464226398364;
	cur_stav_sk[1] = 14924664020304924;
	cur_stav_sk[2] = 128938223800446;
	cur_stav_sk[3] = 66947723896389914;
	cur_stav_sk[4] = 15859635995378716;
	cur_stav_sk[5] = 18358853490213650;
	cur_stav_sk[6] = 11348904547156850;
	cur_stav_sk[7] = 19219496762226288;
	cur_stav_sk[8] = 63934557086691406;
	cur_stav_sk[9] = 65391602954080272;
	cur_stav_sk[10] = 17390937308964372;
	cur_stav_sk[11] = 4794426174440780;
	cur_stav_sk[12] = 554840524568178;
	cur_stav_sk[13] = 24154695905718652;
	cur_stav_sk[14] = 32767443672110364;
	cur_stav_sk[15] = 2746381672649029;
	cur_stav_sk[16] = 68655036457951694;
	cur_stav_sk[17] = 68663200454673732;
	cur_stav_sk[18] = 14619950565031966;
	cur_stav_sk[19] = 6150466326856984;
	cur_stav_sk[20] = 15834624509227792;
	cur_stav_sk[21] = 30901507542089793;
	cur_stav_sk[22] = 9562253059437948;
	cur_stav_sk[23] = 14021048727245176;
	cur_stav_sk[24] = 10673370301895804;
	cur_stav_sk[25] = 20355060678463509;
	cur_stav_sk[26] = 23065369707352340;
	cur_stav_sk[27] = 32062725660390160;
	cur_stav_sk[28] = 11233328813342784;
	cur_stav_sk[29] = 68249455379775770;
	cur_stav_sk[30] = 14716969665168413;
	cur_stav_sk[31] = 9078136613377144;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11751587811033872;
	cur_stav_sk[1] = 31596009126007924;
	cur_stav_sk[2] = 28487139395191824;
	cur_stav_sk[3] = 528139384496632;
	cur_stav_sk[4] = 14760494327869972;
	cur_stav_sk[5] = 15851402782188048;
	cur_stav_sk[6] = 28685784531829057;
	cur_stav_sk[7] = 9526585372770328;
	cur_stav_sk[8] = 29776800515437330;
	cur_stav_sk[9] = 7961387693183088;
	cur_stav_sk[10] = 28278956507858296;
	cur_stav_sk[11] = 14012504758826560;
	cur_stav_sk[12] = 9136136815722516;
	cur_stav_sk[13] = 27137147448131964;
	cur_stav_sk[14] = 18123922324922572;
	cur_stav_sk[15] = 9342718346363256;
	cur_stav_sk[16] = 33205832594657392;
	cur_stav_sk[17] = 14048421417976694;
	cur_stav_sk[18] = 9677727944552856;
	cur_stav_sk[19] = 34296811813540930;
	cur_stav_sk[20] = 11347551649006878;
	cur_stav_sk[21] = 19686832482296436;
	cur_stav_sk[22] = 5928574349935896;
	cur_stav_sk[23] = 17382621728081272;
	cur_stav_sk[24] = 22645491625734650;
	cur_stav_sk[25] = 14728919458460106;
	cur_stav_sk[26] = 56791841480946496;
	cur_stav_sk[27] = 18560131707384984;
	cur_stav_sk[28] = 3659728911963508;
	cur_stav_sk[29] = 63746280021013364;
	cur_stav_sk[30] = 14181244619657496;
	cur_stav_sk[31] = 28694813478323472;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14735522836943124;
	cur_stav_sk[1] = 14725077563306014;
	cur_stav_sk[2] = 5701869282242589;
	cur_stav_sk[3] = 9329122170246428;
	cur_stav_sk[4] = 3897776804003964;
	cur_stav_sk[5] = 13608317190246772;
	cur_stav_sk[6] = 69376785063449624;
	cur_stav_sk[7] = 31657788490649982;
	cur_stav_sk[8] = 27516326601597764;
	cur_stav_sk[9] = 59647630724544116;
	cur_stav_sk[10] = 5912766787748124;
	cur_stav_sk[11] = 35413996077482008;
	cur_stav_sk[12] = 10467392104564036;
	cur_stav_sk[13] = 9685673781396762;
	cur_stav_sk[14] = 17400558580925722;
	cur_stav_sk[15] = 15165844338540873;
	cur_stav_sk[16] = 14030944740343884;
	cur_stav_sk[17] = 11353026640412954;
	cur_stav_sk[18] = 28650804509443190;
	cur_stav_sk[19] = 7274443171071254;
	cur_stav_sk[20] = 10221641071931768;
	cur_stav_sk[21] = 16968770157577029;
	cur_stav_sk[22] = 69886090926302462;
	cur_stav_sk[23] = 18128823360787534;
	cur_stav_sk[24] = 29576629869356912;
	cur_stav_sk[25] = 28263296520168600;
	cur_stav_sk[26] = 20336643638887752;
	cur_stav_sk[27] = 70300261685797020;
	cur_stav_sk[28] = 16960547754804498;
	cur_stav_sk[29] = 19689741303550994;
	cur_stav_sk[30] = 27101688724981118;
	cur_stav_sk[31] = 3467382951445782;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 6509665644713338;
	cur_stav_sk[1] = 21912448760348745;
	cur_stav_sk[2] = 22642826543104022;
	cur_stav_sk[3] = 28696365590390270;
	cur_stav_sk[4] = 31650299737047420;
	cur_stav_sk[5] = 34974327858729078;
	cur_stav_sk[6] = 10671661913538892;
	cur_stav_sk[7] = 13599661160140820;
	cur_stav_sk[8] = 27304728535912513;
	cur_stav_sk[9] = 16044216505607696;
	cur_stav_sk[10] = 11040754073702780;
	cur_stav_sk[11] = 15175261381464188;
	cur_stav_sk[12] = 15166465497862518;
	cur_stav_sk[13] = 12895653819584624;
	cur_stav_sk[14] = 28456226900911352;
	cur_stav_sk[15] = 28255075881460244;
	cur_stav_sk[16] = 28486704202608796;
	cur_stav_sk[17] = 63731423278032077;
	cur_stav_sk[18] = 20356088236736890;
	cur_stav_sk[19] = 17408597752328818;
	cur_stav_sk[20] = 7970185311232786;
	cur_stav_sk[21] = 28257342224938441;
	cur_stav_sk[22] = 13646756613423218;
	cur_stav_sk[23] = 9553331345139150;
	cur_stav_sk[24] = 16282053851284600;
	cur_stav_sk[25] = 19246433572307988;
	cur_stav_sk[26] = 13855503835795576;
	cur_stav_sk[27] = 9332673153508214;
	cur_stav_sk[28] = 28649567538054170;
	cur_stav_sk[29] = 32080264681001234;
	cur_stav_sk[30] = 65383042747932922;
	cur_stav_sk[31] = 63728674289377910;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18551609944806416;
	cur_stav_sk[1] = 2332689147658360;
	cur_stav_sk[2] = 28676163070923792;
	cur_stav_sk[3] = 25280872439526164;
	cur_stav_sk[4] = 66500487873826932;
	cur_stav_sk[5] = 14972880446390642;
	cur_stav_sk[6] = 29354012035756662;
	cur_stav_sk[7] = 10230231681859700;
	cur_stav_sk[8] = 6531267252103024;
	cur_stav_sk[9] = 28693447138545688;
	cur_stav_sk[10] = 20337121722467440;
	cur_stav_sk[11] = 56052722357575932;
	cur_stav_sk[12] = 27579811589568372;
	cur_stav_sk[13] = 24172767985689854;
	cur_stav_sk[14] = 64872840901788738;
	cur_stav_sk[15] = 9136685676106098;
	cur_stav_sk[16] = 30497185226489929;
	cur_stav_sk[17] = 63957085012145008;
	cur_stav_sk[18] = 71004060037035634;
	cur_stav_sk[19] = 14755282531746934;
	cur_stav_sk[20] = 13819505043211286;
	cur_stav_sk[21] = 10267064564941946;
	cur_stav_sk[22] = 1207091457163544;
	cur_stav_sk[23] = 10108379477775637;
	cur_stav_sk[24] = 31641351646785914;
	cur_stav_sk[25] = 57941614095316598;
	cur_stav_sk[26] = 14180168545678716;
	cur_stav_sk[27] = 15175010683879448;
	cur_stav_sk[28] = 18103189455082782;
	cur_stav_sk[29] = 27093142798697496;
	cur_stav_sk[30] = 14628562252071966;
	cur_stav_sk[31] = 5955509112742344;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 54721958696457284;
	cur_stav_sk[1] = 13591012985671953;
	cur_stav_sk[2] = 14057273609847113;
	cur_stav_sk[3] = 5058667460563022;
	cur_stav_sk[4] = 31826222594920466;
	cur_stav_sk[5] = 16274659282423924;
	cur_stav_sk[6] = 11335423130533956;
	cur_stav_sk[7] = 14025306663030806;
	cur_stav_sk[8] = 14763310147875442;
	cur_stav_sk[9] = 17390362936747256;
	cur_stav_sk[10] = 65584224698586142;
	cur_stav_sk[11] = 15852215894148374;
	cur_stav_sk[12] = 9090542090429946;
	cur_stav_sk[13] = 66511218770085750;
	cur_stav_sk[14] = 33182571605340286;
	cur_stav_sk[15] = 10257179783009650;
	cur_stav_sk[16] = 34297636447076376;
	cur_stav_sk[17] = 33849432596623642;
	cur_stav_sk[18] = 34280371688641912;
	cur_stav_sk[19] = 33206152787824918;
	cur_stav_sk[20] = 9685082019013112;
	cur_stav_sk[21] = 10671592045577338;
	cur_stav_sk[22] = 33172227893961282;
	cur_stav_sk[23] = 9353862729511696;
	cur_stav_sk[24] = 32775141481918992;
	cur_stav_sk[25] = 30496191431344408;
	cur_stav_sk[26] = 6131637205013788;
	cur_stav_sk[27] = 19663533903003764;
	cur_stav_sk[28] = 14744207726379080;
	cur_stav_sk[29] = 27145447667999994;
	cur_stav_sk[30] = 23944922774507892;
	cur_stav_sk[31] = 19689646831011145;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15147564832859670;
	cur_stav_sk[1] = 27321246381127440;
	cur_stav_sk[2] = 10248375014557300;
	cur_stav_sk[3] = 3889322822434844;
	cur_stav_sk[4] = 15139526198276298;
	cur_stav_sk[5] = 27568815862448658;
	cur_stav_sk[6] = 33866641389727868;
	cur_stav_sk[7] = 56577026854695324;
	cur_stav_sk[8] = 12473070985688694;
	cur_stav_sk[9] = 22838647512347457;
	cur_stav_sk[10] = 27514883886985502;
	cur_stav_sk[11] = 27127707095957776;
	cur_stav_sk[12] = 27577752028579102;
	cur_stav_sk[13] = 23037741207815697;
	cur_stav_sk[14] = 11330154475591038;
	cur_stav_sk[15] = 27101318805079162;
	cur_stav_sk[16] = 65795688640250736;
	cur_stav_sk[17] = 60341824728204868;
	cur_stav_sk[18] = 27560160088461376;
	cur_stav_sk[19] = 27146743515033880;
	cur_stav_sk[20] = 16256861073511542;
	cur_stav_sk[21] = 13621455713898768;
	cur_stav_sk[22] = 34974301568799090;
	cur_stav_sk[23] = 23741415813329274;
	cur_stav_sk[24] = 32731163009515890;
	cur_stav_sk[25] = 28237458057240730;
	cur_stav_sk[26] = 12895834404599412;
	cur_stav_sk[27] = 69357544142778520;
	cur_stav_sk[28] = 34059329595311130;
	cur_stav_sk[29] = 7261110520419392;
	cur_stav_sk[30] = 9501254179165552;
	cur_stav_sk[31] = 55847251133896058;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11773520248209788;
	cur_stav_sk[1] = 14770366451089528;
	cur_stav_sk[2] = 23773121870168092;
	cur_stav_sk[3] = 11761302502704496;
	cur_stav_sk[4] = 29775700535673204;
	cur_stav_sk[5] = 63738158634972408;
	cur_stav_sk[6] = 5762917865951256;
	cur_stav_sk[7] = 32774584676004254;
	cur_stav_sk[8] = 14022354324841076;
	cur_stav_sk[9] = 14181314211419166;
	cur_stav_sk[10] = 19687379741061624;
	cur_stav_sk[11] = 1812002683266554;
	cur_stav_sk[12] = 30479524324218138;
	cur_stav_sk[13] = 10662681526998384;
	cur_stav_sk[14] = 5052848183968528;
	cur_stav_sk[15] = 29363288630841464;
	cur_stav_sk[16] = 18112906281652510;
	cur_stav_sk[17] = 16274522898805616;
	cur_stav_sk[18] = 28237207736985036;
	cur_stav_sk[19] = 27137686100116502;
	cur_stav_sk[20] = 28227795529409788;
	cur_stav_sk[21] = 71451839455203440;
	cur_stav_sk[22] = 12464690036520562;
	cur_stav_sk[23] = 28253607127397404;
	cur_stav_sk[24] = 5912107377525114;
	cur_stav_sk[25] = 10661557323661590;
	cur_stav_sk[26] = 65382204689061236;
	cur_stav_sk[27] = 28667599584821872;
	cur_stav_sk[28] = 60881951162601850;
	cur_stav_sk[29] = 10212856373400702;
	cur_stav_sk[30] = 11339432536244498;
	cur_stav_sk[31] = 17399596160885264;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27128061634250056;
	cur_stav_sk[1] = 9140188689707844;
	cur_stav_sk[2] = 2349112499521136;
	cur_stav_sk[3] = 30920018313355796;
	cur_stav_sk[4] = 4842256731178354;
	cur_stav_sk[5] = 11778810572145010;
	cur_stav_sk[6] = 15730881595733574;
	cur_stav_sk[7] = 32019240185627968;
	cur_stav_sk[8] = 9544673293403408;
	cur_stav_sk[9] = 10626260338512242;
	cur_stav_sk[10] = 32749262555448446;
	cur_stav_sk[11] = 15164815643227642;
	cur_stav_sk[12] = 31650066810766358;
	cur_stav_sk[13] = 16276845398065481;
	cur_stav_sk[14] = 9563103939760252;
	cur_stav_sk[15] = 9105364511860242;
	cur_stav_sk[16] = 33189135923418181;
	cur_stav_sk[17] = 9343408972055058;
	cur_stav_sk[18] = 28236315468571034;
	cur_stav_sk[19] = 23750411108192374;
	cur_stav_sk[20] = 71243748953035894;
	cur_stav_sk[21] = 9130649589682452;
	cur_stav_sk[22] = 5756535744639050;
	cur_stav_sk[23] = 63720551624869648;
	cur_stav_sk[24] = 11355773969727818;
	cur_stav_sk[25] = 32071181376893552;
	cur_stav_sk[26] = 32985906114757754;
	cur_stav_sk[27] = 10239828048032368;
	cur_stav_sk[28] = 32741055998599578;
	cur_stav_sk[29] = 2225786891572289;
	cur_stav_sk[30] = 11549598705123612;
	cur_stav_sk[31] = 65399851111420280;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33197447407142168;
	cur_stav_sk[1] = 9113813234122876;
	cur_stav_sk[2] = 10244729657831582;
	cur_stav_sk[3] = 9519370969355638;
	cur_stav_sk[4] = 28253633854645824;
	cur_stav_sk[5] = 13634936536891410;
	cur_stav_sk[6] = 31632681185686812;
	cur_stav_sk[7] = 14197586718359828;
	cur_stav_sk[8] = 5739243159008378;
	cur_stav_sk[9] = 9140066439835896;
	cur_stav_sk[10] = 65815777680790808;
	cur_stav_sk[11] = 12464095698714702;
	cur_stav_sk[12] = 13592112814064668;
	cur_stav_sk[13] = 27343209298855038;
	cur_stav_sk[14] = 9114707122455674;
	cur_stav_sk[15] = 34974991361479700;
	cur_stav_sk[16] = 11558757726982524;
	cur_stav_sk[17] = 17381637710914422;
	cur_stav_sk[18] = 32065830837846386;
	cur_stav_sk[19] = 28668354489005892;
	cur_stav_sk[20] = 9518434268161438;
	cur_stav_sk[21] = 14612267031442942;
	cur_stav_sk[22] = 33198547793313918;
	cur_stav_sk[23] = 27540778869724278;
	cur_stav_sk[24] = 29362259394601208;
	cur_stav_sk[25] = 10627431853424764;
	cur_stav_sk[26] = 136001231991832;
	cur_stav_sk[27] = 17174953803515258;
	cur_stav_sk[28] = 22627509071591024;
	cur_stav_sk[29] = 32933129033200400;
	cur_stav_sk[30] = 27576194631546740;
	cur_stav_sk[31] = 28694881124202356;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 80075907405180;
	cur_stav_sk[1] = 3448935738028414;
	cur_stav_sk[2] = 35000716151858708;
	cur_stav_sk[3] = 18085335348250132;
	cur_stav_sk[4] = 31606361745666426;
	cur_stav_sk[5] = 32083611328812312;
	cur_stav_sk[6] = 10231813817894010;
	cur_stav_sk[7] = 66710126959137904;
	cur_stav_sk[8] = 28241262734057724;
	cur_stav_sk[9] = 59436181416215833;
	cur_stav_sk[10] = 27514818005012557;
	cur_stav_sk[11] = 13621856000947578;
	cur_stav_sk[12] = 57914410614031486;
	cur_stav_sk[13] = 32968863743226436;
	cur_stav_sk[14] = 31860281754190864;
	cur_stav_sk[15] = 28241538352349560;
	cur_stav_sk[16] = 31623610884314128;
	cur_stav_sk[17] = 69902899345326153;
	cur_stav_sk[18] = 32053615342825850;
	cur_stav_sk[19] = 18551885418337552;
	cur_stav_sk[20] = 10124919561003262;
	cur_stav_sk[21] = 13796679642186824;
	cur_stav_sk[22] = 11351137602868604;
	cur_stav_sk[23] = 3889871886619970;
	cur_stav_sk[24] = 7269532396139772;
	cur_stav_sk[25] = 69797098543121424;
	cur_stav_sk[26] = 33206975192244752;
	cur_stav_sk[27] = 10261200550266234;
	cur_stav_sk[28] = 19272958171952540;
	cur_stav_sk[29] = 5031467899720054;
	cur_stav_sk[30] = 13583110784385304;
	cur_stav_sk[31] = 14066622965167260;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33196529921819002;
	cur_stav_sk[1] = 32766164996142866;
	cur_stav_sk[2] = 15147564764987772;
	cur_stav_sk[3] = 3467111431123230;
	cur_stav_sk[4] = 3879151184328056;
	cur_stav_sk[5] = 23064269189166366;
	cur_stav_sk[6] = 10212913617286394;
	cur_stav_sk[7] = 66525406668538904;
	cur_stav_sk[8] = 14040315808129436;
	cur_stav_sk[9] = 1671950258951196;
	cur_stav_sk[10] = 14735412725330328;
	cur_stav_sk[11] = 32722151111304980;
	cur_stav_sk[12] = 10248209188716568;
	cur_stav_sk[13] = 6316177900053758;
	cur_stav_sk[14] = 14954765345399574;
	cur_stav_sk[15] = 33179923753833548;
	cur_stav_sk[16] = 22641998687446642;
	cur_stav_sk[17] = 23965537956893812;
	cur_stav_sk[18] = 9308057625624690;
	cur_stav_sk[19] = 11770622082786556;
	cur_stav_sk[20] = 62004586040230938;
	cur_stav_sk[21] = 33196840568850194;
	cur_stav_sk[22] = 27562062180810776;
	cur_stav_sk[23] = 9114709890729330;
	cur_stav_sk[24] = 4639498919216402;
	cur_stav_sk[25] = 10268955970155548;
	cur_stav_sk[26] = 15157693019206416;
	cur_stav_sk[27] = 28271500051944730;
	cur_stav_sk[28] = 11330280578945304;
	cur_stav_sk[29] = 19687312121931536;
	cur_stav_sk[30] = 11779636843086108;
	cur_stav_sk[31] = 32054974376969594;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 23064818342540864;
	cur_stav_sk[1] = 16274221317231950;
	cur_stav_sk[2] = 34289171011248752;
	cur_stav_sk[3] = 30702254498648140;
	cur_stav_sk[4] = 22839748433129933;
	cur_stav_sk[5] = 14023063932572956;
	cur_stav_sk[6] = 18130416785295168;
	cur_stav_sk[7] = 18572810989244784;
	cur_stav_sk[8] = 29768126824907800;
	cur_stav_sk[9] = 28448356910151189;
	cur_stav_sk[10] = 13798488884584856;
	cur_stav_sk[11] = 33161920445907016;
	cur_stav_sk[12] = 2745168308250012;
	cur_stav_sk[13] = 12457293023327309;
	cur_stav_sk[14] = 7275154998367512;
	cur_stav_sk[15] = 9552974856433948;
	cur_stav_sk[16] = 31597304727646238;
	cur_stav_sk[17] = 10653379225092114;
	cur_stav_sk[18] = 15148623614870384;
	cur_stav_sk[19] = 9087124771345776;
	cur_stav_sk[20] = 32740781064532248;
	cur_stav_sk[21] = 33179721023861620;
	cur_stav_sk[22] = 31623980571886610;
	cur_stav_sk[23] = 66507911120856218;
	cur_stav_sk[24] = 18535229553615632;
	cur_stav_sk[25] = 11250553763692824;
	cur_stav_sk[26] = 66529185218102556;
	cur_stav_sk[27] = 9510245169050910;
	cur_stav_sk[28] = 28230553377177880;
	cur_stav_sk[29] = 15315653679481886;
	cur_stav_sk[30] = 10221891859973400;
	cur_stav_sk[31] = 11356036360507422;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 16960590769960012;
	cur_stav_sk[1] = 9517998730051952;
	cur_stav_sk[2] = 66498950347592048;
	cur_stav_sk[3] = 14725308475052314;
	cur_stav_sk[4] = 69904275815141490;
	cur_stav_sk[5] = 28658322911801876;
	cur_stav_sk[6] = 9518834704692848;
	cur_stav_sk[7] = 33188695880565788;
	cur_stav_sk[8] = 13599398894187030;
	cur_stav_sk[9] = 3683121354838086;
	cur_stav_sk[10] = 13618362785346712;
	cur_stav_sk[11] = 19448717884785014;
	cur_stav_sk[12] = 10257099785175326;
	cur_stav_sk[13] = 28658087886268178;
	cur_stav_sk[14] = 12890542807724316;
	cur_stav_sk[15] = 27137532974347076;
	cur_stav_sk[16] = 29787932181529712;
	cur_stav_sk[17] = 14743976340664600;
	cur_stav_sk[18] = 22588984506093694;
	cur_stav_sk[19] = 32778535091188502;
	cur_stav_sk[20] = 10810953139578008;
	cur_stav_sk[21] = 10680343566652796;
	cur_stav_sk[22] = 13609555242292338;
	cur_stav_sk[23] = 18138510737641746;
	cur_stav_sk[24] = 24190247948886526;
	cur_stav_sk[25] = 28642077719659537;
	cur_stav_sk[26] = 2763974211043613;
	cur_stav_sk[27] = 27154741427713434;
	cur_stav_sk[28] = 11755757202416449;
	cur_stav_sk[29] = 13629220073223420;
	cur_stav_sk[30] = 14954216752850454;
	cur_stav_sk[31] = 5752881680888986;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12482617225871693;
	cur_stav_sk[1] = 22625302203369232;
	cur_stav_sk[2] = 71451286547379484;
	cur_stav_sk[3] = 15335549632422140;
	cur_stav_sk[4] = 28474881638282774;
	cur_stav_sk[5] = 10204119345923188;
	cur_stav_sk[6] = 66499021210334836;
	cur_stav_sk[7] = 10433272364335436;
	cur_stav_sk[8] = 16267660075335960;
	cur_stav_sk[9] = 24190910985965950;
	cur_stav_sk[10] = 5718018292159862;
	cur_stav_sk[11] = 23733813930910924;
	cur_stav_sk[12] = 13828425152594254;
	cur_stav_sk[13] = 15861354691167088;
	cur_stav_sk[14] = 15130341534957596;
	cur_stav_sk[15] = 16047988608401690;
	cur_stav_sk[16] = 13643734030746748;
	cur_stav_sk[17] = 23750282303705110;
	cur_stav_sk[18] = 33178972403402512;
	cur_stav_sk[19] = 14197795481163084;
	cur_stav_sk[20] = 5023138342613114;
	cur_stav_sk[21] = 10244867775365184;
	cur_stav_sk[22] = 14180236128523382;
	cur_stav_sk[23] = 2011865438851194;
	cur_stav_sk[24] = 32072567686969106;
	cur_stav_sk[25] = 64872153711972476;
	cur_stav_sk[26] = 14937145179341172;
	cur_stav_sk[27] = 27119183720854130;
	cur_stav_sk[28] = 60368235635454076;
	cur_stav_sk[29] = 13616934507775258;
	cur_stav_sk[30] = 30893946394652696;
	cur_stav_sk[31] = 14921640341492760;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31657707066822928;
	cur_stav_sk[1] = 11752207030813554;
	cur_stav_sk[2] = 27577227111371888;
	cur_stav_sk[3] = 5709726331711740;
	cur_stav_sk[4] = 10451689652302610;
	cur_stav_sk[5] = 11347608628737910;
	cur_stav_sk[6] = 24184475531284754;
	cur_stav_sk[7] = 31631523669377102;
	cur_stav_sk[8] = 13638279094342682;
	cur_stav_sk[9] = 15192027328972060;
	cur_stav_sk[10] = 23770753749356668;
	cur_stav_sk[11] = 3891958321657340;
	cur_stav_sk[12] = 28219727962505330;
	cur_stav_sk[13] = 70316590479769624;
	cur_stav_sk[14] = 32766064477382512;
	cur_stav_sk[15] = 33144534487500058;
	cur_stav_sk[16] = 520005670449660;
	cur_stav_sk[17] = 10240128762777618;
	cur_stav_sk[18] = 16273409031649140;
	cur_stav_sk[19] = 69789058906338834;
	cur_stav_sk[20] = 9343406221313148;
	cur_stav_sk[21] = 14768259543073860;
	cur_stav_sk[22] = 13600770931204882;
	cur_stav_sk[23] = 28254017224020350;
	cur_stav_sk[24] = 28218723959349656;
	cur_stav_sk[25] = 9536095711835766;
	cur_stav_sk[26] = 27307778974385014;
	cur_stav_sk[27] = 10231537732812912;
	cur_stav_sk[28] = 14735179727582620;
	cur_stav_sk[29] = 70302292055523706;
	cur_stav_sk[30] = 19276128923332990;
	cur_stav_sk[31] = 35396513410547788;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32071136059331864;
	cur_stav_sk[1] = 5058104624198420;
	cur_stav_sk[2] = 28262404360451228;
	cur_stav_sk[3] = 9113678697697656;
	cur_stav_sk[4] = 9509568782938992;
	cur_stav_sk[5] = 28245385021849618;
	cur_stav_sk[6] = 32784830911418742;
	cur_stav_sk[7] = 11338459738683206;
	cur_stav_sk[8] = 29370738406208278;
	cur_stav_sk[9] = 28217898109306238;
	cur_stav_sk[10] = 33189590850076956;
	cur_stav_sk[11] = 32044390438831134;
	cur_stav_sk[12] = 31867200538219544;
	cur_stav_sk[13] = 14066702478127734;
	cur_stav_sk[14] = 33207002446267516;
	cur_stav_sk[15] = 68241002357163286;
	cur_stav_sk[16] = 15851402674581833;
	cur_stav_sk[17] = 11769755594727582;
	cur_stav_sk[18] = 1657925576130586;
	cur_stav_sk[19] = 6826829602006806;
	cur_stav_sk[20] = 9510590521062262;
	cur_stav_sk[21] = 28685876344979781;
	cur_stav_sk[22] = 9087759419868528;
	cur_stav_sk[23] = 28219272490423578;
	cur_stav_sk[24] = 10248990333089182;
	cur_stav_sk[25] = 69772634401084744;
	cur_stav_sk[26] = 29363506666931484;
	cur_stav_sk[27] = 9702956266242112;
	cur_stav_sk[28] = 27303915849647124;
	cur_stav_sk[29] = 10214014740926748;
	cur_stav_sk[30] = 27155539624866842;
	cur_stav_sk[31] = 3889625153963842;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9098669393839680;
	cur_stav_sk[1] = 69876719703949377;
	cur_stav_sk[2] = 27541192201479326;
	cur_stav_sk[3] = 9113163829981692;
	cur_stav_sk[4] = 28676986677175806;
	cur_stav_sk[5] = 14753208589522036;
	cur_stav_sk[6] = 32986481100260636;
	cur_stav_sk[7] = 10224844105155856;
	cur_stav_sk[8] = 23732716562257276;
	cur_stav_sk[9] = 34991414312309116;
	cur_stav_sk[10] = 32775898327757000;
	cur_stav_sk[11] = 34279034063794206;
	cur_stav_sk[12] = 28270993084822384;
	cur_stav_sk[13] = 68654280541995120;
	cur_stav_sk[14] = 69367466200760606;
	cur_stav_sk[15] = 19678789630177560;
	cur_stav_sk[16] = 33153332534904182;
	cur_stav_sk[17] = 24184418671689758;
	cur_stav_sk[18] = 27559209226703996;
	cur_stav_sk[19] = 7971395088425417;
	cur_stav_sk[20] = 9553192897446006;
	cur_stav_sk[21] = 19475130472472728;
	cur_stav_sk[22] = 12905317085773944;
	cur_stav_sk[23] = 23944622002943260;
	cur_stav_sk[24] = 27549711988990460;
	cur_stav_sk[25] = 14005308536263952;
	cur_stav_sk[26] = 15175009278632818;
	cur_stav_sk[27] = 33198111471154204;
	cur_stav_sk[28] = 10468182000960793;
	cur_stav_sk[29] = 27120299472299793;
	cur_stav_sk[30] = 55152263676999184;
	cur_stav_sk[31] = 13629038476538688;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14629089801766256;
	cur_stav_sk[1] = 66947095197979922;
	cur_stav_sk[2] = 29344803109799292;
	cur_stav_sk[3] = 31639758331843862;
	cur_stav_sk[4] = 69903108661284988;
	cur_stav_sk[5] = 66731426800043384;
	cur_stav_sk[6] = 14022697803391504;
	cur_stav_sk[7] = 9517584787346716;
	cur_stav_sk[8] = 65813019239551250;
	cur_stav_sk[9] = 29787460757312540;
	cur_stav_sk[10] = 9887362676852088;
	cur_stav_sk[11] = 15165594015793530;
	cur_stav_sk[12] = 14745891911345012;
	cur_stav_sk[13] = 16987279094252096;
	cur_stav_sk[14] = 9130968432247158;
	cur_stav_sk[15] = 28244631594927130;
	cur_stav_sk[16] = 28283454882861086;
	cur_stav_sk[17] = 32081486945728276;
	cur_stav_sk[18] = 11339568909676572;
	cur_stav_sk[19] = 31847100026410098;
	cur_stav_sk[20] = 30491217527870846;
	cur_stav_sk[21] = 10644060218200184;
	cur_stav_sk[22] = 14728094619775761;
	cur_stav_sk[23] = 27577749875365197;
	cur_stav_sk[24] = 7256878771676412;
	cur_stav_sk[25] = 32063759520239892;
	cur_stav_sk[26] = 5709220788241428;
	cur_stav_sk[27] = 11347746519225722;
	cur_stav_sk[28] = 28657950801625468;
	cur_stav_sk[29] = 15133641161946524;
	cur_stav_sk[30] = 28640395103512062;
	cur_stav_sk[31] = 9536440388322422;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14051365287432262;
	cur_stav_sk[1] = 13609553293579294;
	cur_stav_sk[2] = 19678858391593846;
	cur_stav_sk[3] = 30900706139788306;
	cur_stav_sk[4] = 15305850954233712;
	cur_stav_sk[5] = 28280268542255986;
	cur_stav_sk[6] = 33162172123026800;
	cur_stav_sk[7] = 9509502884185464;
	cur_stav_sk[8] = 64159326352441372;
	cur_stav_sk[9] = 28281395509793049;
	cur_stav_sk[10] = 9536138658648340;
	cur_stav_sk[11] = 19642668071084560;
	cur_stav_sk[12] = 9328150372497168;
	cur_stav_sk[13] = 28679005802567034;
	cur_stav_sk[14] = 11250511225202076;
	cur_stav_sk[15] = 32731944706281588;
	cur_stav_sk[16] = 19482562768144192;
	cur_stav_sk[17] = 9510230389134413;
	cur_stav_sk[18] = 60888005541172602;
	cur_stav_sk[19] = 511210115993982;
	cur_stav_sk[20] = 68663200190927998;
	cur_stav_sk[21] = 28429828042817612;
	cur_stav_sk[22] = 15834692960355649;
	cur_stav_sk[23] = 32018452585427276;
	cur_stav_sk[24] = 27154371851653234;
	cur_stav_sk[25] = 34076620713366896;
	cur_stav_sk[26] = 18103203554361672;
	cur_stav_sk[27] = 18570209369260410;
	cur_stav_sk[28] = 1197648615285246;
	cur_stav_sk[29] = 11760685159826554;
	cur_stav_sk[30] = 19212322335588686;
	cur_stav_sk[31] = 29353189175984506;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 71443343063453808;
	cur_stav_sk[1] = 23762230837098310;
	cur_stav_sk[2] = 4628676996444944;
	cur_stav_sk[3] = 65821239725535742;
	cur_stav_sk[4] = 71425310970348658;
	cur_stav_sk[5] = 1620505866310988;
	cur_stav_sk[6] = 23063454912746872;
	cur_stav_sk[7] = 32776404250527088;
	cur_stav_sk[8] = 70799786477334;
	cur_stav_sk[9] = 5059226691273588;
	cur_stav_sk[10] = 9554444272010352;
	cur_stav_sk[11] = 10266626542642802;
	cur_stav_sk[12] = 16986980072501630;
	cur_stav_sk[13] = 12886857240871240;
	cur_stav_sk[14] = 27533714322973898;
	cur_stav_sk[15] = 65391810433753922;
	cur_stav_sk[16] = 23058751192109426;
	cur_stav_sk[17] = 10635744570789952;
	cur_stav_sk[18] = 888688530108446;
	cur_stav_sk[19] = 61085058820411512;
	cur_stav_sk[20] = 14937697615282448;
	cur_stav_sk[21] = 6326345850066298;
	cur_stav_sk[22] = 10256799729386356;
	cur_stav_sk[23] = 9104641331467388;
	cur_stav_sk[24] = 69358153422448496;
	cur_stav_sk[25] = 69370154082063484;
	cur_stav_sk[26] = 5955535355724926;
	cur_stav_sk[27] = 17182486452639092;
	cur_stav_sk[28] = 34270133893445490;
	cur_stav_sk[29] = 15183987707241024;
	cur_stav_sk[30] = 27136638469408026;
	cur_stav_sk[31] = 68657739888100382;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 19238531452772765;
	cur_stav_sk[1] = 9132011232724240;
	cur_stav_sk[2] = 31596340305638930;
	cur_stav_sk[3] = 32950721427350682;
	cur_stav_sk[4] = 27357515966518592;
	cur_stav_sk[5] = 18570220117688946;
	cur_stav_sk[6] = 23926365716874864;
	cur_stav_sk[7] = 32744286350868848;
	cur_stav_sk[8] = 27111308898185744;
	cur_stav_sk[9] = 9140041005597048;
	cur_stav_sk[10] = 10679493249544720;
	cur_stav_sk[11] = 9121624921968665;
	cur_stav_sk[12] = 13620767512040734;
	cur_stav_sk[13] = 33197242739630912;
	cur_stav_sk[14] = 27541753940641048;
	cur_stav_sk[15] = 24859702372284177;
	cur_stav_sk[16] = 32018416405488452;
	cur_stav_sk[17] = 32036547274342682;
	cur_stav_sk[18] = 14207756337772096;
	cur_stav_sk[19] = 23028396162744600;
	cur_stav_sk[20] = 31597318017160218;
	cur_stav_sk[21] = 31605276144304508;
	cur_stav_sk[22] = 28702233525043320;
	cur_stav_sk[23] = 15315060085630736;
	cur_stav_sk[24] = 10652787198201874;
	cur_stav_sk[25] = 16274494998361594;
	cur_stav_sk[26] = 31615225830388124;
	cur_stav_sk[27] = 71030133797757436;
	cur_stav_sk[28] = 32778476386156914;
	cur_stav_sk[29] = 15183874403312124;
	cur_stav_sk[30] = 28226943503339792;
	cur_stav_sk[31] = 12905823431243162;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27118456480891932;
	cur_stav_sk[1] = 6316185906250104;
	cur_stav_sk[2] = 4593008500486344;
	cur_stav_sk[3] = 27361626519176724;
	cur_stav_sk[4] = 13815946850838010;
	cur_stav_sk[5] = 61994905661973362;
	cur_stav_sk[6] = 19689740846269296;
	cur_stav_sk[7] = 27577764018528837;
	cur_stav_sk[8] = 32063496608622358;
	cur_stav_sk[9] = 15174186720764736;
	cur_stav_sk[10] = 13618077639815582;
	cur_stav_sk[11] = 27566985602434320;
	cur_stav_sk[12] = 14048205944782925;
	cur_stav_sk[13] = 32028735807370492;
	cur_stav_sk[14] = 18121882302816506;
	cur_stav_sk[15] = 1460595451139088;
	cur_stav_sk[16] = 32783825752785276;
	cur_stav_sk[17] = 28236109378753654;
	cur_stav_sk[18] = 11351032231896266;
	cur_stav_sk[19] = 23011205888934014;
	cur_stav_sk[20] = 2322725914936340;
	cur_stav_sk[21] = 64150529200002422;
	cur_stav_sk[22] = 57521816810369652;
	cur_stav_sk[23] = 9553468647551308;
	cur_stav_sk[24] = 18543088798273564;
	cur_stav_sk[25] = 32955554804867906;
	cur_stav_sk[26] = 18516402346336788;
	cur_stav_sk[27] = 16977082303546393;
	cur_stav_sk[28] = 9298050687369328;
	cur_stav_sk[29] = 68666636105941524;
	cur_stav_sk[30] = 1619268191659540;
	cur_stav_sk[31] = 28447157069849408;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11243157832644370;
	cur_stav_sk[1] = 519661051384188;
	cur_stav_sk[2] = 24870456970248474;
	cur_stav_sk[3] = 34270435266672024;
	cur_stav_sk[4] = 16265354683778072;
	cur_stav_sk[5] = 31648981589001338;
	cur_stav_sk[6] = 10680454308823109;
	cur_stav_sk[7] = 9544546929411088;
	cur_stav_sk[8] = 14980302289862928;
	cur_stav_sk[9] = 27122345357645074;
	cur_stav_sk[10] = 10222646210237517;
	cur_stav_sk[11] = 22860664122049564;
	cur_stav_sk[12] = 28685783797830676;
	cur_stav_sk[13] = 9676559796441200;
	cur_stav_sk[14] = 27146493825516662;
	cur_stav_sk[15] = 520170074214684;
	cur_stav_sk[16] = 16986456153588036;
	cur_stav_sk[17] = 2341431018291521;
	cur_stav_sk[18] = 61321650133894416;
	cur_stav_sk[19] = 27136790934914672;
	cur_stav_sk[20] = 6184614557463060;
	cur_stav_sk[21] = 32063209760265536;
	cur_stav_sk[22] = 63745867168809238;
	cur_stav_sk[23] = 35397435977515134;
	cur_stav_sk[24] = 28693756469150485;
	cur_stav_sk[25] = 27562049074529562;
	cur_stav_sk[26] = 27128078079435378;
	cur_stav_sk[27] = 16281645170852464;
	cur_stav_sk[28] = 4601284059313014;
	cur_stav_sk[29] = 9113618882658582;
	cur_stav_sk[30] = 10204461339750928;
	cur_stav_sk[31] = 23072803446555674;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 20336689742743408;
	cur_stav_sk[1] = 19633462288973850;
	cur_stav_sk[2] = 27559060916516470;
	cur_stav_sk[3] = 14712433164791834;
	cur_stav_sk[4] = 28676188836598090;
	cur_stav_sk[5] = 9518008305497164;
	cur_stav_sk[6] = 27359853292651805;
	cur_stav_sk[7] = 15833868519342708;
	cur_stav_sk[8] = 14941131441616658;
	cur_stav_sk[9] = 24156003243463796;
	cur_stav_sk[10] = 24154720048025886;
	cur_stav_sk[11] = 14004304205549948;
	cur_stav_sk[12] = 18139019206263926;
	cur_stav_sk[13] = 23961140507607416;
	cur_stav_sk[14] = 2237917955395958;
	cur_stav_sk[15] = 70308219852914962;
	cur_stav_sk[16] = 28693710674268186;
	cur_stav_sk[17] = 34982439382529906;
	cur_stav_sk[18] = 70299067076720078;
	cur_stav_sk[19] = 15850635414745496;
	cur_stav_sk[20] = 14040244005242736;
	cur_stav_sk[21] = 33143683877532225;
	cur_stav_sk[22] = 4631104535758110;
	cur_stav_sk[23] = 15745562729427230;
	cur_stav_sk[24] = 32027403623040324;
	cur_stav_sk[25] = 9527136201704513;
	cur_stav_sk[26] = 13794091381912602;
	cur_stav_sk[27] = 15755827765740866;
	cur_stav_sk[28] = 14206626070106396;
	cur_stav_sk[29] = 1232103796474181;
	cur_stav_sk[30] = 22633108855525750;
	cur_stav_sk[31] = 23064392537342738;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11559322057974546;
	cur_stav_sk[1] = 10242886583203228;
	cur_stav_sk[2] = 62427527955874881;
	cur_stav_sk[3] = 9352312984633720;
	cur_stav_sk[4] = 492669525009916;
	cur_stav_sk[5] = 10222027200144638;
	cur_stav_sk[6] = 32774345216365596;
	cur_stav_sk[7] = 32037096455439382;
	cur_stav_sk[8] = 63931124493340693;
	cur_stav_sk[9] = 9299411985704466;
	cur_stav_sk[10] = 28644164472505204;
	cur_stav_sk[11] = 32071329865597976;
	cur_stav_sk[12] = 28228414550577182;
	cur_stav_sk[13] = 24859414817514654;
	cur_stav_sk[14] = 19273691059623038;
	cur_stav_sk[15] = 69799160335275378;
	cur_stav_sk[16] = 13638178499757684;
	cur_stav_sk[17] = 9556918029226266;
	cur_stav_sk[18] = 18095232162926621;
	cur_stav_sk[19] = 18129783554311745;
	cur_stav_sk[20] = 27558282199545616;
	cur_stav_sk[21] = 1223240582104348;
	cur_stav_sk[22] = 12456993311077790;
	cur_stav_sk[23] = 1798213168239890;
	cur_stav_sk[24] = 14752632908550262;
	cur_stav_sk[25] = 10635127153361180;
	cur_stav_sk[26] = 12879229920098066;
	cur_stav_sk[27] = 10249403671883806;
	cur_stav_sk[28] = 31661122373662230;
	cur_stav_sk[29] = 15177245800629278;
	cur_stav_sk[30] = 23046389854109722;
	cur_stav_sk[31] = 32783698581270650;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15147179815810930;
	cur_stav_sk[1] = 65268416848605210;
	cur_stav_sk[2] = 13854965837992218;
	cur_stav_sk[3] = 9562552711230070;
	cur_stav_sk[4] = 5176004962063736;
	cur_stav_sk[5] = 10266146715677848;
	cur_stav_sk[6] = 18337109484372040;
	cur_stav_sk[7] = 14021885100889372;
	cur_stav_sk[8] = 33161575839793534;
	cur_stav_sk[9] = 15736826959496216;
	cur_stav_sk[10] = 31636803459313692;
	cur_stav_sk[11] = 28703403358302458;
	cur_stav_sk[12] = 12905867301355898;
	cur_stav_sk[13] = 6166197053947932;
	cur_stav_sk[14] = 33145128663617648;
	cur_stav_sk[15] = 10257100991575568;
	cur_stav_sk[16] = 65822900613002048;
	cur_stav_sk[17] = 9563090634846788;
	cur_stav_sk[18] = 31649999029216790;
	cur_stav_sk[19] = 18336837346066548;
	cur_stav_sk[20] = 28236383183086196;
	cur_stav_sk[21] = 14764177856139284;
	cur_stav_sk[22] = 68453126697391170;
	cur_stav_sk[23] = 16265148811412508;
	cur_stav_sk[24] = 22804701720785272;
	cur_stav_sk[25] = 68646723221521480;
	cur_stav_sk[26] = 68249728319788308;
	cur_stav_sk[27] = 63729635766306073;
	cur_stav_sk[28] = 64169511999701108;
	cur_stav_sk[29] = 14725240219514644;
	cur_stav_sk[30] = 9113608906540304;
	cur_stav_sk[31] = 12889643209274224;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27360268368716356;
	cur_stav_sk[1] = 16073815458136082;
	cur_stav_sk[2] = 28684982991814978;
	cur_stav_sk[3] = 27109901920372088;
	cur_stav_sk[4] = 20767652131602764;
	cur_stav_sk[5] = 68671783967785236;
	cur_stav_sk[6] = 10664848414998810;
	cur_stav_sk[7] = 31622785025873172;
	cur_stav_sk[8] = 71424350575460476;
	cur_stav_sk[9] = 29353336115278106;
	cur_stav_sk[10] = 17400395355558170;
	cur_stav_sk[11] = 13609568116286998;
	cur_stav_sk[12] = 21464602673710104;
	cur_stav_sk[13] = 10824450388144506;
	cur_stav_sk[14] = 15833559102174238;
	cur_stav_sk[15] = 18120543799748112;
	cur_stav_sk[16] = 10829396730328574;
	cur_stav_sk[17] = 19660923619966996;
	cur_stav_sk[18] = 15159626656231934;
	cur_stav_sk[19] = 3880002124517658;
	cur_stav_sk[20] = 32730704681153790;
	cur_stav_sk[21] = 670459097186426;
	cur_stav_sk[22] = 5041608917328333;
	cur_stav_sk[23] = 9702165249931126;
	cur_stav_sk[24] = 14034103628543856;
	cur_stav_sk[25] = 28640731180307778;
	cur_stav_sk[26] = 15837145580962122;
	cur_stav_sk[27] = 14056917265528338;
	cur_stav_sk[28] = 1460983605235734;
	cur_stav_sk[29] = 884563027248158;
	cur_stav_sk[30] = 3889348460066326;
	cur_stav_sk[31] = 28464164540716054;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27145779311645972;
	cur_stav_sk[1] = 66948993577821044;
	cur_stav_sk[2] = 30470532953543802;
	cur_stav_sk[3] = 54742071742804754;
	cur_stav_sk[4] = 5744465745679382;
	cur_stav_sk[5] = 33191743555838742;
	cur_stav_sk[6] = 32760667436385604;
	cur_stav_sk[7] = 4621589431616794;
	cur_stav_sk[8] = 10247591805420306;
	cur_stav_sk[9] = 32045092996720120;
	cur_stav_sk[10] = 501452088607557;
	cur_stav_sk[11] = 29785252542943508;
	cur_stav_sk[12] = 32038737055974928;
	cur_stav_sk[13] = 10645021703746680;
	cur_stav_sk[14] = 56286583649075824;
	cur_stav_sk[15] = 66727745392301076;
	cur_stav_sk[16] = 27309138870874569;
	cur_stav_sk[17] = 4630909311812722;
	cur_stav_sk[18] = 69561434833618550;
	cur_stav_sk[19] = 27132379198260242;
	cur_stav_sk[20] = 20336656410182002;
	cur_stav_sk[21] = 11233202975191922;
	cur_stav_sk[22] = 14746269328289821;
	cur_stav_sk[23] = 31842824653570428;
	cur_stav_sk[24] = 55850455038595090;
	cur_stav_sk[25] = 2235270248707696;
	cur_stav_sk[26] = 10468069655937148;
	cur_stav_sk[27] = 21700517892101494;
	cur_stav_sk[28] = 9078423928845936;
	cur_stav_sk[29] = 11778480761208958;
	cur_stav_sk[30] = 28227080752071444;
	cur_stav_sk[31] = 64872977057420360;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 5745804582912832;
	cur_stav_sk[1] = 64142792339977340;
	cur_stav_sk[2] = 1197950756930590;
	cur_stav_sk[3] = 9334598724891456;
	cur_stav_sk[4] = 18130620324873490;
	cur_stav_sk[5] = 15165322447697428;
	cur_stav_sk[6] = 6141124100228420;
	cur_stav_sk[7] = 11348091190936688;
	cur_stav_sk[8] = 28430902787933304;
	cur_stav_sk[9] = 65804608129074552;
	cur_stav_sk[10] = 24145761440531830;
	cur_stav_sk[11] = 16255830817416722;
	cur_stav_sk[12] = 22858039355974810;
	cur_stav_sk[13] = 65395398071947546;
	cur_stav_sk[14] = 11779143987278612;
	cur_stav_sk[15] = 9079580597858886;
	cur_stav_sk[16] = 9913505610077566;
	cur_stav_sk[17] = 9113334343569782;
	cur_stav_sk[18] = 27579756087164950;
	cur_stav_sk[19] = 22641217735360536;
	cur_stav_sk[20] = 31860340741570682;
	cur_stav_sk[21] = 63731752691831880;
	cur_stav_sk[22] = 68452865772044314;
	cur_stav_sk[23] = 17182933733287798;
	cur_stav_sk[24] = 33187691925734000;
	cur_stav_sk[25] = 32969688326841968;
	cur_stav_sk[26] = 30910532879746121;
	cur_stav_sk[27] = 9122186352431228;
	cur_stav_sk[28] = 71451863593260158;
	cur_stav_sk[29] = 66517091680564800;
	cur_stav_sk[30] = 11559858914965108;
	cur_stav_sk[31] = 28245180813841776;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10807133776941178;
	cur_stav_sk[1] = 27532409367310868;
	cur_stav_sk[2] = 15852585934598676;
	cur_stav_sk[3] = 69350557778616852;
	cur_stav_sk[4] = 31872511014404976;
	cur_stav_sk[5] = 15191682119438718;
	cur_stav_sk[6] = 18094350095168370;
	cur_stav_sk[7] = 13616680573613842;
	cur_stav_sk[8] = 65391809095762202;
	cur_stav_sk[9] = 31640334337652094;
	cur_stav_sk[10] = 10634586541011730;
	cur_stav_sk[11] = 27526932952002588;
	cur_stav_sk[12] = 11566629469393180;
	cur_stav_sk[13] = 71024014524756382;
	cur_stav_sk[14] = 20768489715675926;
	cur_stav_sk[15] = 27136707750830964;
	cur_stav_sk[16] = 28670827090129686;
	cur_stav_sk[17] = 23749594198598260;
	cur_stav_sk[18] = 14030404311192854;
	cur_stav_sk[19] = 10643829364391956;
	cur_stav_sk[20] = 1262031132821366;
	cur_stav_sk[21] = 27145120182535196;
	cur_stav_sk[22] = 16054249075290902;
	cur_stav_sk[23] = 4998111561134492;
	cur_stav_sk[24] = 10248441797915934;
	cur_stav_sk[25] = 32766827341723504;
	cur_stav_sk[26] = 10662616624365692;
	cur_stav_sk[27] = 31815473915790622;
	cur_stav_sk[28] = 6149876230264082;
	cur_stav_sk[29] = 6183790382589814;
	cur_stav_sk[30] = 23759903500226585;
	cur_stav_sk[31] = 59647356252917786;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 125169328457328;
	cur_stav_sk[1] = 15855539661963382;
	cur_stav_sk[2] = 19254681054521202;
	cur_stav_sk[3] = 24156025672106264;
	cur_stav_sk[4] = 8393539780379976;
	cur_stav_sk[5] = 18542719966381076;
	cur_stav_sk[6] = 32037519344698740;
	cur_stav_sk[7] = 30904384571248922;
	cur_stav_sk[8] = 71443797781221654;
	cur_stav_sk[9] = 91976747680532;
	cur_stav_sk[10] = 5736707303801886;
	cur_stav_sk[11] = 29353200823697524;
	cur_stav_sk[12] = 10670559642006861;
	cur_stav_sk[13] = 537472494703728;
	cur_stav_sk[14] = 10451551126955166;
	cur_stav_sk[15] = 23714615221756410;
	cur_stav_sk[16] = 10222811391768389;
	cur_stav_sk[17] = 554791210418756;
	cur_stav_sk[18] = 13810182941254140;
	cur_stav_sk[19] = 5049525472329854;
	cur_stav_sk[20] = 14716696330920513;
	cur_stav_sk[21] = 13617250884232818;
	cur_stav_sk[22] = 62427621526274428;
	cur_stav_sk[23] = 15746754567607670;
	cur_stav_sk[24] = 30919949737333836;
	cur_stav_sk[25] = 4601337312781820;
	cur_stav_sk[26] = 28262429006935570;
	cur_stav_sk[27] = 31815763408885056;
	cur_stav_sk[28] = 15842596434845948;
	cur_stav_sk[29] = 111894653383188;
	cur_stav_sk[30] = 27145313451053128;
	cur_stav_sk[31] = 15833558624063510;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28679624256754761;
	cur_stav_sk[1] = 13643638669971832;
	cur_stav_sk[2] = 9095920484221974;
	cur_stav_sk[3] = 32784625300899140;
	cur_stav_sk[4] = 5058746321351068;
	cur_stav_sk[5] = 14602219950376010;
	cur_stav_sk[6] = 7045188472340600;
	cur_stav_sk[7] = 9341893940721020;
	cur_stav_sk[8] = 65804346134594629;
	cur_stav_sk[9] = 31649391766733853;
	cur_stav_sk[10] = 31811637418004756;
	cur_stav_sk[11] = 31605537452168572;
	cur_stav_sk[12] = 60897021881893916;
	cur_stav_sk[13] = 12474883042677114;
	cur_stav_sk[14] = 31632429913454352;
	cur_stav_sk[15] = 9536977114202236;
	cur_stav_sk[16] = 12473417389048184;
	cur_stav_sk[17] = 34983535349969477;
	cur_stav_sk[18] = 68437069839941746;
	cur_stav_sk[19] = 70106256203932920;
	cur_stav_sk[20] = 28474635130736666;
	cur_stav_sk[21] = 69353653422391578;
	cur_stav_sk[22] = 18517444676124700;
	cur_stav_sk[23] = 10415154047358324;
	cur_stav_sk[24] = 24841617405719364;
	cur_stav_sk[25] = 27101729058988912;
	cur_stav_sk[26] = 17400698364563834;
	cur_stav_sk[27] = 4602423282499960;
	cur_stav_sk[28] = 10108034336882972;
	cur_stav_sk[29] = 9078426663002616;
	cur_stav_sk[30] = 65399246917177460;
	cur_stav_sk[31] = 12473758629343858;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 65399589760372814;
	cur_stav_sk[1] = 31636805355926034;
	cur_stav_sk[2] = 10680208361521430;
	cur_stav_sk[3] = 11254367514300692;
	cur_stav_sk[4] = 13600428432594205;
	cur_stav_sk[5] = 14180338722808338;
	cur_stav_sk[6] = 10679142603104882;
	cur_stav_sk[7] = 68222574341161488;
	cur_stav_sk[8] = 12886413408048450;
	cur_stav_sk[9] = 9561922695169812;
	cur_stav_sk[10] = 54739768026011000;
	cur_stav_sk[11] = 546611966092816;
	cur_stav_sk[12] = 27145368611063158;
	cur_stav_sk[13] = 68671656118259830;
	cur_stav_sk[14] = 1664212944125980;
	cur_stav_sk[15] = 32977383833614192;
	cur_stav_sk[16] = 15191161362973980;
	cur_stav_sk[17] = 14025205177258352;
	cur_stav_sk[18] = 6159426642157976;
	cur_stav_sk[19] = 14734147517710714;
	cur_stav_sk[20] = 10124527566328186;
	cur_stav_sk[21] = 10266696272975224;
	cur_stav_sk[22] = 11778811605393816;
	cur_stav_sk[23] = 27558852556195396;
	cur_stav_sk[24] = 22839624818668310;
	cur_stav_sk[25] = 15165124325573918;
	cur_stav_sk[26] = 15861094329320000;
	cur_stav_sk[27] = 11762100814581784;
	cur_stav_sk[28] = 15527036388062460;
	cur_stav_sk[29] = 35423891750655262;
	cur_stav_sk[30] = 70299495883583646;
	cur_stav_sk[31] = 28245179794502420;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28253908917102454;
	cur_stav_sk[1] = 32756955358605513;
	cur_stav_sk[2] = 9342558376395284;
	cur_stav_sk[3] = 18552133251413246;
	cur_stav_sk[4] = 27131717526692368;
	cur_stav_sk[5] = 9521720385778078;
	cur_stav_sk[6] = 11751635242414656;
	cur_stav_sk[7] = 18570000311170928;
	cur_stav_sk[8] = 69774318569272176;
	cur_stav_sk[9] = 66500119516087320;
	cur_stav_sk[10] = 65269010763560560;
	cur_stav_sk[11] = 32748480986813460;
	cur_stav_sk[12] = 31852319549624341;
	cur_stav_sk[13] = 14021484995323134;
	cur_stav_sk[14] = 69577924734816322;
	cur_stav_sk[15] = 65393839541519646;
	cur_stav_sk[16] = 66942627858908444;
	cur_stav_sk[17] = 10688145529771342;
	cur_stav_sk[18] = 27092811763320132;
	cur_stav_sk[19] = 32063150652593532;
	cur_stav_sk[20] = 5709700026631544;
	cur_stav_sk[21] = 8392602878607378;
	cur_stav_sk[22] = 28693780673017364;
	cur_stav_sk[23] = 66522382681973620;
	cur_stav_sk[24] = 14013639915177242;
	cur_stav_sk[25] = 10688153981432694;
	cur_stav_sk[26] = 27553655642961782;
	cur_stav_sk[27] = 65286326444306460;
	cur_stav_sk[28] = 23751107496068122;
	cur_stav_sk[29] = 22645477619447290;
	cur_stav_sk[30] = 13644697939820666;
	cur_stav_sk[31] = 13845218143380636;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31596601961545794;
	cur_stav_sk[1] = 9509075379200408;
	cur_stav_sk[2] = 14742626727006486;
	cur_stav_sk[3] = 29775413758558284;
	cur_stav_sk[4] = 9084044895564184;
	cur_stav_sk[5] = 27119141511439890;
	cur_stav_sk[6] = 17171929958319380;
	cur_stav_sk[7] = 27559840613205622;
	cur_stav_sk[8] = 13849066358444154;
	cur_stav_sk[9] = 132703380583708;
	cur_stav_sk[10] = 65392222702862870;
	cur_stav_sk[11] = 10819751171261816;
	cur_stav_sk[12] = 10215973803885854;
	cur_stav_sk[13] = 9509215571714832;
	cur_stav_sk[14] = 9527289151033413;
	cur_stav_sk[15] = 27343513706173720;
	cur_stav_sk[16] = 14733667429462140;
	cur_stav_sk[17] = 6123830944543504;
	cur_stav_sk[18] = 31596408446128412;
	cur_stav_sk[19] = 14725722275120248;
	cur_stav_sk[20] = 528677188535570;
	cur_stav_sk[21] = 23716015047606296;
	cur_stav_sk[22] = 14021324001194768;
	cur_stav_sk[23] = 32017921632768280;
	cur_stav_sk[24] = 28644069443057221;
	cur_stav_sk[25] = 31618951369814142;
	cur_stav_sk[26] = 5727043079374206;
	cur_stav_sk[27] = 11356804334166545;
	cur_stav_sk[28] = 4838717272568338;
	cur_stav_sk[29] = 10221752259323766;
	cur_stav_sk[30] = 10205122022225353;
	cur_stav_sk[31] = 27153641775080954;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15191544677990772;
	cur_stav_sk[1] = 28667845849941274;
	cur_stav_sk[2] = 31657376786224144;
	cur_stav_sk[3] = 81164687410546;
	cur_stav_sk[4] = 68223328573501592;
	cur_stav_sk[5] = 11549837074249540;
	cur_stav_sk[6] = 9543780728782917;
	cur_stav_sk[7] = 5200147765888282;
	cur_stav_sk[8] = 15129904520004976;
	cur_stav_sk[9] = 1232720789541910;
	cur_stav_sk[10] = 29361985177452914;
	cur_stav_sk[11] = 9139435409442125;
	cur_stav_sk[12] = 32072306436571256;
	cur_stav_sk[13] = 17382802636866678;
	cur_stav_sk[14] = 34974922657923097;
	cur_stav_sk[15] = 9140108180025502;
	cur_stav_sk[16] = 22861887578810132;
	cur_stav_sk[17] = 32061792910291228;
	cur_stav_sk[18] = 4584774794945740;
	cur_stav_sk[19] = 27338813380375032;
	cur_stav_sk[20] = 10248066541880082;
	cur_stav_sk[21] = 27101386449957744;
	cur_stav_sk[22] = 15148596149921652;
	cur_stav_sk[23] = 9545510285476888;
	cur_stav_sk[24] = 2534104273826844;
	cur_stav_sk[25] = 11781490717065290;
	cur_stav_sk[26] = 107223531722008;
	cur_stav_sk[27] = 98081569542420;
	cur_stav_sk[28] = 31649996866359672;
	cur_stav_sk[29] = 69367165882839372;
	cur_stav_sk[30] = 13634582341231478;
	cur_stav_sk[31] = 15177520762361204;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 34974921098343834;
	cur_stav_sk[1] = 32045560949608568;
	cur_stav_sk[2] = 66942327186166034;
	cur_stav_sk[3] = 69367439751517206;
	cur_stav_sk[4] = 27128051959530006;
	cur_stav_sk[5] = 32757274347249816;
	cur_stav_sk[6] = 5946714557253492;
	cur_stav_sk[7] = 14066745377534456;
	cur_stav_sk[8] = 19678264563827058;
	cur_stav_sk[9] = 28643546546634770;
	cur_stav_sk[10] = 13626075340845684;
	cur_stav_sk[11] = 20778506787369846;
	cur_stav_sk[12] = 10265700388352200;
	cur_stav_sk[13] = 27567467242947706;
	cur_stav_sk[14] = 9139264602650228;
	cur_stav_sk[15] = 71002780153656081;
	cur_stav_sk[16] = 69877842447733016;
	cur_stav_sk[17] = 13644752471524636;
	cur_stav_sk[18] = 10266695803274522;
	cur_stav_sk[19] = 5622716170703169;
	cur_stav_sk[20] = 24849518591672698;
	cur_stav_sk[21] = 15851117385811016;
	cur_stav_sk[22] = 2331912782087500;
	cur_stav_sk[23] = 27110389972382744;
	cur_stav_sk[24] = 27154096989897082;
	cur_stav_sk[25] = 34297072132949022;
	cur_stav_sk[26] = 28469661021507606;
	cur_stav_sk[27] = 9703003494027646;
	cur_stav_sk[28] = 23768724827682298;
	cur_stav_sk[29] = 15148690768610934;
	cur_stav_sk[30] = 6518762866225613;
	cur_stav_sk[31] = 29775741270696720;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27568611851511290;
	cur_stav_sk[1] = 15745658302313334;
	cur_stav_sk[2] = 35415232174067828;
	cur_stav_sk[3] = 299933140992028;
	cur_stav_sk[4] = 23948195096981756;
	cur_stav_sk[5] = 34991756795524604;
	cur_stav_sk[6] = 65275949331382394;
	cur_stav_sk[7] = 32022125133202496;
	cur_stav_sk[8] = 22853932833640478;
	cur_stav_sk[9] = 10423237630297460;
	cur_stav_sk[10] = 13638209290278262;
	cur_stav_sk[11] = 23780104430072692;
	cur_stav_sk[12] = 31605191434572876;
	cur_stav_sk[13] = 24849613698053628;
	cur_stav_sk[14] = 27092008007984660;
	cur_stav_sk[15] = 66508983302362486;
	cur_stav_sk[16] = 15754107766936690;
	cur_stav_sk[17] = 28281438061625722;
	cur_stav_sk[18] = 63720636778184978;
	cur_stav_sk[19] = 27110458511392882;
	cur_stav_sk[20] = 69577677456213016;
	cur_stav_sk[21] = 28228411722183186;
	cur_stav_sk[22] = 25988614714725752;
	cur_stav_sk[23] = 34297911393551384;
	cur_stav_sk[24] = 13582068514456944;
	cur_stav_sk[25] = 15174666546057342;
	cur_stav_sk[26] = 19462735643255666;
	cur_stav_sk[27] = 27092867593602170;
	cur_stav_sk[28] = 14936870519476600;
	cur_stav_sk[29] = 16276662816014458;
	cur_stav_sk[30] = 14007063032084340;
	cur_stav_sk[31] = 27145366919188760;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 23927055801397278;
	cur_stav_sk[1] = 32018464273371260;
	cur_stav_sk[2] = 19475269026780536;
	cur_stav_sk[3] = 12887202053076800;
	cur_stav_sk[4] = 4578132950791448;
	cur_stav_sk[5] = 25978203110864502;
	cur_stav_sk[6] = 20355127237129372;
	cur_stav_sk[7] = 18147804037591368;
	cur_stav_sk[8] = 65823025417060374;
	cur_stav_sk[9] = 22836311656727156;
	cur_stav_sk[10] = 10661076270547269;
	cur_stav_sk[11] = 9113584215020562;
	cur_stav_sk[12] = 24147301717489218;
	cur_stav_sk[13] = 4820815734288766;
	cur_stav_sk[14] = 6131528134244210;
	cur_stav_sk[15] = 10661213937414928;
	cur_stav_sk[16] = 13609225191375892;
	cur_stav_sk[17] = 70317980166067274;
	cur_stav_sk[18] = 15855800771519902;
	cur_stav_sk[19] = 14066756915167256;
	cur_stav_sk[20] = 17196529384375322;
	cur_stav_sk[21] = 27150796840206716;
	cur_stav_sk[22] = 28221676820738456;
	cur_stav_sk[23] = 14040303331546128;
	cur_stav_sk[24] = 16977016872408438;
	cur_stav_sk[25] = 28644337896064025;
	cur_stav_sk[26] = 31637080238921074;
	cur_stav_sk[27] = 14005391973516572;
	cur_stav_sk[28] = 23046367179047024;
	cur_stav_sk[29] = 12482829082689560;
	cur_stav_sk[30] = 12670289419141242;
	cur_stav_sk[31] = 10434097466966550;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 17408051296699422;
	cur_stav_sk[1] = 14937722919564050;
	cur_stav_sk[2] = 1198222866399356;
	cur_stav_sk[3] = 30473958048237426;
	cur_stav_sk[4] = 28678730436444530;
	cur_stav_sk[5] = 28684917350859134;
	cur_stav_sk[6] = 33188240945332346;
	cur_stav_sk[7] = 9096346200420476;
	cur_stav_sk[8] = 19680918852044353;
	cur_stav_sk[9] = 9544399943840124;
	cur_stav_sk[10] = 66516913979766386;
	cur_stav_sk[11] = 8383576447760246;
	cur_stav_sk[12] = 16959654483759226;
	cur_stav_sk[13] = 7982072186933372;
	cur_stav_sk[14] = 25967174391104880;
	cur_stav_sk[15] = 7979847461012084;
	cur_stav_sk[16] = 5728213051790364;
	cur_stav_sk[17] = 31606086152594590;
	cur_stav_sk[18] = 105901683836176;
	cur_stav_sk[19] = 13582092659524728;
	cur_stav_sk[20] = 27127741527073018;
	cur_stav_sk[21] = 69352080458297878;
	cur_stav_sk[22] = 69349295797011830;
	cur_stav_sk[23] = 8374915663007762;
	cur_stav_sk[24] = 28226555161130318;
	cur_stav_sk[25] = 27093898774193562;
	cur_stav_sk[26] = 30902123806233972;
	cur_stav_sk[27] = 11347266640425242;
	cur_stav_sk[28] = 25976404807909744;
	cur_stav_sk[29] = 10664580515038586;
	cur_stav_sk[30] = 24198704275559710;
	cur_stav_sk[31] = 3892038227854194;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 35405953893667868;
	cur_stav_sk[1] = 11347516350398840;
	cur_stav_sk[2] = 57931650077431068;
	cur_stav_sk[3] = 66939923831657498;
	cur_stav_sk[4] = 69789400346965404;
	cur_stav_sk[5] = 14752727939719416;
	cur_stav_sk[6] = 30686164324398612;
	cur_stav_sk[7] = 31650285648381202;
	cur_stav_sk[8] = 71003812089200754;
	cur_stav_sk[9] = 71232277955448856;
	cur_stav_sk[10] = 13851377461993884;
	cur_stav_sk[11] = 28676919566754325;
	cur_stav_sk[12] = 9135185006571646;
	cur_stav_sk[13] = 6158166217556756;
	cur_stav_sk[14] = 10670446833668169;
	cur_stav_sk[15] = 27558382845960392;
	cur_stav_sk[16] = 22625343345341720;
	cur_stav_sk[17] = 15728200328193528;
	cur_stav_sk[18] = 71031792303801622;
	cur_stav_sk[19] = 32053864320794648;
	cur_stav_sk[20] = 31608595527730804;
	cur_stav_sk[21] = 14759911267500109;
	cur_stav_sk[22] = 13634375375959418;
	cur_stav_sk[23] = 19228910101501209;
	cur_stav_sk[24] = 13583111124484117;
	cur_stav_sk[25] = 15851758148289814;
	cur_stav_sk[26] = 19696452335055378;
	cur_stav_sk[27] = 14764799948719432;
	cur_stav_sk[28] = 15833524922024722;
	cur_stav_sk[29] = 6853607630999830;
	cur_stav_sk[30] = 12895971315225712;
	cur_stav_sk[31] = 19251905503244668;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9535793913464084;
	cur_stav_sk[1] = 64844900392174354;
	cur_stav_sk[2] = 14726315971393136;
	cur_stav_sk[3] = 14710784374645198;
	cur_stav_sk[4] = 14620485289489822;
	cur_stav_sk[5] = 15175192804622668;
	cur_stav_sk[6] = 32081293668910708;
	cur_stav_sk[7] = 15192025181589520;
	cur_stav_sk[8] = 10454714450153854;
	cur_stav_sk[9] = 10626616210129948;
	cur_stav_sk[10] = 31641272172970358;
	cur_stav_sk[11] = 6166778552422729;
	cur_stav_sk[12] = 65798834754041214;
	cur_stav_sk[13] = 66932838190485622;
	cur_stav_sk[14] = 5727938108784762;
	cur_stav_sk[15] = 14743177544041078;
	cur_stav_sk[16] = 69895425100190796;
	cur_stav_sk[17] = 14039599622198300;
	cur_stav_sk[18] = 35211341271680580;
	cur_stav_sk[19] = 9535864770072645;
	cur_stav_sk[20] = 16273545521206388;
	cur_stav_sk[21] = 60367997673149456;
	cur_stav_sk[22] = 11751679804802326;
	cur_stav_sk[23] = 27550023765918029;
	cur_stav_sk[24] = 9077669003855120;
	cur_stav_sk[25] = 14049372212069656;
	cur_stav_sk[26] = 6149944428007752;
	cur_stav_sk[27] = 27577062359074076;
	cur_stav_sk[28] = 70316524367545628;
	cur_stav_sk[29] = 3879107160749936;
	cur_stav_sk[30] = 24190909983491358;
	cur_stav_sk[31] = 12905825966630398;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27093920807654422;
	cur_stav_sk[1] = 14760286494232688;
	cur_stav_sk[2] = 14940332593660948;
	cur_stav_sk[3] = 11331325248557078;
	cur_stav_sk[4] = 25074230672847474;
	cur_stav_sk[5] = 687544087680078;
	cur_stav_sk[6] = 287253339637022;
	cur_stav_sk[7] = 15333247475488030;
	cur_stav_sk[8] = 24190706996183420;
	cur_stav_sk[9] = 12474860374995998;
	cur_stav_sk[10] = 6158192310400380;
	cur_stav_sk[11] = 28705815524442740;
	cur_stav_sk[12] = 13840444070670620;
	cur_stav_sk[13] = 63722814348334200;
	cur_stav_sk[14] = 15139824227161116;
	cur_stav_sk[15] = 5407128908009492;
	cur_stav_sk[16] = 28494837603186548;
	cur_stav_sk[17] = 28254017721497204;
	cur_stav_sk[18] = 27305016960497430;
	cur_stav_sk[19] = 10249376878695500;
	cur_stav_sk[20] = 5040813824226462;
	cur_stav_sk[21] = 3669900321576000;
	cur_stav_sk[22] = 27577228092806426;
	cur_stav_sk[23] = 12877581653520636;
	cur_stav_sk[24] = 10652968988672028;
	cur_stav_sk[25] = 12482073707020560;
	cur_stav_sk[26] = 1672911383705362;
	cur_stav_sk[27] = 68654061247238516;
	cur_stav_sk[28] = 14752935189512572;
	cur_stav_sk[29] = 28676738246850372;
	cur_stav_sk[30] = 65399575464517836;
	cur_stav_sk[31] = 9324976664220992;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28666842990424948;
	cur_stav_sk[1] = 28281324626214988;
	cur_stav_sk[2] = 18142498752697726;
	cur_stav_sk[3] = 68223122870313421;
	cur_stav_sk[4] = 31649047490478610;
	cur_stav_sk[5] = 18102707686216722;
	cur_stav_sk[6] = 7961149978935570;
	cur_stav_sk[7] = 33170993992434546;
	cur_stav_sk[8] = 66509283346120988;
	cur_stav_sk[9] = 9105981963612188;
	cur_stav_sk[10] = 27366589956363332;
	cur_stav_sk[11] = 27333725995706230;
	cur_stav_sk[12] = 10661238474121794;
	cur_stav_sk[13] = 29371122044601712;
	cur_stav_sk[14] = 1241931830093052;
	cur_stav_sk[15] = 33171790350422302;
	cur_stav_sk[16] = 33172064551010636;
	cur_stav_sk[17] = 13644739652268822;
	cur_stav_sk[18] = 28675339020305790;
	cur_stav_sk[19] = 5765501284549648;
	cur_stav_sk[20] = 14753182827951728;
	cur_stav_sk[21] = 62003732954809366;
	cur_stav_sk[22] = 31816448871936581;
	cur_stav_sk[23] = 69877818884338196;
	cur_stav_sk[24] = 8384400564027678;
	cur_stav_sk[25] = 2349126005261438;
	cur_stav_sk[26] = 32941161351787380;
	cur_stav_sk[27] = 9536415663011277;
	cur_stav_sk[28] = 31641009056026944;
	cur_stav_sk[29] = 11761287600317592;
	cur_stav_sk[30] = 28257009162060926;
	cur_stav_sk[31] = 10802295420793976;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 6299752350496272;
	cur_stav_sk[1] = 32938653426483472;
	cur_stav_sk[2] = 27550411317949852;
	cur_stav_sk[3] = 30901780888962168;
	cur_stav_sk[4] = 13609554353071730;
	cur_stav_sk[5] = 10108461624624250;
	cur_stav_sk[6] = 9324690418377208;
	cur_stav_sk[7] = 15745013506131014;
	cur_stav_sk[8] = 15843487968591996;
	cur_stav_sk[9] = 10257103009039384;
	cur_stav_sk[10] = 23758899778008848;
	cur_stav_sk[11] = 28649799469740186;
	cur_stav_sk[12] = 14768131438183700;
	cur_stav_sk[13] = 13627148695121950;
	cur_stav_sk[14] = 301010652725616;
	cur_stav_sk[15] = 32783937416135488;
	cur_stav_sk[16] = 31613931768709184;
	cur_stav_sk[17] = 27135553114092696;
	cur_stav_sk[18] = 24198812182294729;
	cur_stav_sk[19] = 22642826602616184;
	cur_stav_sk[20] = 18124217749145206;
	cur_stav_sk[21] = 9536137571743506;
	cur_stav_sk[22] = 3474764595631482;
	cur_stav_sk[23] = 27145585972674893;
	cur_stav_sk[24] = 70298929706537288;
	cur_stav_sk[25] = 14048193465529108;
	cur_stav_sk[26] = 10257718260348736;
	cur_stav_sk[27] = 32062244539803154;
	cur_stav_sk[28] = 13634429516133784;
	cur_stav_sk[29] = 32766647712297112;
	cur_stav_sk[30] = 33848004895019126;
	cur_stav_sk[31] = 12895999158490224;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11762112629970294;
	cur_stav_sk[1] = 14726107727639410;
	cur_stav_sk[2] = 28649841543557652;
	cur_stav_sk[3] = 1447815987081624;
	cur_stav_sk[4] = 5005213847421814;
	cur_stav_sk[5] = 14769633937671294;
	cur_stav_sk[6] = 66727741505077278;
	cur_stav_sk[7] = 28270717625541880;
	cur_stav_sk[8] = 64849502449579516;
	cur_stav_sk[9] = 10670586424439572;
	cur_stav_sk[10] = 3469644251758616;
	cur_stav_sk[11] = 9508938836129556;
	cur_stav_sk[12] = 14753002229744144;
	cur_stav_sk[13] = 15855951651607824;
	cur_stav_sk[14] = 10256824444195908;
	cur_stav_sk[15] = 18568776197279858;
	cur_stav_sk[16] = 14709067601641490;
	cur_stav_sk[17] = 1464317028466714;
	cur_stav_sk[18] = 9334609985094674;
	cur_stav_sk[19] = 30497804183179376;
	cur_stav_sk[20] = 14729949579908218;
	cur_stav_sk[21] = 34064252293939316;
	cur_stav_sk[22] = 28280019643990384;
	cur_stav_sk[23] = 28686015791468912;
	cur_stav_sk[24] = 28219317259023512;
	cur_stav_sk[25] = 11761163454685432;
	cur_stav_sk[26] = 22589292452685426;
	cur_stav_sk[27] = 27576686134333468;
	cur_stav_sk[28] = 12668330314204016;
	cur_stav_sk[29] = 14180131422797936;
	cur_stav_sk[30] = 9310981219119632;
	cur_stav_sk[31] = 28468560662638749;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14769565892675094;
	cur_stav_sk[1] = 13625821866341957;
	cur_stav_sk[2] = 63723878093066366;
	cur_stav_sk[3] = 14976890964280944;
	cur_stav_sk[4] = 9910040576565658;
	cur_stav_sk[5] = 17384891146905456;
	cur_stav_sk[6] = 31640539507004530;
	cur_stav_sk[7] = 19237982823756926;
	cur_stav_sk[8] = 63948153771000948;
	cur_stav_sk[9] = 71424513333429628;
	cur_stav_sk[10] = 14048053142042226;
	cur_stav_sk[11] = 9528168673063489;
	cur_stav_sk[12] = 13608757990589820;
	cur_stav_sk[13] = 68663134354650998;
	cur_stav_sk[14] = 15854792404775538;
	cur_stav_sk[15] = 15306067299206000;
	cur_stav_sk[16] = 21489481261953553;
	cur_stav_sk[17] = 18525345067999384;
	cur_stav_sk[18] = 4628493725803774;
	cur_stav_sk[19] = 327385497404696;
	cur_stav_sk[20] = 31649254587958640;
	cur_stav_sk[21] = 71029213060105232;
	cur_stav_sk[22] = 9288708075647224;
	cur_stav_sk[23] = 9342006183036280;
	cur_stav_sk[24] = 26406768919610494;
	cur_stav_sk[25] = 12688156624320114;
	cur_stav_sk[26] = 5387887961354618;
	cur_stav_sk[27] = 69774224069035292;
	cur_stav_sk[28] = 25975214550521112;
	cur_stav_sk[29] = 10828845226726512;
	cur_stav_sk[30] = 25993347318612596;
	cur_stav_sk[31] = 9536688758296944;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 17391899520021104;
	cur_stav_sk[1] = 62023278798841668;
	cur_stav_sk[2] = 32053544082294384;
	cur_stav_sk[3] = 31847112778231836;
	cur_stav_sk[4] = 15148597173293434;
	cur_stav_sk[5] = 14013363091025994;
	cur_stav_sk[6] = 9124969424225557;
	cur_stav_sk[7] = 9521632810832156;
	cur_stav_sk[8] = 29784001092724500;
	cur_stav_sk[9] = 71428635287786828;
	cur_stav_sk[10] = 512103987786232;
	cur_stav_sk[11] = 14040562967282970;
	cur_stav_sk[12] = 19696380734308684;
	cur_stav_sk[13] = 18094529718986260;
	cur_stav_sk[14] = 16264936013337976;
	cur_stav_sk[15] = 14057539180660086;
	cur_stav_sk[16] = 33197147098648854;
	cur_stav_sk[17] = 28244973581705421;
	cur_stav_sk[18] = 9113712921420562;
	cur_stav_sk[19] = 28228685649445912;
	cur_stav_sk[20] = 28684707902685970;
	cur_stav_sk[21] = 33153716591132794;
	cur_stav_sk[22] = 11342329493259538;
	cur_stav_sk[23] = 11236671215633526;
	cur_stav_sk[24] = 32083685417059698;
	cur_stav_sk[25] = 15736929501085974;
	cur_stav_sk[26] = 27138109183656984;
	cur_stav_sk[27] = 14972741892444272;
	cur_stav_sk[28] = 14614295775937392;
	cur_stav_sk[29] = 15146904963031544;
	cur_stav_sk[30] = 3466492815094644;
	cur_stav_sk[31] = 11762167603856414;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14057055931311984;
	cur_stav_sk[1] = 13583028505032518;
	cur_stav_sk[2] = 11779899310047514;
	cur_stav_sk[3] = 10239062649022066;
	cur_stav_sk[4] = 10257444460466196;
	cur_stav_sk[5] = 8189720094188100;
	cur_stav_sk[6] = 63729707549439098;
	cur_stav_sk[7] = 19211627091900276;
	cur_stav_sk[8] = 57493015803397240;
	cur_stav_sk[9] = 6141422608987550;
	cur_stav_sk[10] = 12881264136455454;
	cur_stav_sk[11] = 33189175637734652;
	cur_stav_sk[12] = 11357713670107390;
	cur_stav_sk[13] = 10820943007942464;
	cur_stav_sk[14] = 27146220004676720;
	cur_stav_sk[15] = 71023946287649916;
	cur_stav_sk[16] = 1813470037082134;
	cur_stav_sk[17] = 20337202992821709;
	cur_stav_sk[18] = 9131475280331032;
	cur_stav_sk[19] = 14707375181594902;
	cur_stav_sk[20] = 11339008135565646;
	cur_stav_sk[21] = 32063438539141748;
	cur_stav_sk[22] = 28469274164069402;
	cur_stav_sk[23] = 24163516613628176;
	cur_stav_sk[24] = 29776706026185978;
	cur_stav_sk[25] = 123519129883766;
	cur_stav_sk[26] = 14936623550378362;
	cur_stav_sk[27] = 13634567023699354;
	cur_stav_sk[28] = 32080264070893854;
	cur_stav_sk[29] = 32734527181103258;
	cur_stav_sk[30] = 9688457861833976;
	cur_stav_sk[31] = 70108865580302412;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 132499231224092;
	cur_stav_sk[1] = 33189409176585330;
	cur_stav_sk[2] = 68231944819093624;
	cur_stav_sk[3] = 10118941880267036;
	cur_stav_sk[4] = 9545692143687028;
	cur_stav_sk[5] = 10212375148180990;
	cur_stav_sk[6] = 10249472040730697;
	cur_stav_sk[7] = 9325800206709916;
	cur_stav_sk[8] = 10468456877270473;
	cur_stav_sk[9] = 63746131184616976;
	cur_stav_sk[10] = 25052941539086460;
	cur_stav_sk[11] = 28237484379078940;
	cur_stav_sk[12] = 32062637413863750;
	cur_stav_sk[13] = 16274509424168976;
	cur_stav_sk[14] = 22822295169995800;
	cur_stav_sk[15] = 10212544800994712;
	cur_stav_sk[16] = 14708817935672828;
	cur_stav_sk[17] = 12456167064015945;
	cur_stav_sk[18] = 9135309607601228;
	cur_stav_sk[19] = 70106792717583644;
	cur_stav_sk[20] = 28650900123628402;
	cur_stav_sk[21] = 31643450064507972;
	cur_stav_sk[22] = 70089199953416318;
	cur_stav_sk[23] = 27136572654494226;
	cur_stav_sk[24] = 9078534594893086;
	cur_stav_sk[25] = 66933386674836858;
	cur_stav_sk[26] = 23058576684819710;
	cur_stav_sk[27] = 14918891772847636;
	cur_stav_sk[28] = 32783689985785880;
	cur_stav_sk[29] = 66921462394322974;
	cur_stav_sk[30] = 32954869804606714;
	cur_stav_sk[31] = 28430351399818524;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 13616909271433292;
	cur_stav_sk[1] = 9535875639641108;
	cur_stav_sk[2] = 31632406784246910;
	cur_stav_sk[3] = 15859580399949078;
	cur_stav_sk[4] = 10812421813744406;
	cur_stav_sk[5] = 19654738346136600;
	cur_stav_sk[6] = 9535376622224668;
	cur_stav_sk[7] = 55162159268496708;
	cur_stav_sk[8] = 13634570261804816;
	cur_stav_sk[9] = 35406023235470622;
	cur_stav_sk[10] = 4610076176123154;
	cur_stav_sk[11] = 2323068559524116;
	cur_stav_sk[12] = 5172961226149904;
	cur_stav_sk[13] = 14971531379455384;
	cur_stav_sk[14] = 10635128225444372;
	cur_stav_sk[15] = 14954738106799226;
	cur_stav_sk[16] = 32783894151824410;
	cur_stav_sk[17] = 23755667053249857;
	cur_stav_sk[18] = 31660649470035226;
	cur_stav_sk[19] = 10222741660644474;
	cur_stav_sk[20] = 9554307831334270;
	cur_stav_sk[21] = 35422793258468980;
	cur_stav_sk[22] = 10453098531436148;
	cur_stav_sk[23] = 29555441722926660;
	cur_stav_sk[24] = 9350829945869334;
	cur_stav_sk[25] = 5754052060148288;
	cur_stav_sk[26] = 14065865490501904;
	cur_stav_sk[27] = 32775098063650894;
	cur_stav_sk[28] = 29347655633491064;
	cur_stav_sk[29] = 16986042282779128;
	cur_stav_sk[30] = 10652717812789278;
	cur_stav_sk[31] = 13634308183359772;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10239275268220158;
	cur_stav_sk[1] = 32993663443211634;
	cur_stav_sk[2] = 6854292742767998;
	cur_stav_sk[3] = 10638287733425438;
	cur_stav_sk[4] = 15157392329412986;
	cur_stav_sk[5] = 6131638864418070;
	cur_stav_sk[6] = 27544626601038920;
	cur_stav_sk[7] = 65804195422011410;
	cur_stav_sk[8] = 9518984569136382;
	cur_stav_sk[9] = 15861104867356924;
	cur_stav_sk[10] = 33179548535661076;
	cur_stav_sk[11] = 28227596748001602;
	cur_stav_sk[12] = 9544386115764544;
	cur_stav_sk[13] = 18551887449822324;
	cur_stav_sk[14] = 9536275568996378;
	cur_stav_sk[15] = 28492501141107066;
	cur_stav_sk[16] = 55856022321169736;
	cur_stav_sk[17] = 64141733839246452;
	cur_stav_sk[18] = 9134041074570256;
	cur_stav_sk[19] = 14755106815521654;
	cur_stav_sk[20] = 65813428855154072;
	cur_stav_sk[21] = 33179721485059352;
	cur_stav_sk[22] = 24190936959906420;
	cur_stav_sk[23] = 24198703872869404;
	cur_stav_sk[24] = 14391237130850326;
	cur_stav_sk[25] = 32959543810298236;
	cur_stav_sk[26] = 11233535509431620;
	cur_stav_sk[27] = 18552685758711412;
	cur_stav_sk[28] = 63736701580555894;
	cur_stav_sk[29] = 66710461438034042;
	cur_stav_sk[30] = 14047734971335444;
	cur_stav_sk[31] = 27119185415209584;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31623703091880564;
	cur_stav_sk[1] = 5735954142601470;
	cur_stav_sk[2] = 62444115821635444;
	cur_stav_sk[3] = 66508389471354994;
	cur_stav_sk[4] = 10432747773572248;
	cur_stav_sk[5] = 23751476243300730;
	cur_stav_sk[6] = 12904493918688334;
	cur_stav_sk[7] = 18560379255161206;
	cur_stav_sk[8] = 64141597195286341;
	cur_stav_sk[9] = 31660504639485052;
	cur_stav_sk[10] = 2748466820232476;
	cur_stav_sk[11] = 63747230363727354;
	cur_stav_sk[12] = 10626330523477780;
	cur_stav_sk[13] = 18544051333010558;
	cur_stav_sk[14] = 9351641094721810;
	cur_stav_sk[15] = 69904495410549008;
	cur_stav_sk[16] = 15192533123530876;
	cur_stav_sk[17] = 17170666160103954;
	cur_stav_sk[18] = 33162307391914014;
	cur_stav_sk[19] = 13590864672895860;
	cur_stav_sk[20] = 14711359367746684;
	cur_stav_sk[21] = 9330185695626312;
	cur_stav_sk[22] = 13810195952439574;
	cur_stav_sk[23] = 30902536185872409;
	cur_stav_sk[24] = 33848266840258040;
	cur_stav_sk[25] = 54730191721116744;
	cur_stav_sk[26] = 22650041158545940;
	cur_stav_sk[27] = 71424375749615774;
	cur_stav_sk[28] = 71231593999250242;
	cur_stav_sk[29] = 11770909778252110;
	cur_stav_sk[30] = 23011239036461310;
	cur_stav_sk[31] = 19264161206604096;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14726820230300434;
	cur_stav_sk[1] = 5059777025934706;
	cur_stav_sk[2] = 13638122804257228;
	cur_stav_sk[3] = 32053724471727384;
	cur_stav_sk[4] = 10242911301804184;
	cur_stav_sk[5] = 22632858990282106;
	cur_stav_sk[6] = 61998923113116828;
	cur_stav_sk[7] = 66500392833753502;
	cur_stav_sk[8] = 5006921898016832;
	cur_stav_sk[9] = 14201311579157268;
	cur_stav_sk[10] = 25264034874204412;
	cur_stav_sk[11] = 13820618031859064;
	cur_stav_sk[12] = 15861516849321238;
	cur_stav_sk[13] = 10249105362616594;
	cur_stav_sk[14] = 16976836974556790;
	cur_stav_sk[15] = 30902399963271198;
	cur_stav_sk[16] = 10473430434186526;
	cur_stav_sk[17] = 31630839850631956;
	cur_stav_sk[18] = 1629001845966192;
	cur_stav_sk[19] = 10231882564080926;
	cur_stav_sk[20] = 29794105724732694;
	cur_stav_sk[21] = 59638631157334084;
	cur_stav_sk[22] = 9325513865101434;
	cur_stav_sk[23] = 13581611639846394;
	cur_stav_sk[24] = 63719753118651416;
	cur_stav_sk[25] = 23768998860894590;
	cur_stav_sk[26] = 13643366623118608;
	cur_stav_sk[27] = 24173593837253488;
	cur_stav_sk[28] = 6184759902744700;
	cur_stav_sk[29] = 3469994476077174;
	cur_stav_sk[30] = 12688946689872148;
	cur_stav_sk[31] = 31596479439057144;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15833605737820236;
	cur_stav_sk[1] = 14065302333882384;
	cur_stav_sk[2] = 10116420143060088;
	cur_stav_sk[3] = 33162196869263990;
	cur_stav_sk[4] = 32054961225302298;
	cur_stav_sk[5] = 23055403983845136;
	cur_stav_sk[6] = 4593573960253464;
	cur_stav_sk[7] = 9887366391357592;
	cur_stav_sk[8] = 30901600358568980;
	cur_stav_sk[9] = 23741897394913564;
	cur_stav_sk[10] = 27120093599072790;
	cur_stav_sk[11] = 17196644270421240;
	cur_stav_sk[12] = 19212117265909065;
	cur_stav_sk[13] = 132566334318352;
	cur_stav_sk[14] = 32749811431191800;
	cur_stav_sk[15] = 15737961895047420;
	cur_stav_sk[16] = 12464920140222842;
	cur_stav_sk[17] = 21480914468156944;
	cur_stav_sk[18] = 69885549972947996;
	cur_stav_sk[19] = 35189602843361402;
	cur_stav_sk[20] = 31613533617554714;
	cur_stav_sk[21] = 9554240118556026;
	cur_stav_sk[22] = 9113404691985014;
	cur_stav_sk[23] = 8190155278431764;
	cur_stav_sk[24] = 27561670127457558;
	cur_stav_sk[25] = 68223408097527622;
	cur_stav_sk[26] = 32062175678916208;
	cur_stav_sk[27] = 32760941844574786;
	cur_stav_sk[28] = 29576655778485266;
	cur_stav_sk[29] = 9140532236587338;
	cur_stav_sk[30] = 32062410521378838;
	cur_stav_sk[31] = 14715896523986292;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10636404918629884;
	cur_stav_sk[1] = 31597578345197822;
	cur_stav_sk[2] = 10670835599409436;
	cur_stav_sk[3] = 32730638551155828;
	cur_stav_sk[4] = 14769015461741692;
	cur_stav_sk[5] = 66500421300499836;
	cur_stav_sk[6] = 5718016211628408;
	cur_stav_sk[7] = 10221960099080050;
	cur_stav_sk[8] = 15173843526431128;
	cur_stav_sk[9] = 25984215075828596;
	cur_stav_sk[10] = 27127799441822844;
	cur_stav_sk[11] = 35184954078266640;
	cur_stav_sk[12] = 55153157416695316;
	cur_stav_sk[13] = 27303077268460822;
	cur_stav_sk[14] = 32739684972863738;
	cur_stav_sk[15] = 9132069076406394;
	cur_stav_sk[16] = 10098689578374168;
	cur_stav_sk[17] = 10416257366115446;
	cur_stav_sk[18] = 33170828142850934;
	cur_stav_sk[19] = 31631333172090236;
	cur_stav_sk[20] = 66929883120875378;
	cur_stav_sk[21] = 33172023699091989;
	cur_stav_sk[22] = 31597509692654876;
	cur_stav_sk[23] = 15165870125921402;
	cur_stav_sk[24] = 71213725910344208;
	cur_stav_sk[25] = 18111266130105416;
	cur_stav_sk[26] = 31622453990404976;
	cur_stav_sk[27] = 69350424211558725;
	cur_stav_sk[28] = 14039491115582842;
	cur_stav_sk[29] = 14033726736732180;
	cur_stav_sk[30] = 70326213265892208;
	cur_stav_sk[31] = 69780054508188534;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 1250795431363866;
	cur_stav_sk[1] = 13635255387070974;
	cur_stav_sk[2] = 27524988418236617;
	cur_stav_sk[3] = 16045191104795768;
	cur_stav_sk[4] = 14769842244421490;
	cur_stav_sk[5] = 11752297743144060;
	cur_stav_sk[6] = 64151629252731664;
	cur_stav_sk[7] = 10115718639494300;
	cur_stav_sk[8] = 15333246405719666;
	cur_stav_sk[9] = 22853354784625944;
	cur_stav_sk[10] = 16266001634964338;
	cur_stav_sk[11] = 10679038445912341;
	cur_stav_sk[12] = 9123270764598302;
	cur_stav_sk[13] = 71005677715656734;
	cur_stav_sk[14] = 80352601146384;
	cur_stav_sk[15] = 4574607535116353;
	cur_stav_sk[16] = 31869625474253132;
	cur_stav_sk[17] = 22616780402698000;
	cur_stav_sk[18] = 32775168250394133;
	cur_stav_sk[19] = 18141533857674392;
	cur_stav_sk[20] = 9909630562218817;
	cur_stav_sk[21] = 118582996115960;
	cur_stav_sk[22] = 14742663754155341;
	cur_stav_sk[23] = 31648925270437912;
	cur_stav_sk[24] = 10261499375027525;
	cur_stav_sk[25] = 5612189974855964;
	cur_stav_sk[26] = 59224677968848498;
	cur_stav_sk[27] = 13811258772271892;
	cur_stav_sk[28] = 22643156705518454;
	cur_stav_sk[29] = 30469948847849758;
	cur_stav_sk[30] = 71424831539081750;
	cur_stav_sk[31] = 10670447419273240;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28684161373803634;
	cur_stav_sk[1] = 12457335276864880;
	cur_stav_sk[2] = 30497873109844344;
	cur_stav_sk[3] = 10652719427523856;
	cur_stav_sk[4] = 22632630362944322;
	cur_stav_sk[5] = 18124285332042909;
	cur_stav_sk[6] = 63719521697304858;
	cur_stav_sk[7] = 10230822779236976;
	cur_stav_sk[8] = 19668690754384662;
	cur_stav_sk[9] = 10265251767269232;
	cur_stav_sk[10] = 14736865102181528;
	cur_stav_sk[11] = 9905233119741040;
	cur_stav_sk[12] = 28448219018892562;
	cur_stav_sk[13] = 3466862789011264;
	cur_stav_sk[14] = 13616452540367728;
	cur_stav_sk[15] = 34296409097334906;
	cur_stav_sk[16] = 10661487745770358;
	cur_stav_sk[17] = 32080298846266872;
	cur_stav_sk[18] = 71442793255698718;
	cur_stav_sk[19] = 9686498884232006;
	cur_stav_sk[20] = 27567717578224844;
	cur_stav_sk[21] = 4593518010331290;
	cur_stav_sk[22] = 32955281044812358;
	cur_stav_sk[23] = 27568706282129012;
	cur_stav_sk[24] = 12456260011242006;
	cur_stav_sk[25] = 10260579189359640;
	cur_stav_sk[26] = 13627076550398234;
	cur_stav_sk[27] = 23769069711099262;
	cur_stav_sk[28] = 9107698289623152;
	cur_stav_sk[29] = 7275292907210612;
	cur_stav_sk[30] = 14047986101092734;
	cur_stav_sk[31] = 11763818251325554;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28702413024436430;
	cur_stav_sk[1] = 4798149762159633;
	cur_stav_sk[2] = 69882216734327934;
	cur_stav_sk[3] = 28702578780833648;
	cur_stav_sk[4] = 23073078240580990;
	cur_stav_sk[5] = 23759171977261938;
	cur_stav_sk[6] = 6150398230672150;
	cur_stav_sk[7] = 15165846813180228;
	cur_stav_sk[8] = 2350499909473349;
	cur_stav_sk[9] = 14047484307116148;
	cur_stav_sk[10] = 24193239395250756;
	cur_stav_sk[11] = 31811912238666102;
	cur_stav_sk[12] = 10240676281517337;
	cur_stav_sk[13] = 31606374488514760;
	cur_stav_sk[14] = 7266211135820914;
	cur_stav_sk[15] = 60562918462488858;
	cur_stav_sk[16] = 11338265462773106;
	cur_stav_sk[17] = 29775080381154576;
	cur_stav_sk[18] = 66929546521195802;
	cur_stav_sk[19] = 23065382681907570;
	cur_stav_sk[20] = 23066983742735634;
	cur_stav_sk[21] = 11753304178897520;
	cur_stav_sk[22] = 10469280227725584;
	cur_stav_sk[23] = 27580018138186562;
	cur_stav_sk[24] = 9563294735119984;
	cur_stav_sk[25] = 15543528609429108;
	cur_stav_sk[26] = 10243882004906004;
	cur_stav_sk[27] = 19678885400327794;
	cur_stav_sk[28] = 65269036384301682;
	cur_stav_sk[29] = 5735978425987358;
	cur_stav_sk[30] = 14016455062598465;
	cur_stav_sk[31] = 1119129442194814;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15157759945180992;
	cur_stav_sk[1] = 14056714622251388;
	cur_stav_sk[2] = 5762067147461652;
	cur_stav_sk[3] = 33197415532539930;
	cur_stav_sk[4] = 18551609955353978;
	cur_stav_sk[5] = 9132354828579444;
	cur_stav_sk[6] = 35003818648343578;
	cur_stav_sk[7] = 13599796189003896;
	cur_stav_sk[8] = 15314964391670390;
	cur_stav_sk[9] = 9100940742452476;
	cur_stav_sk[10] = 19452179098539376;
	cur_stav_sk[11] = 10680181916565786;
	cur_stav_sk[12] = 14180762259334976;
	cur_stav_sk[13] = 14047996565984588;
	cur_stav_sk[14] = 65796730576090998;
	cur_stav_sk[15] = 6325497978160196;
	cur_stav_sk[16] = 27111581832091006;
	cur_stav_sk[17] = 5595240800882966;
	cur_stav_sk[18] = 17407843378169206;
	cur_stav_sk[19] = 27155205828269133;
	cur_stav_sk[20] = 70952529563678;
	cur_stav_sk[21] = 30700046749147384;
	cur_stav_sk[22] = 6132212661323634;
	cur_stav_sk[23] = 65795617756415860;
	cur_stav_sk[24] = 28684916214334750;
	cur_stav_sk[25] = 32072211619979894;
	cur_stav_sk[26] = 14057069684359445;
	cur_stav_sk[27] = 9113321539961206;
	cur_stav_sk[28] = 15166488911000178;
	cur_stav_sk[29] = 10240309061879110;
	cur_stav_sk[30] = 18121702366294430;
	cur_stav_sk[31] = 6835532216471932;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 1223099320435736;
	cur_stav_sk[1] = 10214124256802374;
	cur_stav_sk[2] = 27140181297340568;
	cur_stav_sk[3] = 65376192212054548;
	cur_stav_sk[4] = 32749605141905438;
	cur_stav_sk[5] = 15851402782150936;
	cur_stav_sk[6] = 15142341552013690;
	cur_stav_sk[7] = 32028723738181758;
	cur_stav_sk[8] = 24155247937356824;
	cur_stav_sk[9] = 12904242817697100;
	cur_stav_sk[10] = 10124871222530418;
	cur_stav_sk[11] = 15182639078539384;
	cur_stav_sk[12] = 30892365858145605;
	cur_stav_sk[13] = 7249636360589336;
	cur_stav_sk[14] = 15834635583633174;
	cur_stav_sk[15] = 29370781332910462;
	cur_stav_sk[16] = 15141442839187474;
	cur_stav_sk[17] = 5017731398963322;
	cur_stav_sk[18] = 6320824470733182;
	cur_stav_sk[19] = 27567333182903668;
	cur_stav_sk[20] = 7962464249349144;
	cur_stav_sk[21] = 28253668260578580;
	cur_stav_sk[22] = 10213051135927752;
	cur_stav_sk[23] = 64150487514194454;
	cur_stav_sk[24] = 28464775112623134;
	cur_stav_sk[25] = 22840188418298226;
	cur_stav_sk[26] = 22642008942584314;
	cur_stav_sk[27] = 15147152502560118;
	cur_stav_sk[28] = 30470797953139780;
	cur_stav_sk[29] = 13850293046441208;
	cur_stav_sk[30] = 19678859605442582;
	cur_stav_sk[31] = 31613520441709948;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32784721264459853;
	cur_stav_sk[1] = 11752092465377530;
	cur_stav_sk[2] = 12888410943791728;
	cur_stav_sk[3] = 65079576796922130;
	cur_stav_sk[4] = 4602092239355930;
	cur_stav_sk[5] = 30919990887004020;
	cur_stav_sk[6] = 9113388647416948;
	cur_stav_sk[7] = 71433972911277174;
	cur_stav_sk[8] = 19229940898051602;
	cur_stav_sk[9] = 23760108794417170;
	cur_stav_sk[10] = 9329926790854044;
	cur_stav_sk[11] = 71442421644567800;
	cur_stav_sk[12] = 17407570251489438;
	cur_stav_sk[13] = 14919140742729750;
	cur_stav_sk[14] = 18336149006206484;
	cur_stav_sk[15] = 32079850971661596;
	cur_stav_sk[16] = 9088456492925046;
	cur_stav_sk[17] = 57500343182655864;
	cur_stav_sk[18] = 6158192503260276;
	cur_stav_sk[19] = 12877553951650320;
	cur_stav_sk[20] = 9081484076327536;
	cur_stav_sk[21] = 32054687672743749;
	cur_stav_sk[22] = 510215829533300;
	cur_stav_sk[23] = 32722117889627208;
	cur_stav_sk[24] = 31653459633607956;
	cur_stav_sk[25] = 677400719687710;
	cur_stav_sk[26] = 71236152761128212;
	cur_stav_sk[27] = 27515834755843186;
	cur_stav_sk[28] = 65294984443342872;
	cur_stav_sk[29] = 32758306802377590;
	cur_stav_sk[30] = 64159318853973240;
	cur_stav_sk[31] = 9535725649626394;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 69877818882195528;
	cur_stav_sk[1] = 107578744640538;
	cur_stav_sk[2] = 23012317818303738;
	cur_stav_sk[3] = 14708819573933172;
	cur_stav_sk[4] = 28279231918457974;
	cur_stav_sk[5] = 14768740652425746;
	cur_stav_sk[6] = 13617963290558490;
	cur_stav_sk[7] = 66518010252645494;
	cur_stav_sk[8] = 33153948534046994;
	cur_stav_sk[9] = 6835408441050177;
	cur_stav_sk[10] = 11347516210635376;
	cur_stav_sk[11] = 33180449356353914;
	cur_stav_sk[12] = 11752151055695996;
	cur_stav_sk[13] = 8383302115046680;
	cur_stav_sk[14] = 27111216086544916;
	cur_stav_sk[15] = 15835128577987702;
	cur_stav_sk[16] = 14408980714632346;
	cur_stav_sk[17] = 69586335372682701;
	cur_stav_sk[18] = 97064663234044;
	cur_stav_sk[19] = 32757301871751672;
	cur_stav_sk[20] = 33154430493004830;
	cur_stav_sk[21] = 28452345001119770;
	cur_stav_sk[22] = 32985792829994106;
	cur_stav_sk[23] = 31847909113029884;
	cur_stav_sk[24] = 1197993304764538;
	cur_stav_sk[25] = 14039203962533272;
	cur_stav_sk[26] = 70317360533115204;
	cur_stav_sk[27] = 27551319707784310;
	cur_stav_sk[28] = 22861629022307092;
	cur_stav_sk[29] = 13643859183843142;
	cur_stav_sk[30] = 14013211357351960;
	cur_stav_sk[31] = 64848787802357825;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14048259291252088;
	cur_stav_sk[1] = 33197587932558716;
	cur_stav_sk[2] = 5736804137578650;
	cur_stav_sk[3] = 124826318320498;
	cur_stav_sk[4] = 29362030754759442;
	cur_stav_sk[5] = 9527070235137356;
	cur_stav_sk[6] = 14030682284372380;
	cur_stav_sk[7] = 29767289306289178;
	cur_stav_sk[8] = 9121315470804302;
	cur_stav_sk[9] = 29766878591485248;
	cur_stav_sk[10] = 9095765108363588;
	cur_stav_sk[11] = 60869862437462170;
	cur_stav_sk[12] = 10239481811566621;
	cur_stav_sk[13] = 14056793606817046;
	cur_stav_sk[14] = 65373958150324602;
	cur_stav_sk[15] = 12473414497621106;
	cur_stav_sk[16] = 66948924335521916;
	cur_stav_sk[17] = 32757369493730428;
	cur_stav_sk[18] = 32987415318532470;
	cur_stav_sk[19] = 15754521026003828;
	cur_stav_sk[20] = 66528319193355544;
	cur_stav_sk[21] = 14728719748642580;
	cur_stav_sk[22] = 66920812101055090;
	cur_stav_sk[23] = 56815964697395476;
	cur_stav_sk[24] = 62426154179502232;
	cur_stav_sk[25] = 14203224542483529;
	cur_stav_sk[26] = 4621358055392112;
	cur_stav_sk[27] = 15728032409257340;
	cur_stav_sk[28] = 33191919057928570;
	cur_stav_sk[29] = 28693480499126776;
	cur_stav_sk[30] = 16986980059906334;
	cur_stav_sk[31] = 15156320246073200;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12887062814757138;
	cur_stav_sk[1] = 18112190621692436;
	cur_stav_sk[2] = 20769589369766006;
	cur_stav_sk[3] = 10634586660667920;
	cur_stav_sk[4] = 12666679989698678;
	cur_stav_sk[5] = 18143898959479828;
	cur_stav_sk[6] = 9536702219334906;
	cur_stav_sk[7] = 5726811147150748;
	cur_stav_sk[8] = 25289418753418488;
	cur_stav_sk[9] = 56594940964017436;
	cur_stav_sk[10] = 1673214787257714;
	cur_stav_sk[11] = 19686554437752186;
	cur_stav_sk[12] = 30902356892001788;
	cur_stav_sk[13] = 9079523891458552;
	cur_stav_sk[14] = 14207415411355804;
	cur_stav_sk[15] = 9521732063233400;
	cur_stav_sk[16] = 16960550504711197;
	cur_stav_sk[17] = 22624486100534598;
	cur_stav_sk[18] = 32036269531738270;
	cur_stav_sk[19] = 35406300239992084;
	cur_stav_sk[20] = 14039698028892538;
	cur_stav_sk[21] = 29361845054177658;
	cur_stav_sk[22] = 28227014159107958;
	cur_stav_sk[23] = 16986157099626878;
	cur_stav_sk[24] = 15129860372833688;
	cur_stav_sk[25] = 669691250775324;
	cur_stav_sk[26] = 59224937360233549;
	cur_stav_sk[27] = 31833905886012568;
	cur_stav_sk[28] = 13582890139193370;
	cur_stav_sk[29] = 14707625032786501;
	cur_stav_sk[30] = 68672299772679546;
	cur_stav_sk[31] = 9536646866293882;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 65795690051076126;
	cur_stav_sk[1] = 32775166029051166;
	cur_stav_sk[2] = 132854832857106;
	cur_stav_sk[3] = 5031989120143680;
	cur_stav_sk[4] = 23046618421198960;
	cur_stav_sk[5] = 65589737290708474;
	cur_stav_sk[6] = 10205465212650616;
	cur_stav_sk[7] = 60870617280557342;
	cur_stav_sk[8] = 70937357891344;
	cur_stav_sk[9] = 27146469064868884;
	cur_stav_sk[10] = 28218447003693940;
	cur_stav_sk[11] = 9114145752790805;
	cur_stav_sk[12] = 11330006959263870;
	cur_stav_sk[13] = 10240651121431670;
	cur_stav_sk[14] = 27324854681223700;
	cur_stav_sk[15] = 5762306924425846;
	cur_stav_sk[16] = 7266347012953460;
	cur_stav_sk[17] = 14753071160366196;
	cur_stav_sk[18] = 694911028856188;
	cur_stav_sk[19] = 14751974105158002;
	cur_stav_sk[20] = 1205991385268502;
	cur_stav_sk[21] = 71442828220957978;
	cur_stav_sk[22] = 5031970264101246;
	cur_stav_sk[23] = 71445757693396348;
	cur_stav_sk[24] = 35404829023449712;
	cur_stav_sk[25] = 32053586368640028;
	cur_stav_sk[26] = 9351213473821200;
	cur_stav_sk[27] = 1444514688214526;
	cur_stav_sk[28] = 64151655695726102;
	cur_stav_sk[29] = 27128111364015130;
	cur_stav_sk[30] = 56269369042187024;
	cur_stav_sk[31] = 14069037346461052;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15332764288923252;
	cur_stav_sk[1] = 9096898734294344;
	cur_stav_sk[2] = 32062878995952402;
	cur_stav_sk[3] = 12464849881505946;
	cur_stav_sk[4] = 11779279679788100;
	cur_stav_sk[5] = 71498945836564;
	cur_stav_sk[6] = 10678830676903244;
	cur_stav_sk[7] = 22641769969352989;
	cur_stav_sk[8] = 32046109768005066;
	cur_stav_sk[9] = 32053808092057714;
	cur_stav_sk[10] = 12676827434586484;
	cur_stav_sk[11] = 24181110627795017;
	cur_stav_sk[12] = 33853856876499056;
	cur_stav_sk[13] = 1654590189469810;
	cur_stav_sk[14] = 32784995052515954;
	cur_stav_sk[15] = 28694856587294106;
	cur_stav_sk[16] = 9105611119632798;
	cur_stav_sk[17] = 69357773506127636;
	cur_stav_sk[18] = 33188583680259700;
	cur_stav_sk[19] = 17192931341799540;
	cur_stav_sk[20] = 4839368378155288;
	cur_stav_sk[21] = 13627161644598298;
	cur_stav_sk[22] = 15177346753267056;
	cur_stav_sk[23] = 23047487822239000;
	cur_stav_sk[24] = 68645196958958876;
	cur_stav_sk[25] = 68654335509336086;
	cur_stav_sk[26] = 15529120723075352;
	cur_stav_sk[27] = 28667390787028598;
	cur_stav_sk[28] = 17187598142345340;
	cur_stav_sk[29] = 62022106603467004;
	cur_stav_sk[30] = 14959438970229118;
	cur_stav_sk[31] = 27094101115797886;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10687912389554556;
	cur_stav_sk[1] = 1119129516257564;
	cur_stav_sk[2] = 27153640112568176;
	cur_stav_sk[3] = 27524643998119540;
	cur_stav_sk[4] = 12465786115593334;
	cur_stav_sk[5] = 11340311456756548;
	cur_stav_sk[6] = 512117296084854;
	cur_stav_sk[7] = 14735111007445242;
	cur_stav_sk[8] = 1682078328795388;
	cur_stav_sk[9] = 14936323770102861;
	cur_stav_sk[10] = 14395697650795894;
	cur_stav_sk[11] = 32767471133701242;
	cur_stav_sk[12] = 14743727786328950;
	cur_stav_sk[13] = 14612239111497166;
	cur_stav_sk[14] = 13642863090806812;
	cur_stav_sk[15] = 27535455453226817;
	cur_stav_sk[16] = 29787081737539610;
	cur_stav_sk[17] = 10467905965393222;
	cur_stav_sk[18] = 27559825896149272;
	cur_stav_sk[19] = 15174776740295066;
	cur_stav_sk[20] = 12886662289785152;
	cur_stav_sk[21] = 14198768028913693;
	cur_stav_sk[22] = 27154714185904456;
	cur_stav_sk[23] = 27541534766419994;
	cur_stav_sk[24] = 7952568984384022;
	cur_stav_sk[25] = 18332025857881598;
	cur_stav_sk[26] = 19633598642865272;
	cur_stav_sk[27] = 15191813318910462;
	cur_stav_sk[28] = 5059545782957844;
	cur_stav_sk[29] = 14030695098189178;
	cur_stav_sk[30] = 32071479187017852;
	cur_stav_sk[31] = 11224465055037298;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 55847430901432442;
	cur_stav_sk[1] = 13635666614982008;
	cur_stav_sk[2] = 13627105128391064;
	cur_stav_sk[3] = 34982104584398612;
	cur_stav_sk[4] = 16256023952012096;
	cur_stav_sk[5] = 9703026600547708;
	cur_stav_sk[6] = 14734174902961229;
	cur_stav_sk[7] = 5178456237114750;
	cur_stav_sk[8] = 14181245911531804;
	cur_stav_sk[9] = 27137875436081182;
	cur_stav_sk[10] = 10256413789008452;
	cur_stav_sk[11] = 27338975074956150;
	cur_stav_sk[12] = 15173978815436054;
	cur_stav_sk[13] = 28684641988752764;
	cur_stav_sk[14] = 65295424260387224;
	cur_stav_sk[15] = 19254859838326130;
	cur_stav_sk[16] = 28684915860440434;
	cur_stav_sk[17] = 23758896562503792;
	cur_stav_sk[18] = 60887207682347388;
	cur_stav_sk[19] = 10626304961773632;
	cur_stav_sk[20] = 106314065740057;
	cur_stav_sk[21] = 27559001797702682;
	cur_stav_sk[22] = 19267402673717630;
	cur_stav_sk[23] = 35396032926909464;
	cur_stav_sk[24] = 9114477498568728;
	cur_stav_sk[25] = 13796278247244822;
	cur_stav_sk[26] = 23952619223791122;
	cur_stav_sk[27] = 10266995463192954;
	cur_stav_sk[28] = 10664780169589574;
	cur_stav_sk[29] = 546117999558778;
	cur_stav_sk[30] = 19676961647853848;
	cur_stav_sk[31] = 29779134345360796;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12898814445061242;
	cur_stav_sk[1] = 10206833151022462;
	cur_stav_sk[2] = 15147178961875354;
	cur_stav_sk[3] = 9535957115224088;
	cur_stav_sk[4] = 2546235598644730;
	cur_stav_sk[5] = 15183231991656696;
	cur_stav_sk[6] = 9104579117290822;
	cur_stav_sk[7] = 9123270893023384;
	cur_stav_sk[8] = 31640307431244560;
	cur_stav_sk[9] = 28677014153892212;
	cur_stav_sk[10] = 71442517158827124;
	cur_stav_sk[11] = 32722433162936700;
	cur_stav_sk[12] = 27568967809741944;
	cur_stav_sk[13] = 12677126424036382;
	cur_stav_sk[14] = 14040162201515386;
	cur_stav_sk[15] = 69368012727095676;
	cur_stav_sk[16] = 63749450514825336;
	cur_stav_sk[17] = 28488240183342106;
	cur_stav_sk[18] = 15165500000469114;
	cur_stav_sk[19] = 5933683077887506;
	cur_stav_sk[20] = 28257042440454164;
	cur_stav_sk[21] = 889263319549052;
	cur_stav_sk[22] = 9301648733217350;
	cur_stav_sk[23] = 14066151126901912;
	cur_stav_sk[24] = 4609434290848026;
	cur_stav_sk[25] = 10670859347595546;
	cur_stav_sk[26] = 18528462077564282;
	cur_stav_sk[27] = 10635606113386876;
	cur_stav_sk[28] = 7982314898687358;
	cur_stav_sk[29] = 10265871859864644;
	cur_stav_sk[30] = 4996833224888600;
	cur_stav_sk[31] = 1645829926159638;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 5032230666715262;
	cur_stav_sk[1] = 22624105509879928;
	cur_stav_sk[2] = 4847202989052954;
	cur_stav_sk[3] = 9315906539881502;
	cur_stav_sk[4] = 5709314534970394;
	cur_stav_sk[5] = 31807240589378894;
	cur_stav_sk[6] = 6836509303141904;
	cur_stav_sk[7] = 10415431539918100;
	cur_stav_sk[8] = 10222811505693054;
	cur_stav_sk[9] = 23063650118927389;
	cur_stav_sk[10] = 25271936087429244;
	cur_stav_sk[11] = 11347584452658204;
	cur_stav_sk[12] = 15191787950149652;
	cur_stav_sk[13] = 28281464215274620;
	cur_stav_sk[14] = 27525137068458108;
	cur_stav_sk[15] = 14958612704966420;
	cur_stav_sk[16] = 15173817736565872;
	cur_stav_sk[17] = 3661129985687932;
	cur_stav_sk[18] = 66930854270800920;
	cur_stav_sk[19] = 32766370079282196;
	cur_stav_sk[20] = 34982997404844306;
	cur_stav_sk[21] = 6166746470122776;
	cur_stav_sk[22] = 11348685503791181;
	cur_stav_sk[23] = 65795949634357010;
	cur_stav_sk[24] = 65267421608812796;
	cur_stav_sk[25] = 11356393896293884;
	cur_stav_sk[26] = 33198411960693620;
	cur_stav_sk[27] = 6730622934421528;
	cur_stav_sk[28] = 13811548621209878;
	cur_stav_sk[29] = 17411527980364406;
	cur_stav_sk[30] = 14760504448681336;
	cur_stav_sk[31] = 64855549899904370;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18103408697029445;
	cur_stav_sk[1] = 31617047436099868;
	cur_stav_sk[2] = 10212638208131137;
	cur_stav_sk[3] = 22861911223570708;
	cur_stav_sk[4] = 12468048145744656;
	cur_stav_sk[5] = 14197739189900570;
	cur_stav_sk[6] = 27136501249127454;
	cur_stav_sk[7] = 1412879642440260;
	cur_stav_sk[8] = 22654286393250166;
	cur_stav_sk[9] = 1655348047016062;
	cur_stav_sk[10] = 65611740085073778;
	cur_stav_sk[11] = 32030854108972100;
	cur_stav_sk[12] = 68457250814853908;
	cur_stav_sk[13] = 16267510777614456;
	cur_stav_sk[14] = 1461694103638128;
	cur_stav_sk[15] = 28650898507300880;
	cur_stav_sk[16] = 28244632342990960;
	cur_stav_sk[17] = 17385312575533210;
	cur_stav_sk[18] = 68646183864074870;
	cur_stav_sk[19] = 6847765350888910;
	cur_stav_sk[20] = 31613506551775257;
	cur_stav_sk[21] = 9334599387551041;
	cur_stav_sk[22] = 27136022149955606;
	cur_stav_sk[23] = 19254956003767452;
	cur_stav_sk[24] = 27325620990443898;
	cur_stav_sk[25] = 10098746550461594;
	cur_stav_sk[26] = 13607589892920342;
	cur_stav_sk[27] = 14188129272636946;
	cur_stav_sk[28] = 9099565820253214;
	cur_stav_sk[29] = 28696813336770040;
	cur_stav_sk[30] = 19264377895457908;
	cur_stav_sk[31] = 88184270561485;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15736971720987922;
	cur_stav_sk[1] = 14708035184492926;
	cur_stav_sk[2] = 10688358645895282;
	cur_stav_sk[3] = 55837734893126004;
	cur_stav_sk[4] = 33875227408334874;
	cur_stav_sk[5] = 31850546742564212;
	cur_stav_sk[6] = 10643965680072562;
	cur_stav_sk[7] = 24146175172846194;
	cur_stav_sk[8] = 29345445877817874;
	cur_stav_sk[9] = 10670037205287192;
	cur_stav_sk[10] = 69788785097142854;
	cur_stav_sk[11] = 18535091374757246;
	cur_stav_sk[12] = 35396583738979908;
	cur_stav_sk[13] = 13625921789993416;
	cur_stav_sk[14] = 18551597533925492;
	cur_stav_sk[15] = 28679048276612416;
	cur_stav_sk[16] = 519621338007158;
	cur_stav_sk[17] = 10249448215392784;
	cur_stav_sk[18] = 30892253968773956;
	cur_stav_sk[19] = 33856470212522270;
	cur_stav_sk[20] = 27551046458019956;
	cur_stav_sk[21] = 5718386589773944;
	cur_stav_sk[22] = 32757343656738840;
	cur_stav_sk[23] = 66711252575368052;
	cur_stav_sk[24] = 10267065703105648;
	cur_stav_sk[25] = 33205353168863506;
	cur_stav_sk[26] = 28693514917745989;
	cur_stav_sk[27] = 33874599331583052;
	cur_stav_sk[28] = 54946724644791828;
	cur_stav_sk[29] = 56388185399500958;
	cur_stav_sk[30] = 30902426274173202;
	cur_stav_sk[31] = 14013557285074202;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31635990385919048;
	cur_stav_sk[1] = 56389284924511254;
	cur_stav_sk[2] = 30700321219360528;
	cur_stav_sk[3] = 23047418897250162;
	cur_stav_sk[4] = 29345503703795016;
	cur_stav_sk[5] = 71316405596190;
	cur_stav_sk[6] = 13599533997458456;
	cur_stav_sk[7] = 13643573191086404;
	cur_stav_sk[8] = 10099100219324272;
	cur_stav_sk[9] = 30488801072756240;
	cur_stav_sk[10] = 10645270796533790;
	cur_stav_sk[11] = 32780294412284528;
	cur_stav_sk[12] = 12689743955690614;
	cur_stav_sk[13] = 33153375006438808;
	cur_stav_sk[14] = 68645677080843388;
	cur_stav_sk[15] = 32062532588242204;
	cur_stav_sk[16] = 118339793756574;
	cur_stav_sk[17] = 14066691669341598;
	cur_stav_sk[18] = 14708749634668048;
	cur_stav_sk[19] = 28452617517745169;
	cur_stav_sk[20] = 5015224415794962;
	cur_stav_sk[21] = 68644992483135506;
	cur_stav_sk[22] = 32985903652779034;
	cur_stav_sk[23] = 70089335433004412;
	cur_stav_sk[24] = 57500643733897594;
	cur_stav_sk[25] = 69797853916407568;
	cur_stav_sk[26] = 11347610759468058;
	cur_stav_sk[27] = 114932793315352;
	cur_stav_sk[28] = 68456150145570078;
	cur_stav_sk[29] = 10661901115397146;
	cur_stav_sk[30] = 13599795459371256;
	cur_stav_sk[31] = 33182822388606486;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 56788215570628940;
	cur_stav_sk[1] = 14602097463161982;
	cur_stav_sk[2] = 5952488262205564;
	cur_stav_sk[3] = 27570914631987322;
	cur_stav_sk[4] = 6308822721663350;
	cur_stav_sk[5] = 28696839042008432;
	cur_stav_sk[6] = 27092536129564922;
	cur_stav_sk[7] = 57509781813757208;
	cur_stav_sk[8] = 9501310564831554;
	cur_stav_sk[9] = 27551295503902109;
	cur_stav_sk[10] = 14179926001815838;
	cur_stav_sk[11] = 27550011975644790;
	cur_stav_sk[12] = 65593307566052686;
	cur_stav_sk[13] = 9095740019283056;
	cur_stav_sk[14] = 6836507835303240;
	cur_stav_sk[15] = 7248493433794684;
	cur_stav_sk[16] = 2226096743223320;
	cur_stav_sk[17] = 12888300823971700;
	cur_stav_sk[18] = 14770020567836176;
	cur_stav_sk[19] = 12477815782159864;
	cur_stav_sk[20] = 9087470136165744;
	cur_stav_sk[21] = 22624119416033302;
	cur_stav_sk[22] = 65276707191882352;
	cur_stav_sk[23] = 14628688901318046;
	cur_stav_sk[24] = 7970117820229492;
	cur_stav_sk[25] = 11242787781841172;
	cur_stav_sk[26] = 678966752720658;
	cur_stav_sk[27] = 688256720900120;
	cur_stav_sk[28] = 11549988007740692;
	cur_stav_sk[29] = 14039257568350324;
	cur_stav_sk[30] = 14975928338809872;
	cur_stav_sk[31] = 18507881059844124;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12667341616153718;
	cur_stav_sk[1] = 69354476398675070;
	cur_stav_sk[2] = 13845068897341465;
	cur_stav_sk[3] = 13833799835988248;
	cur_stav_sk[4] = 4997835694163008;
	cur_stav_sk[5] = 9676594139381880;
	cur_stav_sk[6] = 30910121800204410;
	cur_stav_sk[7] = 16960043769279896;
	cur_stav_sk[8] = 10266170200716442;
	cur_stav_sk[9] = 10259987201013150;
	cur_stav_sk[10] = 1092781416003446;
	cur_stav_sk[11] = 19633777296774522;
	cur_stav_sk[12] = 9553249745254165;
	cur_stav_sk[13] = 63740756486927642;
	cur_stav_sk[14] = 69357818943475990;
	cur_stav_sk[15] = 28667333758255734;
	cur_stav_sk[16] = 9123271083427096;
	cur_stav_sk[17] = 32758535978680700;
	cur_stav_sk[18] = 5023470195094082;
	cur_stav_sk[19] = 9131223357142642;
	cur_stav_sk[20] = 28433951053017668;
	cur_stav_sk[21] = 27339913360843980;
	cur_stav_sk[22] = 31864142814343744;
	cur_stav_sk[23] = 27303414941962264;
	cur_stav_sk[24] = 11549551129137436;
	cur_stav_sk[25] = 16255530177496690;
	cur_stav_sk[26] = 31635215350629756;
	cur_stav_sk[27] = 14179871588890386;
	cur_stav_sk[28] = 1118701560303736;
	cur_stav_sk[29] = 14918790636471678;
	cur_stav_sk[30] = 31595668692538490;
	cur_stav_sk[31] = 70926583509150;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15177245729300888;
	cur_stav_sk[1] = 16281654838833680;
	cur_stav_sk[2] = 12482373264324372;
	cur_stav_sk[3] = 65383040608731248;
	cur_stav_sk[4] = 18113169069113416;
	cur_stav_sk[5] = 31653331298813005;
	cur_stav_sk[6] = 31605879447295358;
	cur_stav_sk[7] = 27119127898849406;
	cur_stav_sk[8] = 15173945735426930;
	cur_stav_sk[9] = 56281419624943680;
	cur_stav_sk[10] = 22642868078194974;
	cur_stav_sk[11] = 7257538449781568;
	cur_stav_sk[12] = 519949076664696;
	cur_stav_sk[13] = 17393765606307444;
	cur_stav_sk[14] = 10688979277752080;
	cur_stav_sk[15] = 520773586224500;
	cur_stav_sk[16] = 14708130216482164;
	cur_stav_sk[17] = 2757235927580688;
	cur_stav_sk[18] = 6139884003950878;
	cur_stav_sk[19] = 25271757430555922;
	cur_stav_sk[20] = 24201768868151828;
	cur_stav_sk[21] = 60764037127807132;
	cur_stav_sk[22] = 7979093075112446;
	cur_stav_sk[23] = 1672044418504013;
	cur_stav_sk[24] = 11329649155118108;
	cur_stav_sk[25] = 9500131596345545;
	cur_stav_sk[26] = 5033364786393621;
	cur_stav_sk[27] = 4838133579363708;
	cur_stav_sk[28] = 9500953753301500;
	cur_stav_sk[29] = 3888702666376212;
	cur_stav_sk[30] = 13796552250591040;
	cur_stav_sk[31] = 18147307887690516;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 62419100762509686;
	cur_stav_sk[1] = 7978431180420720;
	cur_stav_sk[2] = 27339089489789460;
	cur_stav_sk[3] = 28667228451083336;
	cur_stav_sk[4] = 31641228638818424;
	cur_stav_sk[5] = 14057908549329430;
	cur_stav_sk[6] = 32783868160442384;
	cur_stav_sk[7] = 10221918037766172;
	cur_stav_sk[8] = 14049086061459014;
	cur_stav_sk[9] = 65607726037992468;
	cur_stav_sk[10] = 32987306474287986;
	cur_stav_sk[11] = 32072703121109620;
	cur_stav_sk[12] = 15168824909006710;
	cur_stav_sk[13] = 25993287657394460;
	cur_stav_sk[14] = 32725657416631062;
	cur_stav_sk[15] = 10682715389407736;
	cur_stav_sk[16] = 15165089843610905;
	cur_stav_sk[17] = 14031794542613882;
	cur_stav_sk[18] = 16255531018924822;
	cur_stav_sk[19] = 12482316955694104;
	cur_stav_sk[20] = 35423040875472502;
	cur_stav_sk[21] = 71024015732282398;
	cur_stav_sk[22] = 22647224043766810;
	cur_stav_sk[23] = 19642738203165822;
	cur_stav_sk[24] = 6141124097671282;
	cur_stav_sk[25] = 9695294893392192;
	cur_stav_sk[26] = 14056988799795312;
	cur_stav_sk[27] = 28283773373943836;
	cur_stav_sk[28] = 28244877680410646;
	cur_stav_sk[29] = 22589059134002450;
	cur_stav_sk[30] = 14619676182918474;
	cur_stav_sk[31] = 23012522081792528;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 65812950185054586;
	cur_stav_sk[1] = 32018566332974206;
	cur_stav_sk[2] = 9526257008476236;
	cur_stav_sk[3] = 31825934493189394;
	cur_stav_sk[4] = 64863040717686092;
	cur_stav_sk[5] = 27136569762447682;
	cur_stav_sk[6] = 70308219584349464;
	cur_stav_sk[7] = 29783922628138256;
	cur_stav_sk[8] = 32071273581588734;
	cur_stav_sk[9] = 28701888314479632;
	cur_stav_sk[10] = 13634570471343169;
	cur_stav_sk[11] = 5024018067334600;
	cur_stav_sk[12] = 32019242272260112;
	cur_stav_sk[13] = 4844206108521592;
	cur_stav_sk[14] = 32986181525513840;
	cur_stav_sk[15] = 15833933826430282;
	cur_stav_sk[16] = 68242087583649916;
	cur_stav_sk[17] = 24140290516812052;
	cur_stav_sk[18] = 2324112030970140;
	cur_stav_sk[19] = 28244903239061628;
	cur_stav_sk[20] = 29345698604716364;
	cur_stav_sk[21] = 5603474198508402;
	cur_stav_sk[22] = 35405379833664882;
	cur_stav_sk[23] = 33145677884011928;
	cur_stav_sk[24] = 10630014684860494;
	cur_stav_sk[25] = 16255540682699033;
	cur_stav_sk[26] = 16264187196420126;
	cur_stav_sk[27] = 31632763442300016;
	cur_stav_sk[28] = 23714300824977534;
	cur_stav_sk[29] = 12693595002856000;
	cur_stav_sk[30] = 13638926025434898;
	cur_stav_sk[31] = 27092590421546316;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32775968258949440;
	cur_stav_sk[1] = 29766562591478906;
	cur_stav_sk[2] = 27152882171257460;
	cur_stav_sk[3] = 29349022376690046;
	cur_stav_sk[4] = 27550482790495094;
	cur_stav_sk[5] = 7955684960583704;
	cur_stav_sk[6] = 71452138963514494;
	cur_stav_sk[7] = 13811533566197788;
	cur_stav_sk[8] = 65796501397811320;
	cur_stav_sk[9] = 13634087472084344;
	cur_stav_sk[10] = 11252134846989384;
	cur_stav_sk[11] = 13818428283556892;
	cur_stav_sk[12] = 512172791071094;
	cur_stav_sk[13] = 14725620269711480;
	cur_stav_sk[14] = 10626991952672532;
	cur_stav_sk[15] = 4592460371887390;
	cur_stav_sk[16] = 15324201273500432;
	cur_stav_sk[17] = 20785751462605328;
	cur_stav_sk[18] = 28486177855312972;
	cur_stav_sk[19] = 27339113963521402;
	cur_stav_sk[20] = 15859428821898616;
	cur_stav_sk[21] = 15324819136263290;
	cur_stav_sk[22] = 9105829219672692;
	cur_stav_sk[23] = 8190130255036948;
	cur_stav_sk[24] = 10221342554849609;
	cur_stav_sk[25] = 27101591557215608;
	cur_stav_sk[26] = 10680386457534744;
	cur_stav_sk[27] = 14751629452292886;
	cur_stav_sk[28] = 14726411614926154;
	cur_stav_sk[29] = 61301640414666956;
	cur_stav_sk[30] = 9323926995777396;
	cur_stav_sk[31] = 9545787622863230;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 63948163746017784;
	cur_stav_sk[1] = 32760290417513502;
	cur_stav_sk[2] = 33161897306362952;
	cur_stav_sk[3] = 7040790895768180;
	cur_stav_sk[4] = 4848330509399322;
	cur_stav_sk[5] = 9125813366830864;
	cur_stav_sk[6] = 66929572958384704;
	cur_stav_sk[7] = 13644672011560188;
	cur_stav_sk[8] = 29561351131042168;
	cur_stav_sk[9] = 11227323896272158;
	cur_stav_sk[10] = 10239207957210738;
	cur_stav_sk[11] = 10230369129108602;
	cur_stav_sk[12] = 33145428729835896;
	cur_stav_sk[13] = 22624268517352310;
	cur_stav_sk[14] = 8404054516311664;
	cur_stav_sk[15] = 8383303402815608;
	cur_stav_sk[16] = 12675753092125724;
	cur_stav_sk[17] = 18524948861321748;
	cur_stav_sk[18] = 10812123850215804;
	cur_stav_sk[19] = 11251952190764562;
	cur_stav_sk[20] = 5402470450696560;
	cur_stav_sk[21] = 10652787148592504;
	cur_stav_sk[22] = 28644095762182396;
	cur_stav_sk[23] = 32779498240963708;
	cur_stav_sk[24] = 28244836289459484;
	cur_stav_sk[25] = 16282125951346448;
	cur_stav_sk[26] = 19254749096248397;
	cur_stav_sk[27] = 29371203800470640;
	cur_stav_sk[28] = 1205576059389302;
	cur_stav_sk[29] = 15147590200792390;
	cur_stav_sk[30] = 2755036996048158;
	cur_stav_sk[31] = 24862018503124220;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 63719810802160660;
	cur_stav_sk[1] = 679298681538164;
	cur_stav_sk[2] = 23719221033109108;
	cur_stav_sk[3] = 23714274309309972;
	cur_stav_sk[4] = 16977108215436574;
	cur_stav_sk[5] = 14716925167052560;
	cur_stav_sk[6] = 30894040208048248;
	cur_stav_sk[7] = 14769152913216588;
	cur_stav_sk[8] = 65601842122461304;
	cur_stav_sk[9] = 15861368247231736;
	cur_stav_sk[10] = 57513295632224276;
	cur_stav_sk[11] = 34983788196857112;
	cur_stav_sk[12] = 18095448978231324;
	cur_stav_sk[13] = 57518850635572758;
	cur_stav_sk[14] = 9139766194648314;
	cur_stav_sk[15] = 15843145443180914;
	cur_stav_sk[16] = 10652649566253588;
	cur_stav_sk[17] = 23029098712892536;
	cur_stav_sk[18] = 4825000096137490;
	cur_stav_sk[19] = 19255299552356112;
	cur_stav_sk[20] = 56260529535878002;
	cur_stav_sk[21] = 25976612031570450;
	cur_stav_sk[22] = 33179788663788613;
	cur_stav_sk[23] = 15833881481385242;
	cur_stav_sk[24] = 32766621720195194;
	cur_stav_sk[25] = 30475996004483448;
	cur_stav_sk[26] = 19651329203220894;
	cur_stav_sk[27] = 10125161546621340;
	cur_stav_sk[28] = 69903587012425330;
	cur_stav_sk[29] = 14751808966493204;
	cur_stav_sk[30] = 65377841008916240;
	cur_stav_sk[31] = 10248348185265216;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32739916354295166;
	cur_stav_sk[1] = 23777862385702268;
	cur_stav_sk[2] = 16985357702964850;
	cur_stav_sk[3] = 31807665602532638;
	cur_stav_sk[4] = 20778040321967120;
	cur_stav_sk[5] = 31652232878564881;
	cur_stav_sk[6] = 123715812618870;
	cur_stav_sk[7] = 14716788820185202;
	cur_stav_sk[8] = 10125077775201044;
	cur_stav_sk[9] = 31631583152968990;
	cur_stav_sk[10] = 10687558523748476;
	cur_stav_sk[11] = 32767102771724566;
	cur_stav_sk[12] = 10267133889982874;
	cur_stav_sk[13] = 31595883037541396;
	cur_stav_sk[14] = 70108764708840256;
	cur_stav_sk[15] = 11777987347681552;
	cur_stav_sk[16] = 9545772584830324;
	cur_stav_sk[17] = 14751940278194558;
	cur_stav_sk[18] = 10646886776307742;
	cur_stav_sk[19] = 31811653146183536;
	cur_stav_sk[20] = 9538855430861680;
	cur_stav_sk[21] = 23771233849119768;
	cur_stav_sk[22] = 1431156740227392;
	cur_stav_sk[23] = 9140533498644340;
	cur_stav_sk[24] = 12464892750627960;
	cur_stav_sk[25] = 10441817137250684;
	cur_stav_sk[26] = 27579709669609758;
	cur_stav_sk[27] = 32036213692895508;
	cur_stav_sk[28] = 14021608607712540;
	cur_stav_sk[29] = 14042261442462750;
	cur_stav_sk[30] = 68438418575033726;
	cur_stav_sk[31] = 16959758569710454;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 23011513771787796;
	cur_stav_sk[1] = 10433821634941978;
	cur_stav_sk[2] = 28221857729352990;
	cur_stav_sk[3] = 68223136048091262;
	cur_stav_sk[4] = 10433059171079286;
	cur_stav_sk[5] = 14623353731842422;
	cur_stav_sk[6] = 19255032725995548;
	cur_stav_sk[7] = 9351366814106389;
	cur_stav_sk[8] = 6845359147749660;
	cur_stav_sk[9] = 32748231953593872;
	cur_stav_sk[10] = 32991704870106644;
	cur_stav_sk[11] = 14400323737560318;
	cur_stav_sk[12] = 32784514688685182;
	cur_stav_sk[13] = 14769291427446800;
	cur_stav_sk[14] = 65813952853815876;
	cur_stav_sk[15] = 2341485334561150;
	cur_stav_sk[16] = 71443409504784412;
	cur_stav_sk[17] = 31859591083918656;
	cur_stav_sk[18] = 32072294302384197;
	cur_stav_sk[19] = 9132148596876446;
	cur_stav_sk[20] = 19265372696249618;
	cur_stav_sk[21] = 9509706760234774;
	cur_stav_sk[22] = 9517514942087284;
	cur_stav_sk[23] = 66511244766282780;
	cur_stav_sk[24] = 9122474043946866;
	cur_stav_sk[25] = 69349021994759700;
	cur_stav_sk[26] = 11022637355696240;
	cur_stav_sk[27] = 19263519960380702;
	cur_stav_sk[28] = 66947163934340932;
	cur_stav_sk[29] = 64162386516747770;
	cur_stav_sk[30] = 10099637276878612;
	cur_stav_sk[31] = 27533703032616768;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 68645555266618482;
	cur_stav_sk[1] = 14954075934457968;
	cur_stav_sk[2] = 3874238895685906;
	cur_stav_sk[3] = 33188859500372376;
	cur_stav_sk[4] = 494526310584340;
	cur_stav_sk[5] = 9500128377738518;
	cur_stav_sk[6] = 32766337803359518;
	cur_stav_sk[7] = 23038899844981526;
	cur_stav_sk[8] = 11350863790342474;
	cur_stav_sk[9] = 9561428547503121;
	cur_stav_sk[10] = 9288707011069048;
	cur_stav_sk[11] = 29784745879320340;
	cur_stav_sk[12] = 7277878007608080;
	cur_stav_sk[13] = 14065578818384706;
	cur_stav_sk[14] = 22601134025705332;
	cur_stav_sk[15] = 69783596963345216;
	cur_stav_sk[16] = 70324974771929104;
	cur_stav_sk[17] = 19677484555899153;
	cur_stav_sk[18] = 27567787912496504;
	cur_stav_sk[19] = 71030133726610812;
	cur_stav_sk[20] = 28703787742696824;
	cur_stav_sk[21] = 33147638085714254;
	cur_stav_sk[22] = 9553592870995824;
	cur_stav_sk[23] = 28274353290182672;
	cur_stav_sk[24] = 10687904789497206;
	cur_stav_sk[25] = 27550263208583750;
	cur_stav_sk[26] = 22625014763499676;
	cur_stav_sk[27] = 10828890328793460;
	cur_stav_sk[28] = 59224743542633724;
	cur_stav_sk[29] = 15138587893053046;
	cur_stav_sk[30] = 22597798840832120;
	cur_stav_sk[31] = 32760910649233784;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31843112636547450;
	cur_stav_sk[1] = 71442242817918229;
	cur_stav_sk[2] = 9686222311788868;
	cur_stav_sk[3] = 2331932109521018;
	cur_stav_sk[4] = 24167202836218132;
	cur_stav_sk[5] = 10635652438001010;
	cur_stav_sk[6] = 32723420413182997;
	cur_stav_sk[7] = 13836684979471388;
	cur_stav_sk[8] = 13581816726163828;
	cur_stav_sk[9] = 14069219328103450;
	cur_stav_sk[10] = 13620241096705146;
	cur_stav_sk[11] = 56260709759329044;
	cur_stav_sk[12] = 22651619072520350;
	cur_stav_sk[13] = 9096336490542864;
	cur_stav_sk[14] = 4592391101367414;
	cur_stav_sk[15] = 19668475330827290;
	cur_stav_sk[16] = 28455944123782468;
	cur_stav_sk[17] = 32062588016103550;
	cur_stav_sk[18] = 32765932461797658;
	cur_stav_sk[19] = 15192577100128508;
	cur_stav_sk[20] = 10249449290770038;
	cur_stav_sk[21] = 35397338042507844;
	cur_stav_sk[22] = 9125826797834264;
	cur_stav_sk[23] = 66521561187656468;
	cur_stav_sk[24] = 9140797459211792;
	cur_stav_sk[25] = 32723490048020557;
	cur_stav_sk[26] = 16045728528761104;
	cur_stav_sk[27] = 14022173817373048;
	cur_stav_sk[28] = 14709093715257204;
	cur_stav_sk[29] = 33188285119365498;
	cur_stav_sk[30] = 19255231204702070;
	cur_stav_sk[31] = 6166172363764766;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 4593297939236172;
	cur_stav_sk[1] = 14628552869614708;
	cur_stav_sk[2] = 10645298726414149;
	cur_stav_sk[3] = 27549920238384156;
	cur_stav_sk[4] = 14942093047758968;
	cur_stav_sk[5] = 7056559935165456;
	cur_stav_sk[6] = 32722115144986829;
	cur_stav_sk[7] = 32037109403296273;
	cur_stav_sk[8] = 9332685321221908;
	cur_stav_sk[9] = 63745877764157852;
	cur_stav_sk[10] = 546189322031472;
	cur_stav_sk[11] = 21911995108594812;
	cur_stav_sk[12] = 31648672603083036;
	cur_stav_sk[13] = 32062419094472466;
	cur_stav_sk[14] = 31631582280618108;
	cur_stav_sk[15] = 19445692620801054;
	cur_stav_sk[16] = 19252181796169586;
	cur_stav_sk[17] = 28640970233123192;
	cur_stav_sk[18] = 13590545172636272;
	cur_stav_sk[19] = 19247394701313138;
	cur_stav_sk[20] = 28640152580305050;
	cur_stav_sk[21] = 334807954493552;
	cur_stav_sk[22] = 11241893966778646;
	cur_stav_sk[23] = 3476456791290238;
	cur_stav_sk[24] = 28239474609926554;
	cur_stav_sk[25] = 12904175763760588;
	cur_stav_sk[26] = 2341787053981808;
	cur_stav_sk[27] = 10265690238333084;
	cur_stav_sk[28] = 12482246578701338;
	cur_stav_sk[29] = 10682440599573524;
	cur_stav_sk[30] = 13828451111170300;
	cur_stav_sk[31] = 71240422963752988;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 13855503315056028;
	cur_stav_sk[1] = 24171874824983574;
	cur_stav_sk[2] = 65822225015377946;
	cur_stav_sk[3] = 10248305302569796;
	cur_stav_sk[4] = 5191376451339376;
	cur_stav_sk[5] = 28696915814495858;
	cur_stav_sk[6] = 22852942308409628;
	cur_stav_sk[7] = 10230410324129148;
	cur_stav_sk[8] = 14753004497536528;
	cur_stav_sk[9] = 11023571044561430;
	cur_stav_sk[10] = 14021869993109062;
	cur_stav_sk[11] = 898870579921728;
	cur_stav_sk[12] = 9545691535614460;
	cur_stav_sk[13] = 33154157238428878;
	cur_stav_sk[14] = 11340176301163589;
	cur_stav_sk[15] = 9345627730214928;
	cur_stav_sk[16] = 31605960227128350;
	cur_stav_sk[17] = 71435206658924700;
	cur_stav_sk[18] = 23064006680326558;
	cur_stav_sk[19] = 9510161473312112;
	cur_stav_sk[20] = 33165187330946460;
	cur_stav_sk[21] = 16987417623762241;
	cur_stav_sk[22] = 14004841454437449;
	cur_stav_sk[23] = 64159489787761790;
	cur_stav_sk[24] = 33153951207260182;
	cur_stav_sk[25] = 10259583806354680;
	cur_stav_sk[26] = 29563673150161174;
	cur_stav_sk[27] = 22636626293098358;
	cur_stav_sk[28] = 28218724335358074;
	cur_stav_sk[29] = 19484587967517818;
	cur_stav_sk[30] = 14761293648957814;
	cur_stav_sk[31] = 28694786047718526;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 63939518736502808;
	cur_stav_sk[1] = 31631648636047680;
	cur_stav_sk[2] = 9694977815822364;
	cur_stav_sk[3] = 29370987310874898;
	cur_stav_sk[4] = 28263570778887448;
	cur_stav_sk[5] = 71019963776536954;
	cur_stav_sk[6] = 10811115809473564;
	cur_stav_sk[7] = 4820279461478428;
	cur_stav_sk[8] = 31843923830945298;
	cur_stav_sk[9] = 14961920693908288;
	cur_stav_sk[10] = 27136596071887474;
	cur_stav_sk[11] = 27550687430543174;
	cur_stav_sk[12] = 31632498590745978;
	cur_stav_sk[13] = 10643853518733340;
	cur_stav_sk[14] = 10208724076598394;
	cur_stav_sk[15] = 64846797608461846;
	cur_stav_sk[16] = 13628864869982838;
	cur_stav_sk[17] = 14202259162005878;
	cur_stav_sk[18] = 10203852984685840;
	cur_stav_sk[19] = 71442424494109558;
	cur_stav_sk[20] = 9095946329391122;
	cur_stav_sk[21] = 129210022961560;
	cur_stav_sk[22] = 56788503758635034;
	cur_stav_sk[23] = 27128008888558078;
	cur_stav_sk[24] = 15165183443808664;
	cur_stav_sk[25] = 69366822281356542;
	cur_stav_sk[26] = 31615237498943250;
	cur_stav_sk[27] = 10688907353093748;
	cur_stav_sk[28] = 34974922263520880;
	cur_stav_sk[29] = 10455561615511668;
	cur_stav_sk[30] = 5965408461956720;
	cur_stav_sk[31] = 2746379543748814;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33197422180013438;
	cur_stav_sk[1] = 16963031385794064;
	cur_stav_sk[2] = 10673894818448412;
	cur_stav_sk[3] = 12476918655820400;
	cur_stav_sk[4] = 13837099443062546;
	cur_stav_sk[5] = 35422858752374642;
	cur_stav_sk[6] = 55848146412343412;
	cur_stav_sk[7] = 15859991601022324;
	cur_stav_sk[8] = 65294460436517953;
	cur_stav_sk[9] = 9088115058976630;
	cur_stav_sk[10] = 65804128799797630;
	cur_stav_sk[11] = 32071730185143360;
	cur_stav_sk[12] = 13849341630743836;
	cur_stav_sk[13] = 546009551536148;
	cur_stav_sk[14] = 27558234136839196;
	cur_stav_sk[15] = 10251406810154000;
	cur_stav_sk[16] = 11761001374287180;
	cur_stav_sk[17] = 9131188586492060;
	cur_stav_sk[18] = 71213477502695190;
	cur_stav_sk[19] = 18129329760175388;
	cur_stav_sk[20] = 9526929036476530;
	cur_stav_sk[21] = 12879641094521874;
	cur_stav_sk[22] = 32757299491996690;
	cur_stav_sk[23] = 13639034466419318;
	cur_stav_sk[24] = 28228385356742682;
	cur_stav_sk[25] = 34995948856218653;
	cur_stav_sk[26] = 5006988115480640;
	cur_stav_sk[27] = 63948151400609650;
	cur_stav_sk[28] = 19659959600366706;
	cur_stav_sk[29] = 57497067032880638;
	cur_stav_sk[30] = 69789653359965592;
	cur_stav_sk[31] = 18516621133788538;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28703401743484784;
	cur_stav_sk[1] = 24182417687388408;
	cur_stav_sk[2] = 10249473001193586;
	cur_stav_sk[3] = 13638349283558772;
	cur_stav_sk[4] = 59664778448479644;
	cur_stav_sk[5] = 27550194980750452;
	cur_stav_sk[6] = 14736990574839152;
	cur_stav_sk[7] = 18554945525264664;
	cur_stav_sk[8] = 9693780592899858;
	cur_stav_sk[9] = 66508392900216342;
	cur_stav_sk[10] = 10213189094979020;
	cur_stav_sk[11] = 33857073055637964;
	cur_stav_sk[12] = 14051070492222226;
	cur_stav_sk[13] = 71030173979193372;
	cur_stav_sk[14] = 27524710235342110;
	cur_stav_sk[15] = 28694856505046902;
	cur_stav_sk[16] = 14752934774177814;
	cur_stav_sk[17] = 60368443800531826;
	cur_stav_sk[18] = 9693920032663109;
	cur_stav_sk[19] = 31648994478207098;
	cur_stav_sk[20] = 61999403489334644;
	cur_stav_sk[21] = 14709092304360476;
	cur_stav_sk[22] = 66931472195720257;
	cur_stav_sk[23] = 9562003824162168;
	cur_stav_sk[24] = 24181291874162036;
	cur_stav_sk[25] = 9553262823997470;
	cur_stav_sk[26] = 29784496234369150;
	cur_stav_sk[27] = 2326608012477820;
	cur_stav_sk[28] = 3887975139935308;
	cur_stav_sk[29] = 308694609130004;
	cur_stav_sk[30] = 34060415655065033;
	cur_stav_sk[31] = 15837259811262846;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 1232007816230678;
	cur_stav_sk[1] = 30471623337670160;
	cur_stav_sk[2] = 14976204353396888;
	cur_stav_sk[3] = 31597510749623312;
	cur_stav_sk[4] = 64868030934155636;
	cur_stav_sk[5] = 5701249736610846;
	cur_stav_sk[6] = 14760355398292600;
	cur_stav_sk[7] = 33205351239062804;
	cur_stav_sk[8] = 23073669212481140;
	cur_stav_sk[9] = 8392098286339188;
	cur_stav_sk[10] = 13608591011283228;
	cur_stav_sk[11] = 11342341300126064;
	cur_stav_sk[12] = 71021379719004489;
	cur_stav_sk[13] = 18138374905860116;
	cur_stav_sk[14] = 68232492569953658;
	cur_stav_sk[15] = 32080194612379768;
	cur_stav_sk[16] = 6125489028400206;
	cur_stav_sk[17] = 18295881143395094;
	cur_stav_sk[18] = 24155271888610834;
	cur_stav_sk[19] = 14004018246649114;
	cur_stav_sk[20] = 15140007284084862;
	cur_stav_sk[21] = 19668687910778142;
	cur_stav_sk[22] = 28253124407760148;
	cur_stav_sk[23] = 32044197502784542;
	cur_stav_sk[24] = 23947155714937210;
	cur_stav_sk[25] = 55135085759759736;
	cur_stav_sk[26] = 8383600085336852;
	cur_stav_sk[27] = 15147524169896980;
	cur_stav_sk[28] = 12878680835457908;
	cur_stav_sk[29] = 9563297661948233;
	cur_stav_sk[30] = 9552630109943068;
	cur_stav_sk[31] = 27145368051848312;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18086504729149462;
	cur_stav_sk[1] = 27102488171910212;
	cur_stav_sk[2] = 322325507313946;
	cur_stav_sk[3] = 64846546358506772;
	cur_stav_sk[4] = 24868715900568336;
	cur_stav_sk[5] = 32787398154687560;
	cur_stav_sk[6] = 31842410795803156;
	cur_stav_sk[7] = 25052131472189302;
	cur_stav_sk[8] = 28687769192609276;
	cur_stav_sk[9] = 30496541653250886;
	cur_stav_sk[10] = 13635614057563250;
	cur_stav_sk[11] = 69563943236248432;
	cur_stav_sk[12] = 24181866862182522;
	cur_stav_sk[13] = 15535006252709374;
	cur_stav_sk[14] = 29785515806044312;
	cur_stav_sk[15] = 28471894635213334;
	cur_stav_sk[16] = 9556892403368314;
	cur_stav_sk[17] = 9511985223775860;
	cur_stav_sk[18] = 4578233878512918;
	cur_stav_sk[19] = 15174708958275020;
	cur_stav_sk[20] = 13643356814881556;
	cur_stav_sk[21] = 10652652233978388;
	cur_stav_sk[22] = 6122732041208092;
	cur_stav_sk[23] = 57500917490385008;
	cur_stav_sk[24] = 11762113686839452;
	cur_stav_sk[25] = 9887801447134066;
	cur_stav_sk[26] = 32723465492767606;
	cur_stav_sk[27] = 66938576824045938;
	cur_stav_sk[28] = 11245767885067386;
	cur_stav_sk[29] = 24190016789677176;
	cur_stav_sk[30] = 35000786933452820;
	cur_stav_sk[31] = 28438761100453649;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 29792811267335696;
	cur_stav_sk[1] = 10802709479703672;
	cur_stav_sk[2] = 69561460515386900;
	cur_stav_sk[3] = 1215815119606802;
	cur_stav_sk[4] = 9510328371421814;
	cur_stav_sk[5] = 27304729126085022;
	cur_stav_sk[6] = 59665566499374153;
	cur_stav_sk[7] = 27344312688148598;
	cur_stav_sk[8] = 1796538185463668;
	cur_stav_sk[9] = 33179479212101650;
	cur_stav_sk[10] = 66508982706865432;
	cur_stav_sk[11] = 115523954507802;
	cur_stav_sk[12] = 32784420822257686;
	cur_stav_sk[13] = 14970982540645500;
	cur_stav_sk[14] = 10674020246389017;
	cur_stav_sk[15] = 63738446084458096;
	cur_stav_sk[16] = 12671464166101368;
	cur_stav_sk[17] = 80065796519796;
	cur_stav_sk[18] = 15138519568025458;
	cur_stav_sk[19] = 27518343013446008;
	cur_stav_sk[20] = 12482513989947416;
	cur_stav_sk[21] = 28694856497304602;
	cur_stav_sk[22] = 28263912782529392;
	cur_stav_sk[23] = 9122609947869308;
	cur_stav_sk[24] = 1447374519231040;
	cur_stav_sk[25] = 9325515740565526;
	cur_stav_sk[26] = 24156097598554189;
	cur_stav_sk[27] = 65395107849195640;
	cur_stav_sk[28] = 31653445468648050;
	cur_stav_sk[29] = 1232557388739834;
	cur_stav_sk[30] = 26407623085436952;
	cur_stav_sk[31] = 65607738788840725;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27111283146862714;
	cur_stav_sk[1] = 31657031954153598;
	cur_stav_sk[2] = 14755339767317788;
	cur_stav_sk[3] = 9104730531669782;
	cur_stav_sk[4] = 8374290743726401;
	cur_stav_sk[5] = 7276326575756408;
	cur_stav_sk[6] = 11770072475205648;
	cur_stav_sk[7] = 65394997587874840;
	cur_stav_sk[8] = 114848852018196;
	cur_stav_sk[9] = 5022946542139154;
	cur_stav_sk[10] = 13608386598143098;
	cur_stav_sk[11] = 15147772517189398;
	cur_stav_sk[12] = 32741056684962322;
	cur_stav_sk[13] = 12899406598452758;
	cur_stav_sk[14] = 1654590001283192;
	cur_stav_sk[15] = 19643675093238293;
	cur_stav_sk[16] = 9130486917120122;
	cur_stav_sk[17] = 13810448638968850;
	cur_stav_sk[18] = 28488103754469744;
	cur_stav_sk[19] = 8377865217507602;
	cur_stav_sk[20] = 65382326355767753;
	cur_stav_sk[21] = 16267980140987516;
	cur_stav_sk[22] = 23741579588342136;
	cur_stav_sk[23] = 65294299928858998;
	cur_stav_sk[24] = 15139801281831418;
	cur_stav_sk[25] = 13590807097259888;
	cur_stav_sk[26] = 17381980230254710;
	cur_stav_sk[27] = 15174321391969140;
	cur_stav_sk[28] = 66924665944311414;
	cur_stav_sk[29] = 5921702808532248;
	cur_stav_sk[30] = 63737963234103678;
	cur_stav_sk[31] = 19247026812060693;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 3452328540025244;
	cur_stav_sk[1] = 28658871511777556;
	cur_stav_sk[2] = 30911428454285584;
	cur_stav_sk[3] = 16265630845862164;
	cur_stav_sk[4] = 11225197350487924;
	cur_stav_sk[5] = 28641760449926424;
	cur_stav_sk[6] = 9554623459600244;
	cur_stav_sk[7] = 20337490575341332;
	cur_stav_sk[8] = 27560160226386806;
	cur_stav_sk[9] = 32783622281970814;
	cur_stav_sk[10] = 14745787743311222;
	cur_stav_sk[11] = 31595917817459966;
	cur_stav_sk[12] = 15731473837229170;
	cur_stav_sk[13] = 64857653155148656;
	cur_stav_sk[14] = 9130994797843834;
	cur_stav_sk[15] = 16968865516730132;
	cur_stav_sk[16] = 1664144680255552;
	cur_stav_sk[17] = 31817534549394558;
	cur_stav_sk[18] = 68648130888742301;
	cur_stav_sk[19] = 7248961044779082;
	cur_stav_sk[20] = 9325803234227830;
	cur_stav_sk[21] = 12688643566285328;
	cur_stav_sk[22] = 64871777701823772;
	cur_stav_sk[23] = 32028601471598718;
	cur_stav_sk[24] = 12693454326760722;
	cur_stav_sk[25] = 28257110611395706;
	cur_stav_sk[26] = 32722364316193040;
	cur_stav_sk[27] = 7973175762059596;
	cur_stav_sk[28] = 18543199720932474;
	cur_stav_sk[29] = 9536838987883932;
	cur_stav_sk[30] = 10687870961590430;
	cur_stav_sk[31] = 28227518428418166;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 64855551305409040;
	cur_stav_sk[1] = 32053587022886418;
	cur_stav_sk[2] = 71433997067948412;
	cur_stav_sk[3] = 10627638407238678;
	cur_stav_sk[4] = 28459276885492764;
	cur_stav_sk[5] = 107277575336564;
	cur_stav_sk[6] = 27154257723852156;
	cur_stav_sk[7] = 68663958602940542;
	cur_stav_sk[8] = 59225846351115122;
	cur_stav_sk[9] = 10239438481764980;
	cur_stav_sk[10] = 18354716686123134;
	cur_stav_sk[11] = 10828914601923704;
	cur_stav_sk[12] = 15305338249295132;
	cur_stav_sk[13] = 546544336250482;
	cur_stav_sk[14] = 1226915547849882;
	cur_stav_sk[15] = 27132131973287966;
	cur_stav_sk[16] = 28679623392736020;
	cur_stav_sk[17] = 11250920912826524;
	cur_stav_sk[18] = 66525638131232892;
	cur_stav_sk[19] = 21471818259346196;
	cur_stav_sk[20] = 17382015667811454;
	cur_stav_sk[21] = 27138083348349054;
	cur_stav_sk[22] = 2335050259891480;
	cur_stav_sk[23] = 7964799485153608;
	cur_stav_sk[24] = 14049370686928496;
	cur_stav_sk[25] = 10216279958730110;
	cur_stav_sk[26] = 6853906894500722;
	cur_stav_sk[27] = 27524711592624498;
	cur_stav_sk[28] = 33197517019394334;
	cur_stav_sk[29] = 27102501284942872;
	cur_stav_sk[30] = 32951821318107516;
	cur_stav_sk[31] = 15157419563090964;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33191194963902590;
	cur_stav_sk[1] = 5393934558868504;
	cur_stav_sk[2] = 12478250108261648;
	cur_stav_sk[3] = 15834703145567346;
	cur_stav_sk[4] = 19428126745637488;
	cur_stav_sk[5] = 60888305039573018;
	cur_stav_sk[6] = 34982652580696446;
	cur_stav_sk[7] = 61321536507483156;
	cur_stav_sk[8] = 71450901543326846;
	cur_stav_sk[9] = 13582825018528116;
	cur_stav_sk[10] = 12465170913601298;
	cur_stav_sk[11] = 14725860909383706;
	cur_stav_sk[12] = 14770666546770452;
	cur_stav_sk[13] = 12895698584835188;
	cur_stav_sk[14] = 28685809238770810;
	cur_stav_sk[15] = 4803257575735312;
	cur_stav_sk[16] = 9914042156451696;
	cur_stav_sk[17] = 23759928762385534;
	cur_stav_sk[18] = 32730953846781080;
	cur_stav_sk[19] = 16982812951707762;
	cur_stav_sk[20] = 31659082461459312;
	cur_stav_sk[21] = 6176581284171792;
	cur_stav_sk[22] = 31623086369768570;
	cur_stav_sk[23] = 28676164864082448;
	cur_stav_sk[24] = 132865440089468;
	cur_stav_sk[25] = 6141352820674686;
	cur_stav_sk[26] = 65294460055335422;
	cur_stav_sk[27] = 9518065551086920;
	cur_stav_sk[28] = 5026746056314958;
	cur_stav_sk[29] = 16071428873924636;
	cur_stav_sk[30] = 11753192574358904;
	cur_stav_sk[31] = 6167812708317042;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18120234029355132;
	cur_stav_sk[1] = 19254990153811732;
	cur_stav_sk[2] = 19686763228792136;
	cur_stav_sk[3] = 11348215539643926;
	cur_stav_sk[4] = 6730458587239698;
	cur_stav_sk[5] = 63728686248854908;
	cur_stav_sk[6] = 10644929568932168;
	cur_stav_sk[7] = 27092865437860212;
	cur_stav_sk[8] = 71451312861746554;
	cur_stav_sk[9] = 28271818725294206;
	cur_stav_sk[10] = 4812319817631094;
	cur_stav_sk[11] = 18097740739739664;
	cur_stav_sk[12] = 31635098442830876;
	cur_stav_sk[13] = 15191813132825200;
	cur_stav_sk[14] = 63745909988819574;
	cur_stav_sk[15] = 13617057046618742;
	cur_stav_sk[16] = 28236519613301064;
	cur_stav_sk[17] = 2745210637226362;
	cur_stav_sk[18] = 27550289053487122;
	cur_stav_sk[19] = 14049384113079312;
	cur_stav_sk[20] = 30919166250549362;
	cur_stav_sk[21] = 15164977153717530;
	cur_stav_sk[22] = 63747229875110208;
	cur_stav_sk[23] = 32071180636595730;
	cur_stav_sk[24] = 17381979812995224;
	cur_stav_sk[25] = 65594397332649080;
	cur_stav_sk[26] = 32036570372280606;
	cur_stav_sk[27] = 19273508103344204;
	cur_stav_sk[28] = 29353598582194710;
	cur_stav_sk[29] = 22642067945292150;
	cur_stav_sk[30] = 28256966742554386;
	cur_stav_sk[31] = 16986728804222326;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 13616922156339217;
	cur_stav_sk[1] = 10128562818200060;
	cur_stav_sk[2] = 12877581653553940;
	cur_stav_sk[3] = 10819819337070096;
	cur_stav_sk[4] = 17382831841847056;
	cur_stav_sk[5] = 33180750144864632;
	cur_stav_sk[6] = 28253287241520150;
	cur_stav_sk[7] = 28668216374900594;
	cur_stav_sk[8] = 12455986261598838;
	cur_stav_sk[9] = 27346659411596314;
	cur_stav_sk[10] = 55855224180380946;
	cur_stav_sk[11] = 14039408425730074;
	cur_stav_sk[12] = 34086804555773456;
	cur_stav_sk[13] = 11241505672869746;
	cur_stav_sk[14] = 64854560246632564;
	cur_stav_sk[15] = 33188009551533072;
	cur_stav_sk[16] = 68226468177978898;
	cur_stav_sk[17] = 14200911077642516;
	cur_stav_sk[18] = 35397200672260220;
	cur_stav_sk[19] = 69366820807094558;
	cur_stav_sk[20] = 32080195297383748;
	cur_stav_sk[21] = 9545853119500404;
	cur_stav_sk[22] = 20355539079037308;
	cur_stav_sk[23] = 69779963839919516;
	cur_stav_sk[24] = 14030393892446412;
	cur_stav_sk[25] = 27533496809722142;
	cur_stav_sk[26] = 25292757463929976;
	cur_stav_sk[27] = 14717749817542002;
	cur_stav_sk[28] = 11233510603818312;
	cur_stav_sk[29] = 6843708813423998;
	cur_stav_sk[30] = 71019822722757118;
	cur_stav_sk[31] = 33145636480582008;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 68224315556925466;
	cur_stav_sk[1] = 28237181426315796;
	cur_stav_sk[2] = 27129151464903964;
	cur_stav_sk[3] = 19694734155407516;
	cur_stav_sk[4] = 19459023646459250;
	cur_stav_sk[5] = 32760815488938768;
	cur_stav_sk[6] = 10248510389879154;
	cur_stav_sk[7] = 32079955990053964;
	cur_stav_sk[8] = 27118990721365622;
	cur_stav_sk[9] = 10654095338045468;
	cur_stav_sk[10] = 32767238064584062;
	cur_stav_sk[11] = 1258284448256018;
	cur_stav_sk[12] = 32783896303019514;
	cur_stav_sk[13] = 25273035741466740;
	cur_stav_sk[14] = 9121911061918972;
	cur_stav_sk[15] = 1636175375007868;
	cur_stav_sk[16] = 15147772195739002;
	cur_stav_sk[17] = 13644737509003280;
	cur_stav_sk[18] = 33180064415522970;
	cur_stav_sk[19] = 64142970124740040;
	cur_stav_sk[20] = 61291344425301370;
	cur_stav_sk[21] = 16978116543260484;
	cur_stav_sk[22] = 31605342572294604;
	cur_stav_sk[23] = 68232495858287990;
	cur_stav_sk[24] = 27571119520371828;
	cur_stav_sk[25] = 28485115476677914;
	cur_stav_sk[26] = 9087700041016732;
	cur_stav_sk[27] = 28684985118982172;
	cur_stav_sk[28] = 56374946926166388;
	cur_stav_sk[29] = 10819912910307610;
	cur_stav_sk[30] = 11252229753144700;
	cur_stav_sk[31] = 9536922877669758;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 16273407973008890;
	cur_stav_sk[1] = 16969617873502330;
	cur_stav_sk[2] = 29555215829406836;
	cur_stav_sk[3] = 27518263560635514;
	cur_stav_sk[4] = 9114146651767070;
	cur_stav_sk[5] = 14707373033993594;
	cur_stav_sk[6] = 6160906338718742;
	cur_stav_sk[7] = 9510590370030028;
	cur_stav_sk[8] = 14056780122030706;
	cur_stav_sk[9] = 27535556187788406;
	cur_stav_sk[10] = 8375082815001664;
	cur_stav_sk[11] = 11347873973112852;
	cur_stav_sk[12] = 11329990737958004;
	cur_stav_sk[13] = 1242203552712056;
	cur_stav_sk[14] = 13607692484744574;
	cur_stav_sk[15] = 19668963883258998;
	cur_stav_sk[16] = 14013103374906492;
	cur_stav_sk[17] = 27128817597284476;
	cur_stav_sk[18] = 14735480961020528;
	cur_stav_sk[19] = 28455228321599870;
	cur_stav_sk[20] = 23750351171391860;
	cur_stav_sk[21] = 32044818262070292;
	cur_stav_sk[22] = 28236564628644344;
	cur_stav_sk[23] = 69885585058998038;
	cur_stav_sk[24] = 1664417622103314;
	cur_stav_sk[25] = 69798680219427070;
	cur_stav_sk[26] = 9905255446823026;
	cur_stav_sk[27] = 31657696126371088;
	cur_stav_sk[28] = 28640385434845208;
	cur_stav_sk[29] = 29555455810708080;
	cur_stav_sk[30] = 13625524526321944;
	cur_stav_sk[31] = 66500418548924493;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10653130582598518;
	cur_stav_sk[1] = 10661549808813174;
	cur_stav_sk[2] = 12482281537352904;
	cur_stav_sk[3] = 61291813981750598;
	cur_stav_sk[4] = 31640996756787230;
	cur_stav_sk[5] = 32729504279860598;
	cur_stav_sk[6] = 71454723974497860;
	cur_stav_sk[7] = 31643619463337340;
	cur_stav_sk[8] = 30470567842714736;
	cur_stav_sk[9] = 10460508830777412;
	cur_stav_sk[10] = 24867467262001942;
	cur_stav_sk[11] = 1101237342504304;
	cur_stav_sk[12] = 12482488912082454;
	cur_stav_sk[13] = 28257457096569718;
	cur_stav_sk[14] = 57519610312234104;
	cur_stav_sk[15] = 28253881882121492;
	cur_stav_sk[16] = 28702576164086264;
	cur_stav_sk[17] = 12482581049999484;
	cur_stav_sk[18] = 66949088742277188;
	cur_stav_sk[19] = 64159053097960732;
	cur_stav_sk[20] = 32044770802894196;
	cur_stav_sk[21] = 32732218511135760;
	cur_stav_sk[22] = 9116999710278734;
	cur_stav_sk[23] = 9315082116629828;
	cur_stav_sk[24] = 18526503567581341;
	cur_stav_sk[25] = 15833869005554814;
	cur_stav_sk[26] = 68452028049147000;
	cur_stav_sk[27] = 12888275116889204;
	cur_stav_sk[28] = 56068229207101509;
	cur_stav_sk[29] = 27155193817075826;
	cur_stav_sk[30] = 16282467985929338;
	cur_stav_sk[31] = 34270364483585394;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12879272490636656;
	cur_stav_sk[1] = 4610010131038582;
	cur_stav_sk[2] = 12464105755944346;
	cur_stav_sk[3] = 27524919035265054;
	cur_stav_sk[4] = 11333166174601598;
	cur_stav_sk[5] = 16968288967665054;
	cur_stav_sk[6] = 11773232824615698;
	cur_stav_sk[7] = 28694262247797618;
	cur_stav_sk[8] = 14030394379150750;
	cur_stav_sk[9] = 15315377924960378;
	cur_stav_sk[10] = 28488078999069254;
	cur_stav_sk[11] = 33856800986476872;
	cur_stav_sk[12] = 27118361767281018;
	cur_stav_sk[13] = 15729093798397306;
	cur_stav_sk[14] = 66499223681433628;
	cur_stav_sk[15] = 555190035744530;
	cur_stav_sk[16] = 15175421911859324;
	cur_stav_sk[17] = 10433033388097808;
	cur_stav_sk[18] = 19677372282639641;
	cur_stav_sk[19] = 32018692021593497;
	cur_stav_sk[20] = 27579722674615060;
	cur_stav_sk[21] = 30496196795897208;
	cur_stav_sk[22] = 33188585278936136;
	cur_stav_sk[23] = 27560182926321520;
	cur_stav_sk[24] = 71442175157862678;
	cur_stav_sk[25] = 33205875229197590;
	cur_stav_sk[26] = 4996530089989144;
	cur_stav_sk[27] = 27321221693411612;
	cur_stav_sk[28] = 18141890387744076;
	cur_stav_sk[29] = 27146686824846656;
	cur_stav_sk[30] = 13616921492948348;
	cur_stav_sk[31] = 14708242437637494;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 19651948410483478;
	cur_stav_sk[1] = 12898757140480380;
	cur_stav_sk[2] = 19250144036130888;
	cur_stav_sk[3] = 68662961761453342;
	cur_stav_sk[4] = 31613589094794768;
	cur_stav_sk[5] = 27128074908536958;
	cur_stav_sk[6] = 11251925812773908;
	cur_stav_sk[7] = 10256800670810132;
	cur_stav_sk[8] = 56375814283138074;
	cur_stav_sk[9] = 31865802816722300;
	cur_stav_sk[10] = 1664390789338234;
	cur_stav_sk[11] = 28262332960125132;
	cur_stav_sk[12] = 505724077876756;
	cur_stav_sk[13] = 31639682989130866;
	cur_stav_sk[14] = 1120613893115934;
	cur_stav_sk[15] = 10223017200489840;
	cur_stav_sk[16] = 66920748954259478;
	cur_stav_sk[17] = 26186549357139064;
	cur_stav_sk[18] = 16283595079287826;
	cur_stav_sk[19] = 8182048003200030;
	cur_stav_sk[20] = 27113543492941306;
	cur_stav_sk[21] = 538504561038400;
	cur_stav_sk[22] = 70308207383032062;
	cur_stav_sk[23] = 27140294222754320;
	cur_stav_sk[24] = 27114822405263736;
	cur_stav_sk[25] = 12473795288203548;
	cur_stav_sk[26] = 23063442022113434;
	cur_stav_sk[27] = 32933979498172528;
	cur_stav_sk[28] = 10258243236364356;
	cur_stav_sk[29] = 9519096159151256;
	cur_stav_sk[30] = 10678806119363446;
	cur_stav_sk[31] = 9096978051339388;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 1233110345553268;
	cur_stav_sk[1] = 6124147906462840;
	cur_stav_sk[2] = 14731013612717128;
	cur_stav_sk[3] = 14628939534570522;
	cur_stav_sk[4] = 56378119606899832;
	cur_stav_sk[5] = 2541963532187036;
	cur_stav_sk[6] = 10626264092021116;
	cur_stav_sk[7] = 13581736794492945;
	cur_stav_sk[8] = 63728641214621130;
	cur_stav_sk[9] = 14056806814589182;
	cur_stav_sk[10] = 28489761694954618;
	cur_stav_sk[11] = 14958038579646480;
	cur_stav_sk[12] = 10433572956537456;
	cur_stav_sk[13] = 71019754927993086;
	cur_stav_sk[14] = 28262883741925758;
	cur_stav_sk[15] = 27559884608119194;
	cur_stav_sk[16] = 9088296378925336;
	cur_stav_sk[17] = 5049870278698394;
	cur_stav_sk[18] = 31641547130635640;
	cur_stav_sk[19] = 10627707286860028;
	cur_stav_sk[20] = 60369336289498232;
	cur_stav_sk[21] = 69797621580832926;
	cur_stav_sk[22] = 61317912377262148;
	cur_stav_sk[23] = 4826589239144650;
	cur_stav_sk[24] = 668603485913370;
	cur_stav_sk[25] = 69375889474761540;
	cur_stav_sk[26] = 15851403815485716;
	cur_stav_sk[27] = 15727996912829554;
	cur_stav_sk[28] = 32768956592664984;
	cur_stav_sk[29] = 13626002953764890;
	cur_stav_sk[30] = 27550411478863892;
	cur_stav_sk[31] = 98493467100178;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15166489589954324;
	cur_stav_sk[1] = 510205085200664;
	cur_stav_sk[2] = 5763121957344274;
	cur_stav_sk[3] = 6187162548306802;
	cur_stav_sk[4] = 27568968816309824;
	cur_stav_sk[5] = 9561702150416246;
	cur_stav_sk[6] = 24173344589894678;
	cur_stav_sk[7] = 9143225774113908;
	cur_stav_sk[8] = 32988964336193660;
	cur_stav_sk[9] = 6176333711081598;
	cur_stav_sk[10] = 27098296891671834;
	cur_stav_sk[11] = 29774779444860030;
	cur_stav_sk[12] = 10242568236437530;
	cur_stav_sk[13] = 22819131184252030;
	cur_stav_sk[14] = 29582917295485054;
	cur_stav_sk[15] = 29344732758848276;
	cur_stav_sk[16] = 27550208380667258;
	cur_stav_sk[17] = 2236207006519416;
	cur_stav_sk[18] = 56288197149623320;
	cur_stav_sk[19] = 13616978275378426;
	cur_stav_sk[20] = 5735155421184020;
	cur_stav_sk[21] = 545570055762044;
	cur_stav_sk[22] = 14975140229809532;
	cur_stav_sk[23] = 14058042884498842;
	cur_stav_sk[24] = 31639828135478384;
	cur_stav_sk[25] = 5388573966472476;
	cur_stav_sk[26] = 13594242784376434;
	cur_stav_sk[27] = 30681464018776592;
	cur_stav_sk[28] = 20337494333392254;
	cur_stav_sk[29] = 33206999233344118;
	cur_stav_sk[30] = 31847797441856116;
	cur_stav_sk[31] = 10450342095132022;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32779197610754378;
	cur_stav_sk[1] = 16256242513543290;
	cur_stav_sk[2] = 9289504978276630;
	cur_stav_sk[3] = 24163653714379796;
	cur_stav_sk[4] = 9563488393725460;
	cur_stav_sk[5] = 32062658073363738;
	cur_stav_sk[6] = 10831400183706744;
	cur_stav_sk[7] = 10478077939627793;
	cur_stav_sk[8] = 22589473050071164;
	cur_stav_sk[9] = 14021884423114768;
	cur_stav_sk[10] = 31810011912155294;
	cur_stav_sk[11] = 27533522573439258;
	cur_stav_sk[12] = 9509269676139976;
	cur_stav_sk[13] = 9677437629539486;
	cur_stav_sk[14] = 35405998308958458;
	cur_stav_sk[15] = 28244606413809150;
	cur_stav_sk[16] = 15155745117278578;
	cur_stav_sk[17] = 71445783547958342;
	cur_stav_sk[18] = 68223054014387326;
	cur_stav_sk[19] = 14937721324875930;
	cur_stav_sk[20] = 33187758438462492;
	cur_stav_sk[21] = 14065601968113172;
	cur_stav_sk[22] = 28692928521831548;
	cur_stav_sk[23] = 5762091166105880;
	cur_stav_sk[24] = 5040573241037170;
	cur_stav_sk[25] = 9553139759691036;
	cur_stav_sk[26] = 14198905610512196;
	cur_stav_sk[27] = 9087413544530706;
	cur_stav_sk[28] = 64142045569913106;
	cur_stav_sk[29] = 28246209462702202;
	cur_stav_sk[30] = 28245120667431966;
	cur_stav_sk[31] = 22624407039050260;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31649449211868542;
	cur_stav_sk[1] = 24147410500424724;
	cur_stav_sk[2] = 9324301016401176;
	cur_stav_sk[3] = 2744456203641617;
	cur_stav_sk[4] = 32027498654904950;
	cur_stav_sk[5] = 66516888188878873;
	cur_stav_sk[6] = 9685355276378520;
	cur_stav_sk[7] = 18542471186351634;
	cur_stav_sk[8] = 9136397807883640;
	cur_stav_sk[9] = 61292054501401364;
	cur_stav_sk[10] = 28262771926044958;
	cur_stav_sk[11] = 16977358409928990;
	cur_stav_sk[12] = 547082822201878;
	cur_stav_sk[13] = 17169978948338758;
	cur_stav_sk[14] = 14206066665754640;
	cur_stav_sk[15] = 7055171122189384;
	cur_stav_sk[16] = 27325904997941782;
	cur_stav_sk[17] = 32766107618086980;
	cur_stav_sk[18] = 27092740886332694;
	cur_stav_sk[19] = 679296394311280;
	cur_stav_sk[20] = 6184301961774098;
	cur_stav_sk[21] = 62435704992371742;
	cur_stav_sk[22] = 14197450557490298;
	cur_stav_sk[23] = 57723303447929340;
	cur_stav_sk[24] = 30901325168645496;
	cur_stav_sk[25] = 10652145043354224;
	cur_stav_sk[26] = 6844339959631992;
	cur_stav_sk[27] = 15190928434070290;
	cur_stav_sk[28] = 14772656532619845;
	cur_stav_sk[29] = 14602153698494320;
	cur_stav_sk[30] = 32765890500309825;
	cur_stav_sk[31] = 13626140318109816;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12877857074124062;
	cur_stav_sk[1] = 14207689622361410;
	cur_stav_sk[2] = 9095509421504278;
	cur_stav_sk[3] = 28658502700631066;
	cur_stav_sk[4] = 24865772775030810;
	cur_stav_sk[5] = 22643073022329112;
	cur_stav_sk[6] = 2243621122630212;
	cur_stav_sk[7] = 63949388775489558;
	cur_stav_sk[8] = 528678263928342;
	cur_stav_sk[9] = 28281464347362330;
	cur_stav_sk[10] = 69367712410743966;
	cur_stav_sk[11] = 14066415591826034;
	cur_stav_sk[12] = 10688153385730832;
	cur_stav_sk[13] = 10679108709090577;
	cur_stav_sk[14] = 11755639237620764;
	cur_stav_sk[15] = 23952453558604146;
	cur_stav_sk[16] = 13634717564183704;
	cur_stav_sk[17] = 32743323694284876;
	cur_stav_sk[18] = 4602368853127582;
	cur_stav_sk[19] = 8375534877872248;
	cur_stav_sk[20] = 28253837451965822;
	cur_stav_sk[21] = 34289171608482324;
	cur_stav_sk[22] = 28464738135639162;
	cur_stav_sk[23] = 32967763231445012;
	cur_stav_sk[24] = 10243881401594386;
	cur_stav_sk[25] = 27329254012616728;
	cur_stav_sk[26] = 13819904142550900;
	cur_stav_sk[27] = 28271509485007258;
	cur_stav_sk[28] = 10231608202639126;
	cur_stav_sk[29] = 33187733799638046;
	cur_stav_sk[30] = 22634373048436082;
	cur_stav_sk[31] = 14207716403163294;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31650202500542748;
	cur_stav_sk[1] = 66929857283818102;
	cur_stav_sk[2] = 13836946422305042;
	cur_stav_sk[3] = 68232135924155772;
	cur_stav_sk[4] = 17399288946840604;
	cur_stav_sk[5] = 28483679882067996;
	cur_stav_sk[6] = 33153675062903922;
	cur_stav_sk[7] = 28685510593356306;
	cur_stav_sk[8] = 31641419477135984;
	cur_stav_sk[9] = 23742610504299332;
	cur_stav_sk[10] = 59217115606851998;
	cur_stav_sk[11] = 27526878203117688;
	cur_stav_sk[12] = 35203206075334686;
	cur_stav_sk[13] = 17385138562171258;
	cur_stav_sk[14] = 19694595103011196;
	cur_stav_sk[15] = 66951149116040444;
	cur_stav_sk[16] = 2032790308992628;
	cur_stav_sk[17] = 318463378010392;
	cur_stav_sk[18] = 30469972811912314;
	cur_stav_sk[19] = 34983167389500700;
	cur_stav_sk[20] = 68648119698195578;
	cur_stav_sk[21] = 28433238834020376;
	cur_stav_sk[22] = 54740042182297204;
	cur_stav_sk[23] = 528388961764730;
	cur_stav_sk[24] = 28262429385658742;
	cur_stav_sk[25] = 35397476091270174;
	cur_stav_sk[26] = 32956518887338610;
	cur_stav_sk[27] = 12476776536474648;
	cur_stav_sk[28] = 28272643492676113;
	cur_stav_sk[29] = 11753315461705850;
	cur_stav_sk[30] = 3449999069545804;
	cur_stav_sk[31] = 14039396681736398;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10422887201772568;
	cur_stav_sk[1] = 10637884550095378;
	cur_stav_sk[2] = 20566920139219144;
	cur_stav_sk[3] = 9098525833954332;
	cur_stav_sk[4] = 17192682168123666;
	cur_stav_sk[5] = 33171746682126460;
	cur_stav_sk[6] = 19247602484459124;
	cur_stav_sk[7] = 32748479776567828;
	cur_stav_sk[8] = 16968520294470014;
	cur_stav_sk[9] = 24171972486299716;
	cur_stav_sk[10] = 27133229876053274;
	cur_stav_sk[11] = 30471390689526142;
	cur_stav_sk[12] = 10626236116370453;
	cur_stav_sk[13] = 9122997101495369;
	cur_stav_sk[14] = 9563514561991038;
	cur_stav_sk[15] = 9563365393051852;
	cur_stav_sk[16] = 65602929490703889;
	cur_stav_sk[17] = 538561319052048;
	cur_stav_sk[18] = 6325014744175692;
	cur_stav_sk[19] = 13837934735197208;
	cur_stav_sk[20] = 16282217804075384;
	cur_stav_sk[21] = 32054617827619602;
	cur_stav_sk[22] = 35423316689100958;
	cur_stav_sk[23] = 10437145824933654;
	cur_stav_sk[24] = 15130961697215512;
	cur_stav_sk[25] = 10678669014246908;
	cur_stav_sk[26] = 65805022612717690;
	cur_stav_sk[27] = 9342295825519644;
	cur_stav_sk[28] = 18528805897155916;
	cur_stav_sk[29] = 27551042682361106;
	cur_stav_sk[30] = 33206178550646140;
	cur_stav_sk[31] = 7257564422910104;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14743151927628826;
	cur_stav_sk[1] = 29582916769256464;
	cur_stav_sk[2] = 14207360638780438;
	cur_stav_sk[3] = 56797571640987670;
	cur_stav_sk[4] = 10671383323738234;
	cur_stav_sk[5] = 17390912621393686;
	cur_stav_sk[6] = 68663215082344470;
	cur_stav_sk[7] = 19265098423833884;
	cur_stav_sk[8] = 133732409635446;
	cur_stav_sk[9] = 32758332969267352;
	cur_stav_sk[10] = 5952373915684890;
	cur_stav_sk[11] = 28482717343851934;
	cur_stav_sk[12] = 35396334259045746;
	cur_stav_sk[13] = 9509351262060816;
	cur_stav_sk[14] = 28702242162751344;
	cur_stav_sk[15] = 15323350260228604;
	cur_stav_sk[16] = 10635952880161810;
	cur_stav_sk[17] = 14753046233855054;
	cur_stav_sk[18] = 16063334790037534;
	cur_stav_sk[19] = 33179489477067850;
	cur_stav_sk[20] = 31851496879535734;
	cur_stav_sk[21] = 33207276257281406;
	cur_stav_sk[22] = 13635694993805330;
	cur_stav_sk[23] = 60351196510328090;
	cur_stav_sk[24] = 28670758437619068;
	cur_stav_sk[25] = 3898735177339932;
	cur_stav_sk[26] = 33189110220596636;
	cur_stav_sk[27] = 10671136712922480;
	cur_stav_sk[28] = 6166891907133720;
	cur_stav_sk[29] = 1206997161767444;
	cur_stav_sk[30] = 32976144718824566;
	cur_stav_sk[31] = 15728450639692058;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 2217513158710336;
	cur_stav_sk[1] = 27132406399763222;
	cur_stav_sk[2] = 27535829920678417;
	cur_stav_sk[3] = 16054661908026392;
	cur_stav_sk[4] = 69894391164019962;
	cur_stav_sk[5] = 17198045033088530;
	cur_stav_sk[6] = 33179489433650204;
	cur_stav_sk[7] = 9316457444868218;
	cur_stav_sk[8] = 23723620917359122;
	cur_stav_sk[9] = 10811325173564444;
	cur_stav_sk[10] = 10653244418667280;
	cur_stav_sk[11] = 13608535242147190;
	cur_stav_sk[12] = 28694856587359094;
	cur_stav_sk[13] = 18569109977896212;
	cur_stav_sk[14] = 29766617664008976;
	cur_stav_sk[15] = 25996345257317452;
	cur_stav_sk[16] = 27567581150127890;
	cur_stav_sk[17] = 65803592367648792;
	cur_stav_sk[18] = 59646692460729362;
	cur_stav_sk[19] = 19677096851157780;
	cur_stav_sk[20] = 27577682821713936;
	cur_stav_sk[21] = 9562254481306224;
	cur_stav_sk[22] = 17170665091145886;
	cur_stav_sk[23] = 13616976920773752;
	cur_stav_sk[24] = 10213644849623873;
	cur_stav_sk[25] = 502256915382806;
	cur_stav_sk[26] = 9500679592612980;
	cur_stav_sk[27] = 19229117274980376;
	cur_stav_sk[28] = 6166985914462234;
	cur_stav_sk[29] = 28667668160983198;
	cur_stav_sk[30] = 25263280050635892;
	cur_stav_sk[31] = 13832688977871988;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 63947878749778546;
	cur_stav_sk[1] = 55838800091096473;
	cur_stav_sk[2] = 4609981758060308;
	cur_stav_sk[3] = 14716239102259277;
	cur_stav_sk[4] = 9501780321080594;
	cur_stav_sk[5] = 60772309452915740;
	cur_stav_sk[6] = 32775650347877754;
	cur_stav_sk[7] = 22625094242337916;
	cur_stav_sk[8] = 27346933158617592;
	cur_stav_sk[9] = 33154567342294396;
	cur_stav_sk[10] = 10644445785260060;
	cur_stav_sk[11] = 66930399071965296;
	cur_stav_sk[12] = 32019504173090172;
	cur_stav_sk[13] = 12474059296347262;
	cur_stav_sk[14] = 28222848204386424;
	cur_stav_sk[15] = 5404965770372508;
	cur_stav_sk[16] = 32030909461271930;
	cur_stav_sk[17] = 28685740430754078;
	cur_stav_sk[18] = 64854518015624820;
	cur_stav_sk[19] = 32986481100202488;
	cur_stav_sk[20] = 14033235505657244;
	cur_stav_sk[21] = 64863496458122492;
	cur_stav_sk[22] = 15165365472305562;
	cur_stav_sk[23] = 26407321414275440;
	cur_stav_sk[24] = 8375056373420400;
	cur_stav_sk[25] = 10213325398750716;
	cur_stav_sk[26] = 10231539276329584;
	cur_stav_sk[27] = 10239483371201786;
	cur_stav_sk[28] = 11252225978339348;
	cur_stav_sk[29] = 14979613493448730;
	cur_stav_sk[30] = 13846979948950128;
	cur_stav_sk[31] = 11242331445863028;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 13839593869018236;
	cur_stav_sk[1] = 15175011759067162;
	cur_stav_sk[2] = 32721908962406428;
	cur_stav_sk[3] = 27146400392418326;
	cur_stav_sk[4] = 31865789456207888;
	cur_stav_sk[5] = 14185628935488790;
	cur_stav_sk[6] = 69798680892244336;
	cur_stav_sk[7] = 19267708694300022;
	cur_stav_sk[8] = 15156661648143216;
	cur_stav_sk[9] = 6141421591377402;
	cur_stav_sk[10] = 32071937829184022;
	cur_stav_sk[11] = 60342191502886166;
	cur_stav_sk[12] = 30900679623475224;
	cur_stav_sk[13] = 4842016761557244;
	cur_stav_sk[14] = 28696666828017728;
	cur_stav_sk[15] = 23038965877376124;
	cur_stav_sk[16] = 14751946208813210;
	cur_stav_sk[17] = 31640480762438904;
	cur_stav_sk[18] = 19431976844022902;
	cur_stav_sk[19] = 14760127093887090;
	cur_stav_sk[20] = 27128807753322612;
	cur_stav_sk[21] = 27136158506684784;
	cur_stav_sk[22] = 59638065505777170;
	cur_stav_sk[23] = 5969831889637658;
	cur_stav_sk[24] = 71425269225927284;
	cur_stav_sk[25] = 10231879328596592;
	cur_stav_sk[26] = 3881101720691868;
	cur_stav_sk[27] = 5049992488027510;
	cur_stav_sk[28] = 12879504743366929;
	cur_stav_sk[29] = 13644752630952566;
	cur_stav_sk[30] = 1235412446523802;
	cur_stav_sk[31] = 32062384269328454;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9565687901041016;
	cur_stav_sk[1] = 63719753177371672;
	cur_stav_sk[2] = 64159765114093592;
	cur_stav_sk[3] = 10119842296509040;
	cur_stav_sk[4] = 31847147079213682;
	cur_stav_sk[5] = 14768781987386746;
	cur_stav_sk[6] = 1214444098256972;
	cur_stav_sk[7] = 9294479422818166;
	cur_stav_sk[8] = 32062794384057968;
	cur_stav_sk[9] = 27101690001817716;
	cur_stav_sk[10] = 31631308258346264;
	cur_stav_sk[11] = 28272598800205072;
	cur_stav_sk[12] = 5058715111929080;
	cur_stav_sk[13] = 27128886789543024;
	cur_stav_sk[14] = 66521035000940054;
	cur_stav_sk[15] = 28236177476911734;
	cur_stav_sk[16] = 9561991454996244;
	cur_stav_sk[17] = 3475323874654066;
	cur_stav_sk[18] = 23723574066185501;
	cur_stav_sk[19] = 33851624615542906;
	cur_stav_sk[20] = 13620323979367502;
	cur_stav_sk[21] = 4618038429058334;
	cur_stav_sk[22] = 32731120124297490;
	cur_stav_sk[23] = 1231621080945993;
	cur_stav_sk[24] = 29770212903912516;
	cur_stav_sk[25] = 32062821697402482;
	cur_stav_sk[26] = 10451441080872985;
	cur_stav_sk[27] = 106339653527318;
	cur_stav_sk[28] = 6131527146911553;
	cur_stav_sk[29] = 13582133998555156;
	cur_stav_sk[30] = 27111500087922800;
	cur_stav_sk[31] = 15533906950636570;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 64150030371299736;
	cur_stav_sk[1] = 29353267075416432;
	cur_stav_sk[2] = 32766277656248438;
	cur_stav_sk[3] = 13814818679460886;
	cur_stav_sk[4] = 11242307770552652;
	cur_stav_sk[5] = 18551390444266004;
	cur_stav_sk[6] = 68223259508187546;
	cur_stav_sk[7] = 19256399126856261;
	cur_stav_sk[8] = 16063073019428986;
	cur_stav_sk[9] = 27101193256141944;
	cur_stav_sk[10] = 65059921075372048;
	cur_stav_sk[11] = 65373336990680644;
	cur_stav_sk[12] = 16256643700819277;
	cur_stav_sk[13] = 27579710600873492;
	cur_stav_sk[14] = 10266766133310324;
	cur_stav_sk[15] = 69349323558392862;
	cur_stav_sk[16] = 22641244066559096;
	cur_stav_sk[17] = 9334335116215412;
	cur_stav_sk[18] = 28677263169847674;
	cur_stav_sk[19] = 27541399621795964;
	cur_stav_sk[20] = 33206701785874704;
	cur_stav_sk[21] = 27153956933670168;
	cur_stav_sk[22] = 9131278999524474;
	cur_stav_sk[23] = 28684435713234290;
	cur_stav_sk[24] = 66510599823364220;
	cur_stav_sk[25] = 18564090995441688;
	cur_stav_sk[26] = 5049596952543557;
	cur_stav_sk[27] = 31640513305314320;
	cur_stav_sk[28] = 14746141069684856;
	cur_stav_sk[29] = 9140958500439158;
	cur_stav_sk[30] = 75898586170384;
	cur_stav_sk[31] = 34081592549119004;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33852330957182274;
	cur_stav_sk[1] = 25272430524379594;
	cur_stav_sk[2] = 27146481949749780;
	cur_stav_sk[3] = 9104058826556700;
	cur_stav_sk[4] = 23030102209300086;
	cur_stav_sk[5] = 10455264193283102;
	cur_stav_sk[6] = 11349108224591176;
	cur_stav_sk[7] = 23021317170286622;
	cur_stav_sk[8] = 24171875170881650;
	cur_stav_sk[9] = 28246005502096912;
	cur_stav_sk[10] = 31597028798988574;
	cur_stav_sk[11] = 1645973946696984;
	cur_stav_sk[12] = 12465240302072188;
	cur_stav_sk[13] = 1680885385539610;
	cur_stav_sk[14] = 54720719439074328;
	cur_stav_sk[15] = 10670722252636274;
	cur_stav_sk[16] = 68461923742712952;
	cur_stav_sk[17] = 11360115506353272;
	cur_stav_sk[18] = 14021759956980852;
	cur_stav_sk[19] = 27153306510970908;
	cur_stav_sk[20] = 31632762249118029;
	cur_stav_sk[21] = 9088021095678326;
	cur_stav_sk[22] = 10830014195336304;
	cur_stav_sk[23] = 10819502603370512;
	cur_stav_sk[24] = 31631676289169010;
	cur_stav_sk[25] = 317213878858308;
	cur_stav_sk[26] = 27110044258281232;
	cur_stav_sk[27] = 14754763700601106;
	cur_stav_sk[28] = 28271269036668412;
	cur_stav_sk[29] = 33171790910296446;
	cur_stav_sk[30] = 10679107626796061;
	cur_stav_sk[31] = 31641284053439558;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27347882357527922;
	cur_stav_sk[1] = 10124311140471056;
	cur_stav_sk[2] = 15148623122010134;
	cur_stav_sk[3] = 71029170257011472;
	cur_stav_sk[4] = 65267772130362494;
	cur_stav_sk[5] = 35404924566938060;
	cur_stav_sk[6] = 9535957188571384;
	cur_stav_sk[7] = 14726659164811673;
	cur_stav_sk[8] = 70753366021149;
	cur_stav_sk[9] = 69884825985717268;
	cur_stav_sk[10] = 10098687885653149;
	cur_stav_sk[11] = 14197255999071813;
	cur_stav_sk[12] = 63738228028380185;
	cur_stav_sk[13] = 61110349143814264;
	cur_stav_sk[14] = 114656388027412;
	cur_stav_sk[15] = 11330487274190660;
	cur_stav_sk[16] = 4582840769328156;
	cur_stav_sk[17] = 13798613316514074;
	cur_stav_sk[18] = 30479018072548600;
	cur_stav_sk[19] = 10832223751999512;
	cur_stav_sk[20] = 5006714722423928;
	cur_stav_sk[21] = 16959999691945590;
	cur_stav_sk[22] = 12693454468510276;
	cur_stav_sk[23] = 6838623647707384;
	cur_stav_sk[24] = 521105716514886;
	cur_stav_sk[25] = 14038788944606736;
	cur_stav_sk[26] = 28455503764946960;
	cur_stav_sk[27] = 11331159357034782;
	cur_stav_sk[28] = 10098842053968248;
	cur_stav_sk[29] = 15745793726353426;
	cur_stav_sk[30] = 22853643683328157;
	cur_stav_sk[31] = 33848084891271290;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12877555556487450;
	cur_stav_sk[1] = 11234747223015450;
	cur_stav_sk[2] = 18359390172390908;
	cur_stav_sk[3] = 10680455841939736;
	cur_stav_sk[4] = 22623902918095378;
	cur_stav_sk[5] = 116304516003600;
	cur_stav_sk[6] = 10204944533361692;
	cur_stav_sk[7] = 31825095846303100;
	cur_stav_sk[8] = 31657420859908882;
	cur_stav_sk[9] = 65269008997122376;
	cur_stav_sk[10] = 14021254136595737;
	cur_stav_sk[11] = 22610204979790868;
	cur_stav_sk[12] = 33206836002264396;
	cur_stav_sk[13] = 5057963968279709;
	cur_stav_sk[14] = 7267586387916356;
	cur_stav_sk[15] = 13634904652677648;
	cur_stav_sk[16] = 6316520905180536;
	cur_stav_sk[17] = 33856717519759174;
	cur_stav_sk[18] = 33866956046373912;
	cur_stav_sk[19] = 10454712759845398;
	cur_stav_sk[20] = 2341757908353046;
	cur_stav_sk[21] = 31851485534717302;
	cur_stav_sk[22] = 32063702555698584;
	cur_stav_sk[23] = 27140225507820610;
	cur_stav_sk[24] = 13820617117499672;
	cur_stav_sk[25] = 65804195812324976;
	cur_stav_sk[26] = 26200955365331058;
	cur_stav_sk[27] = 19255433623479109;
	cur_stav_sk[28] = 13617197051155486;
	cur_stav_sk[29] = 12467505759490328;
	cur_stav_sk[30] = 11330403661414782;
	cur_stav_sk[31] = 13599396952606076;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 16960822318137630;
	cur_stav_sk[1] = 16960549506360734;
	cur_stav_sk[2] = 14014189863277684;
	cur_stav_sk[3] = 32985355280188530;
	cur_stav_sk[4] = 9099219139101766;
	cur_stav_sk[5] = 65383098032062832;
	cur_stav_sk[6] = 71004087637640774;
	cur_stav_sk[7] = 31869076113539096;
	cur_stav_sk[8] = 27093645863650588;
	cur_stav_sk[9] = 16265907332355350;
	cur_stav_sk[10] = 16986317020173328;
	cur_stav_sk[11] = 64853911303161166;
	cur_stav_sk[12] = 27118649546835216;
	cur_stav_sk[13] = 14049292902044944;
	cur_stav_sk[14] = 9553412799608338;
	cur_stav_sk[15] = 29785584667264880;
	cur_stav_sk[16] = 12908101921706008;
	cur_stav_sk[17] = 27559057789060380;
	cur_stav_sk[18] = 19687585178808849;
	cur_stav_sk[19] = 27324581028140144;
	cur_stav_sk[20] = 18125866559436402;
	cur_stav_sk[21] = 32991403140222992;
	cur_stav_sk[22] = 4628205514007410;
	cur_stav_sk[23] = 69789068025431418;
	cur_stav_sk[24] = 65390223732683134;
	cur_stav_sk[25] = 71030203102462232;
	cur_stav_sk[26] = 15174709702164600;
	cur_stav_sk[27] = 9544685578593434;
	cur_stav_sk[28] = 5973377186743416;
	cur_stav_sk[29] = 32037369557661200;
	cur_stav_sk[30] = 10469030638781820;
	cur_stav_sk[31] = 34973547207082106;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14720155981661202;
	cur_stav_sk[1] = 66931358938474256;
	cur_stav_sk[2] = 32026664024478786;
	cur_stav_sk[3] = 25293101557715058;
	cur_stav_sk[4] = 16978211343115890;
	cur_stav_sk[5] = 14964246421352980;
	cur_stav_sk[6] = 57519719243973908;
	cur_stav_sk[7] = 10635264263553144;
	cur_stav_sk[8] = 32776404586116980;
	cur_stav_sk[9] = 24136864890172824;
	cur_stav_sk[10] = 14057425297682682;
	cur_stav_sk[11] = 687845669175676;
	cur_stav_sk[12] = 30902701542257144;
	cur_stav_sk[13] = 10627362460542742;
	cur_stav_sk[14] = 12465787390361630;
	cur_stav_sk[15] = 68664040674525468;
	cur_stav_sk[16] = 15164633497534584;
	cur_stav_sk[17] = 10467793298764309;
	cur_stav_sk[18] = 9081558023898896;
	cur_stav_sk[19] = 17408369585684604;
	cur_stav_sk[20] = 63935119414330640;
	cur_stav_sk[21] = 1645699624407152;
	cur_stav_sk[22] = 14719879493072406;
	cur_stav_sk[23] = 27343500463068233;
	cur_stav_sk[24] = 9081263843775508;
	cur_stav_sk[25] = 15314484566786077;
	cur_stav_sk[26] = 22592772671370772;
	cur_stav_sk[27] = 59656150993183298;
	cur_stav_sk[28] = 5032004353930396;
	cur_stav_sk[29] = 16960685931110928;
	cur_stav_sk[30] = 31843811493643544;
	cur_stav_sk[31] = 71434728240002930;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 63948976584523900;
	cur_stav_sk[1] = 28255007753375858;
	cur_stav_sk[2] = 28254706502631540;
	cur_stav_sk[3] = 27361104759619960;
	cur_stav_sk[4] = 10802569905177884;
	cur_stav_sk[5] = 12877866194964850;
	cur_stav_sk[6] = 10231742837569040;
	cur_stav_sk[7] = 13837084816192020;
	cur_stav_sk[8] = 27357244848743190;
	cur_stav_sk[9] = 65277528683942928;
	cur_stav_sk[10] = 9289541301023002;
	cur_stav_sk[11] = 5050339842033014;
	cur_stav_sk[12] = 6853878966849812;
	cur_stav_sk[13] = 23750007364326770;
	cur_stav_sk[14] = 19255848821621020;
	cur_stav_sk[15] = 14030555437630740;
	cur_stav_sk[16] = 74371653959700;
	cur_stav_sk[17] = 28675913627349114;
	cur_stav_sk[18] = 15168448495523188;
	cur_stav_sk[19] = 1822884138161472;
	cur_stav_sk[20] = 14021541377803336;
	cur_stav_sk[21] = 4610078868873846;
	cur_stav_sk[22] = 11356416671253574;
	cur_stav_sk[23] = 2350282946086169;
	cur_stav_sk[24] = 70317443500716822;
	cur_stav_sk[25] = 11232919042592880;
	cur_stav_sk[26] = 10203603740209528;
	cur_stav_sk[27] = 66947896301192570;
	cur_stav_sk[28] = 17382828001867540;
	cur_stav_sk[29] = 33179280790221430;
	cur_stav_sk[30] = 14012675493004610;
	cur_stav_sk[31] = 18569177313050653;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10652579979400306;
	cur_stav_sk[1] = 14958723156246848;
	cur_stav_sk[2] = 32053874581636982;
	cur_stav_sk[3] = 28253127184221552;
	cur_stav_sk[4] = 10265964585035284;
	cur_stav_sk[5] = 32775178397942042;
	cur_stav_sk[6] = 14709023896973822;
	cur_stav_sk[7] = 60896277646644338;
	cur_stav_sk[8] = 14066013605573234;
	cur_stav_sk[9] = 2534105956652406;
	cur_stav_sk[10] = 33153332052559172;
	cur_stav_sk[11] = 60870894637483026;
	cur_stav_sk[12] = 61309682274144280;
	cur_stav_sk[13] = 10249582986794264;
	cur_stav_sk[14] = 27136858599063672;
	cur_stav_sk[15] = 69790203040367377;
	cur_stav_sk[16] = 135957204238608;
	cur_stav_sk[17] = 13635945255798340;
	cur_stav_sk[18] = 70307247977997726;
	cur_stav_sk[19] = 65382032150496838;
	cur_stav_sk[20] = 31657638078842137;
	cur_stav_sk[21] = 68223670226432577;
	cur_stav_sk[22] = 4580036105384442;
	cur_stav_sk[23] = 10691517937423742;
	cur_stav_sk[24] = 17399390868443408;
	cur_stav_sk[25] = 14039753307240050;
	cur_stav_sk[26] = 15129931428005652;
	cur_stav_sk[27] = 54738739206595736;
	cur_stav_sk[28] = 26195620881649780;
	cur_stav_sk[29] = 24864088602510460;
	cur_stav_sk[30] = 28262744748688412;
	cur_stav_sk[31] = 68433634386970128;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32723533014476184;
	cur_stav_sk[1] = 18515987948998936;
	cur_stav_sk[2] = 13643378432901490;
	cur_stav_sk[3] = 6187087758328081;
	cur_stav_sk[4] = 9526477449310460;
	cur_stav_sk[5] = 309795408020338;
	cur_stav_sk[6] = 1653945203302552;
	cur_stav_sk[7] = 14004553829529105;
	cur_stav_sk[8] = 13635831434322170;
	cur_stav_sk[9] = 65602092836864026;
	cur_stav_sk[10] = 290827353157658;
	cur_stav_sk[11] = 31622246675218508;
	cur_stav_sk[12] = 31660573709873528;
	cur_stav_sk[13] = 19211353350616946;
	cur_stav_sk[14] = 64871740645091138;
	cur_stav_sk[15] = 27102788966417430;
	cur_stav_sk[16] = 10240375122137156;
	cur_stav_sk[17] = 15854894879867934;
	cur_stav_sk[18] = 31639636210483277;
	cur_stav_sk[19] = 2345014114170232;
	cur_stav_sk[20] = 9501463626846334;
	cur_stav_sk[21] = 494317118293324;
	cur_stav_sk[22] = 14031903575118102;
	cur_stav_sk[23] = 18569658882131324;
	cur_stav_sk[24] = 32730842160149266;
	cur_stav_sk[25] = 28253288307000702;
	cur_stav_sk[26] = 66947896680845598;
	cur_stav_sk[27] = 9553448299442302;
	cur_stav_sk[28] = 12455986612417918;
	cur_stav_sk[29] = 65276729958479004;
	cur_stav_sk[30] = 27532731651273494;
	cur_stav_sk[31] = 27560225587544186;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28668147255279902;
	cur_stav_sk[1] = 17399254197761278;
	cur_stav_sk[2] = 33187758298498074;
	cur_stav_sk[3] = 11356622748430488;
	cur_stav_sk[4] = 7275282163872072;
	cur_stav_sk[5] = 9131463933366298;
	cur_stav_sk[6] = 28253881889722490;
	cur_stav_sk[7] = 28482640970588312;
	cur_stav_sk[8] = 2331613339649136;
	cur_stav_sk[9] = 15729275743572082;
	cur_stav_sk[10] = 27526960485741072;
	cur_stav_sk[11] = 29794092641104926;
	cur_stav_sk[12] = 3476480765965724;
	cur_stav_sk[13] = 30471596838946420;
	cur_stav_sk[14] = 27312987221885310;
	cur_stav_sk[15] = 33154498153022481;
	cur_stav_sk[16] = 27127776960450684;
	cur_stav_sk[17] = 19443531173401622;
	cur_stav_sk[18] = 14737746489184624;
	cur_stav_sk[19] = 32083343101407856;
	cur_stav_sk[20] = 9536425890189434;
	cur_stav_sk[21] = 30902124361421082;
	cur_stav_sk[22] = 17381637622023280;
	cur_stav_sk[23] = 15147797439222896;
	cur_stav_sk[24] = 10240443975770520;
	cur_stav_sk[25] = 55846743046062448;
	cur_stav_sk[26] = 30901231134016796;
	cur_stav_sk[27] = 5053305447134740;
	cur_stav_sk[28] = 17197216557314672;
	cur_stav_sk[29] = 10213986364855668;
	cur_stav_sk[30] = 21895502251209852;
	cur_stav_sk[31] = 9078151600608638;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 502232626761746;
	cur_stav_sk[1] = 33145813643703057;
	cur_stav_sk[2] = 13643364345721214;
	cur_stav_sk[3] = 68671199722681974;
	cur_stav_sk[4] = 32994012005111160;
	cur_stav_sk[5] = 17381633864644046;
	cur_stav_sk[6] = 14021938869895292;
	cur_stav_sk[7] = 10670447037199124;
	cur_stav_sk[8] = 6324970708144248;
	cur_stav_sk[9] = 14717930262466886;
	cur_stav_sk[10] = 22598842528969210;
	cur_stav_sk[11] = 17409398148141424;
	cur_stav_sk[12] = 10267066311839856;
	cur_stav_sk[13] = 28447668713764626;
	cur_stav_sk[14] = 31631524887365441;
	cur_stav_sk[15] = 27120379332170100;
	cur_stav_sk[16] = 29354285930418458;
	cur_stav_sk[17] = 32767127992022138;
	cur_stav_sk[18] = 23715396368926068;
	cur_stav_sk[19] = 68673041052996980;
	cur_stav_sk[20] = 10627360781383446;
	cur_stav_sk[21] = 66499295936657264;
	cur_stav_sk[22] = 12887132272460152;
	cur_stav_sk[23] = 24182346840781686;
	cur_stav_sk[24] = 11779429066249488;
	cur_stav_sk[25] = 9123480395134528;
	cur_stav_sk[26] = 24853208968790300;
	cur_stav_sk[27] = 9688583684068630;
	cur_stav_sk[28] = 14735206213169728;
	cur_stav_sk[29] = 27146397912760650;
	cur_stav_sk[30] = 31658245351118206;
	cur_stav_sk[31] = 14717887713813960;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 13581872697554200;
	cur_stav_sk[1] = 14743935203545364;
	cur_stav_sk[2] = 1645904629403664;
	cur_stav_sk[3] = 35186300618305566;
	cur_stav_sk[4] = 13793656449356818;
	cur_stav_sk[5] = 33187735275865157;
	cur_stav_sk[6] = 9078124949045368;
	cur_stav_sk[7] = 54946190082281801;
	cur_stav_sk[8] = 60887248481105022;
	cur_stav_sk[9] = 14769332748895812;
	cur_stav_sk[10] = 9299402145772304;
	cur_stav_sk[11] = 59665084464935698;
	cur_stav_sk[12] = 60342675268141086;
	cur_stav_sk[13] = 17390803223871514;
	cur_stav_sk[14] = 11245741129773182;
	cur_stav_sk[15] = 31615157105070110;
	cur_stav_sk[16] = 13622705879283318;
	cur_stav_sk[17] = 32766576115398046;
	cur_stav_sk[18] = 27101620668211708;
	cur_stav_sk[19] = 9114764833456402;
	cur_stav_sk[20] = 27111556950918674;
	cur_stav_sk[21] = 14954216080451066;
	cur_stav_sk[22] = 10678530570334834;
	cur_stav_sk[23] = 64150268262450556;
	cur_stav_sk[24] = 33180545434482196;
	cur_stav_sk[25] = 28693617275384436;
	cur_stav_sk[26] = 62434767282647920;
	cur_stav_sk[27] = 12474062049542428;
	cur_stav_sk[28] = 15157074410406933;
	cur_stav_sk[29] = 63939368885225026;
	cur_stav_sk[30] = 31619488509890844;
	cur_stav_sk[31] = 27523941704542017;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9351502709489940;
	cur_stav_sk[1] = 23065287638168178;
	cur_stav_sk[2] = 10475214798665488;
	cur_stav_sk[3] = 69894610949682458;
	cur_stav_sk[4] = 1821759449662750;
	cur_stav_sk[5] = 71011760595178768;
	cur_stav_sk[6] = 65295216907092240;
	cur_stav_sk[7] = 14004347809871684;
	cur_stav_sk[8] = 27154406295994750;
	cur_stav_sk[9] = 8386634690888990;
	cur_stav_sk[10] = 12888343230973048;
	cur_stav_sk[11] = 12684934205633564;
	cur_stav_sk[12] = 10678867771962622;
	cur_stav_sk[13] = 10231305740981626;
	cur_stav_sk[14] = 18560406985643028;
	cur_stav_sk[15] = 10221890237989916;
	cur_stav_sk[16] = 13837935340040476;
	cur_stav_sk[17] = 22590021203072026;
	cur_stav_sk[18] = 64845654673207754;
	cur_stav_sk[19] = 14033726205657466;
	cur_stav_sk[20] = 31641573435675674;
	cur_stav_sk[21] = 14724871276003442;
	cur_stav_sk[22] = 32739641880556310;
	cur_stav_sk[23] = 14602619851276670;
	cur_stav_sk[24] = 33180613080880157;
	cur_stav_sk[25] = 14030680264515789;
	cur_stav_sk[26] = 5174101014786628;
	cur_stav_sk[27] = 27131122326275097;
	cur_stav_sk[28] = 5014398565744658;
	cur_stav_sk[29] = 9113322596958486;
	cur_stav_sk[30] = 72299886121078;
	cur_stav_sk[31] = 28280062442538561;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 2771647639949380;
	cur_stav_sk[1] = 31616942684803446;
	cur_stav_sk[2] = 33205388151043450;
	cur_stav_sk[3] = 10248485140337940;
	cur_stav_sk[4] = 9694206857515085;
	cur_stav_sk[5] = 9562002733577748;
	cur_stav_sk[6] = 69797210350297876;
	cur_stav_sk[7] = 28694603705942296;
	cur_stav_sk[8] = 8383509540279618;
	cur_stav_sk[9] = 7252158849315016;
	cur_stav_sk[10] = 32080277568521232;
	cur_stav_sk[11] = 64855250655196022;
	cur_stav_sk[12] = 1637796741920626;
	cur_stav_sk[13] = 6148748804732158;
	cur_stav_sk[14] = 13829127430014016;
	cur_stav_sk[15] = 29371808173854996;
	cur_stav_sk[16] = 65295326949635700;
	cur_stav_sk[17] = 23045840513742078;
	cur_stav_sk[18] = 9547284616806676;
	cur_stav_sk[19] = 32037027571630158;
	cur_stav_sk[20] = 27145311278555716;
	cur_stav_sk[21] = 9510601849874292;
	cur_stav_sk[22] = 9547214286717458;
	cur_stav_sk[23] = 27550813111292182;
	cur_stav_sk[24] = 68231851181933644;
	cur_stav_sk[25] = 31657764446942456;
	cur_stav_sk[26] = 32767473195844889;
	cur_stav_sk[27] = 27551292530864916;
	cur_stav_sk[28] = 28702783323676824;
	cur_stav_sk[29] = 72039017742362;
	cur_stav_sk[30] = 19695353903678536;
	cur_stav_sk[31] = 9122746849821462;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32787373943652734;
	cur_stav_sk[1] = 28244837222061438;
	cur_stav_sk[2] = 32080113235694712;
	cur_stav_sk[3] = 9556803671295254;
	cur_stav_sk[4] = 28235694830687346;
	cur_stav_sk[5] = 32977270071427094;
	cur_stav_sk[6] = 13829400698914672;
	cur_stav_sk[7] = 10673920506560536;
	cur_stav_sk[8] = 66499224141083154;
	cur_stav_sk[9] = 24154997209895034;
	cur_stav_sk[10] = 56067014724629758;
	cur_stav_sk[11] = 9134302797595666;
	cur_stav_sk[12] = 32986181744895764;
	cur_stav_sk[13] = 11779694556644982;
	cur_stav_sk[14] = 14946241465483382;
	cur_stav_sk[15] = 9545759783751710;
	cur_stav_sk[16] = 9684265416696340;
	cur_stav_sk[17] = 27523348818768758;
	cur_stav_sk[18] = 33180270623613558;
	cur_stav_sk[19] = 12887200857784700;
	cur_stav_sk[20] = 28659627493066048;
	cur_stav_sk[21] = 9086453373834352;
	cur_stav_sk[22] = 28245146978439198;
	cur_stav_sk[23] = 30919328524612464;
	cur_stav_sk[24] = 6176881378140412;
	cur_stav_sk[25] = 30685451975434650;
	cur_stav_sk[26] = 66941735017616754;
	cur_stav_sk[27] = 23753511603045494;
	cur_stav_sk[28] = 6140608247179126;
	cur_stav_sk[29] = 68250360214364410;
	cur_stav_sk[30] = 9140797438406808;
	cur_stav_sk[31] = 28486702588596594;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28467523741974808;
	cur_stav_sk[1] = 24189947301098618;
	cur_stav_sk[2] = 19237956004712572;
	cur_stav_sk[3] = 66929848916119666;
	cur_stav_sk[4] = 28272324524782622;
	cur_stav_sk[5] = 28469112559063117;
	cur_stav_sk[6] = 9130961980691574;
	cur_stav_sk[7] = 5031451125228698;
	cur_stav_sk[8] = 10416530174050426;
	cur_stav_sk[9] = 11347851210523932;
	cur_stav_sk[10] = 15174159185674268;
	cur_stav_sk[11] = 16976467121569904;
	cur_stav_sk[12] = 33847852891766908;
	cur_stav_sk[13] = 1673283311149466;
	cur_stav_sk[14] = 14611097635882104;
	cur_stav_sk[15] = 28694740929446770;
	cur_stav_sk[16] = 15155742047187225;
	cur_stav_sk[17] = 15843707628331512;
	cur_stav_sk[18] = 57914537333891908;
	cur_stav_sk[19] = 16256146483521089;
	cur_stav_sk[20] = 27532257032283292;
	cur_stav_sk[21] = 519756008657016;
	cur_stav_sk[22] = 9512260565578824;
	cur_stav_sk[23] = 69885033151607160;
	cur_stav_sk[24] = 23776773684409104;
	cur_stav_sk[25] = 14179413090309490;
	cur_stav_sk[26] = 28701958111047706;
	cur_stav_sk[27] = 21472643383694456;
	cur_stav_sk[28] = 14610935495036954;
	cur_stav_sk[29] = 9702776259359504;
	cur_stav_sk[30] = 27560113796629618;
	cur_stav_sk[31] = 32779222146060414;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 26390029420925001;
	cur_stav_sk[1] = 27330290091231092;
	cur_stav_sk[2] = 30911193180766532;
	cur_stav_sk[3] = 31596466694694416;
	cur_stav_sk[4] = 33189109806039416;
	cur_stav_sk[5] = 7955959851000592;
	cur_stav_sk[6] = 59647367871534450;
	cur_stav_sk[7] = 33170452797395228;
	cur_stav_sk[8] = 12878130349638002;
	cur_stav_sk[9] = 3467343963337214;
	cur_stav_sk[10] = 1259796259148868;
	cur_stav_sk[11] = 31859481151563378;
	cur_stav_sk[12] = 56071802023351617;
	cur_stav_sk[13] = 79817225475088;
	cur_stav_sk[14] = 128247883117722;
	cur_stav_sk[15] = 33162992988525842;
	cur_stav_sk[16] = 28650875559773566;
	cur_stav_sk[17] = 35413925277639540;
	cur_stav_sk[18] = 65391891769669492;
	cur_stav_sk[19] = 17390527355728376;
	cur_stav_sk[20] = 19677718163891454;
	cur_stav_sk[21] = 3454558782296084;
	cur_stav_sk[22] = 22857504587026800;
	cur_stav_sk[23] = 23716265772495002;
	cur_stav_sk[24] = 9114683431073350;
	cur_stav_sk[25] = 15850647878341918;
	cur_stav_sk[26] = 11755653744145984;
	cur_stav_sk[27] = 59452673887776924;
	cur_stav_sk[28] = 23715400207515672;
	cur_stav_sk[29] = 24146062488765204;
	cur_stav_sk[30] = 3457857300366364;
	cur_stav_sk[31] = 18526297408255354;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18552407598081528;
	cur_stav_sk[1] = 71003512569202452;
	cur_stav_sk[2] = 14183292745888024;
	cur_stav_sk[3] = 5718495778023024;
	cur_stav_sk[4] = 23715190277607454;
	cur_stav_sk[5] = 31604395049641116;
	cur_stav_sk[6] = 6750861809975408;
	cur_stav_sk[7] = 7964480600703352;
	cur_stav_sk[8] = 33180817090294597;
	cur_stav_sk[9] = 22636512537777522;
	cur_stav_sk[10] = 9096053014147582;
	cur_stav_sk[11] = 14022423240030064;
	cur_stav_sk[12] = 17390156356200000;
	cur_stav_sk[13] = 16276731585804146;
	cur_stav_sk[14] = 65295284753634576;
	cur_stav_sk[15] = 2341429481662068;
	cur_stav_sk[16] = 1645280174552694;
	cur_stav_sk[17] = 12879504955736690;
	cur_stav_sk[18] = 65399451065188978;
	cur_stav_sk[19] = 32937689276035096;
	cur_stav_sk[20] = 31653570080420044;
	cur_stav_sk[21] = 27568677822247750;
	cur_stav_sk[22] = 18561754915573878;
	cur_stav_sk[23] = 12691730902388810;
	cur_stav_sk[24] = 7055082954466810;
	cur_stav_sk[25] = 9519041597576268;
	cur_stav_sk[26] = 32072555810357324;
	cur_stav_sk[27] = 282320171974681;
	cur_stav_sk[28] = 14621105396266864;
	cur_stav_sk[29] = 11752479536788762;
	cur_stav_sk[30] = 28255050704856318;
	cur_stav_sk[31] = 15138726867140886;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14726546908845328;
	cur_stav_sk[1] = 10626305356042268;
	cur_stav_sk[2] = 5383226663449104;
	cur_stav_sk[3] = 23759489035883546;
	cur_stav_sk[4] = 27568016478507284;
	cur_stav_sk[5] = 5616268373529886;
	cur_stav_sk[6] = 10257969497646328;
	cur_stav_sk[7] = 28482607329522450;
	cur_stav_sk[8] = 33196692369019262;
	cur_stav_sk[9] = 32045408130106738;
	cur_stav_sk[10] = 9079568933983260;
	cur_stav_sk[11] = 32036820692505668;
	cur_stav_sk[12] = 59427235555606896;
	cur_stav_sk[13] = 2745141250831608;
	cur_stav_sk[14] = 28257480527053936;
	cur_stav_sk[15] = 15165399671145590;
	cur_stav_sk[16] = 13639722062447994;
	cur_stav_sk[17] = 23776972933251194;
	cur_stav_sk[18] = 15192326448358258;
	cur_stav_sk[19] = 32071192510693402;
	cur_stav_sk[20] = 9341880840356988;
	cur_stav_sk[21] = 14964247107155100;
	cur_stav_sk[22] = 31811628430427258;
	cur_stav_sk[23] = 19686582988312856;
	cur_stav_sk[24] = 28641761519601018;
	cur_stav_sk[25] = 33153262192310134;
	cur_stav_sk[26] = 7969912123565464;
	cur_stav_sk[27] = 32722427584577814;
	cur_stav_sk[28] = 10221985801765144;
	cur_stav_sk[29] = 32774835000344698;
	cur_stav_sk[30] = 32036831834870038;
	cur_stav_sk[31] = 33205901846676600;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14772075644097562;
	cur_stav_sk[1] = 11545978183251100;
	cur_stav_sk[2] = 19450092944638582;
	cur_stav_sk[3] = 65056897824474646;
	cur_stav_sk[4] = 13608480019522326;
	cur_stav_sk[5] = 14066059792508432;
	cur_stav_sk[6] = 18143347550201340;
	cur_stav_sk[7] = 6326138936530296;
	cur_stav_sk[8] = 28659420424320410;
	cur_stav_sk[9] = 13590877634920474;
	cur_stav_sk[10] = 10249403685233168;
	cur_stav_sk[11] = 28254017714784022;
	cur_stav_sk[12] = 11339131364114546;
	cur_stav_sk[13] = 14743633363992848;
	cur_stav_sk[14] = 27102514553721206;
	cur_stav_sk[15] = 11234017527956764;
	cur_stav_sk[16] = 9088365028578422;
	cur_stav_sk[17] = 71213999267942680;
	cur_stav_sk[18] = 15314554913273114;
	cur_stav_sk[19] = 32766894077150590;
	cur_stav_sk[20] = 33182921641001232;
	cur_stav_sk[21] = 19670131698382404;
	cur_stav_sk[22] = 65798835697091702;
	cur_stav_sk[23] = 10661791054833522;
	cur_stav_sk[24] = 32053602344505664;
	cur_stav_sk[25] = 4618985471350041;
	cur_stav_sk[26] = 13590612265574556;
	cur_stav_sk[27] = 9139902559823004;
	cur_stav_sk[28] = 14726315590390041;
	cur_stav_sk[29] = 56805772266087930;
	cur_stav_sk[30] = 33172092079617306;
	cur_stav_sk[31] = 32734322649675280;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15517166565590092;
	cur_stav_sk[1] = 13591988884976502;
	cur_stav_sk[2] = 9130652809532692;
	cur_stav_sk[3] = 29370227634251002;
	cur_stav_sk[4] = 8163216927704080;
	cur_stav_sk[5] = 28219934136861712;
	cur_stav_sk[6] = 9123271289057688;
	cur_stav_sk[7] = 31648649268577810;
	cur_stav_sk[8] = 35408258006843676;
	cur_stav_sk[9] = 27155194891605114;
	cur_stav_sk[10] = 10476976326676850;
	cur_stav_sk[11] = 16273423458042386;
	cur_stav_sk[12] = 28244740258178674;
	cur_stav_sk[13] = 12878379449528572;
	cur_stav_sk[14] = 27145503294457162;
	cur_stav_sk[15] = 27570739466908956;
	cur_stav_sk[16] = 33179762895565636;
	cur_stav_sk[17] = 18561687957157648;
	cur_stav_sk[18] = 1682145437618204;
	cur_stav_sk[19] = 32760335102706032;
	cur_stav_sk[20] = 32766003796981573;
	cur_stav_sk[21] = 31650080344114556;
	cur_stav_sk[22] = 6742140967096694;
	cur_stav_sk[23] = 10126260035593372;
	cur_stav_sk[24] = 13625798783579418;
	cur_stav_sk[25] = 9562210640507766;
	cur_stav_sk[26] = 27146686767638384;
	cur_stav_sk[27] = 28272299910497613;
	cur_stav_sk[28] = 13610996203856126;
	cur_stav_sk[29] = 10462469555291262;
	cur_stav_sk[30] = 33180612630525718;
	cur_stav_sk[31] = 20358506299129982;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33170994128331280;
	cur_stav_sk[1] = 31869103685669144;
	cur_stav_sk[2] = 11241990467270014;
	cur_stav_sk[3] = 14742325005496732;
	cur_stav_sk[4] = 9526794409247504;
	cur_stav_sk[5] = 1209056401507448;
	cur_stav_sk[6] = 27115638762190234;
	cur_stav_sk[7] = 1241997709218844;
	cur_stav_sk[8] = 18092606862037118;
	cur_stav_sk[9] = 16986291251876940;
	cur_stav_sk[10] = 31619360524961558;
	cur_stav_sk[11] = 18551803071636032;
	cur_stav_sk[12] = 31816313633637502;
	cur_stav_sk[13] = 14944353810555766;
	cur_stav_sk[14] = 33856994937321332;
	cur_stav_sk[15] = 10116500192965114;
	cur_stav_sk[16] = 1663352854460226;
	cur_stav_sk[17] = 16259345574991128;
	cur_stav_sk[18] = 28676670408164382;
	cur_stav_sk[19] = 28228688801628444;
	cur_stav_sk[20] = 32062363199243288;
	cur_stav_sk[21] = 28271294199793016;
	cur_stav_sk[22] = 6174887988278805;
	cur_stav_sk[23] = 13846994435179793;
	cur_stav_sk[24] = 69888848709231768;
	cur_stav_sk[25] = 18086652886790676;
	cur_stav_sk[26] = 32752044612093308;
	cur_stav_sk[27] = 30479078675190033;
	cur_stav_sk[28] = 11356863203710028;
	cur_stav_sk[29] = 7059696269460854;
	cur_stav_sk[30] = 10626785251668756;
	cur_stav_sk[31] = 15851251662127216;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9536690339643410;
	cur_stav_sk[1] = 11342422438839060;
	cur_stav_sk[2] = 16265848758908788;
	cur_stav_sk[3] = 35425851814457968;
	cur_stav_sk[4] = 10688154650936594;
	cur_stav_sk[5] = 10098197948872464;
	cur_stav_sk[6] = 70319925654691998;
	cur_stav_sk[7] = 69350396763485814;
	cur_stav_sk[8] = 6748601667424378;
	cur_stav_sk[9] = 15183782214575517;
	cur_stav_sk[10] = 7248755440817264;
	cur_stav_sk[11] = 33206178030563956;
	cur_stav_sk[12] = 15156569350472721;
	cur_stav_sk[13] = 63736498114101876;
	cur_stav_sk[14] = 13837387142991996;
	cur_stav_sk[15] = 4628756335341773;
	cur_stav_sk[16] = 22822033840603420;
	cur_stav_sk[17] = 17411458199957534;
	cur_stav_sk[18] = 32978345353446421;
	cur_stav_sk[19] = 124868081030218;
	cur_stav_sk[20] = 70114489311593806;
	cur_stav_sk[21] = 14197546660275452;
	cur_stav_sk[22] = 27321543518266874;
	cur_stav_sk[23] = 12905524258623566;
	cur_stav_sk[24] = 27558786181207062;
	cur_stav_sk[25] = 71424924440105076;
	cur_stav_sk[26] = 75632244527121;
	cur_stav_sk[27] = 64159738213892124;
	cur_stav_sk[28] = 106685400060182;
	cur_stav_sk[29] = 14003675530856000;
	cur_stav_sk[30] = 69886959532478844;
	cur_stav_sk[31] = 13591893452668952;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32030465070531660;
	cur_stav_sk[1] = 16267761026380926;
	cur_stav_sk[2] = 13857185915369030;
	cur_stav_sk[3] = 15851484659196156;
	cur_stav_sk[4] = 10213466126066550;
	cur_stav_sk[5] = 20786217412900720;
	cur_stav_sk[6] = 6167811086725532;
	cur_stav_sk[7] = 63720688942579988;
	cur_stav_sk[8] = 1655321286554778;
	cur_stav_sk[9] = 13845892637373822;
	cur_stav_sk[10] = 66940153931137044;
	cur_stav_sk[11] = 12482239577600785;
	cur_stav_sk[12] = 27540588209247518;
	cur_stav_sk[13] = 19264619487395960;
	cur_stav_sk[14] = 27523936606782842;
	cur_stav_sk[15] = 14619342830732670;
	cur_stav_sk[16] = 27553596306097222;
	cur_stav_sk[17] = 9077873013228926;
	cur_stav_sk[18] = 64171490380972304;
	cur_stav_sk[19] = 13617197033468186;
	cur_stav_sk[20] = 28677217548402972;
	cur_stav_sk[21] = 28272573093286170;
	cur_stav_sk[22] = 27524918229868744;
	cur_stav_sk[23] = 16044678936826950;
	cur_stav_sk[24] = 13582548727116406;
	cur_stav_sk[25] = 19458886342226452;
	cur_stav_sk[26] = 27157281850560882;
	cur_stav_sk[27] = 10239484098426238;
	cur_stav_sk[28] = 23750169023971606;
	cur_stav_sk[29] = 9545690531702388;
	cur_stav_sk[30] = 9527230746857744;
	cur_stav_sk[31] = 14031161032185153;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14730025761838200;
	cur_stav_sk[1] = 28649662028055837;
	cur_stav_sk[2] = 14058101263414520;
	cur_stav_sk[3] = 14962984838234236;
	cur_stav_sk[4] = 19457024568078748;
	cur_stav_sk[5] = 2772714452890486;
	cur_stav_sk[6] = 28245935654380144;
	cur_stav_sk[7] = 35210379350807836;
	cur_stav_sk[8] = 60887330086434370;
	cur_stav_sk[9] = 13637914891911284;
	cur_stav_sk[10] = 8400918481253788;
	cur_stav_sk[11] = 62443882610075510;
	cur_stav_sk[12] = 18104371986080153;
	cur_stav_sk[13] = 56815648136528150;
	cur_stav_sk[14] = 35395484770010388;
	cur_stav_sk[15] = 9078468148793668;
	cur_stav_sk[16] = 64169247393593200;
	cur_stav_sk[17] = 12887202935584204;
	cur_stav_sk[18] = 9088250729674104;
	cur_stav_sk[19] = 22610205181130104;
	cur_stav_sk[20] = 15745631499228282;
	cur_stav_sk[21] = 10212750429324306;
	cur_stav_sk[22] = 32955395262096798;
	cur_stav_sk[23] = 14047434334212212;
	cur_stav_sk[24] = 5965817106010396;
	cur_stav_sk[25] = 15166282894980428;
	cur_stav_sk[26] = 32036225699615860;
	cur_stav_sk[27] = 33853696358252912;
	cur_stav_sk[28] = 16273807721301362;
	cur_stav_sk[29] = 32072210017358200;
	cur_stav_sk[30] = 11234566284870929;
	cur_stav_sk[31] = 97174995207192;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 5041896696938868;
	cur_stav_sk[1] = 69797716481757260;
	cur_stav_sk[2] = 537211914950008;
	cur_stav_sk[3] = 12890017211229592;
	cur_stav_sk[4] = 66508185650757752;
	cur_stav_sk[5] = 14737715015156500;
	cur_stav_sk[6] = 15166490743869462;
	cur_stav_sk[7] = 70105566902976588;
	cur_stav_sk[8] = 16048338575376462;
	cur_stav_sk[9] = 9104991975277948;
	cur_stav_sk[10] = 14040221868113993;
	cur_stav_sk[11] = 9140878501717242;
	cur_stav_sk[12] = 2236205341344368;
	cur_stav_sk[13] = 7952328445329736;
	cur_stav_sk[14] = 9562058630956352;
	cur_stav_sk[15] = 64150830454211866;
	cur_stav_sk[16] = 27576443586707732;
	cur_stav_sk[17] = 5050749542400112;
	cur_stav_sk[18] = 9312181129720342;
	cur_stav_sk[19] = 70112624704239376;
	cur_stav_sk[20] = 12895079971029320;
	cur_stav_sk[21] = 15833866183349366;
	cur_stav_sk[22] = 27542290675736729;
	cur_stav_sk[23] = 14192358274872848;
	cur_stav_sk[24] = 6513299855463540;
	cur_stav_sk[25] = 10820049138759536;
	cur_stav_sk[26] = 66507266875929930;
	cur_stav_sk[27] = 9553319068374396;
	cur_stav_sk[28] = 30479293400155469;
	cur_stav_sk[29] = 13626281430398832;
	cur_stav_sk[30] = 19695239536968720;
	cur_stav_sk[31] = 33205602895110674;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31622303009539446;
	cur_stav_sk[1] = 65267111175356440;
	cur_stav_sk[2] = 14770572382966136;
	cur_stav_sk[3] = 33197930598696256;
	cur_stav_sk[4] = 9132480301241368;
	cur_stav_sk[5] = 16066654464209049;
	cur_stav_sk[6] = 16274509422593098;
	cur_stav_sk[7] = 32081075695878168;
	cur_stav_sk[8] = 11570577967785420;
	cur_stav_sk[9] = 27146767275864222;
	cur_stav_sk[10] = 5721477479769622;
	cur_stav_sk[11] = 32721496313364760;
	cur_stav_sk[12] = 66940267760386884;
	cur_stav_sk[13] = 546007603908976;
	cur_stav_sk[14] = 16264889912001857;
	cur_stav_sk[15] = 65392181835600146;
	cur_stav_sk[16] = 9563378735223070;
	cur_stav_sk[17] = 65382421058883706;
	cur_stav_sk[18] = 32730888666027382;
	cur_stav_sk[19] = 17381155532080404;
	cur_stav_sk[20] = 502290536890441;
	cur_stav_sk[21] = 13583110227772446;
	cur_stav_sk[22] = 3467110225192560;
	cur_stav_sk[23] = 23029085288304966;
	cur_stav_sk[24] = 68235641307463698;
	cur_stav_sk[25] = 12482007118164496;
	cur_stav_sk[26] = 10802563928306546;
	cur_stav_sk[27] = 34992815683735624;
	cur_stav_sk[28] = 27128533018838800;
	cur_stav_sk[29] = 69350077256466753;
	cur_stav_sk[30] = 32063759453193232;
	cur_stav_sk[31] = 31641281777968281;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 34271369958830960;
	cur_stav_sk[1] = 14048258273579382;
	cur_stav_sk[2] = 24180836670546072;
	cur_stav_sk[3] = 10688702059548786;
	cur_stav_sk[4] = 18517775146025332;
	cur_stav_sk[5] = 14707650246120476;
	cur_stav_sk[6] = 9342008062857334;
	cur_stav_sk[7] = 10248441051391762;
	cur_stav_sk[8] = 19264136441762844;
	cur_stav_sk[9] = 70299036279996992;
	cur_stav_sk[10] = 9562829205906292;
	cur_stav_sk[11] = 7266224065259120;
	cur_stav_sk[12] = 27303765518652692;
	cur_stav_sk[13] = 15834375188808978;
	cur_stav_sk[14] = 27571202877141068;
	cur_stav_sk[15] = 9290080502301808;
	cur_stav_sk[16] = 35413971584816508;
	cur_stav_sk[17] = 12887518628132350;
	cur_stav_sk[18] = 13646106010519062;
	cur_stav_sk[19] = 9518270367217430;
	cur_stav_sk[20] = 9545236264846460;
	cur_stav_sk[21] = 23064268586759290;
	cur_stav_sk[22] = 17189384369968242;
	cur_stav_sk[23] = 1646344802370164;
	cur_stav_sk[24] = 10807931415400722;
	cur_stav_sk[25] = 15157624652107846;
	cur_stav_sk[26] = 11753057214103934;
	cur_stav_sk[27] = 29792647923045648;
	cur_stav_sk[28] = 33189201412424258;
	cur_stav_sk[29] = 91964043624824;
	cur_stav_sk[30] = 1622370353484096;
	cur_stav_sk[31] = 10108847123173662;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14203223331255550;
	cur_stav_sk[1] = 28237115607692698;
	cur_stav_sk[2] = 28218928020664824;
	cur_stav_sk[3] = 12887105275368769;
	cur_stav_sk[4] = 11753302564017432;
	cur_stav_sk[5] = 70325697957921146;
	cur_stav_sk[6] = 9104786758828662;
	cur_stav_sk[7] = 19634602444587285;
	cur_stav_sk[8] = 4635821473928222;
	cur_stav_sk[9] = 64871972024004764;
	cur_stav_sk[10] = 17191556545480062;
	cur_stav_sk[11] = 23751587596087450;
	cur_stav_sk[12] = 69779481310500210;
	cur_stav_sk[13] = 28263046803887380;
	cur_stav_sk[14] = 28283181220598138;
	cur_stav_sk[15] = 5763095582904690;
	cur_stav_sk[16] = 6175755640214814;
	cur_stav_sk[17] = 32074820604602228;
	cur_stav_sk[18] = 32071194716668948;
	cur_stav_sk[19] = 11039653566415472;
	cur_stav_sk[20] = 10415431605486710;
	cur_stav_sk[21] = 10459660641861918;
	cur_stav_sk[22] = 18569258087064013;
	cur_stav_sk[23] = 27137683501285405;
	cur_stav_sk[24] = 29371122110787742;
	cur_stav_sk[25] = 17193894630765690;
	cur_stav_sk[26] = 13854403313352824;
	cur_stav_sk[27] = 28684433770812534;
	cur_stav_sk[28] = 17180725497803260;
	cur_stav_sk[29] = 3465760114774036;
	cur_stav_sk[30] = 5700862257484826;
	cur_stav_sk[31] = 28258306913247813;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14005128702734750;
	cur_stav_sk[1] = 27576712309477500;
	cur_stav_sk[2] = 27113561815613774;
	cur_stav_sk[3] = 5058392911361486;
	cur_stav_sk[4] = 23019658036544832;
	cur_stav_sk[5] = 16273339043872796;
	cur_stav_sk[6] = 69568067270390550;
	cur_stav_sk[7] = 15129931228808566;
	cur_stav_sk[8] = 15192808631382396;
	cur_stav_sk[9] = 34992610055455810;
	cur_stav_sk[10] = 32027200752891209;
	cur_stav_sk[11] = 32054631635162133;
	cur_stav_sk[12] = 27347622374371486;
	cur_stav_sk[13] = 59224880907650582;
	cur_stav_sk[14] = 69886146687336470;
	cur_stav_sk[15] = 69780467876307100;
	cur_stav_sk[16] = 4583263892799822;
	cur_stav_sk[17] = 31621961408648570;
	cur_stav_sk[18] = 11752755024606488;
	cur_stav_sk[19] = 15175286829973782;
	cur_stav_sk[20] = 69799024955622732;
	cur_stav_sk[21] = 27140193760710928;
	cur_stav_sk[22] = 31640379086701840;
	cur_stav_sk[23] = 32783662621963465;
	cur_stav_sk[24] = 9078138776953365;
	cur_stav_sk[25] = 27128254955849032;
	cur_stav_sk[26] = 15846055367620468;
	cur_stav_sk[27] = 32758604851196318;
	cur_stav_sk[28] = 27128834128478322;
	cur_stav_sk[29] = 27138026820169976;
	cur_stav_sk[30] = 32721840801321074;
	cur_stav_sk[31] = 15834899728438654;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 32019308683223618;
	cur_stav_sk[1] = 31851485131186806;
	cur_stav_sk[2] = 29784344420057420;
	cur_stav_sk[3] = 18568641490793544;
	cur_stav_sk[4] = 27365250514555201;
	cur_stav_sk[5] = 9538500672591126;
	cur_stav_sk[6] = 14760261397647638;
	cur_stav_sk[7] = 9132011678794872;
	cur_stav_sk[8] = 60767470102754714;
	cur_stav_sk[9] = 27148802974352668;
	cur_stav_sk[10] = 12455850779104382;
	cur_stav_sk[11] = 15315472409265181;
	cur_stav_sk[12] = 4635134825693310;
	cur_stav_sk[13] = 28262057352593736;
	cur_stav_sk[14] = 27570595546015350;
	cur_stav_sk[15] = 57491913160111984;
	cur_stav_sk[16] = 10679011601645596;
	cur_stav_sk[17] = 69349023401575188;
	cur_stav_sk[18] = 7257412559799408;
	cur_stav_sk[19] = 66516956463039604;
	cur_stav_sk[20] = 8164731219492880;
	cur_stav_sk[21] = 11334333877224780;
	cur_stav_sk[22] = 5622565001015576;
	cur_stav_sk[23] = 60344664781198716;
	cur_stav_sk[24] = 15736653902512154;
	cur_stav_sk[25] = 21902657331168538;
	cur_stav_sk[26] = 6158258952873494;
	cur_stav_sk[27] = 537184990139676;
	cur_stav_sk[28] = 63718929149100308;
	cur_stav_sk[29] = 33209241743328532;
	cur_stav_sk[30] = 32774378456121677;
	cur_stav_sk[31] = 6854100022919490;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 31649724075611256;
	cur_stav_sk[1] = 15853999962326292;
	cur_stav_sk[2] = 32732287284281618;
	cur_stav_sk[3] = 27550837220119674;
	cur_stav_sk[4] = 32783800203154800;
	cur_stav_sk[5] = 14049509340722380;
	cur_stav_sk[6] = 4627645817914950;
	cur_stav_sk[7] = 13626873748095250;
	cur_stav_sk[8] = 4627624928542996;
	cur_stav_sk[9] = 30488915756815640;
	cur_stav_sk[10] = 28701724619281786;
	cur_stav_sk[11] = 23010597886796565;
	cur_stav_sk[12] = 27544020675007762;
	cur_stav_sk[13] = 33144270895603864;
	cur_stav_sk[14] = 31843510976447604;
	cur_stav_sk[15] = 33153675666654584;
	cur_stav_sk[16] = 30488433635661124;
	cur_stav_sk[17] = 27118784494195736;
	cur_stav_sk[18] = 22835763982071318;
	cur_stav_sk[19] = 9122117774770802;
	cur_stav_sk[20] = 30918927328772980;
	cur_stav_sk[21] = 32079943425099132;
	cur_stav_sk[22] = 9535876922914674;
	cur_stav_sk[23] = 24146243324695674;
	cur_stav_sk[24] = 1425833663477148;
	cur_stav_sk[25] = 28642035452182554;
	cur_stav_sk[26] = 9324151134442522;
	cur_stav_sk[27] = 10671659678011762;
	cur_stav_sk[28] = 11762263023630196;
	cur_stav_sk[29] = 33179763285434996;
	cur_stav_sk[30] = 20356100932052169;
	cur_stav_sk[31] = 11562772119753852;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 63727311716044820;
	cur_stav_sk[1] = 23029233485742110;
	cur_stav_sk[2] = 32775579493611072;
	cur_stav_sk[3] = 9114420229440790;
	cur_stav_sk[4] = 33154498272559218;
	cur_stav_sk[5] = 32774478893024273;
	cur_stav_sk[6] = 18336013266781458;
	cur_stav_sk[7] = 15168205358396542;
	cur_stav_sk[8] = 23010928343421210;
	cur_stav_sk[9] = 24164796182402320;
	cur_stav_sk[10] = 32062385808508274;
	cur_stav_sk[11] = 15833558624896284;
	cur_stav_sk[12] = 14056875190256500;
	cur_stav_sk[13] = 23751175809971728;
	cur_stav_sk[14] = 14930712387683704;
	cur_stav_sk[15] = 28245246836344090;
	cur_stav_sk[16] = 28661903976760080;
	cur_stav_sk[17] = 63728660856570140;
	cur_stav_sk[18] = 32725456208135192;
	cur_stav_sk[19] = 15316090687324184;
	cur_stav_sk[20] = 1654591198073886;
	cur_stav_sk[21] = 16063609760129274;
	cur_stav_sk[22] = 14764860308751638;
	cur_stav_sk[23] = 25271743616591636;
	cur_stav_sk[24] = 17391532298778132;
	cur_stav_sk[25] = 14734080730399049;
	cur_stav_sk[26] = 14039132474709374;
	cur_stav_sk[27] = 69349083040265374;
	cur_stav_sk[28] = 34070634133737586;
	cur_stav_sk[29] = 16062511514387576;
	cur_stav_sk[30] = 35197083112059162;
	cur_stav_sk[31] = 14031150834287646;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 29559852062024188;
	cur_stav_sk[1] = 34297509681825050;
	cur_stav_sk[2] = 28218792011989016;
	cur_stav_sk[3] = 14602714883196180;
	cur_stav_sk[4] = 9547650571703314;
	cur_stav_sk[5] = 24867191929452305;
	cur_stav_sk[6] = 59242003730666878;
	cur_stav_sk[7] = 69379119372515136;
	cur_stav_sk[8] = 13626678192219594;
	cur_stav_sk[9] = 68663707263468310;
	cur_stav_sk[10] = 66940017772963064;
	cur_stav_sk[11] = 7982417044275580;
	cur_stav_sk[12] = 9114268916449352;
	cur_stav_sk[13] = 70109025979691262;
	cur_stav_sk[14] = 65294048870303000;
	cur_stav_sk[15] = 5736502286327965;
	cur_stav_sk[16] = 18148362857387388;
	cur_stav_sk[17] = 14746243021435932;
	cur_stav_sk[18] = 33198206944576274;
	cur_stav_sk[19] = 10115581294744596;
	cur_stav_sk[20] = 1258215509885810;
	cur_stav_sk[21] = 28640705427513852;
	cur_stav_sk[22] = 23714408538964348;
	cur_stav_sk[23] = 8383294124229652;
	cur_stav_sk[24] = 6157950710023186;
	cur_stav_sk[25] = 3470066281817620;
	cur_stav_sk[26] = 27338839085756830;
	cur_stav_sk[27] = 32777676511449412;
	cur_stav_sk[28] = 13582034819908892;
	cur_stav_sk[29] = 6135210463068438;
	cur_stav_sk[30] = 34299924661015920;
	cur_stav_sk[31] = 28659603282740288;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18551853158666364;
	cur_stav_sk[1] = 63747010584281206;
	cur_stav_sk[2] = 18561504536105072;
	cur_stav_sk[3] = 32070765949957746;
	cur_stav_sk[4] = 30488157365286721;
	cur_stav_sk[5] = 14031585904064118;
	cur_stav_sk[6] = 19687681261282838;
	cur_stav_sk[7] = 32723531993583894;
	cur_stav_sk[8] = 32071604825853978;
	cur_stav_sk[9] = 1216022963392886;
	cur_stav_sk[10] = 15191476180858736;
	cur_stav_sk[11] = 69897945436586262;
	cur_stav_sk[12] = 16265381195248240;
	cur_stav_sk[13] = 29343934384148598;
	cur_stav_sk[14] = 32758031460934042;
	cur_stav_sk[15] = 13637903613441810;
	cur_stav_sk[16] = 13634167591875345;
	cur_stav_sk[17] = 65813474711503226;
	cur_stav_sk[18] = 10802800758105972;
	cur_stav_sk[19] = 31608434134945912;
	cur_stav_sk[20] = 12464723627226994;
	cur_stav_sk[21] = 33154226496287864;
	cur_stav_sk[22] = 32780322685059442;
	cur_stav_sk[23] = 10257651087836188;
	cur_stav_sk[24] = 66499455406683512;
	cur_stav_sk[25] = 66526602285921662;
	cur_stav_sk[26] = 33874541354557852;
	cur_stav_sk[27] = 14752083309663256;
	cur_stav_sk[28] = 63951485972775698;
	cur_stav_sk[29] = 10212569496302616;
	cur_stav_sk[30] = 9510575866348052;
	cur_stav_sk[31] = 10226149628482888;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 56270768128230726;
	cur_stav_sk[1] = 4592047704903958;
	cur_stav_sk[2] = 9695456712341366;
	cur_stav_sk[3] = 27569022556210556;
	cur_stav_sk[4] = 1675447504045182;
	cur_stav_sk[5] = 63724140129383544;
	cur_stav_sk[6] = 12483037598327574;
	cur_stav_sk[7] = 32053599770883702;
	cur_stav_sk[8] = 10204086053867792;
	cur_stav_sk[9] = 28684651179769880;
	cur_stav_sk[10] = 9323926791631380;
	cur_stav_sk[11] = 28272255215511104;
	cur_stav_sk[12] = 27551085791293724;
	cur_stav_sk[13] = 32749851567231309;
	cur_stav_sk[14] = 13634802127181168;
	cur_stav_sk[15] = 18560267400820242;
	cur_stav_sk[16] = 11357205114983704;
	cur_stav_sk[17] = 68223948659527696;
	cur_stav_sk[18] = 19466198060466966;
	cur_stav_sk[19] = 16960617162650696;
	cur_stav_sk[20] = 14069187062235668;
	cur_stav_sk[21] = 31811775345173364;
	cur_stav_sk[22] = 28649981553522288;
	cur_stav_sk[23] = 15833550151688724;
	cur_stav_sk[24] = 13646149965877374;
	cur_stav_sk[25] = 10238789253247228;
	cur_stav_sk[26] = 12473417714667800;
	cur_stav_sk[27] = 1663592852591894;
	cur_stav_sk[28] = 65796525540049016;
	cur_stav_sk[29] = 28281326169661640;
	cur_stav_sk[30] = 32072556215177492;
	cur_stav_sk[31] = 11770027384922364;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 65795400216707100;
	cur_stav_sk[1] = 9288715718463610;
	cur_stav_sk[2] = 13625990526140692;
	cur_stav_sk[3] = 55864980217958520;
	cur_stav_sk[4] = 70299849913610494;
	cur_stav_sk[5] = 14200609353598236;
	cur_stav_sk[6] = 28702576237347186;
	cur_stav_sk[7] = 71445859167670642;
	cur_stav_sk[8] = 19643126882571376;
	cur_stav_sk[9] = 1619167780114810;
	cur_stav_sk[10] = 25273311210968142;
	cur_stav_sk[11] = 4843630204552314;
	cur_stav_sk[12] = 12886653320201240;
	cur_stav_sk[13] = 32722528137118482;
	cur_stav_sk[14] = 3458682939969864;
	cur_stav_sk[15] = 9563022852694898;
	cur_stav_sk[16] = 60790517768880150;
	cur_stav_sk[17] = 1417026224796278;
	cur_stav_sk[18] = 33197378557129848;
	cur_stav_sk[19] = 59431494067557654;
	cur_stav_sk[20] = 9562182599411016;
	cur_stav_sk[21] = 25290242766153332;
	cur_stav_sk[22] = 4574591704141894;
	cur_stav_sk[23] = 13603715336811120;
	cur_stav_sk[24] = 71020513522386296;
	cur_stav_sk[25] = 27576650036032116;
	cur_stav_sk[26] = 31622854296274453;
	cur_stav_sk[27] = 31632475010564220;
	cur_stav_sk[28] = 19479777149105012;
	cur_stav_sk[29] = 27155480974589977;
	cur_stav_sk[30] = 13626083014644086;
	cur_stav_sk[31] = 15183807915078164;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 69797759973856630;
	cur_stav_sk[1] = 9079496441235264;
	cur_stav_sk[2] = 27571146434450716;
	cur_stav_sk[3] = 15754838451552528;
	cur_stav_sk[4] = 9535809351861276;
	cur_stav_sk[5] = 16046016818382356;
	cur_stav_sk[6] = 28658732668237686;
	cur_stav_sk[7] = 9104559790035218;
	cur_stav_sk[8] = 27552886520874310;
	cur_stav_sk[9] = 70320233663213646;
	cur_stav_sk[10] = 31631594537977112;
	cur_stav_sk[11] = 71224606459166;
	cur_stav_sk[12] = 14022708813440413;
	cur_stav_sk[13] = 27567868757677082;
	cur_stav_sk[14] = 15174458907919126;
	cur_stav_sk[15] = 12485295988605552;
	cur_stav_sk[16] = 5042034998251550;
	cur_stav_sk[17] = 8374941422826304;
	cur_stav_sk[18] = 9131257237865793;
	cur_stav_sk[19] = 2771717844209788;
	cur_stav_sk[20] = 10832249525933378;
	cur_stav_sk[21] = 11760383439364720;
	cur_stav_sk[22] = 29344458904306974;
	cur_stav_sk[23] = 33197071948449392;
	cur_stav_sk[24] = 30480185213915518;
	cur_stav_sk[25] = 69885300653109370;
	cur_stav_sk[26] = 65610638753236250;
	cur_stav_sk[27] = 27576718531077910;
	cur_stav_sk[28] = 19669788634583056;
	cur_stav_sk[29] = 10670311554678802;
	cur_stav_sk[30] = 19229090444936208;
	cur_stav_sk[31] = 13581749266375930;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 3683093388895040;
	cur_stav_sk[1] = 14004512901923100;
	cur_stav_sk[2] = 27128144306705436;
	cur_stav_sk[3] = 3466518580737438;
	cur_stav_sk[4] = 10820117858124054;
	cur_stav_sk[5] = 28254777036998008;
	cur_stav_sk[6] = 32018826590105628;
	cur_stav_sk[7] = 28271294208083266;
	cur_stav_sk[8] = 27576321992394260;
	cur_stav_sk[9] = 11233546643710106;
	cur_stav_sk[10] = 18089882791515252;
	cur_stav_sk[11] = 28236288626729844;
	cur_stav_sk[12] = 5595145372598558;
	cur_stav_sk[13] = 10687835015493236;
	cur_stav_sk[14] = 17187848701760540;
	cur_stav_sk[15] = 14189767781753084;
	cur_stav_sk[16] = 5757599292175940;
	cur_stav_sk[17] = 15861421813282420;
	cur_stav_sk[18] = 27356707828037232;
	cur_stav_sk[19] = 69886629477613590;
	cur_stav_sk[20] = 69349364850570866;
	cur_stav_sk[21] = 13617210669138202;
	cur_stav_sk[22] = 31614126390087954;
	cur_stav_sk[23] = 10652926170329616;
	cur_stav_sk[24] = 32761602600472700;
	cur_stav_sk[25] = 34270203627734340;
	cur_stav_sk[26] = 20346425701237884;
	cur_stav_sk[27] = 28271543511360762;
	cur_stav_sk[28] = 1223283548586514;
	cur_stav_sk[29] = 11013013406777470;
	cur_stav_sk[30] = 19676970308548370;
	cur_stav_sk[31] = 35405132892442740;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 24136622091833726;
	cur_stav_sk[1] = 66508982392271734;
	cur_stav_sk[2] = 528047444660500;
	cur_stav_sk[3] = 68223123693994006;
	cur_stav_sk[4] = 14004015952016589;
	cur_stav_sk[5] = 32044404938015762;
	cur_stav_sk[6] = 2546249492615188;
	cur_stav_sk[7] = 31661109689618754;
	cur_stav_sk[8] = 496641098645886;
	cur_stav_sk[9] = 14769745670681928;
	cur_stav_sk[10] = 9130902387364218;
	cur_stav_sk[11] = 64160659949815062;
	cur_stav_sk[12] = 18542444356277524;
	cur_stav_sk[13] = 30920198636570484;
	cur_stav_sk[14] = 28219590450681112;
	cur_stav_sk[15] = 5621329719857278;
	cur_stav_sk[16] = 32998435621441812;
	cur_stav_sk[17] = 9096108651578224;
	cur_stav_sk[18] = 33188319552349044;
	cur_stav_sk[19] = 2237923518821238;
	cur_stav_sk[20] = 65294158939914310;
	cur_stav_sk[21] = 27531994499781908;
	cur_stav_sk[22] = 28668491368137033;
	cur_stav_sk[23] = 11778480427340926;
	cur_stav_sk[24] = 18314721019400208;
	cur_stav_sk[25] = 68438557093860378;
	cur_stav_sk[26] = 13643379162481222;
	cur_stav_sk[27] = 31632131425735964;
	cur_stav_sk[28] = 16959654400036936;
	cur_stav_sk[29] = 105640099620346;
	cur_stav_sk[30] = 24202216094962974;
	cur_stav_sk[31] = 1205407286531344;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9528100942741581;
	cur_stav_sk[1] = 68224167645618589;
	cur_stav_sk[2] = 65804358557533210;
	cur_stav_sk[3] = 27137149100819578;
	cur_stav_sk[4] = 28667917252269078;
	cur_stav_sk[5] = 27540689885968192;
	cur_stav_sk[6] = 56392034761408785;
	cur_stav_sk[7] = 68240741658595610;
	cur_stav_sk[8] = 32063481957975166;
	cur_stav_sk[9] = 6183790456025974;
	cur_stav_sk[10] = 14742943954711410;
	cur_stav_sk[11] = 66948443214659608;
	cur_stav_sk[12] = 63958323180602132;
	cur_stav_sk[13] = 27111490361243154;
	cur_stav_sk[14] = 5761474434056318;
	cur_stav_sk[15] = 32951846081900956;
	cur_stav_sk[16] = 4575855009203320;
	cur_stav_sk[17] = 32758673367348730;
	cur_stav_sk[18] = 34278498325435464;
	cur_stav_sk[19] = 31631032974525464;
	cur_stav_sk[20] = 14179905462305142;
	cur_stav_sk[21] = 10687970891575576;
	cur_stav_sk[22] = 10213094609674868;
	cur_stav_sk[23] = 15852240793216886;
	cur_stav_sk[24] = 32739615168484476;
	cur_stav_sk[25] = 64863176084435230;
	cur_stav_sk[26] = 32722297186239556;
	cur_stav_sk[27] = 20786919156158738;
	cur_stav_sk[28] = 23072707747034524;
	cur_stav_sk[29] = 17398815558246904;
	cur_stav_sk[30] = 10661790327051585;
	cur_stav_sk[31] = 13581805983085332;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14746062162333724;
	cur_stav_sk[1] = 62206827262115914;
	cur_stav_sk[2] = 68655503223619704;
	cur_stav_sk[3] = 71426025612812912;
	cur_stav_sk[4] = 13599865852311038;
	cur_stav_sk[5] = 32775100664145048;
	cur_stav_sk[6] = 11348355393726070;
	cur_stav_sk[7] = 12878380047603064;
	cur_stav_sk[8] = 15165181841441150;
	cur_stav_sk[9] = 28236110584812614;
	cur_stav_sk[10] = 54712597458420340;
	cur_stav_sk[11] = 6530991720267804;
	cur_stav_sk[12] = 28492226980454910;
	cur_stav_sk[13] = 28686151550075260;
	cur_stav_sk[14] = 5763467619304000;
	cur_stav_sk[15] = 4829920576898328;
	cur_stav_sk[16] = 69780193535235454;
	cur_stav_sk[17] = 28684994587439384;
	cur_stav_sk[18] = 32062588078862362;
	cur_stav_sk[19] = 6134935670534654;
	cur_stav_sk[20] = 5182899255476293;
	cur_stav_sk[21] = 27357254294765588;
	cur_stav_sk[22] = 14190233717735488;
	cur_stav_sk[23] = 9336283007722654;
	cur_stav_sk[24] = 31825931162976538;
	cur_stav_sk[25] = 9509474346075516;
	cur_stav_sk[26] = 18554950892495004;
	cur_stav_sk[27] = 34063574829969989;
	cur_stav_sk[28] = 9702739376148752;
	cur_stav_sk[29] = 14039119139583226;
	cur_stav_sk[30] = 14192082312573556;
	cur_stav_sk[31] = 31596395601793304;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 12666717223321620;
	cur_stav_sk[1] = 34297966606791536;
	cur_stav_sk[2] = 10224870332086085;
	cur_stav_sk[3] = 31648900520051022;
	cur_stav_sk[4] = 29784276106973513;
	cur_stav_sk[5] = 14012277741561462;
	cur_stav_sk[6] = 28245703373828932;
	cur_stav_sk[7] = 31605240617210228;
	cur_stav_sk[8] = 8392327979606218;
	cur_stav_sk[9] = 14039463657714546;
	cur_stav_sk[10] = 65293842716234098;
	cur_stav_sk[11] = 3898349163787544;
	cur_stav_sk[12] = 11243061590298748;
	cur_stav_sk[13] = 18103478832727570;
	cur_stav_sk[14] = 9112784588744304;
	cur_stav_sk[15] = 10205423398491164;
	cur_stav_sk[16] = 32774961001640816;
	cur_stav_sk[17] = 28227598276212293;
	cur_stav_sk[18] = 27542318129480824;
	cur_stav_sk[19] = 31640185965884796;
	cur_stav_sk[20] = 10670243346908536;
	cur_stav_sk[21] = 9553183099528442;
	cur_stav_sk[22] = 35404900961419280;
	cur_stav_sk[23] = 65805585166901500;
	cur_stav_sk[24] = 65060882401665272;
	cur_stav_sk[25] = 5005924055811102;
	cur_stav_sk[26] = 25996239289843998;
	cur_stav_sk[27] = 27567303975285522;
	cur_stav_sk[28] = 9140068048934256;
	cur_stav_sk[29] = 14707097752961308;
	cur_stav_sk[30] = 5736186217567508;
	cur_stav_sk[31] = 33147292883181818;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 33144739501379654;
	cur_stav_sk[1] = 13608109957320777;
	cur_stav_sk[2] = 18085143076180344;
	cur_stav_sk[3] = 14066345738437744;
	cur_stav_sk[4] = 32986040944251926;
	cur_stav_sk[5] = 31606045340487698;
	cur_stav_sk[6] = 31830606285145408;
	cur_stav_sk[7] = 14207690083776324;
	cur_stav_sk[8] = 11347859211228440;
	cur_stav_sk[9] = 98688702064120;
	cur_stav_sk[10] = 33180889047839998;
	cur_stav_sk[11] = 32775922492683120;
	cur_stav_sk[12] = 18094817485849618;
	cur_stav_sk[13] = 69886708276954228;
	cur_stav_sk[14] = 4826050271683442;
	cur_stav_sk[15] = 33206149899392332;
	cur_stav_sk[16] = 32055042837938456;
	cur_stav_sk[17] = 10239689065637492;
	cur_stav_sk[18] = 9324702565860420;
	cur_stav_sk[19] = 6835155520205328;
	cur_stav_sk[20] = 9131930148418714;
	cur_stav_sk[21] = 14004279887476548;
	cur_stav_sk[22] = 10108449413399580;
	cur_stav_sk[23] = 5735608866714226;
	cur_stav_sk[24] = 10626882095514896;
	cur_stav_sk[25] = 27127708695429398;
	cur_stav_sk[26] = 30922225408259604;
	cur_stav_sk[27] = 6851647595021682;
	cur_stav_sk[28] = 33171720418113348;
	cur_stav_sk[29] = 63745911798105164;
	cur_stav_sk[30] = 9905245451730974;
	cur_stav_sk[31] = 10260202347837202;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14769300164162320;
	cur_stav_sk[1] = 16962858669974032;
	cur_stav_sk[2] = 63719754388440344;
	cur_stav_sk[3] = 71433901990700608;
	cur_stav_sk[4] = 13625934754424216;
	cur_stav_sk[5] = 6185679614611785;
	cur_stav_sk[6] = 80077123851536;
	cur_stav_sk[7] = 27514459226158870;
	cur_stav_sk[8] = 9104526720504850;
	cur_stav_sk[9] = 12694854959142000;
	cur_stav_sk[10] = 14955040763543570;
	cur_stav_sk[11] = 27155265364956532;
	cur_stav_sk[12] = 10823555491410204;
	cur_stav_sk[13] = 15129654805673846;
	cur_stav_sk[14] = 33197047323775004;
	cur_stav_sk[15] = 65373143191356702;
	cur_stav_sk[16] = 31595873230524444;
	cur_stav_sk[17] = 20759828915782942;
	cur_stav_sk[18] = 17400146805368058;
	cur_stav_sk[19] = 32739062591960216;
	cur_stav_sk[20] = 13635875450847352;
	cur_stav_sk[21] = 11250990239777138;
	cur_stav_sk[22] = 28279512619615606;
	cur_stav_sk[23] = 10679932252161352;
	cur_stav_sk[24] = 70308098775748884;
	cur_stav_sk[25] = 15834990441334036;
	cur_stav_sk[26] = 71441900817698844;
	cur_stav_sk[27] = 27562405099601938;
	cur_stav_sk[28] = 9693865364750450;
	cur_stav_sk[29] = 10432473104024862;
	cur_stav_sk[30] = 65286076872566642;
	cur_stav_sk[31] = 12473717356578322;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 65593445331238942;
	cur_stav_sk[1] = 19237841502941718;
	cur_stav_sk[2] = 13845367729902708;
	cur_stav_sk[3] = 18560654262543772;
	cur_stav_sk[4] = 33865196057793566;
	cur_stav_sk[5] = 24181385293767881;
	cur_stav_sk[6] = 7042928315966070;
	cur_stav_sk[7] = 14752428360929552;
	cur_stav_sk[8] = 56375018121896818;
	cur_stav_sk[9] = 22651665321013781;
	cur_stav_sk[10] = 32037505390281840;
	cur_stav_sk[11] = 13828075101750650;
	cur_stav_sk[12] = 10626397651612178;
	cur_stav_sk[13] = 70097058653099126;
	cur_stav_sk[14] = 64855276408246554;
	cur_stav_sk[15] = 12896727708303984;
	cur_stav_sk[16] = 14038941435377144;
	cur_stav_sk[17] = 6140254970750738;
	cur_stav_sk[18] = 71003468630167928;
	cur_stav_sk[19] = 28281120093376784;
	cur_stav_sk[20] = 1233135242182686;
	cur_stav_sk[21] = 28640704565875062;
	cur_stav_sk[22] = 14205698292678768;
	cur_stav_sk[23] = 69587024169701653;
	cur_stav_sk[24] = 32044577053835638;
	cur_stav_sk[25] = 28274634822844944;
	cur_stav_sk[26] = 69789537787520410;
	cur_stav_sk[27] = 66499363268330610;
	cur_stav_sk[28] = 5740033274368024;
	cur_stav_sk[29] = 32959817954971670;
	cur_stav_sk[30] = 5941654559064346;
	cur_stav_sk[31] = 69378469286229618;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 23725089573077616;
	cur_stav_sk[1] = 13828438559068440;
	cur_stav_sk[2] = 10213190778913146;
	cur_stav_sk[3] = 23749552701912094;
	cur_stav_sk[4] = 19256026864096286;
	cur_stav_sk[5] = 70089335629971730;
	cur_stav_sk[6] = 6730803792683468;
	cur_stav_sk[7] = 27136569890897944;
	cur_stav_sk[8] = 14760606121068305;
	cur_stav_sk[9] = 13609294377629204;
	cur_stav_sk[10] = 33191643718848628;
	cur_stav_sk[11] = 9099520136655098;
	cur_stav_sk[12] = 33152584749355290;
	cur_stav_sk[13] = 33206176952615030;
	cur_stav_sk[14] = 10222992313258354;
	cur_stav_sk[15] = 28693437007139865;
	cur_stav_sk[16] = 24868565037319290;
	cur_stav_sk[17] = 24173663137739006;
	cur_stav_sk[18] = 14766196885127284;
	cur_stav_sk[19] = 15305828613863024;
	cur_stav_sk[20] = 14764822111846672;
	cur_stav_sk[21] = 62426223294120560;
	cur_stav_sk[22] = 3897596386837784;
	cur_stav_sk[23] = 4583826399891832;
	cur_stav_sk[24] = 31613715279196996;
	cur_stav_sk[25] = 23072732443087228;
	cur_stav_sk[26] = 32760984573936925;
	cur_stav_sk[27] = 65399604066977302;
	cur_stav_sk[28] = 14751664885141618;
	cur_stav_sk[29] = 32985903435392408;
	cur_stav_sk[30] = 29784965831413833;
	cur_stav_sk[31] = 13626047045907056;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 59237182860002590;
	cur_stav_sk[1] = 33198481285654130;
	cur_stav_sk[2] = 10454189379301786;
	cur_stav_sk[3] = 28702268414076432;
	cur_stav_sk[4] = 9095784098047506;
	cur_stav_sk[5] = 18552710072599676;
	cur_stav_sk[6] = 9083623598457118;
	cur_stav_sk[7] = 10206977445369118;
	cur_stav_sk[8] = 32732329646784890;
	cur_stav_sk[9] = 9517393404732664;
	cur_stav_sk[10] = 14726039562485880;
	cur_stav_sk[11] = 12886925981496730;
	cur_stav_sk[12] = 15156594930910580;
	cur_stav_sk[13] = 12886925914312824;
	cur_stav_sk[14] = 14955425158136432;
	cur_stav_sk[15] = 10239550531961116;
	cur_stav_sk[16] = 33877987076552056;
	cur_stav_sk[17] = 66947962248757526;
	cur_stav_sk[18] = 10116339864846360;
	cur_stav_sk[19] = 32760941088145940;
	cur_stav_sk[20] = 31658050083685450;
	cur_stav_sk[21] = 13644462784160534;
	cur_stav_sk[22] = 10257443838100760;
	cur_stav_sk[23] = 33172091559510040;
	cur_stav_sk[24] = 71030200348713464;
	cur_stav_sk[25] = 5727217038164250;
	cur_stav_sk[26] = 10265048075179330;
	cur_stav_sk[27] = 11752481701179973;
	cur_stav_sk[28] = 292226894967930;
	cur_stav_sk[29] = 68223122935353408;
	cur_stav_sk[30] = 31653296135341424;
	cur_stav_sk[31] = 19221120595280924;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28677287274517874;
	cur_stav_sk[1] = 24200074835526676;
	cur_stav_sk[2] = 11764461585344114;
	cur_stav_sk[3] = 57504017915741461;
	cur_stav_sk[4] = 18570291295228952;
	cur_stav_sk[5] = 66939029183956036;
	cur_stav_sk[6] = 22614759997391988;
	cur_stav_sk[7] = 27559344868328572;
	cur_stav_sk[8] = 10221986955305368;
	cur_stav_sk[9] = 13625441988117884;
	cur_stav_sk[10] = 35417055163678744;
	cur_stav_sk[11] = 31824581449188632;
	cur_stav_sk[12] = 10266446292992028;
	cur_stav_sk[13] = 10460211951015194;
	cur_stav_sk[14] = 27154224212939128;
	cur_stav_sk[15] = 35397433090520694;
	cur_stav_sk[16] = 23754706589811016;
	cur_stav_sk[17] = 64167529203343984;
	cur_stav_sk[18] = 9101502259036190;
	cur_stav_sk[19] = 35397502326612754;
	cur_stav_sk[20] = 56787858343465334;
	cur_stav_sk[21] = 19696109755020540;
	cur_stav_sk[22] = 30489418789598716;
	cur_stav_sk[23] = 32019709930964754;
	cur_stav_sk[24] = 28649593333658385;
	cur_stav_sk[25] = 32062959203430778;
	cur_stav_sk[26] = 71023397610455066;
	cur_stav_sk[27] = 14012537314248560;
	cur_stav_sk[28] = 10680387741098492;
	cur_stav_sk[29] = 66940017252762905;
	cur_stav_sk[30] = 15842332014675265;
	cur_stav_sk[31] = 11752285811999606;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 62445190493090322;
	cur_stav_sk[1] = 32054961294848882;
	cur_stav_sk[2] = 27155494456308890;
	cur_stav_sk[3] = 12905318767657082;
	cur_stav_sk[4] = 6184027662693232;
	cur_stav_sk[5] = 15754425373327376;
	cur_stav_sk[6] = 10679355669480566;
	cur_stav_sk[7] = 35423592261289334;
	cur_stav_sk[8] = 11755034036617846;
	cur_stav_sk[9] = 2217267207583248;
	cur_stav_sk[10] = 55841832828900730;
	cur_stav_sk[11] = 10820850871666034;
	cur_stav_sk[12] = 9130719500542742;
	cur_stav_sk[13] = 1258559257052690;
	cur_stav_sk[14] = 1472414132342804;
	cur_stav_sk[15] = 11562746139555320;
	cur_stav_sk[16] = 10249131462365248;
	cur_stav_sk[17] = 13810434123413364;
	cur_stav_sk[18] = 33144811930583414;
	cur_stav_sk[19] = 23076486963467380;
	cur_stav_sk[20] = 5710688047534364;
	cur_stav_sk[21] = 14610935457349758;
	cur_stav_sk[22] = 5761445763261300;
	cur_stav_sk[23] = 12886927667377532;
	cur_stav_sk[24] = 4602036129895704;
	cur_stav_sk[25] = 10669692511756822;
	cur_stav_sk[26] = 9121040602173456;
	cur_stav_sk[27] = 63949252607508504;
	cur_stav_sk[28] = 10643371549209978;
	cur_stav_sk[29] = 19228840988525120;
	cur_stav_sk[30] = 31622453721834006;
	cur_stav_sk[31] = 1444490921771034;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9517928871133970;
	cur_stav_sk[1] = 61309199541207412;
	cur_stav_sk[2] = 1673257741128720;
	cur_stav_sk[3] = 32071242024399994;
	cur_stav_sk[4] = 11348626513499504;
	cur_stav_sk[5] = 12888299959848986;
	cur_stav_sk[6] = 6730484873102718;
	cur_stav_sk[7] = 62436118433269112;
	cur_stav_sk[8] = 15745358386659352;
	cur_stav_sk[9] = 10248349456044304;
	cur_stav_sk[10] = 10238754431992180;
	cur_stav_sk[11] = 23055426817623324;
	cur_stav_sk[12] = 27549953905623416;
	cur_stav_sk[13] = 19686511639695732;
	cur_stav_sk[14] = 66509283034236233;
	cur_stav_sk[15] = 16268034830733680;
	cur_stav_sk[16] = 23028890491421718;
	cur_stav_sk[17] = 9131448972153214;
	cur_stav_sk[18] = 1258876603539984;
	cur_stav_sk[19] = 14611785988899090;
	cur_stav_sk[20] = 14619457733670512;
	cur_stav_sk[21] = 32937826841612404;
	cur_stav_sk[22] = 14734287492691224;
	cur_stav_sk[23] = 10803626777683194;
	cur_stav_sk[24] = 28677288876672788;
	cur_stav_sk[25] = 30480186755854962;
	cur_stav_sk[26] = 28447422221268752;
	cur_stav_sk[27] = 494304237978698;
	cur_stav_sk[28] = 31659300856103192;
	cur_stav_sk[29] = 66947989780213916;
	cur_stav_sk[30] = 29353391354028444;
	cur_stav_sk[31] = 64855619089544002;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 18111505259959412;
	cur_stav_sk[1] = 10805344239596053;
	cur_stav_sk[2] = 34059064292148242;
	cur_stav_sk[3] = 14751697826283900;
	cur_stav_sk[4] = 65400964970320965;
	cur_stav_sk[5] = 32062546888110494;
	cur_stav_sk[6] = 10661996489774458;
	cur_stav_sk[7] = 28670458417393688;
	cur_stav_sk[8] = 9132067107734600;
	cur_stav_sk[9] = 9526242510144624;
	cur_stav_sk[10] = 20345465707234322;
	cur_stav_sk[11] = 35406000061945106;
	cur_stav_sk[12] = 71428635434516592;
	cur_stav_sk[13] = 557575481692432;
	cur_stav_sk[14] = 33198246606049650;
	cur_stav_sk[15] = 32741055393019508;
	cur_stav_sk[16] = 31623912062027026;
	cur_stav_sk[17] = 82517548270404;
	cur_stav_sk[18] = 14050794525304592;
	cur_stav_sk[19] = 14207632313098701;
	cur_stav_sk[20] = 16256791218586130;
	cur_stav_sk[21] = 31637080283451713;
	cur_stav_sk[22] = 23056582745497848;
	cur_stav_sk[23] = 13607628920303772;
	cur_stav_sk[24] = 32062134898459760;
	cur_stav_sk[25] = 29576894483898782;
	cur_stav_sk[26] = 15129896352248985;
	cur_stav_sk[27] = 20777023702503806;
	cur_stav_sk[28] = 65822112417756314;
	cur_stav_sk[29] = 60887823995090710;
	cur_stav_sk[30] = 71452250220081778;
	cur_stav_sk[31] = 9132012432459586;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9702876654240894;
	cur_stav_sk[1] = 13847005858496538;
	cur_stav_sk[2] = 11769245681983688;
	cur_stav_sk[3] = 9122321642356862;
	cur_stav_sk[4] = 64864388269445236;
	cur_stav_sk[5] = 34995598698324472;
	cur_stav_sk[6] = 32775350165898264;
	cur_stav_sk[7] = 23742405350331664;
	cur_stav_sk[8] = 28696771069772820;
	cur_stav_sk[9] = 4637564040774938;
	cur_stav_sk[10] = 9139731762029770;
	cur_stav_sk[11] = 14620143306867736;
	cur_stav_sk[12] = 14058292536214546;
	cur_stav_sk[13] = 68224426684974152;
	cur_stav_sk[14] = 22835213689951606;
	cur_stav_sk[15] = 14029867184361590;
	cur_stav_sk[16] = 32774796670218750;
	cur_stav_sk[17] = 30913006783661086;
	cur_stav_sk[18] = 54941927099216404;
	cur_stav_sk[19] = 14971942998484549;
	cur_stav_sk[20] = 14612036239765912;
	cur_stav_sk[21] = 14029979910473084;
	cur_stav_sk[22] = 5032024651596656;
	cur_stav_sk[23] = 64142570608885825;
	cur_stav_sk[24] = 32046180230205554;
	cur_stav_sk[25] = 32747931253904760;
	cur_stav_sk[26] = 66526507977114998;
	cur_stav_sk[27] = 16977288482931838;
	cur_stav_sk[28] = 13603024719512858;
	cur_stav_sk[29] = 16959834272774004;
	cur_stav_sk[30] = 32760814613927192;
	cur_stav_sk[31] = 65821720824779892;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10671618422407450;
	cur_stav_sk[1] = 10661481697617780;
	cur_stav_sk[2] = 32063906820096626;
	cur_stav_sk[3] = 71426298358413174;
	cur_stav_sk[4] = 28271679693980700;
	cur_stav_sk[5] = 17409216297795604;
	cur_stav_sk[6] = 10467768049741900;
	cur_stav_sk[7] = 16066942251332113;
	cur_stav_sk[8] = 31860569261081664;
	cur_stav_sk[9] = 23073064965054920;
	cur_stav_sk[10] = 23046386509918458;
	cur_stav_sk[11] = 31596120954377494;
	cur_stav_sk[12] = 32758032387822714;
	cur_stav_sk[13] = 28262496523790198;
	cur_stav_sk[14] = 65796720562574608;
	cur_stav_sk[15] = 13626108174374014;
	cur_stav_sk[16] = 12473896215052664;
	cur_stav_sk[17] = 17409151396220946;
	cur_stav_sk[18] = 11542143799591288;
	cur_stav_sk[19] = 12473511668725020;
	cur_stav_sk[20] = 65381986035742580;
	cur_stav_sk[21] = 10635537342117018;
	cur_stav_sk[22] = 56805807632584730;
	cur_stav_sk[23] = 32977545641333884;
	cur_stav_sk[24] = 23943958503457138;
	cur_stav_sk[25] = 15130181492835706;
	cur_stav_sk[26] = 70316523823207536;
	cur_stav_sk[27] = 18534429077472790;
	cur_stav_sk[28] = 32731915767091572;
	cur_stav_sk[29] = 32784787349803030;
	cur_stav_sk[30] = 13638647397921552;
	cur_stav_sk[31] = 32027473558680862;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 9121140795719756;
	cur_stav_sk[1] = 11558622972458866;
	cur_stav_sk[2] = 15182613837029784;
	cur_stav_sk[3] = 9122241438843160;
	cur_stav_sk[4] = 27096313017246204;
	cur_stav_sk[5] = 3888909480559742;
	cur_stav_sk[6] = 32748436915060758;
	cur_stav_sk[7] = 16969595306050636;
	cur_stav_sk[8] = 27339940207882362;
	cur_stav_sk[9] = 32762977522593948;
	cur_stav_sk[10] = 32960642639958289;
	cur_stav_sk[11] = 18560588033639441;
	cur_stav_sk[12] = 27348707318965500;
	cur_stav_sk[13] = 68452175152616513;
	cur_stav_sk[14] = 7250007007105136;
	cur_stav_sk[15] = 63740299834197014;
	cur_stav_sk[16] = 9293353683357720;
	cur_stav_sk[17] = 9706369550627653;
	cur_stav_sk[18] = 27532477086335352;
	cur_stav_sk[19] = 35413687919248656;
	cur_stav_sk[20] = 14039238263539008;
	cur_stav_sk[21] = 2753184700444878;
	cur_stav_sk[22] = 4618091716673564;
	cur_stav_sk[23] = 15746208249643132;
	cur_stav_sk[24] = 13802037955592304;
	cur_stav_sk[25] = 14983762412965914;
	cur_stav_sk[26] = 32779197467395088;
	cur_stav_sk[27] = 14769701649679378;
	cur_stav_sk[28] = 18570303135560306;
	cur_stav_sk[29] = 18534281217184882;
	cur_stav_sk[30] = 19236572461139222;
	cur_stav_sk[31] = 32785200742051996;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 14942092438939413;
	cur_stav_sk[1] = 63719730561753336;
	cur_stav_sk[2] = 66930259397580056;
	cur_stav_sk[3] = 14769223227298836;
	cur_stav_sk[4] = 10450476849824120;
	cur_stav_sk[5] = 70300376423076210;
	cur_stav_sk[6] = 16273397776712052;
	cur_stav_sk[7] = 32030290931351826;
	cur_stav_sk[8] = 18542995453400180;
	cur_stav_sk[9] = 33188556643963922;
	cur_stav_sk[10] = 502288389406736;
	cur_stav_sk[11] = 3467339989100308;
	cur_stav_sk[12] = 14049439674926869;
	cur_stav_sk[13] = 9510300182905880;
	cur_stav_sk[14] = 28271293682291830;
	cur_stav_sk[15] = 10671661285967178;
	cur_stav_sk[16] = 15852297562948880;
	cur_stav_sk[17] = 10230231663084866;
	cur_stav_sk[18] = 61318393458073758;
	cur_stav_sk[19] = 8188653738541178;
	cur_stav_sk[20] = 32021589860357146;
	cur_stav_sk[21] = 32062385736881174;
	cur_stav_sk[22] = 14603540589118738;
	cur_stav_sk[23] = 10257721464881472;
	cur_stav_sk[24] = 27553353724563776;
	cur_stav_sk[25] = 32030738665144393;
	cur_stav_sk[26] = 27360014903457558;
	cur_stav_sk[27] = 12466200039011952;
	cur_stav_sk[28] = 28483706726453364;
	cur_stav_sk[29] = 71442827653390540;
	cur_stav_sk[30] = 33865584668415004;
	cur_stav_sk[31] = 29783794328712220;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 15166463213843216;
	cur_stav_sk[1] = 14707753117777942;
	cur_stav_sk[2] = 14743143732066166;
	cur_stav_sk[3] = 10679289163199250;
	cur_stav_sk[4] = 10636432889693718;
	cur_stav_sk[5] = 11250855414497810;
	cur_stav_sk[6] = 13828301412147984;
	cur_stav_sk[7] = 14743175950176540;
	cur_stav_sk[8] = 14057023181960048;
	cur_stav_sk[9] = 62427254825824281;
	cur_stav_sk[10] = 9703541908505876;
	cur_stav_sk[11] = 9079316266399100;
	cur_stav_sk[12] = 69877294849991186;
	cur_stav_sk[13] = 55856870027838584;
	cur_stav_sk[14] = 27153305102624152;
	cur_stav_sk[15] = 16986043284501708;
	cur_stav_sk[16] = 7248928833340446;
	cur_stav_sk[17] = 19689165709358237;
	cur_stav_sk[18] = 4635823645197588;
	cur_stav_sk[19] = 27576308590773624;
	cur_stav_sk[20] = 17390731677545238;
	cur_stav_sk[21] = 28686083044521084;
	cur_stav_sk[22] = 33179899070322040;
	cur_stav_sk[23] = 71019892560073036;
	cur_stav_sk[24] = 14188723589354774;
	cur_stav_sk[25] = 31632473001337622;
	cur_stav_sk[26] = 16264737370997116;
	cur_stav_sk[27] = 9501253642233340;
	cur_stav_sk[28] = 2753760709117210;
	cur_stav_sk[29] = 14707718784550260;
	cur_stav_sk[30] = 14743212390294680;
	cur_stav_sk[31] = 5747532233580798;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 30707777291812986;
	cur_stav_sk[1] = 2244727701649268;
	cur_stav_sk[2] = 33154154537362888;
	cur_stav_sk[3] = 64168534900847641;
	cur_stav_sk[4] = 27338276978507890;
	cur_stav_sk[5] = 71219524595974472;
	cur_stav_sk[6] = 32045024335118460;
	cur_stav_sk[7] = 32044653370868338;
	cur_stav_sk[8] = 30709117796299160;
	cur_stav_sk[9] = 16959860567457910;
	cur_stav_sk[10] = 32972984245032306;
	cur_stav_sk[11] = 65267796742649618;
	cur_stav_sk[12] = 65294021825466652;
	cur_stav_sk[13] = 33145334243103000;
	cur_stav_sk[14] = 13635679487932230;
	cur_stav_sk[15] = 65385376373802360;
	cur_stav_sk[16] = 10257924938007666;
	cur_stav_sk[17] = 15307202307138324;
	cur_stav_sk[18] = 8393153376763982;
	cur_stav_sk[19] = 31833642084775320;
	cur_stav_sk[20] = 30488981256745488;
	cur_stav_sk[21] = 1110044105834830;
	cur_stav_sk[22] = 32071341209626132;
	cur_stav_sk[23] = 13855512958382104;
	cur_stav_sk[24] = 71012583470956822;
	cur_stav_sk[25] = 13590648788029456;
	cur_stav_sk[26] = 13792978992502082;
	cur_stav_sk[27] = 16282206524118042;
	cur_stav_sk[28] = 27119266604323094;
	cur_stav_sk[29] = 25993312477741338;
	cur_stav_sk[30] = 15165663209260062;
	cur_stav_sk[31] = 7952222911554166;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 28451929459627286;
	cur_stav_sk[1] = 9553469905995036;
	cur_stav_sk[2] = 9130721654714896;
	cur_stav_sk[3] = 14971531235365150;
	cur_stav_sk[4] = 19267333886509170;
	cur_stav_sk[5] = 1646867832045688;
	cur_stav_sk[6] = 21902957437859348;
	cur_stav_sk[7] = 32760359656293502;
	cur_stav_sk[8] = 57915224675332508;
	cur_stav_sk[9] = 9130674817822876;
	cur_stav_sk[10] = 27136607393776656;
	cur_stav_sk[11] = 13792416360608534;
	cur_stav_sk[12] = 27119529597764117;
	cur_stav_sk[13] = 9562542041301620;
	cur_stav_sk[14] = 28275734191812984;
	cur_stav_sk[15] = 16281978376302974;
	cur_stav_sk[16] = 16264662326580346;
	cur_stav_sk[17] = 9527629528728434;
	cur_stav_sk[18] = 28281393478317308;
	cur_stav_sk[19] = 1223616924287350;
	cur_stav_sk[20] = 3448969537926606;
	cur_stav_sk[21] = 27131384321873172;
	cur_stav_sk[22] = 68648714677094258;
	cur_stav_sk[23] = 19239079998554996;
	cur_stav_sk[24] = 33182879413634074;
	cur_stav_sk[25] = 31635085961168758;
	cur_stav_sk[26] = 1205716732117368;
	cur_stav_sk[27] = 28262713598895636;
	cur_stav_sk[28] = 31599281354121470;
	cur_stav_sk[29] = 12476851075421564;
	cur_stav_sk[30] = 6166162356090268;
	cur_stav_sk[31] = 69367741454590106;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11566928900719478;
	cur_stav_sk[1] = 5032426071859732;
	cur_stav_sk[2] = 66948375099805968;
	cur_stav_sk[3] = 55841959534474768;
	cur_stav_sk[4] = 8163056737400220;
	cur_stav_sk[5] = 33170692250076790;
	cur_stav_sk[6] = 11013300645340666;
	cur_stav_sk[7] = 15753808538801268;
	cur_stav_sk[8] = 16978114722926616;
	cur_stav_sk[9] = 29371080726360574;
	cur_stav_sk[10] = 4997860393617270;
	cur_stav_sk[11] = 32045341555295516;
	cur_stav_sk[12] = 1205999467495536;
	cur_stav_sk[13] = 63935807195578394;
	cur_stav_sk[14] = 22857214671078980;
	cur_stav_sk[15] = 27093621149094938;
	cur_stav_sk[16] = 10679907780568316;
	cur_stav_sk[17] = 31604464307451384;
	cur_stav_sk[18] = 10230437310179094;
	cur_stav_sk[19] = 14031712138658582;
	cur_stav_sk[20] = 24156026334680446;
	cur_stav_sk[21] = 65268692921959550;
	cur_stav_sk[22] = 1420161003098444;
	cur_stav_sk[23] = 18569176436090389;
	cur_stav_sk[24] = 7055023893778548;
	cur_stav_sk[25] = 14760495867049210;
	cur_stav_sk[26] = 22633383784157248;
	cur_stav_sk[27] = 884974791950614;
	cur_stav_sk[28] = 12474885528170520;
	cur_stav_sk[29] = 23037618318902900;
	cur_stav_sk[30] = 13851380130398330;
	cur_stav_sk[31] = 6175233669510174;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27570946177368342;
	cur_stav_sk[1] = 10661731410354430;
	cur_stav_sk[2] = 16274772474134545;
	cur_stav_sk[3] = 31807230182651412;
	cur_stav_sk[4] = 14761570154184828;
	cur_stav_sk[5] = 9134680014521618;
	cur_stav_sk[6] = 11752781262533918;
	cur_stav_sk[7] = 9131312028873236;
	cur_stav_sk[8] = 14923702546609609;
	cur_stav_sk[9] = 4610531500490782;
	cur_stav_sk[10] = 72310421984528;
	cur_stav_sk[11] = 14760770732326984;
	cur_stav_sk[12] = 27133616369041688;
	cur_stav_sk[13] = 66948992979436922;
	cur_stav_sk[14] = 1240873184505880;
	cur_stav_sk[15] = 66499389893288720;
	cur_stav_sk[16] = 1812919184826900;
	cur_stav_sk[17] = 33187769771594876;
	cur_stav_sk[18] = 1234862696338716;
	cur_stav_sk[19] = 32766278330464020;
	cur_stav_sk[20] = 33205875151570454;
	cur_stav_sk[21] = 32756957641647518;
	cur_stav_sk[22] = 7980079927767550;
	cur_stav_sk[23] = 15755619995201662;
	cur_stav_sk[24] = 32943185355125520;
	cur_stav_sk[25] = 11347530651537482;
	cur_stav_sk[26] = 18314294739244316;
	cur_stav_sk[27] = 31649241417847872;
	cur_stav_sk[28] = 27157873857299472;
	cur_stav_sk[29] = 32985905570396020;
	cur_stav_sk[30] = 62434951274329338;
	cur_stav_sk[31] = 33180613704257906;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27576582987847026;
	cur_stav_sk[1] = 10652694309996402;
	cur_stav_sk[2] = 23742587409895758;
	cur_stav_sk[3] = 9509133377094780;
	cur_stav_sk[4] = 32723191027602548;
	cur_stav_sk[5] = 5916231044497530;
	cur_stav_sk[6] = 10626090676979014;
	cur_stav_sk[7] = 31617073408050300;
	cur_stav_sk[8] = 32766895543661982;
	cur_stav_sk[9] = 9078468018902046;
	cur_stav_sk[10] = 33161922540740766;
	cur_stav_sk[11] = 10652829955896600;
	cur_stav_sk[12] = 32063494853834256;
	cur_stav_sk[13] = 32028574003139614;
	cur_stav_sk[14] = 19264230987171190;
	cur_stav_sk[15] = 27550024238240120;
	cur_stav_sk[16] = 5595282066944280;
	cur_stav_sk[17] = 28641939760715126;
	cur_stav_sk[18] = 5710663167344916;
	cur_stav_sk[19] = 22627611823640854;
	cur_stav_sk[20] = 66948540601275416;
	cur_stav_sk[21] = 56600392221675632;
	cur_stav_sk[22] = 32751839003030648;
	cur_stav_sk[23] = 10246664539833624;
	cur_stav_sk[24] = 28667048755236982;
	cur_stav_sk[25] = 70105430696956274;
	cur_stav_sk[26] = 12907510130578804;
	cur_stav_sk[27] = 6166686747374873;
	cur_stav_sk[28] = 18569178029961502;
	cur_stav_sk[29] = 11779418339194110;
	cur_stav_sk[30] = 18552686366330992;
	cur_stav_sk[31] = 13833522809050385;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 11779717369566328;
	cur_stav_sk[1] = 14712321894195354;
	cur_stav_sk[2] = 10653794356835858;
	cur_stav_sk[3] = 14766196566491510;
	cur_stav_sk[4] = 32721773274542612;
	cur_stav_sk[5] = 3471026765443580;
	cur_stav_sk[6] = 60881469792194680;
	cur_stav_sk[7] = 10265107687875092;
	cur_stav_sk[8] = 32950996679479414;
	cur_stav_sk[9] = 19695104807441776;
	cur_stav_sk[10] = 65813774078410876;
	cur_stav_sk[11] = 65399061435678746;
	cur_stav_sk[12] = 2322737794528541;
	cur_stav_sk[13] = 32072097796690302;
	cur_stav_sk[14] = 10680114333824280;
	cur_stav_sk[15] = 10688084708164978;
	cur_stav_sk[16] = 28649432993038364;
	cur_stav_sk[17] = 14761774213800720;
	cur_stav_sk[18] = 16274442036774002;
	cur_stav_sk[19] = 6298376876168222;
	cur_stav_sk[20] = 16045067492130940;
	cur_stav_sk[21] = 10451716480635202;
	cur_stav_sk[22] = 56392679614625554;
	cur_stav_sk[23] = 32039273855685958;
	cur_stav_sk[24] = 9544660951508500;
	cur_stav_sk[25] = 9685464944443760;
	cur_stav_sk[26] = 10126302581229078;
	cur_stav_sk[27] = 18141253277221918;
	cur_stav_sk[28] = 12895723616339570;
	cur_stav_sk[29] = 11338812988830580;
	cur_stav_sk[30] = 14206522389435768;
	cur_stav_sk[31] = 14746237129209462;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 17409124346396702;
	cur_stav_sk[1] = 18296156100891712;
	cur_stav_sk[2] = 11570854045267824;
	cur_stav_sk[3] = 10688082634674546;
	cur_stav_sk[4] = 32054894653322102;
	cur_stav_sk[5] = 17390911013463924;
	cur_stav_sk[6] = 10231606453023298;
	cur_stav_sk[7] = 33858175796905036;
	cur_stav_sk[8] = 2552533979365656;
	cur_stav_sk[9] = 1620482101359100;
	cur_stav_sk[10] = 11243361092928374;
	cur_stav_sk[11] = 16978183497060720;
	cur_stav_sk[12] = 1671909651284344;
	cur_stav_sk[13] = 28693473395712884;
	cur_stav_sk[14] = 10828890861506672;
	cur_stav_sk[15] = 10222303015494904;
	cur_stav_sk[16] = 28240714586207642;
	cur_stav_sk[17] = 1821853794874618;
	cur_stav_sk[18] = 9139376961688956;
	cur_stav_sk[19] = 69799160110715924;
	cur_stav_sk[20] = 27307475084382588;
	cur_stav_sk[21] = 15306128709419030;
	cur_stav_sk[22] = 71424926239429961;
	cur_stav_sk[23] = 13637559951626264;
	cur_stav_sk[24] = 65796500258292766;
	cur_stav_sk[25] = 23943958086713720;
	cur_stav_sk[26] = 14047984085771074;
	cur_stav_sk[27] = 4574860673164442;
	cur_stav_sk[28] = 2552521304118558;
	cur_stav_sk[29] = 15859937171694198;
	cur_stav_sk[30] = 69903417019414081;
	cur_stav_sk[31] = 14048561148502806;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10239576979158520;
	cur_stav_sk[1] = 19255365443457042;
	cur_stav_sk[2] = 11357138208247836;
	cur_stav_sk[3] = 65277830872445716;
	cur_stav_sk[4] = 11331118101827700;
	cur_stav_sk[5] = 2323826092545406;
	cur_stav_sk[6] = 13600785103619094;
	cur_stav_sk[7] = 13795889894478920;
	cur_stav_sk[8] = 13828028472997396;
	cur_stav_sk[9] = 24859369717825816;
	cur_stav_sk[10] = 6131460025460496;
	cur_stav_sk[11] = 63729362542736144;
	cur_stav_sk[12] = 32036214766744336;
	cur_stav_sk[13] = 538009615599428;
	cur_stav_sk[14] = 6844216903077906;
	cur_stav_sk[15] = 28240574139863316;
	cur_stav_sk[16] = 27128199928153204;
	cur_stav_sk[17] = 15156293874397304;
	cur_stav_sk[18] = 13581208982622485;
	cur_stav_sk[19] = 9122198084031600;
	cur_stav_sk[20] = 19264437090337392;
	cur_stav_sk[21] = 10450750716118640;
	cur_stav_sk[22] = 35413857040368712;
	cur_stav_sk[23] = 109830921650250;
	cur_stav_sk[24] = 9545486106920056;
	cur_stav_sk[25] = 56055057616900429;
	cur_stav_sk[26] = 9702163708089724;
	cur_stav_sk[27] = 11252203910406392;
	cur_stav_sk[28] = 12896821112636442;
	cur_stav_sk[29] = 65268208535180310;
	cur_stav_sk[30] = 10805587631572092;
	cur_stav_sk[31] = 71007427377967384;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 34271304006209656;
	cur_stav_sk[1] = 15731674421396553;
	cur_stav_sk[2] = 15856062554912158;
	cur_stav_sk[3] = 17391052274808138;
	cur_stav_sk[4] = 64168331288449300;
	cur_stav_sk[5] = 14725035218801944;
	cur_stav_sk[6] = 2771614404313361;
	cur_stav_sk[7] = 14031492351362836;
	cur_stav_sk[8] = 9132070263521353;
	cur_stav_sk[9] = 9501505042059644;
	cur_stav_sk[10] = 11233546221827405;
	cur_stav_sk[11] = 23028932345790844;
	cur_stav_sk[12] = 60781582605160564;
	cur_stav_sk[13] = 28465974870816248;
	cur_stav_sk[14] = 33189407775564400;
	cur_stav_sk[15] = 13624928513201534;
	cur_stav_sk[16] = 57940790912820848;
	cur_stav_sk[17] = 9535175375392890;
	cur_stav_sk[18] = 32757551827026038;
	cur_stav_sk[19] = 55865987930366486;
	cur_stav_sk[20] = 1673284053468992;
	cur_stav_sk[21] = 23022841483105304;
	cur_stav_sk[22] = 27576653045335154;
	cur_stav_sk[23] = 28684161418428700;
	cur_stav_sk[24] = 18120809617068400;
	cur_stav_sk[25] = 28254731736392562;
	cur_stav_sk[26] = 14621198264939280;
	cur_stav_sk[27] = 28280271220985972;
	cur_stav_sk[28] = 31808603718549528;
	cur_stav_sk[29] = 11347620366955677;
	cur_stav_sk[30] = 63959845722030462;
	cur_stav_sk[31] = 34977350443401340;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 27093689530492056;
	cur_stav_sk[1] = 4636259964654706;
	cur_stav_sk[2] = 30893052843524216;
	cur_stav_sk[3] = 17180450162623606;
	cur_stav_sk[4] = 14744003926660468;
	cur_stav_sk[5] = 33198480681804613;
	cur_stav_sk[6] = 13590655976898930;
	cur_stav_sk[7] = 27101331953434640;
	cur_stav_sk[8] = 14751975195709814;
	cur_stav_sk[9] = 6325383235404922;
	cur_stav_sk[10] = 10680205585122584;
	cur_stav_sk[11] = 8374566143304210;
	cur_stav_sk[12] = 34974233843282452;
	cur_stav_sk[13] = 22598198276962166;
	cur_stav_sk[14] = 501960491249786;
	cur_stav_sk[15] = 33857198746375288;
	cur_stav_sk[16] = 10204391875020054;
	cur_stav_sk[17] = 56269225019311176;
	cur_stav_sk[18] = 65377653091504510;
	cur_stav_sk[19] = 14065613116617584;
	cur_stav_sk[20] = 14047985617155604;
	cur_stav_sk[21] = 14744068744704576;
	cur_stav_sk[22] = 31825121608413974;
	cur_stav_sk[23] = 33179488340640022;
	cur_stav_sk[24] = 4997093660131448;
	cur_stav_sk[25] = 35405201743983126;
	cur_stav_sk[26] = 12478090648290321;
	cur_stav_sk[27] = 11568794003596869;
	cur_stav_sk[28] = 68662951214481534;
	cur_stav_sk[29] = 106178576973852;
	cur_stav_sk[30] = 13831737377558858;
	cur_stav_sk[31] = 27533176156816666;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);
	for(int k=0; k<musas_sunu_skaits/4; ++k) cur_stavoklis.skaitli[k]=my_dist(my_rand);
	cur_stav_sk[0] = 10662822176260429;
	cur_stav_sk[1] = 23073877032612629;
	cur_stav_sk[2] = 29766629497672728;
	cur_stav_sk[3] = 34979068925220120;
	cur_stav_sk[4] = 8400848662234742;
	cur_stav_sk[5] = 63720636469326608;
	cur_stav_sk[6] = 5603693764650138;
	cur_stav_sk[7] = 28280295444624712;
	cur_stav_sk[8] = 27542497982821752;
	cur_stav_sk[9] = 17390456421693302;
	cur_stav_sk[10] = 32766303891035412;
	cur_stav_sk[11] = 30480188515517509;
	cur_stav_sk[12] = 16277118736335945;
	cur_stav_sk[13] = 11566367414203416;
	cur_stav_sk[14] = 14040300251459866;
	cur_stav_sk[15] = 32757552041624950;
	cur_stav_sk[16] = 28650898995492218;
	cur_stav_sk[17] = 12878611592122386;
	cur_stav_sk[18] = 65056870574162802;
	cur_stav_sk[19] = 15191201825031792;
	cur_stav_sk[20] = 1258215174407454;
	cur_stav_sk[21] = 14047944292938140;
	cur_stav_sk[22] = 32723217873810680;
	cur_stav_sk[23] = 9351916636944240;
	cur_stav_sk[24] = 10644955314163740;
	cur_stav_sk[25] = 18508717522026512;
	cur_stav_sk[26] = 10258243930489460;
	cur_stav_sk[27] = 10260160486183198;
	cur_stav_sk[28] = 89133473465456;
	cur_stav_sk[29] = 12886936180458520;
	cur_stav_sk[30] = 13616716483858765;
	cur_stav_sk[31] = 14021254084735856;

	atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_stavoklis);

	ok = true;
	for(int k=0; k<musas_sunu_skaits/4; ++k){
		 if(cur_stav_sk[k] != (cur_stavoklis.skaitli[k]) ){
			 ok=false;
			 break;
		 }
	}
	CHECK(ok == true);

}
