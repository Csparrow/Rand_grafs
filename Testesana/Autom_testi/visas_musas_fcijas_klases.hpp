#include <bits/stdc++.h>


#define musa_ne_cikla 0
#define musa_cikla 1

#define musas_gali 1


using namespace std;

typedef long long ll;







struct baseins{
    int baseina_lielums;

    int cikla_garums;
    //int cikla_starta_v;
    int lielakais_att_lidz_ciklam;

    baseins(){
        baseina_lielums=0;
        cikla_garums=0;
        lielakais_att_lidz_ciklam=0;
    }

};






struct faili_stav_grafa_analizei_03{

    FILE *ciklu_skaits;
    FILE *garakais_cikls;
    FILE *vid_cikla_garums;
    FILE *vid_baseins;
    FILE *lielakais_baseins;

    FILE *lapu_sk;
    FILE *lielakais_att_lidz_ciklam;
    FILE *vid_att_lidz_ciklam;

    FILE *visu_ciklu_info;//sis jauns, noradis failu, kur info par visiem cikliem (info par katru ta garums, uc)

};




struct cikls{
    //prieksh stav_grafa_analize_saujot

    ll starta_virs;
    int cikla_garums;
    ll reizes_trapits;
    int gar_cels_lidz;

    cikls(){
        starta_virs=-1;
        cikla_garums=0;
        reizes_trapits=0;
        gar_cels_lidz=-1;
    }
};



struct suuna{
    bool SLP;
    bool wg;
    bool WG;
    bool en;
    bool EN;
    bool hh;
    bool HH;
    bool ptc;
    bool PTC;
    bool PH;
    bool SMO;
    bool ci;
    bool CI;
    bool CIA;
    bool CIR;

    void izdruka_suunu(){

        cout<<SLP<<wg<<WG;
        cout<<en<<EN<<hh;
        cout<<HH<<ptc<<PTC;
        cout<<PH<<SMO<<ci;
        cout<<CI<<CIA<<CIR;

    }
};


bool derigs_musas_stav(ll cur_stavoklis){
     //parbauda vai cur stavoklis atbilst iespejamam konstanto genu vertibam
     // tas ir, vai visi SLP ir pareizi

     /**
     sis tiri prieksh si briza 15x2 geniem:
     1. sunas pirmajam (SLP) jabut false un
     2. sunas pirmajam (SLP) jabut true.
     **/


    static ll maska1 = 1<<14;

    if( (cur_stavoklis&maska1) == 0) return false;




    /**
    sis tiri prieksh si briza 15x4 geniem
    1. = false
    2. = false
    3. = true
    4. = true
    **/

    /*
    static ll jabut = ((ll)1<<29) + ((ll)1<<14);

    static ll maska = ((ll)1<<59) + ((ll)1<<44) + jabut;

    if( (ll)(cur_stavoklis&maska) != jabut) return false;
    */


     return true;

}//end of derigs_musas_stav



ll atrod_jaunu_stavokli_musai(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, suuna &cur_suuna, const ll cur_stavoklis){


    ll temp_sk = cur_stavoklis;
    ll rez=0;





    for(int cur_suna=musas_sunu_skaits-1; cur_suna>=0; cur_suna--){
        //par cik SLP nemainaas - i==0 ir jau aizpildits
        //bija i>=0
        //parlabots uz: i>0
        for(int i=musas_genu_skaits-1; i>0; i--){
            musas_genu_vertibas[cur_suna][i] = (bool)(temp_sk & 1);
            temp_sk = temp_sk>>1;
        }
    }



    /// izrekina rez sakums

    #if musas_gali==musa_ne_cikla

        //pirmaas suunas sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //pirmaas suunas beigas


        for(int i=1; i<musas_sunu_skaits-1; ++i){

            //par cur suunu sakums
            {
                //SLP paliek ka konstante

                cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.wg;


                cur_suuna.WG = visas_sunas[i]->wg;
                rez = rez<<1;
                rez += (ll)cur_suuna.WG;


                cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
                rez = rez<<1;
                rez += (ll)cur_suuna.en;


                cur_suuna.EN = visas_sunas[i]->en;
                rez = rez<<1;
                rez += (ll)cur_suuna.EN;


                cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.hh;


                cur_suuna.HH = visas_sunas[i]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.HH;


                cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.ptc;


                cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
                rez = rez<<1;
                rez += (ll)cur_suuna.PTC;


                cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.PH;


                cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.SMO;


                cur_suuna.ci = !visas_sunas[i]->EN;
                rez = rez<<1;
                rez += (ll)cur_suuna.ci;


                cur_suuna.CI = visas_sunas[i]->ci;
                rez = rez<<1;
                rez += (ll)cur_suuna.CI;

                cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.CIA;

                cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.CIR;
            }

            //par cur suunu beigas


        }//end of for




        //par pedejo suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && visas_sunas[musas_sunu_skaits-2]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //par pedejo suunu beigas


    #elif musas_gali == musa_cikla

        //pirmaas suunas sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[musas_sunu_skaits-1]->WG || visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[musas_sunu_skaits-1]->HH && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;


            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[musas_sunu_skaits-1]->hh && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;

        }
        //pirmaas suunas beigas


        for(int i=1; i<musas_sunu_skaits-1; ++i){

            //par cur suunu sakums
            {
                //SLP paliek ka konstante

                cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.wg;


                cur_suuna.WG = visas_sunas[i]->wg;
                rez = rez<<1;
                rez += (ll)cur_suuna.WG;


                cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
                rez = rez<<1;
                rez += (ll)cur_suuna.en;


                cur_suuna.EN = visas_sunas[i]->en;
                rez = rez<<1;
                rez += (ll)cur_suuna.EN;


                cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.hh;


                cur_suuna.HH = visas_sunas[i]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.HH;


                cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.ptc;


                cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
                rez = rez<<1;
                rez += (ll)cur_suuna.PTC;


                cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.PH;


                cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.SMO;


                cur_suuna.ci = !visas_sunas[i]->EN;
                rez = rez<<1;
                rez += (ll)cur_suuna.ci;


                cur_suuna.CI = visas_sunas[i]->ci;
                rez = rez<<1;
                rez += (ll)cur_suuna.CI;

                cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.CIA;

                cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.CIR;
            }

            //par cur suunu beigas


        }//end of for


        //par pedejo suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;


            cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;


            cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG || visas_sunas[0]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;


            cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;


            cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;


            cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;


            cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH && !visas_sunas[0]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;


            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh && !visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //par pedejo suunu beigas


    #endif // musas_gali



    /// izrekina rez beigas


    return rez;

}//end of atrod_jaunu_stavokli_musai



class stavoklis{
public:

    ll *skaitli;
    int skaitlu_sk;


    stavoklis(const int sk){

        /*
        while(sk>4 || sk<=0){
            printf("noraditajam skaitlu skaitam (%d) jabut <= par %d\n", sk, 4);
            cout<<"ievadiet sk skaitu: ";
            cin>>sk;
        }
        */
        skaitlu_sk = sk;
        skaitli = new ll[skaitlu_sk];
    }

    //kopijas konstruktora sakums
    stavoklis(const stavoklis &s){

        skaitlu_sk = s.skaitlu_sk;
        skaitli = new ll[skaitlu_sk];
        for(int i=0; i<skaitlu_sk; ++i) skaitli[i] = s.skaitli[i];
    }//kopijas konstruktora beigas

    bool operator< (const stavoklis &la)const{
        for(int i=0; i<skaitlu_sk; i++){
            if(skaitli[i] < la.skaitli[i]) return true;
            else if(skaitli[i]>la.skaitli[i]) return false;
        }
        return false;
    }

};







void atrod_jaunu_stavokli_musai_2(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, suuna &cur_suuna, stavoklis &cur_stavoklis){


    ll temp_sk;
    ll rez;

    int cur_suunas_nr = musas_sunu_skaits-1;



    for(int sk_ind=cur_stavoklis.skaitlu_sk-1; sk_ind>=0; --sk_ind){

        temp_sk = cur_stavoklis.skaitli[sk_ind];

        for(int k=0; k<4; ++k){//2 vai 4 apzime cik viena skaitli glaba suunas

            for(int i=musas_genu_skaits-1; i>0; i--){
                musas_genu_vertibas[cur_suunas_nr][i] = (bool)(temp_sk & 1);
                temp_sk = temp_sk>>1;
            }

            --cur_suunas_nr;
        }//viena skaitla for beigas


    }//skaitlu for beigas


    for(int i=0; i<cur_stavoklis.skaitlu_sk; ++i) cur_stavoklis.skaitli[i]=0; //nonulle, jo so pasu masivu izmantos, lai saglabatu jauno rezultatu






    /// izrekina rez sakums

    #if musas_gali == musa_cikla

        ///gali noslegti cikla


        //pirmaas suunas sakums
        rez = 0;
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[0]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[musas_sunu_skaits-1]->WG || visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[0]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[musas_sunu_skaits-1]->HH && !visas_sunas[1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[0]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[0]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[musas_sunu_skaits-1]->hh || visas_sunas[1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[musas_sunu_skaits-1]->hh && !visas_sunas[1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;

        }
        //pirmaas suunas beigas



        for(int i=1; i<musas_sunu_skaits-1; ++i){

            //par cur suunu sakums
            {
                //SLP paliek ka konstante

                cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.wg;

                cur_suuna.WG = visas_sunas[i]->wg;
                rez = rez<<1;
                rez += (ll)cur_suuna.WG;

                cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
                rez = rez<<1;
                rez += (ll)cur_suuna.en;


                cur_suuna.EN = visas_sunas[i]->en;
                rez = rez<<1;
                rez += (ll)cur_suuna.EN;

                cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.hh;

                cur_suuna.HH = visas_sunas[i]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.HH;

                cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
                rez = rez<<1;
                rez += (ll)cur_suuna.ptc;

                cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
                rez = rez<<1;
                rez += (ll)cur_suuna.PTC;


                cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.PH;



                cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.SMO;


                cur_suuna.ci = !visas_sunas[i]->EN;
                rez = rez<<1;
                rez += (ll)cur_suuna.ci;


                cur_suuna.CI = visas_sunas[i]->ci;
                rez = rez<<1;
                rez += (ll)cur_suuna.CI;

                cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
                rez = rez<<1;
                rez += (ll)cur_suuna.CIA;

                cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
                rez = rez<<1;
                rez += (ll)cur_suuna.CIR;
            }

            //par cur suunu beigas

            if(i%4==3){ //ja viena skatli 4 sunas
            //if(i%2==1){ //ja viena skaitli 2 sunas
                cur_stavoklis.skaitli[i/4] = rez; //ja viena skatli 4 sunas
                //cur_stavoklis.skaitli[i/2] = rez; //ja viena skatli 2 sunas
                rez=0;
            }


        }//end of for

        //par pedejo suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG || visas_sunas[0]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH && !visas_sunas[0]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh || visas_sunas[0]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh && !visas_sunas[0]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }
        //par pedejo suunu beigas


        ///gali noslegti cikla

    #elif musas_gali == musa_ne_cikla


    ///gali noslegti ne-cikla sakums

    //pirmaas suunas sakums
    rez = 0;
    {
        //SLP paliek ka konstante

        cur_suuna.wg = ( (visas_sunas[0]->CIA && visas_sunas[0]->SLP) || (visas_sunas[0]->wg && (visas_sunas[0]->CIA || visas_sunas[0]->SLP) ) ) && !visas_sunas[0]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.wg;


        cur_suuna.WG = visas_sunas[0]->wg;
        rez = rez<<1;
        rez += (ll)cur_suuna.WG;


        cur_suuna.en = (visas_sunas[1]->WG) && !visas_sunas[0]->SLP;
        rez = rez<<1;
        rez += (ll)cur_suuna.en;


        cur_suuna.EN = visas_sunas[0]->en;
        rez = rez<<1;
        rez += (ll)cur_suuna.EN;


        cur_suuna.hh = visas_sunas[0]->EN && !visas_sunas[0]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.hh;


        cur_suuna.HH = visas_sunas[0]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.HH;


        cur_suuna.ptc = visas_sunas[0]->CIA && !visas_sunas[0]->EN && !visas_sunas[0]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.ptc;


        cur_suuna.PTC = visas_sunas[0]->ptc || (visas_sunas[0]->PTC && !visas_sunas[1]->HH);
        rez = rez<<1;
        rez += (ll)cur_suuna.PTC;


        cur_suuna.PH = cur_suuna.PTC && visas_sunas[1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.PH;



        cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.SMO;


        cur_suuna.ci = !visas_sunas[0]->EN;
        rez = rez<<1;
        rez += (ll)cur_suuna.ci;


        cur_suuna.CI = visas_sunas[0]->ci;
        rez = rez<<1;
        rez += (ll)cur_suuna.CI;

        cur_suuna.CIA = visas_sunas[0]->CI && (visas_sunas[0]->SMO || visas_sunas[1]->hh);
        rez = rez<<1;
        rez += (ll)cur_suuna.CIA;

        cur_suuna.CIR = visas_sunas[0]->CI && !visas_sunas[0]->SMO && !visas_sunas[1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.CIR;

    }
    //pirmaas suunas beigas



    for(int i=1; i<musas_sunu_skaits-1; ++i){

        //par cur suunu sakums
        {
            //SLP paliek ka konstante

            cur_suuna.wg = ( (visas_sunas[i]->CIA && visas_sunas[i]->SLP) || (visas_sunas[i]->wg && (visas_sunas[i]->CIA || visas_sunas[i]->SLP) ) ) && !visas_sunas[i]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.wg;

            cur_suuna.WG = visas_sunas[i]->wg;
            rez = rez<<1;
            rez += (ll)cur_suuna.WG;

            cur_suuna.en = (visas_sunas[i-1]->WG || visas_sunas[i+1]->WG) && !visas_sunas[i]->SLP;
            rez = rez<<1;
            rez += (ll)cur_suuna.en;


            cur_suuna.EN = visas_sunas[i]->en;
            rez = rez<<1;
            rez += (ll)cur_suuna.EN;

            cur_suuna.hh = visas_sunas[i]->EN && !visas_sunas[i]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.hh;

            cur_suuna.HH = visas_sunas[i]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.HH;

            cur_suuna.ptc = visas_sunas[i]->CIA && !visas_sunas[i]->EN && !visas_sunas[i]->CIR;
            rez = rez<<1;
            rez += (ll)cur_suuna.ptc;

            cur_suuna.PTC = visas_sunas[i]->ptc || (visas_sunas[i]->PTC && !visas_sunas[i-1]->HH && !visas_sunas[i+1]->HH);
            rez = rez<<1;
            rez += (ll)cur_suuna.PTC;


            cur_suuna.PH = cur_suuna.PTC && (visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.PH;



            cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.SMO;


            cur_suuna.ci = !visas_sunas[i]->EN;
            rez = rez<<1;
            rez += (ll)cur_suuna.ci;


            cur_suuna.CI = visas_sunas[i]->ci;
            rez = rez<<1;
            rez += (ll)cur_suuna.CI;

            cur_suuna.CIA = visas_sunas[i]->CI && (visas_sunas[i]->SMO || visas_sunas[i-1]->hh || visas_sunas[i+1]->hh);
            rez = rez<<1;
            rez += (ll)cur_suuna.CIA;

            cur_suuna.CIR = visas_sunas[i]->CI && !visas_sunas[i]->SMO && !visas_sunas[i-1]->hh && !visas_sunas[i+1]->hh;
            rez = rez<<1;
            rez += (ll)cur_suuna.CIR;
        }

        //par cur suunu beigas

        if(i%4==3){ //ja viena skatli 4 sunas
        //if(i%2==1){ //ja viena skaitli 2 sunas
            cur_stavoklis.skaitli[i/4] = rez; //ja viena skatli 4 sunas
            //cur_stavoklis.skaitli[i/2] = rez; //ja viena skatli 2 sunas
            rez=0;
        }


    }//end of for

    //par pedejo suunu sakums
    {
        //SLP paliek ka konstante

        cur_suuna.wg = ( (visas_sunas[musas_sunu_skaits-1]->CIA && visas_sunas[musas_sunu_skaits-1]->SLP) || (visas_sunas[musas_sunu_skaits-1]->wg && (visas_sunas[musas_sunu_skaits-1]->CIA || visas_sunas[musas_sunu_skaits-1]->SLP) ) ) && !visas_sunas[musas_sunu_skaits-1]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.wg;


        cur_suuna.WG = visas_sunas[musas_sunu_skaits-1]->wg;
        rez = rez<<1;
        rez += (ll)cur_suuna.WG;


        cur_suuna.en = (visas_sunas[musas_sunu_skaits-2]->WG) && !visas_sunas[musas_sunu_skaits-1]->SLP;
        rez = rez<<1;
        rez += (ll)cur_suuna.en;


        cur_suuna.EN = visas_sunas[musas_sunu_skaits-1]->en;
        rez = rez<<1;
        rez += (ll)cur_suuna.EN;


        cur_suuna.hh = visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.hh;


        cur_suuna.HH = visas_sunas[musas_sunu_skaits-1]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.HH;


        cur_suuna.ptc = visas_sunas[musas_sunu_skaits-1]->CIA && !visas_sunas[musas_sunu_skaits-1]->EN && !visas_sunas[musas_sunu_skaits-1]->CIR;
        rez = rez<<1;
        rez += (ll)cur_suuna.ptc;


        cur_suuna.PTC = visas_sunas[musas_sunu_skaits-1]->ptc || (visas_sunas[musas_sunu_skaits-1]->PTC && !visas_sunas[musas_sunu_skaits-2]->HH);
        rez = rez<<1;
        rez += (ll)cur_suuna.PTC;


        cur_suuna.PH = cur_suuna.PTC && visas_sunas[musas_sunu_skaits-2]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.PH;



        cur_suuna.SMO = !cur_suuna.PTC || visas_sunas[musas_sunu_skaits-2]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.SMO;


        cur_suuna.ci = !visas_sunas[musas_sunu_skaits-1]->EN;
        rez = rez<<1;
        rez += (ll)cur_suuna.ci;


        cur_suuna.CI = visas_sunas[musas_sunu_skaits-1]->ci;
        rez = rez<<1;
        rez += (ll)cur_suuna.CI;

        cur_suuna.CIA = visas_sunas[musas_sunu_skaits-1]->CI && (visas_sunas[musas_sunu_skaits-1]->SMO || visas_sunas[musas_sunu_skaits-2]->hh);
        rez = rez<<1;
        rez += (ll)cur_suuna.CIA;

        cur_suuna.CIR = visas_sunas[musas_sunu_skaits-1]->CI && !visas_sunas[musas_sunu_skaits-1]->SMO && !visas_sunas[musas_sunu_skaits-2]->hh;
        rez = rez<<1;
        rez += (ll)cur_suuna.CIR;
    }
    //par pedejo suunu beigas


    ///gali noslegti ne-cikla beigas


    #endif // musas_gali






    cur_stavoklis.skaitli[ cur_stavoklis.skaitlu_sk-1 ] = rez; //saglaba info pedeja skaitli


    /// izrekina rez beigas




}//end of atrod_jaunu_stavokli_musai_2






void izdruka_musas_genu_vertibas(FILE *out_file2,FILE *out_file3, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll &cur_stavoklis){


    ll temp_sk = cur_stavoklis;


    for(int cur_suna=musas_sunu_skaits-1; cur_suna>=0; cur_suna--){
        //par cik SLP nemainaas - i==0 ir jau aizpildits
        //bija i>=0
        //parlabots uz: i>0
        for(int i=musas_genu_skaits-1; i>0; i--){
            musas_genu_vertibas[cur_suna][i] = (bool)(temp_sk & 1);
            temp_sk = temp_sk>>1;
        }
    }

    ///




    //izdruka faila par katru genu kada ta vertiba katra suuna
    // sis der tikai tad, ja genu skaits ir 15
    static char genu_nos[15][5]={"SLP", "wg", "WG", "en", "EN", "hh",
                 "HH", "ptc", "PTC", "PH", "SMO", "ci", "CI", "CIA","CIR"};



    for(int cur_g=1; cur_g<musas_genu_skaits; ++cur_g){//sak no 0 ja grib izdrukat SLP no 1 ja nee.

        fprintf(out_file2 ,"%s: ", genu_nos[cur_g]);//lai butu klat gena nosaukums
        for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
            //printf("%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
            fprintf(out_file2 ,"%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        //printf("\n");
        fprintf(out_file2, "\n");
    }



    for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
        for(int cur_g=0; cur_g<musas_genu_skaits; ++cur_g){
            fprintf(out_file3, "%d", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        fprintf(out_file3, "  ");
    }

    fprintf(out_file2, "\n\n\n\n");//4 enter lai labak izskatitos
    fprintf(out_file3, "\n");



}//end of izdruka_musas_genu_vertibas









void stav_grafa_analize_saujot2_musai(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll musas_stav_skaits, const int laika_limits, suuna &cur_suuna){

    /***
    * butiba stav_grafa analize saujot2 pielagota musai
    ***/

    //FILE *out_file=fopen("stav_grafa_analize_saujot2_musai.txt", "a");
    //FILE *out_file2=fopen("genu_v_pa_geniem_saujot_musai.txt", "a");
    //FILE *out_file3=fopen("genu_v_pa_suunam_saujot_musai.txt", "a");
    FILE *out_file;
    FILE *out_file2;
    FILE *out_file3;
    const char f1_v[]="pilns_info.txt";
    const char f2_v[]="genu_v_pa_geniem.txt";
    const char f3_v[]="genu_v_pa_suunam.txt";

    char pre_f_v[40];
    char f_v[100];


    ll virsotnu_skaits = musas_stav_skaits;

    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, virsotnu_skaits-1);

    ll cur_virsotne;

    //ll it_skaits = 1000000000;
    ll cur_it = 0;

    //int maks_laiks = 60;

    unordered_map<ll, int> cels;
    unordered_map<ll, int>::iterator atr_virs;



    int cur_cela_garums;
    int cur_cikla_garums;
    int cur_cels_lidz;

    cikls *cikli;
    int maks_ciklu_sk=1000;
    int ciklu_sk=0;
    bool jauns_cikls;
    int atr_cikla_ind;

    int gar_cikls=0;

    time_t sakuma_t;
    time_t beigu_t;
    time_t cur_it_t;

    struct tm* time_info;





    pair <unordered_map<ll, int>::iterator, bool> atr_rez;

    cels.rehash(40);

    cikli = new cikls[maks_ciklu_sk];

    time(&sakuma_t);

    time_info=localtime(&sakuma_t);
    strftime(pre_f_v, 80, "out_faili/%Y%m%d_%H%M%S_", time_info);

    sprintf(f_v,"%s%s", pre_f_v, f1_v);
    out_file = fopen(f_v, "w");
    sprintf(f_v,"%s%s", pre_f_v, f2_v);
    out_file2 = fopen(f_v, "w");
    sprintf(f_v,"%s%s", pre_f_v, f3_v);
    out_file3 = fopen(f_v, "w");

    if(out_file==NULL || out_file2==NULL || out_file3==NULL){
        printf("\tKLUDA: neizdevas atvert failus!Parbaudit vai ir folderis: 'out_faili'. Ja nav uztaisit!\n");
        return;
    }

    printf("sakas stav_grafa_analize ar sausanu 2 Musai.\n");
    printf("musas sunu skaits: %d\n", musas_sunu_skaits);
    printf("Laika limits: %d\n", laika_limits);
    //time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    do{


        cels.clear();


        //genere kamer atrod derigu
        /*
        do{
           cur_virsotne = my_dist(my_rand);
        }
        while(!derigs_musas_stav(cur_virsotne) );
        */

        cur_virsotne = my_dist(my_rand);

        cur_cela_garums = 0;

        cels.emplace(cur_virsotne, 1);



        do{
            cur_cela_garums++;

            cur_virsotne = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_virsotne);

            atr_rez = cels.emplace(cur_virsotne, cur_cela_garums+1);
        }
        while(atr_rez.second);

        atr_virs = atr_rez.first;


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;
        for(int i=0; i<ciklu_sk; i++){

            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }




        if(jauns_cikls){
            //atrasts jauns cikls
            if(cur_cikla_garums>gar_cikls) gar_cikls = cur_cikla_garums;

            if(ciklu_sk==maks_ciklu_sk){
                cout<<"\n\t!!!ciklu vairak ka: "<<maks_ciklu_sk<<endl;
                cur_it++;
                break;
            }

            cikli[ciklu_sk].starta_virs = atr_virs->first;
            cikli[ciklu_sk].cikla_garums = cur_cikla_garums;
            cikli[ciklu_sk].gar_cels_lidz = cur_cels_lidz;
            cikli[ciklu_sk].reizes_trapits = 1;

            ciklu_sk++;
        }//endif jauns cikls
        else{
            //nav atrasts jauns cikls -trapits pa kadu velreiz
            if(cikli[atr_cikla_ind].gar_cels_lidz < cur_cels_lidz) cikli[atr_cikla_ind].gar_cels_lidz = cur_cels_lidz;

            cikli[atr_cikla_ind].reizes_trapits++;
        }

        ++cur_it;//par cik do-while arii tad, ja iziet viena iteracija cur it buus 1, nevis 0

    }
    while(cur_it_t-sakuma_t<=laika_limits);//iteraciju do-while beigas

    time(&beigu_t);



    ///
    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file2, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file3, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);

    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file2, "ciklu skaits := %d\n\n", ciklu_sk);
    fprintf(out_file3, "ciklu skaits := %d\n\n", ciklu_sk);

    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "iteraciju skaits := %lld\n", cur_it);
    fprintf(out_file, "analize ar sausanu2 musai laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz      reizes_trapits  relativie_trapijumi    starta_stavoklis\n\n");


    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d", i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz);
        fprintf(out_file,"%20lld", cikli[i].reizes_trapits);
        fprintf(out_file,"%21.8f",(double)cikli[i].reizes_trapits/(double)cur_it);

        fprintf(out_file, "%20lld\n", cikli[i].starta_virs);

        fprintf(out_file3, "cikls: %d\t", i+1);
        izdruka_musas_genu_vertibas(out_file2,out_file3, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cikli[i].starta_virs);
    }
    fprintf(out_file, "\n####################################################################################################################\n");
    fprintf(out_file2, "\n####################################################################################################################\n");
    fprintf(out_file3, "\n####################################################################################################################\n");
    ///


    printf("\n\tstav_grafa_analize ar sausanu 2 musai.beidz darbu.\n");
    printf("rekinat beigts: %s\n", ctime(&beigu_t));
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));



}//stav_grafa_analize_saujot2_musai




void stav_grafa_analize_musai(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll musas_stav_skaits, suuna &cur_suuna){

    /**
    stav_grafa_analize_03_3var pielagota musai
    **/

    FILE *out_file = fopen("stav_gr_analize_Musai.txt", "a");

    int stavoklu_sk = musas_stav_skaits;

    int8_t *pieder_baseinam;

    baseins *baseini;
    int8_t maks_bas_sk = (1<<7)-5;//pietiktu ar -1, tacu vnk drosibai, iekavas obligati!!!


    int kop_ciklu_garums;
    int ciklu_skaits;
    int garakais_cikls;

    int lielakais_baseins;

    double vid_cikla_garums;
    double vid_baseins;

    int cur_v;
    int v_skaits_cela;


    vector <int> cels;
    static int maks_v_cela = 1<<2;
    int cur_ind;

    int cur_cikla_sakums;
    int cur_cikla_garums;

    int cikla_nr;

    time_t sakuma_t;
    time_t beigu_t;



    int *att_lidz_cikliem;
    ll lielakais_att_lidz_ciklam;
    ll kop_att_lidz_ciklam;
    int cur_att_lidz_ciklam;

    double vid_att_lidz_ciklam;
    int lapu_sk;


    cels.resize(maks_v_cela);

    pieder_baseinam = new int8_t[stavoklu_sk];

    memset(pieder_baseinam, -1, stavoklu_sk*sizeof(int8_t));
    //pieder baseinam:
    //-1 - nekam
    //ciklu skaits ir bijis saja cela
    //cits - att baseins

    baseini = new baseins[maks_bas_sk];


    att_lidz_cikliem = new int[stavoklu_sk];
    memset(att_lidz_cikliem, 0, stavoklu_sk*sizeof(int));




    kop_ciklu_garums=0;
    ciklu_skaits=0;
    garakais_cikls=0;
    lielakais_baseins=0;

    lielakais_att_lidz_ciklam=0;
    kop_att_lidz_ciklam=0;
    lapu_sk=0;


    printf("sakas stav_grafa_analize_musai veiktaa analize\n\n");
    time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));

    for(int i=0; i<stavoklu_sk; i++){

        //if(pieder_baseinam[i]==-1 && derigs_musas_stav((ll)i) ){ //aizkomentets, jo tagadeja varianta, stavoklii vispaar konstantais SLP nav ieklauts.
        if(pieder_baseinam[i]==-1){
            cur_v=i;
            v_skaits_cela=0;

            do{
                pieder_baseinam[cur_v] = ciklu_skaits;
                cels.at(v_skaits_cela) = cur_v;

                v_skaits_cela++;


                cur_v = atrod_jaunu_stavokli_musai(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_v);



                //cela resizosanas ifa sakums
                if(v_skaits_cela==maks_v_cela-1){

                    maks_v_cela = maks_v_cela<<1;
                    cels.resize(maks_v_cela);
                }
                //cela resizosanas ifa beigas
            }
            while(pieder_baseinam[cur_v]==-1);


            if(pieder_baseinam[cur_v]==ciklu_skaits){
                //atrasts jauns cikls


                cur_cikla_sakums = cur_v;
                cur_cikla_garums = 0;

                cur_ind = v_skaits_cela-1;

                do{
                    cur_cikla_garums++;
                    cur_v = cels[cur_ind];
                    cur_ind--;

                }
                while(cur_v!=cur_cikla_sakums);

                kop_ciklu_garums+=cur_cikla_garums;
                if(garakais_cikls<cur_cikla_garums) garakais_cikls = cur_cikla_garums;

                //saglaba jauno ciklu
                baseini[ciklu_skaits].baseina_lielums = v_skaits_cela;
                baseini[ciklu_skaits].cikla_garums = cur_cikla_garums;
                //baseini[ciklu_skaits].cikla_starta_v = cur_cikla_sakums;

                ciklu_skaits++;

                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, sakums
                cur_att_lidz_ciklam = v_skaits_cela - cur_cikla_garums;
                cur_v = cels[0]; //jeb cur_v = i;
                cur_ind = 1;
                while(cur_v != cur_cikla_sakums){
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_v = cels[cur_ind];
                    cur_ind++;

                    cur_att_lidz_ciklam--;
                }
                ///atzime attalumus lidz ciklam, kad atrasts jauns cikls, beigas


                if(maks_bas_sk==ciklu_skaits){

                    printf("\n!!!!!!!Kluda!!!!!!!!\n");
                    printf("Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    printf("Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");

                    fprintf(out_file, "\n!!!!!!!Kluda!!!!!!!!\n");
                    fprintf(out_file, "Ciklu skaits lielaks, kaa: %d!\n", (int)maks_bas_sk);
                    fprintf(out_file, "Visi talakie raditaji ir par doto bridi - tatad neprecizi!\n");


                    break;
                }//ifa, kas parbauda vai nav parsniegts ciklu skaits beigas


            }//atrasts jauns cikls ifa beigas
            else{
                //nav jauns cikls
                //atduras pret citam baseinam/ciklam(cikla nr) piederoso virsotni
                cikla_nr = pieder_baseinam[cur_v];

                baseini[cikla_nr].baseina_lielums += v_skaits_cela;//pieliek si cela virsotnu skaitu baseinam

                if(att_lidz_cikliem[cur_v]<0) att_lidz_cikliem[cur_v] *= -1;//atzime, ka noteikti nav lapa

                cur_att_lidz_ciklam = v_skaits_cela + att_lidz_cikliem[cur_v];

                //atzimee, ka visas saja cela sastaptas virsotnes pieder baseinam pret kuru atduraas.
                cur_ind=0; //cur_v=i;
                for(int k=0; k<v_skaits_cela; k++){
                    cur_v = cels[cur_ind];//tas pats, kas [k], tacu lai visur vienadi liku [cur_ind]
                    cur_ind++;
                    pieder_baseinam[cur_v] = cikla_nr;
                    att_lidz_cikliem[cur_v] = cur_att_lidz_ciklam;

                    cur_att_lidz_ciklam--;
                }
            }//nav jauns cikls else beigas

            att_lidz_cikliem[i] *= -1; //virsotni, no kuras saaka staigat atzimee kaa potencialo lapu
        }
    }

    time(&beigu_t);

    vid_cikla_garums = (double)kop_ciklu_garums/(double)ciklu_skaits;
    vid_baseins = (double)stavoklu_sk/(double)ciklu_skaits;

    for(int8_t i=0; i<ciklu_skaits; i++) if(baseini[i].baseina_lielums>lielakais_baseins) lielakais_baseins = baseini[i].baseina_lielums;



    for(int i=0; i<stavoklu_sk; i++){
        if(att_lidz_cikliem[i]<0){
            //ir lapa
            att_lidz_cikliem[i]*=-1;
            lapu_sk++;
            kop_att_lidz_ciklam += (ll)att_lidz_cikliem[i];

            if(att_lidz_cikliem[i] > lielakais_att_lidz_ciklam) lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

            if( att_lidz_cikliem[i] > baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam ) baseini[pieder_baseinam[i]].lielakais_att_lidz_ciklam = att_lidz_cikliem[i];

        }
    }

    if(lapu_sk!=0) vid_att_lidz_ciklam = (double)kop_att_lidz_ciklam/(double)lapu_sk;
    else vid_att_lidz_ciklam = 0;




    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file, "ciklu skaits := %d\n", ciklu_skaits);
    fprintf(out_file, "garakais cikls := %d\n", garakais_cikls);
    fprintf(out_file, "vid cikla garums := %.2f\n", vid_cikla_garums);
    fprintf(out_file, "vid baseins := %.2f\n", vid_baseins);
    fprintf(out_file, "lielakais baseins := %d\n", lielakais_baseins);

    fprintf(out_file, "\nlapu sk: %d\n", lapu_sk);
    fprintf(out_file, "lielakais attalums lidz ciklam: %lld\n", lielakais_att_lidz_ciklam);
    fprintf(out_file, "videjais attalums lidz ciklam: %.2f\n", vid_att_lidz_ciklam);

    fprintf(out_file, "\nanalize03 laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\ncikls  baseina_lielums  cikla_garums   liel_att_lidz_c\n\n");

    for(int8_t i=0; i<ciklu_skaits; i++){
        fprintf(out_file, "%5d  %15d %14d %16d\n", (int)i+1, baseini[i].baseina_lielums, baseini[i].cikla_garums, baseini[i].lielakais_att_lidz_ciklam);
    }

    fprintf(out_file, "\n#####################################################\n");



    delete[] baseini;
    delete[] pieder_baseinam;
    delete[] att_lidz_cikliem;


}//stav_grafa_analize_musai



void aizpilda_musas_SLP(suuna **visas_sunas, const int &musas_sunu_skaits){

    if(musas_sunu_skaits==2){
        //musas_genu_vertibas[0][0] = false; //1.sunas SLP ir false
        //musas_genu_vertibas[1][0] = true; //2.suunas SLP ir true
        visas_sunas[0]->SLP = false; //1.sunas SLP ir false
        visas_sunas[1]->SLP = true; //2.suunas SLP ir true
    }
    else if(musas_sunu_skaits%4==0){
        //%4==0 nozime, ka ik pa 4 suunam
        /**
        15x4 gadijumaa:
        1. = false
        2. = false
        3. = true
        4. = true
        **/
        for(int i=0; i<musas_sunu_skaits; i++){
            //if( (i+1)%4 == 1 || (i+1)%4 == 2) musas_genu_vertibas[i][0] = false;
            //else musas_genu_vertibas[i][0] = true;
            if( (i+1)%4 == 1 || (i+1)%4 == 2) visas_sunas[i]->SLP = false;
            else visas_sunas[i]->SLP = true;
        }
    }


}//end of aizpilda_musas_SLP





void izdruka_musas_genu_vertibas_2(FILE *out_file2,FILE *out_file3, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const stavoklis &cur_stavoklis){


    ll temp_sk;

    int cur_suunas_nr = musas_sunu_skaits-1;



    for(int sk_ind=cur_stavoklis.skaitlu_sk-1; sk_ind>=0; --sk_ind){

        temp_sk = cur_stavoklis.skaitli[sk_ind];

        for(int k=0; k<4; ++k){//2 vai 4 apzime cik viena skaitli glaba suunas

            for(int i=musas_genu_skaits-1; i>0; i--){
                musas_genu_vertibas[cur_suunas_nr][i] = (bool)(temp_sk & 1);
                temp_sk = temp_sk>>1;
            }

            --cur_suunas_nr;
        }//viena skaitla for beigas

    }//skaitlu for beigas

    ///




    //izdruka faila par katru genu kada ta vertiba katra suuna
    // sis der tikai tad, ja genu skaits ir 15
    static char genu_nos[15][5]={"SLP", "wg", "WG", "en", "EN", "hh",
                 "HH", "ptc", "PTC", "PH", "SMO", "ci", "CI", "CIA","CIR"};



    for(int cur_g=1; cur_g<musas_genu_skaits; ++cur_g){//sak no 0 ja grib izdrukat SLP no 1 ja nee.
        //printf("cur gens= %d : ", cur_g);
        fprintf(out_file2 ,"%s: ", genu_nos[cur_g]);//lai butu klat gena nosaukums
        for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
            //printf("%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
            fprintf(out_file2 ,"%d ", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        //printf("\n");
        fprintf(out_file2, "\n");
    }



    for(int suunas_nr=0; suunas_nr<musas_sunu_skaits; ++suunas_nr){
        for(int cur_g=0; cur_g<musas_genu_skaits; ++cur_g){
            fprintf(out_file3, "%d", (int)musas_genu_vertibas[suunas_nr][cur_g]);
        }
        fprintf(out_file3, "  ");
    }

    fprintf(out_file2, "\n\n\n\n");//4 enter lai labak izskatitos
    fprintf(out_file3, "\n");



}//end of izdruka_musas_genu_vertibas_2







class cikls_2{
    //prieksh stav_grafa_analize_saujot
public:

    stavoklis starta_virs;
    int cikla_garums;
    ll reizes_trapits;
    int gar_cels_lidz;

    cikls_2(const int musas_sunu_skaits):starta_virs(musas_sunu_skaits/4){
        cikla_garums=0;
        reizes_trapits=0;
        gar_cels_lidz=-1;
    }
    cikls_2(const cikls_2 &a):starta_virs(a.starta_virs.skaitlu_sk){
        cikla_garums=0;
        reizes_trapits=0;
        gar_cels_lidz=-1;
    }
};



void stav_grafa_analize_saujot2_musai_2(suuna **visas_sunas, bool **musas_genu_vertibas, const int &musas_genu_skaits, const int &musas_sunu_skaits, const ll musas_skaitla_robeza, const int laika_limits, suuna &cur_suuna){

    /***
    * butiba stav_grafa analize saujot2 pielagota musai
    * Atskiribaa ar 1 variantu, pielagota musas stavokla glabasanai varakos skaitlos
    ***/

    //FILE *out_file=fopen("stav_grafa_analize_saujot2_musai_2.txt", "a");
    //FILE *out_file2=fopen("genu_v_pa_geniem_saujot_musai_2.txt", "a");
    //FILE *out_file3=fopen("genu_v_pa_suunam_saujot_musai_2.txt", "a");
    FILE *out_file;
    FILE *out_file2;
    FILE *out_file3;
    const char f1_v[]="pilns_info.txt";
    const char f2_v[]="genu_v_pa_geniem.txt";
    const char f3_v[]="genu_v_pa_suunam.txt";

    char pre_f_v[40];
    char f_v[100];


    mt19937_64 my_rand;
    uniform_int_distribution<ll> my_dist(0, musas_skaitla_robeza-1);

    //stavoklis cur_virsotne(musas_sunu_skaits/2); //jo 2 1 skaitli
    stavoklis cur_virsotne(musas_sunu_skaits/4); //jo 4 1 skaitli

    //int it_skaits = 1000000000;
    ll cur_it = 0;

    //int maks_laiks = 360;




    map<stavoklis, int> cels;
    map<stavoklis, int>::iterator atr_virs;





    int cur_cela_garums;
    int cur_cikla_garums;
    int cur_cels_lidz;

    //cikls_2 *cikli;
    int maks_ciklu_sk=1000;

    vector<cikls_2> cikli(maks_ciklu_sk, cikls_2(musas_sunu_skaits));
    int ciklu_sk=0;
    bool jauns_cikls;
    int atr_cikla_ind;

    int gar_cikls=0;

    time_t sakuma_t;
    time_t beigu_t;
    time_t cur_it_t;
    struct tm* time_info;




    pair < map<stavoklis, int>::iterator, bool> atr_rez;

    //cels.rehash(40);

    //cikli = new cikls_2[maks_ciklu_sk];


    time(&sakuma_t);

    time_info=localtime(&sakuma_t);
    strftime(pre_f_v, 80, "out_faili/%Y%m%d_%H%M%S_", time_info);

    sprintf(f_v,"%s%s", pre_f_v, f1_v);
    out_file = fopen(f_v, "w");
    sprintf(f_v,"%s%s", pre_f_v, f2_v);
    out_file2 = fopen(f_v, "w");
    sprintf(f_v,"%s%s", pre_f_v, f3_v);
    out_file3 = fopen(f_v, "w");

    if(out_file==NULL || out_file2==NULL || out_file3==NULL){
        printf("\tKLUDA: neizdevas atvert failus!Parbaudit vai ir folderis: 'out_faili'. Ja nav uztaisit!\n");
        return;
    }

    printf("sakas stav_grafa_analize ar sausanu 2 Musai 2 variants.\n");
    printf("musas sunu skaits: %d\n", musas_sunu_skaits);
    printf("Laika limits: %d\n", laika_limits);
    //time(&sakuma_t);
    printf("sakuma laiks: %s\n", ctime(&sakuma_t));


    do{


        cels.clear();


        //uzgenere virsotni
        for(int i=0; i<cur_virsotne.skaitlu_sk; ++i){
            cur_virsotne.skaitli[i] = my_dist(my_rand);
        }




        cur_cela_garums = 0;

        cels.emplace(cur_virsotne, 1);
        //cels.emplace(make_pair(stavoklis(cur_virsotne), 1));





        do{
            cur_cela_garums++;

            atrod_jaunu_stavokli_musai_2(visas_sunas, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cur_suuna, cur_virsotne);

            atr_rez = cels.emplace(cur_virsotne, cur_cela_garums+1);
        }
        while(atr_rez.second);


        atr_virs = atr_rez.first;


        time(&cur_it_t);//uznem kartejas iteracijas laiku

        cur_cikla_garums = cur_cela_garums + 1 - atr_virs->second;
        cur_cels_lidz = cur_cela_garums - cur_cikla_garums;

        //parabauda vai atrasts jauns cikls
        jauns_cikls = true;
        atr_cikla_ind = -1;



        for(int i=0; i<ciklu_sk; i++){

            if(cels.find(cikli[i].starta_virs) != cels.end()){
                jauns_cikls=false;
                atr_cikla_ind = i;
                break;
            }
        }

        //if(jauns_cikls) cout<<"jauns cikls"<<endl;
        //else cout<<"nav j cikls"<<endl;
        //cout<<endl<<"#####################################################"<<endl<<endl;;



        if(jauns_cikls){
            //atrasts jauns cikls
            if(cur_cikla_garums>gar_cikls) gar_cikls = cur_cikla_garums;

            if(ciklu_sk==maks_ciklu_sk){
                cout<<"\n\t!!!ciklu vairak ka: "<<maks_ciklu_sk<<endl;
                cur_it++;
                break;
            }

            //cikli[ciklu_sk].starta_virs = atr_virs->first;

            //cikli[ciklu_sk].starta_virs.skaitlu_sk = atr_virs->first.skaitlu_sk;

            for(int i=0; i<cur_virsotne.skaitlu_sk; ++i){
                cikli[ciklu_sk].starta_virs.skaitli[i] = atr_virs->first.skaitli[i];
            }

            //cikli[ciklu_sk].cikla_garums = cur_cikla_garums;
            //cikli[ciklu_sk].gar_cels_lidz = cur_cels_lidz;
            //cikli[ciklu_sk].reizes_trapits = 1;
            cikli.at(ciklu_sk).cikla_garums = cur_cikla_garums;
            cikli.at(ciklu_sk).gar_cels_lidz = cur_cels_lidz;
            cikli.at(ciklu_sk).reizes_trapits = 1;

            ciklu_sk++;
        }//endif jauns cikls
        else{
            //nav atrasts jauns cikls -trapits pa kadu velreiz
            if(cikli[atr_cikla_ind].gar_cels_lidz < cur_cels_lidz) cikli[atr_cikla_ind].gar_cels_lidz = cur_cels_lidz;

            cikli[atr_cikla_ind].reizes_trapits++;
        }

        ++cur_it;//par cik do-while arii tad, ja iziet viena iteracija cur it buus 1, nevis 0

    }
    while(cur_it_t-sakuma_t<=laika_limits);//iteraciju do-while beigas

    time(&beigu_t);


    fprintf(out_file, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file2, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);
    fprintf(out_file3, "\t\tmusas genu sk: %d musas sunu skaits: %d\n\n", musas_genu_skaits, musas_sunu_skaits);

    fprintf(out_file, "ciklu skaits := %d\n", ciklu_sk);
    fprintf(out_file2, "ciklu skaits := %d\n\n", ciklu_sk);
    fprintf(out_file3, "ciklu skaits := %d\n\n", ciklu_sk);

    fprintf(out_file, "garakais cikls := %d\n", gar_cikls);
    fprintf(out_file, "iteraciju skaits := %lld\n", cur_it);
    fprintf(out_file, "analize ar sausanu2 musai laiks: %ds\n\n", (int)(beigu_t-sakuma_t));

    fprintf(out_file, "\tinfo par visiem cikliem:\n\n");

    fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz      reizes_trapits  relativie_trapijumi    starta_stavoklis\n\n");

    //fprintf(out_file, "cikls  cikla_garums  gar_cels_lidz     reizes_trapits  sakuma_virsotne(binari)\n\n");
    for(int i=0; i<ciklu_sk; i++){
        fprintf(out_file,"%5d %13d %14d", i+1, cikli[i].cikla_garums, cikli[i].gar_cels_lidz);
        //fprintf(out_file,"%18d",(int) round((double)cikli[i].reizes_trapits/(double)cur_it)*100);
        fprintf(out_file,"%20lld", cikli[i].reizes_trapits);
        fprintf(out_file,"%21.8f",(double)cikli[i].reizes_trapits/(double)cur_it);

        fprintf(out_file, "%20lld", cikli[i].starta_virs.skaitli[0]);
        for(int a=1; a<cikli[i].starta_virs.skaitlu_sk; ++a) fprintf(out_file, "  %lld", cikli[i].starta_virs.skaitli[a]);
        fprintf(out_file, "\n");

        fprintf(out_file3, "cikls: %d\t", i+1);
        izdruka_musas_genu_vertibas_2(out_file2,out_file3, musas_genu_vertibas, musas_genu_skaits, musas_sunu_skaits, cikli[i].starta_virs);
    }
    fprintf(out_file, "\n####################################################################################################################\n");
    fprintf(out_file2, "\n####################################################################################################################\n");
    fprintf(out_file3, "\n####################################################################################################################\n");


    printf("\n\tstav_grafa_analize ar sausanu 2 musai 2.beidz darbu.\n");
    printf("rekinat beigts: %s\n", ctime(&beigu_t));
    printf("Kopejais darbibas laiks: %ds\n\n", (int)(beigu_t-sakuma_t));



    ///


}//stav_grafa_analize_saujot2_musai_2




void help(const int max_musas_sunu_skaits){
    cout<<endl<<endl;
    cout<<"\t\t----- Musas HELP ------"<<endl;
    cout<<"No komand rindas jasanem 2 parametri (2 pozitivi veseli skaitli) atdaliti ar tuksumu vai tab sekojosa seciba:"<<endl;
    cout<<"\t#1 suunu skaits. Musas sunu skaitam jabut >0 un <=max musas sunu skaitu("<<max_musas_sunu_skaits<<") un jadalas ar 4"<<endl;
    cout<<"\t#2 laika limits(sekundees) cik ilgi darbinat analizi. Laika limitam jabut >0"<<endl;
    cout<<endl<<"\t\t----- Musas HELP beigas ------"<<endl;
    cout<<endl<<endl<<endl;
}//help



bool parametru_apstr(const int argc, const char *argv[], int &musas_sunu_skaits, int &laika_limits, const int max_musas_sunu_skaits){


    ///
    istringstream cur_arg;


    if(argc==2){
        if(strcmp(argv[1], "-h")==0 || strcmp(argv[1], "--help")==0 || strcmp(argv[1], "?")==0){
            help(max_musas_sunu_skaits);
            return false;
        }
        else{
            cout<<"\tKluda: nepareizs arguments!"<<endl;
            return false;
        }
    }
    else if(argc!=3){
        cout<<"\tKluda: nepareizs parametru skaits!"<<endl;
        return false;
    }
    else{
        //parametru skaits ir pareizs

        cur_arg.str(argv[1]);
        if(!(cur_arg>>musas_sunu_skaits) || !cur_arg.eof()){
            cout<<"\tKluda: 1. arguments '"<<argv[1]<<"'nav skaitlis!"<<endl;
            return false;
        }

        cur_arg.ignore(255, '\n');
        cur_arg.clear();


        cur_arg.str(argv[2]);
        if(!(cur_arg>>laika_limits) || !cur_arg.eof()){
            cout<<"\tKluda: 2. arguments '"<<argv[2]<<"' nav skaitlis!"<<endl;
            return false;
        }

        if(musas_sunu_skaits%4!=0 || musas_sunu_skaits<=0 || musas_sunu_skaits>max_musas_sunu_skaits || laika_limits<=0){
            cout<<"\tKluda: nepareizas parametru vertibas!"<<endl;
            return false;
        }
    }

    return true;

}//pareizi parametri




bool abcd(string &a){
    return a=="abcd";
}
