St�vok�u grafa anal�ze ar noteico�aj�m b�la funkcij�m.
Ar 50 g�niem un 2,3,7 ietekm�jo�iem.
Katra konfigur�cija palaista 5 reizes.


		genu sk: 50 ietekm sk: 2

ciklu skaits := 1
garakais cikls := 2
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             2             23    4661945/4661945

###############################################
		genu sk: 50 ietekm sk: 2

ciklu skaits := 2
garakais cikls := 2
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             2             17    6059371/6062524
    2             2             13       3153/6062524

###############################################
		genu sk: 50 ietekm sk: 2

ciklu skaits := 3
garakais cikls := 9
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             9             53    1907419/2477386
    2             3             24     569966/2477386
    3             1              3          1/2477386

###############################################
		genu sk: 50 ietekm sk: 2

ciklu skaits := 1
garakais cikls := 1
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             1             31    4161081/4161081

###############################################
		genu sk: 50 ietekm sk: 2

ciklu skaits := 7
garakais cikls := 5
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             1             15    1679424/6478761
    2             5             12    1218404/6478761
    3             5             12    3539022/6478761
    4             3              9      18200/6478761
    5             1              9      19169/6478761
    6             3              9       4354/6478761
    7             1              6        188/6478761

###############################################


/////////////////////////////////////////////////////////////////////////////////////////////


		genu sk: 50 ietekm sk: 3

ciklu skaits := 24
garakais cikls := 6
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             4             16    1814826/6170718
    2             4             17     409485/6170718
    3             2             23    2861736/6170718
    4             2             14     124795/6170718
    5             6             10      74010/6170718
    6             6             10      72867/6170718
    7             4             12     149213/6170718
    8             6             17     387871/6170718
    9             4             15      77438/6170718
   10             2             11     151170/6170718
   11             2              8       1815/6170718
   12             6              9      16391/6170718
   13             2             11      22203/6170718
   14             6              6       2494/6170718
   15             6              6        807/6170718
   16             4              7        302/6170718
   17             2              4         60/6170718
   18             6              5        292/6170718
   19             6              6        868/6170718
   20             2              7        252/6170718
   21             4              7        651/6170718
   22             4              7        626/6170718
   23             2              7        253/6170718
   24             4              7        293/6170718

###############################################
		genu sk: 50 ietekm sk: 3

ciklu skaits := 2
garakais cikls := 2
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             2             14    6091621/6597233
    2             2             19     505612/6597233

###############################################
		genu sk: 50 ietekm sk: 3

ciklu skaits := 1
garakais cikls := 1
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             1             22    5658696/5658696

###############################################
		genu sk: 50 ietekm sk: 3

ciklu skaits := 1
garakais cikls := 2
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             2             14    6410318/6410318

###############################################
		genu sk: 50 ietekm sk: 3

ciklu skaits := 4
garakais cikls := 4
analize ar sausanu laiks: 121s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             4             33    3043820/3227962
    2             4             13      46644/3227962
    3             4             16      35441/3227962
    4             4             16     102057/3227962

###############################################

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		genu sk: 50 ietekm sk: 7

ciklu skaits := 3
garakais cikls := 18
analize ar sausanu laiks: 241s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            18             46    3516910/3568104
    2             3             24      51192/3568104
    3             4              6          2/3568104

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 10
garakais cikls := 3
analize ar sausanu laiks: 208s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             1             20    7583710/10000000
    2             3             19    2202797/10000000
    3             3             14     104276/10000000
    4             3             16      94980/10000000
    5             3             12       8580/10000000
    6             1              7       1053/10000000
    7             3              8       1878/10000000
    8             1             11       1625/10000000
    9             3              9       1015/10000000
   10             1              9         86/10000000

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 8
garakais cikls := 35
analize ar sausanu laiks: 241s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1            10             58     647678/2273618
    2            35             60    1529793/2273618
    3            21             33      89047/2273618
    4             4             20       3111/2273618
    5             4             14       1147/2273618
    6             6             17       1363/2273618
    7            16             10       1396/2273618
    8             2              8         83/2273618

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 1
garakais cikls := 1
analize ar sausanu laiks: 130s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             1             12   10000000/10000000

###############################################
		genu sk: 50 ietekm sk: 7

ciklu skaits := 1
garakais cikls := 1
analize ar sausanu laiks: 216s

	info par visiem cikliem:

cikls  cikla_garums  gar_cels_lidz  reizes_trapits

    1             1             17   10000000/10000000

###############################################
